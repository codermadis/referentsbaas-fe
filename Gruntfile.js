module.exports = function (grunt) {

   // load all grunt tasks
   grunt.loadNpmTasks('grunt-contrib-less');
   grunt.loadNpmTasks('grunt-contrib-watch');

   grunt.initConfig({
      
      pkg: grunt.file.readJSON('package.json'),
      
      concat: {
         libs: {
         	src: [
         		'node_modules/angular/angular.min.js',
         		'assets/js/et-locale.js',
         		'node_modules/angular-cookies/angular-cookies.min.js',
         		'node_modules/angular-*/js/angular-*.min.js',
         		'node_modules/@uirouter/angularjs/release/angular-ui-router.js',
         		'node_modules/angular-*/angular-*.min.js',
         		'node_modules/angular-*/dist/*.min.js',
         		'node_modules/ng-tags-input/build/*.min.js',
         		'node_modules/ng-file-upload/dist/*.min.js',
         		'assets/aes/mdo-angular-cryptography.js',
         		'node_modules/moment/moment.js',
         		'node_modules/moment/locale/*.js'
         		],
         	dest: 'dist/libs.js'
         },
         app: {
            src:[
               'app/common/app.intro.js',
         		'app/services/*.js',
         		'app/directives/**/*.js',
         		'app/common/app.config.js',
         		'app/common/app.mainCtrl.js',
         		'app/filters/*.js',
         		'app/modules/**/*.js',
         		'app/views/**/*.js',
         		'app/common/app.outro.js'
            ],
            dest: 'dist/app.js'
         },
         html: {
            src: ["app/**/*.tpl"],
            dest: 'dist/templates.html'
         }
      },
      
      ngAnnotate: {
          options: {
              singleQuotes: true
          },
          app: {
              files: {
                  'dist/app.min.js': ['dist/app.js']
              }
          }
      },
      
      uglify: {
          app: { //target
              src: ['dist/app.js'],
              dest: 'dist/app.min.js'
          }
      },
      
      minifyHtml: {
         options: {
            cdata: true
         },
         dist: {
            files: {
               'dist/templates.html': 'dist/templates.html'
            }
         }
      },
      
      watch: {
         css: {
            files: ["assets/styles/*.less"],
            tasks: ["less"]
         },
         app: {
            files: ['app/**/*.js'],
            tasks: ['concat:app']
         },
         html: {
            files: ["app/**/*.tpl"],
            tasks: ['concat:html', 'minifyHtml']
         }
      },

      // "less"-task configuration
      less: {
         // production config is also available
         development: {
            options: {
               // Specifies directories to scan for @import directives when parsing. 
               paths: ["static/styles/"],
					compress: true
            },
            files: {
               // compilation.css  :  source.less
               "dist/default.css": "assets/styles/build.less"
            }
         }
      }
   });

   grunt.loadNpmTasks('grunt-contrib-uglify');
   grunt.loadNpmTasks('grunt-contrib-concat');
   grunt.loadNpmTasks('grunt-ng-annotate'); 
   grunt.loadNpmTasks('grunt-minify-html'); 
   
   // the default task (running "grunt" in console) is "watch"
   grunt.registerTask('default', ['concat', 'ngAnnotate', 'uglify', 'less', 'minifyHtml', 'watch']);

};