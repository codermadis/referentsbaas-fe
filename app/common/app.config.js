App.config(function ($compileProvider, $locationProvider, $httpProvider){
   
   $locationProvider.html5Mode({
  		enabled: true,
  		requireBase: false
	});
	
	
   $httpProvider.defaults.withCredentials = true;
   
   $httpProvider.interceptors.push(function($q, $location, $rootScope, $interval) {
      return {
         'responseError': function(response) {
            if(response.status === 404) {
               return $q.reject(response);
            } 
            if(response.status === 401) {
               $location.path('/login');
               return $q.reject(response);
            } 
            if(response.status === 412) {
               return $q.reject(response);
            } 
            if(response.status === 400) {
               return $q.reject(response);
            } 
            if(response.status === 403) {
               return $q.reject(response);
            } 
         }
      };       
   });

});
App.config(['$cryptoProvider', function($cryptoProvider){
    $cryptoProvider.setCryptographyKey('zuperSecretKey4C00kies');
}]);
App.config(function($mdDateLocaleProvider){
   $mdDateLocaleProvider.firstDayOfWeek = 1;
   $mdDateLocaleProvider.formatDate = function(date) {
      var m = moment(date);
      return m.isValid() ? m.format('L') : '';
   };
   $mdDateLocaleProvider.parseDate = function(dateString) {
      var m = moment(dateString, 'L', true);
      return m.isValid() ? m.toDate() : new Date(NaN);
   };
});