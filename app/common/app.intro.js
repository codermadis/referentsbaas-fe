var App = angular.module("App", ['mdo-angular-cryptography','ngCookies','ui.router', 'ngAnimate', 'ngSanitize', 'ng-breadcrumbs', 'ngMaterial', 'ngTagsInput', 'ngFileUpload', 'uiBreadcrumbs']);

var BE_URL = window.location.host.includes('c9users') ? "https://twn-referentsbaas-be-raimoj.c9users.io/api/v1/" : "https://referentsid.twn.ee/api/v1/";
const EMAIL_PATTERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const NAME_PATTERN = /^([^0-9]{2,30})$/;

moment.locale('et');

var settings = {
	template: "/dist/templates.html",
	prefix: BE_URL,
	pattern: {
		email: EMAIL_PATTERN,
		name: NAME_PATTERN
	},
	resolveAll: {
		status: function($user) {
			return $user.status();
		},
		tags: function($settings) {
			return $settings.tags();
		},
		translations: function($settings){
			return;
		}
	}
};
