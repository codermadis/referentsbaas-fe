App.controller('mainCtrl', function ($scope, $http, $rootScope, $state, $sanitize, $location, $mdDateLocale, $filter, $transitions, $mdToast, $user) {   
	 
	//$rootScope.stateLabel = { "finished": "Lõpetatud", "unfinished": "Töös" };
	// $mdDateLocale.formatDate = function(date) {
	// 	if(date) { return $filter('date')(date, 'd MMM, y'); }
	// 	else return "";
	// };
	$rootScope.importStatus = { importInProgress : false, projectsImported : false };
	
	
	$rootScope.headerless = true;
	
	$transitions.onEnter({ }, function(trans) {
    $rootScope.headerless =  false;
 		window.scrollTo(0, 0);
	});
  
	$rootScope.toastNotification = function(opts) {
		switch(opts.type){
			case 'simple': {
				$mdToast.show(
					$mdToast.simple().textContent(opts.message ||'').position(opts.position || 'bottom right')
					  .hideDelay(opts.hideDelay || 3000).toastClass(opts.class || '')
				); break;
			}
			case 'custom': {
				alert('Custom toast coming soon...');
			}
		}
	}
	
	$rootScope.getClasses = function() {
		$http.get(settings.prefix + 'class').then(function(response){
			$rootScope.classRegistry = response.data;	
		});
	};
	
	$rootScope.logOut = function(){
		$user.logout();
	};
	
});