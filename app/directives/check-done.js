App.directive("checkdone", function($timeout) {
    return {
        restrict: "E",
        replace: false,
        scope: true,
        template: '<svg ng-if="displaySvg" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">' +
                    '<polyline class="path check" fill="none" stroke="#2E9F63" stroke-width="20" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>' +
                  '</svg>',
        link: function($scope, $elem, $attrs){
            
            
            $scope.displaySvg = true;
            
            if( $attrs.timeout !== undefined ){
                //console.log("hiding");
                $timeout(function(){
                    //console.log("time out");
                    $scope.displaySvg = false;
                    $scope.$apply();
                }, parseInt( $attrs.timeout ) );   
            }
            /*
            if ($scope.saveDoneDebounce) {
                clearTimeout($scope.saveDoneDebounce);
            }
        
            $scope.saveDoneDebounce = setTimeout(function() {
                $scope.displaySvg = false;
                $scope.$apply();
            }, $attrs.timeout);
            */
        }
    };
});