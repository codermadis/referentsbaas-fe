App.directive("modal", function($mdDialog, $location, $timeout, $rootScope) {
  return {
    restrict: "EA",
    scope: true,
    link: function ($scope, $elem, $attrs) {
      const TAG_MERGE_SUCCESS = 'Duplikaat-silt ühtlustatud!';
      
      $scope.openModal = function ($event){
        $rootScope.importDialog = true;
        $mdDialog.show({
          controller: $attrs.controller,
          templateUrl:  $attrs.templateUrl,
          parent: angular.element(document.querySelector( 'body' )),
          targetEvent: $event,
          clickOutsideToClose:false,
          locals: {parentData : $attrs.locals || {}},
          bindToController: $attrs.locals ? true : false
        }).finally(function(){
          $rootScope.importDialog = false;
        });
      };
      
      $scope.openTagConfirm = function ($event, item){
      
        $mdDialog.show({
          controller: $attrs.controller,
          templateUrl:  $attrs.templateUrl,
          parent: angular.element(document.querySelector( 'body' )),
          targetEvent: $event,
          clickOutsideToClose:false,
          locals: {parentData : $attrs.locals || {}},
          bindToController: $attrs.locals ? true : false
        }).then(function(boolean) {
          if(boolean === true){
            item.save();
          } else {
            item.undo();
          }
        }, function() {
          item.undo();
        });
      };
      
      $scope.mergeTag = function ($event, subject, list) {
        $mdDialog.show({
          controller: 'manage_tags_merge',
          templateUrl: 'manage_tags_merge.tpl',
          parent: angular.element(document.querySelector( 'body' )),
          targetEvent: $event,
          clickOutsideToClose: false,
          locals: { parentData : $attrs.locals || {} },
          bindToController: $attrs.locals ? true : false
        }).then(function(data){
          if(!data.error && data.target) {
            var target = list.find(function(tag){ return tag.attributes.id === data.target.attributes.id });
            if(target) {
              console.log('target found!');
              target.attributes.count = data.target.attributes.count;
              target._previousAttributes = angular.copy(target.attributes);
              $scope.giveToast(TAG_MERGE_SUCCESS, 'toast-success');
            }
            subject.removeFromList();
            
          } else {
            console.log(data);
          }
        });
      };
      
      $scope.openTagDelete = function ($event, item){
        $mdDialog.show({
          controller: $attrs.controller,
          templateUrl:  $attrs.templateUrl,
          parent: angular.element(document.querySelector( 'body' )),
          targetEvent: $event,
          clickOutsideToClose:false,
          locals: {parentData : $attrs.locals || {}},
          bindToController: $attrs.locals ? true : false
        }).then(function(boolean) {
          if(boolean === true){
            item.remove();
          } else {
            item.undo();
          }
        }, function() {
          item.undo();
        });
      };
      
    }
  };
});