/*
    @input
    {
        pageCount: Int,
        page: Int
    }
*/
App.directive("paginator", function($timeout, $location) {
    return {
        restrict: "AEC",
        scope: true,
        templateUrl: 'paginator.tpl',
        link: function ($scope, $elem, $attrs) {
            
            
            $scope.changePage = function(params){
                
                if( $attrs.scope !== undefined ){
                    $scope.$parent.changePage( params.page );
                }else{
                    var searchParams = $location.search();
                    if(params.page != undefined && params.page > 0) searchParams.page = params.page;
                    else delete searchParams.page;   
                    $location.search( searchParams );
                }
                
            }
            
            $scope.construct = function(){

                var pagination = JSON.parse( $attrs.data );
                
                var pages = [];
        
                if(pagination.pageCount > 1){
                var range = 5;
                var before = 2, after = 2;
                var start, stop;
                if(pagination.pageCount > range){
                    var page = pagination.page;
                
                    start = page - before > 0 ? page - before : 1;
                    
                    stop = page + after < pagination.pageCount ? page + after : pagination.pageCount;
                    
                    
                    if( stop - page < after ){
                        var detuct = (stop - page - after)*(-1);
                        start-= detuct;
                    }
                    
                    if( page - before <= 0 ){
                       var add = (page - before - start)*(-1); 
                       stop+= add;
                    }  
                } else {
                    start = 1;
                    stop = pagination.pageCount
                }
                    
                
                for(var i = start; i <= stop; i++){pages.push(i)}
                $scope.startPage = start;
                $scope.stopPage = stop;
                }
                
                $scope.pages = pages;
                
            }
            
            $scope.$watch(function(){
                return $attrs.data
            }, function(){
                if( $attrs.data !== ""){
                    $scope.construct(); 
                }
            }, false);
            
        }
    };
});