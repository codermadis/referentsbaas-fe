<script type="text/ng-template" id="paginator.tpl">
    <div class="d-flex justify-content-center show-more" ng-if="pages.length > 0">
      <div class="pagination">
         <a href="" ng-class="pagination.page > 1 ? 'page': 'page disabled'" ng-click="changePage({page: pagination.page - 1})">Eelmine</a>
         
         <a href="" ng-class="page == pagination.page ? 'page active' : 'page'" 
         ng-click="changePage({page: page})"
         ng-repeat="page in pages">
            {{page}}
         </a>
         <a ng-if="stopPage < pagination.pageCount">...</a>
         <a class="page" ng-if="stopPage < pagination.pageCount" href="" ng-click="changePage({page: pagination.pageCount})">{{ pagination.pageCount }}</a>
         <a href="" ng-class="pagination.page < pagination.pageCount ? 'page' : 'page disabled'" ng-click="changePage({page: pagination.page + 1})">Järgmine</a>
      </div>
   </div>
</script>