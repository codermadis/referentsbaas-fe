/*!
 * @module ui-router-breadcrumbs
 * @description A Simple directive that creates breadcrumbs on the fly for AngularJs pages using angular-ui-router
 * @version v1.2.2
 * @link https://github.com/Sibiraj-S/ui-router-breadcrumbs#readme
 * @licence MIT License, https://opensource.org/licenses/MIT
 */
(function() {
    "use strict";
    var r, a, t;
    r = function() {
        var a;
        return a = !1, {
            setAbstract: function(r) {
                r = JSON.parse(r), a = r
            },
            $get: function() {
                return a
            }
        }
    }, a = function(c, u) {
        var a;
        return a = function(r) {
            var a, t, e, n;
            for (t in r = r || u, a = [], n = [], e = [], c.$current.includes) n.push(t);
            for (t in n) "" !== n[t] && "" !== c.get(n[t]).$$state().parent.self.name && e.push(c.get(n[t]).$$state().parent.self);
            if (r) a = e;
            else
                for (t in e) e[t].abstract || a.push(e[t]);
            return a.push(c.current), a
        }, {
            getbreadcrumbs: function(r) {
                return a(r)
            }
        }
    }, (t = function(r, a, c) {
        return {
            restrict: "E",
            transclude: !0,
            template: '<nav aria-label="breadcrumb">  <ol class="breadcrumb">  <li class="breadcrumb-item"><a ui-sref="dashboard">Töölaud</a></li>  <li class="breadcrumb-item" ng-repeat="data in $breadcrumbs" ui-sref-active="active">      <a ui-sref="{{data.abstract || data.name}}" ng-class="{\'last-item\': data.abstract || $last }">{{data.data.label || data.name}}</a>    </li>  </ol></nav>',
            link: function(r, a, t) {
                var e, n;
                t.abstract = !!t.abstract && t.abstract, e = JSON.parse(t.abstract), n = function() {
                    r.$breadcrumbs !== c.getbreadcrumbs(e) && (r.$breadcrumbs = c.getbreadcrumbs(e))
                }, r.$on("$viewContentLoaded", function() {
                    n()
                })
            }
        }
    }).$inject = ["$state", "$rootScope", "breadcrumbsService"], a.$inject = ["$state", "breadcrumbconfig"], angular.module("uiBreadcrumbs", ["ui.router", "ngSanitize"]).directive("uiBreadcrumb", t).provider("breadcrumbconfig", r).factory("breadcrumbsService", a)
}).call(this);
//# sourceMappingURL=ui-router-breadcrumbs.min.js.map
