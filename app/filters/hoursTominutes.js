App.filter('hoursTominutes', function($converter) {
    return function(input) { return $converter.hoursTominutes(input) };
});