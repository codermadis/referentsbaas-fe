App.filter('language', function() {
  return function(language) {
    
    switch (language) {
      case 'et':
        return "eesti keel";
        break;
      case 'en':
        return "inglise keel";
        break;
      case 'ru':
        return "vene keel";
        break;
    }
    
  }
});