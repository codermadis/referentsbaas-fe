App.filter('minutesTohours', function($converter) {
    return function(input) { return $converter.minutesTohours(input) };
});