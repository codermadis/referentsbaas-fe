App.filter('relativeTimestamp', function($converter) {
    return function(input) { return $converter.relativeTimestamp(input) };
});