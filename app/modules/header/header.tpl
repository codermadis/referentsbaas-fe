<script type="text/ng-template" id="header.tpl">

  <nav class="navbar navbar-toggleable-md fixed-top navbar-inverse">
  
    <a class="navbar-brand" ui-sref="dashboard">
      <img src="assets/imgs/logo.svg">
      <p class="navbar-title">Referentsbaas</p>
    </a>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="nav navbar-nav ml-auto w-100 justify-content-end">
      
        <li class="nav-item" ng-if="!projectAdd">
            <div class="nav-link project" modal template-url="import.tpl" controller="import" ng-click="openModal($event)">
              <md-button class="btn btn-primary">
                Lisa projekt
              </md-button>
            </div>
        </li>
        <li class="nav-item">
          <div class="dropdown">
            <img src="assets/imgs/user.svg" />
            <a class="dropdown-toggle nav-link user align-middle" id="userMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span ng-if="userData.first_name">{{userData.first_name}} {{userData.last_name ? userData.last_name : ''}}</span>
              <span ng-if="!userData.first_name">{{userData.email}}</span>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userMenu">
                <a class="dropdown-item" ui-sref="settings.account">Seaded</a>
                <a class="dropdown-item" href="javascript:void(0);" modal template-url="help-modal.tpl" controller="help-modal" ng-click="openModal($event)">Abi</a>
                <a class="dropdown-item" ng-click="logOut()">Logi välja</a>
              </div>
            </a>
          </div>
        </li>
      </ul>
    </div>
    
    <div class="mobile-menu">
      <button class="mobile-menu-btn" type="button" ng-click="menuToggle()"><i class="material-icons">menu</i></button>
      <div class="mobile-menu-content" ng-if="menuContent">
        <div class="mobile-menu-user">
          <span ng-if="userData.first_name && userData.last_name">{{userData.first_name}} {{userData.last_name}}</span>
          <span ng-if="!userData.first_name || !userData.last_name">{{userData.email}}</span>
        </div>
        <md-button class="btn btn-primary" ng-if="!projectAdd" modal template-url="import.tpl" controller="import" ng-click="openModal($event); menuToggle()">Lisa projekt</md-button>
        <a class="mobile-menu-link" ui-sref="settings.account" ng-click="menuToggle()">Seaded</a>
        <a class="mobile-menu-link" href="javascript:void(0);" modal template-url="help-modal.tpl" controller="help-modal" ng-click="openModal($event); menuToggle()">Abi</a>
        <a href="javascript:void(0);" class="mobile-menu-link" ng-click="logOut()">Logi välja</a>
      </div>
    </div>
  </nav>
    
</script>