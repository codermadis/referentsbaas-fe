App.controller('import', function($scope, $mdDialog, $anchorScroll, $http, $rootScope, $location, $state, $dummyData, $timeout) {
  var PER_PAGE = 20;
  $scope.import_list = {};
  $scope.project_group = {};
  
  
  $scope.dummyPlaceholderData = $dummyData.table(PER_PAGE, 5, 1, 8);
  $scope.search_attributes = {
    archived: false,
    unbillable: false
  };
  
  $scope.hide = function() {
    $mdDialog.hide();
  };
  
  $scope.changePage = function(page_nr){$scope.search(page_nr)};

  $scope.search = function (page) {
    $scope.loader = true;
    $scope.showData = false;
    var str = "";
    
    if($scope.search_attributes.archived == true) str += '&enabled=false';
    if($scope.search_attributes.unbillable == true) str += '&billable=false';
    if($scope.search_attributes.project_group_name)  str += '&project_group_name=' + $scope.search_attributes.project_group_name;
    
    if($scope.search_attributes.name) str += '&name=' + $scope.search_attributes.name;
    if(page) str += '&page=' + page;
    
    $http({ url: settings.prefix + 'projects/import/preview?per_page='+ PER_PAGE + str, method: 'get' })
    .then(function(response){
      $scope.loader = false;
      $scope.showData = true;
      $scope.projects = response.data.list;
      $scope.pagination = {
        page: response.data.page,
        pageCount: response.data.last_page
      };
      //console.log(response.data);
    }, function(err){
      $scope.loader = false;
      console.error(err);
    });
  };
  $scope.search(); //initial list
  
 
  
  $scope.import = function (){
    if($scope.importLoader === true) return console.log('Import already underway');
   
    //console.log($scope.import_list);
    var filtered_ids = [];
    for(var id in $scope.import_list){
      if($scope.import_list[id] === true) filtered_ids.push(id);
    }
   
    if(!filtered_ids.length) return console.log('No projects selected');
    
    var projectIdsCsv = "";//'billable='+ $scope.freckle.billable + '&enabled='+ enabled;
      for(var i in filtered_ids){
         //str += '&' + key + '=' + $scope.freckle[key];
        if(i == 0) projectIdsCsv +=  filtered_ids[i];
        else projectIdsCsv += ',' + filtered_ids[i];

    }
    
    $http({url: settings.prefix + 'projects/import?project_ids=' + projectIdsCsv, method: 'get' })
      .then(function(response){
        $scope.import_list = {};
        initImportState();
      }, function(err){
        console.error(err);
      });
  }; //import
  
  function initImportState (){
      $scope.importProgress = 0;
      $scope.importLoader = true;
      $scope.showData = false;
  }
  
  function watchImportState (){
    $http.get(settings.prefix + 'projects/import/status').then(function(response){
      
      if($rootScope.importDialog) {
        
        if(response.data.status == 'UNDERWAY'){
          if($scope.importLoader == false || $scope.importLoader == undefined){
             initImportState();
          }
          
          $scope.importProgress = response.data.progress;
        } else if(response.data.status == 'READY' &&  $scope.importLoader == true) {
          $scope.importProgress = 100;
          
          $timeout(function(){
            $scope.showData = true;
            $scope.importLoader = false;
            $location.search({});
            if($location.path() == "/toolaud/minu_projektid") $state.reload();
            else $location.path("/toolaud/minu_projektid");
            
            $scope.hide();
          }, 1000);
        }
        $timeout(function() {
            watchImportState();
        }, 1000);
      }
    }, function(err){
      console.error(err);
    });
  }
  watchImportState();
  
  $scope.createProjectManually = function(){
    
    if($state.$current.name === 'projects.general'){
      $state.go($state.current, {id:'loo'}, {reload: true});
    } else {
      $state.go('projects.general', {id:'loo'});
    }
    $scope.hide();
  }
});