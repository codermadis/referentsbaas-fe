<script type="text/ng-template" id="import.tpl">
  <md-dialog aria-label="dialog">
    <div class="d-flex py-3 justify-content-between align-items-center flex-shrink-0">
      <h4 class="m-0">Lisa projekt</h4>
      <md-button aria-label="close modal" ng-click="hide()" class="md-primary modal-close"><i class="material-icons">close</i></md-button>
    </div>
    
    <md-divider class="mb-3"></md-divider>
  
    <ng-include src="'import_search.tpl'" ></ng-include>  
  
    <md-divider class="w-100 table-top-divider"></md-divider>
  
    <md-dialog-content>
    
      <div ng-if="importLoader" class="loader-box">
        <div class="d-flex align-items-center flex-column">
          <div class="d-flex justify-content-center align-items-center loader-checkdone" style="height: 50px; width: 50px;">
            <md-progress-circular ng-if="importProgress <= 0 && importProgress < 100" md-mode="indeterminate"></md-progress-circular>
            <md-progress-circular ng-if="importProgress > 0 && importProgress < 100" md-mode="determinate" value="{{importProgress}}"></md-progress-circular>
            <checkdone ng-if="importProgress >= 100"></checkdone>
          </div>
          <label class="mt-3 mb-0">
            
            <h4>Import</h4>
           
          </label>
          <label ><p><b>{{importProgress}}%</b></p></label>
        </div>
      </div>
      
      <div ng-if="loader" class="loader-box">
        <md-progress-circular md-mode="indeterminate"></md-progress-circular>
      </div>
     
      <ng-include src="'import_preview.tpl'"></ng-include> 
    
    </md-dialog-content>
  
   
   
    <md-dialog-actions class="flex-column flex-shrink-0 px-0">
    
      <md-divider class="w-100 table-bottom-divider"></md-divider>
      
      <paginator class="py-2" data="{{ pagination }}" scope="true"></paginator>
      
      <div class="d-flex w-100 justify-content-between p-0 align-items-center">
        <md-button class="btn btn-primary mx-1" ng-click="import()">Impordi</md-button>
        
        <div class="d-flex align-items-center">
          <p class="mb-0 mr-2">Ei leidnud?</p>
          <a>
            <md-button ng-click="createProjectManually()" class="btn btn-light mx-1">Lisa käsitsi</md-button>
          </a>
        </div>
        
      </div>
      
    </md-dialog-actions>
</md-dialog>
</script>