<script type="text/ng-template" id="import_preview.tpl">
   
   <form >
     <div class="table-overflow">
      <table class="table table-bordered mb-0 table-add-project" >
         <thead>
            <tr>
               <th style="width: 120px;">Impordi</th>
               <th style="width: 35%">Projekt</th>
               <th style="width: 20%">Projektijuht</th>
               <th style="width: 40%">Töötajad</th>
               <th style="width: 20%">Töötunnid</th>
            </tr>
         </thead>
         <tbody>
            <tr ng-repeat="item in dummyPlaceholderData" ng-if="loader || importLoader" class="blokk-font">
               <td ng-repeat="col in item">{{ col }}</td>
            </tr>
            <tr ng-repeat="project in projects" ng-if="showData">
               <td>
                  <label class="custom-control custom-checkbox d-block">
                     <input class="custom-control-input" ng-model="import_list[project.project_freckle_id]" type="checkbox">
                     <span class="custom-control-indicator"></span>
                     <span class="custom-control-description">Vali</span>
                  </label>
               </td>
               <td>{{project.project_name}}<span class="hint-text import-date" ng-if="project.localData">Imporditud {{project.localData.imported_at | date:'d. MMM, y @ HH:mm'}}</span></td>
               <td>{{project.project_group.name}}</td>
               <td>
                  <span ng-repeat="participant in project.participants">
                     <span ng-if="!$last">{{participant.name}},</span>
                     <span ng-if="$last"> {{participant.name}}</span>
                  </span>
               </td>
               <td>
                  {{project.project_minutes | minutesTohours }}
               </td>
            </tr>
         </tbody>
      </table>
     </div>
   </form>
</script>