<script type="text/ng-template" id="import_search.tpl">
  
    
  <form class="flex-shrink-0 d-flex align-items-end flex-wrap flex-sm-nowrap" ng-submit="search()">
    <div class="form-group form-half-width mr-2">
      <label>Otsi projekti Freckle'st</label>
      <div class="d-flex">
        <input class="form-control" type="text" ng-model="search_attributes.name" placeholder="Sisesta projekti nimetus" md-autofocus />
       <!-- <button type="submit" class="btn btn-primary btn-icon"><img src="assets/imgs/magnify.png" /></button> -->
      </div>
    </div><!--/form-group-->
 
  
    <div class="form-group form-half-width">
      <div class="d-flex">
        <input class="form-control" type="text" ng-model="search_attributes.project_group_name" placeholder="Sisesta projektijuhi nimi" />
        <button type="submit" class="btn btn-primary btn-icon"><img src="assets/imgs/magnify.png" /></button>
      </div>
    </div><!--/form-group-->
  </form>
    <div class="d-flex flex-column flex-sm-row">
      <div class="form-check form-check-inline">
        <label class="custom-control custom-checkbox">
          <input class="custom-control-input" ng-click="search()" ng-model="search_attributes.unbillable" type="checkbox" />
          <span class="custom-control-indicator"></span>
          <span class="custom-control-description">Mittearveldatav projekt</span>
        </label>
      </div><!--/form-check-->
      <div class="form-check form-check-inline ml-0 ml-sm-3">
        <label class="custom-control custom-checkbox">
          <input class="custom-control-input" ng-click="search()" ng-model="search_attributes.archived" type="checkbox" />
          <span class="custom-control-indicator"></span>
          <span class="custom-control-description">Arhiveeritud projekt</span>
        </label>
      </div><!--/form-check-->
    </div>
 
</script>