<script type="text/ng-template" id="outlook_toast_service.tpl">
  <md-toast class="full-width">
    <span>Seadista oma Outlook-i konto süsteemist e-mailide väljasaatmiseks. Seadistamiseks <a ui-sref="settings.system" ng-click="closeToast()">kliki siia.</a></span>
    <i class="material-icons" ng-click="closeToast()">close</i>
  </md-toast>
</script>