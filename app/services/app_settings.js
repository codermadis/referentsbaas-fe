App.factory('$settings', function($http, $q, $location, $rootScope, $mdToast, $state) {
    
	return {
		outlookServiceStatus: function(){
			
			if($rootScope.showOutlookAccountWarning === false) return;
			
			$http({url: settings.prefix + 'settings/outlook', method: "get"})
			.then(function(response) {
      	if(response.data.password == false || !response.data.username.length){
      		
	      		if($state.$current.name != 'settings.system'){
	      			$mdToast.show({
			          hideDelay   : false,
			          position    : 'top right',
			          controller  : 'outlook_toast_service',
			          templateUrl : 'outlook_toast_service.tpl',
			          toastClass	: 'toast-warning'
			        });
	      		}
		        $rootScope.showOutlookAccountWarning = true;
	      	} else {
	      		$rootScope.showOutlookAccountWarning = false;
	      		$mdToast.hide();
	      	}
	    }, function(err) {
	      console.error(err);
	    });
		},
		tags: function(){
			var deferred = $q.defer();
			var promise = $http.get(settings.prefix + 'class');

			promise.then(function(response){
			if(response.data){
			  $rootScope.classRegistry = response.data;	
				deferred.resolve(response);
				
			} else { $location.path("/login") }
			
			}, function(error){
				console.log(error.data);
			});
			
			return deferred.promise;
		}
	};
});