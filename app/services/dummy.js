App.factory('$dummyData', function() {
    
	return {
		table: function(row, col, wordCount, wordLength){
		  var wc = wordCount || 1;
		  var wl = wordLength || 4;
		  var word = "";
		  for(var w = 0; w < wc; w++){
		    word += '#'.repeat(wl) + " ";
		  }
		  var output = [];
  		for(var r = 0; r < row; r++){
        var obj = {};
        for(var c = 0; c < col; c++){
          obj[ 'key' + c ] = word;
        }
        output.push(obj);
      }
      return output;
		}
		
	};
});