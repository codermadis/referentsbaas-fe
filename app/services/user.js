App.factory("$user", function($http, $q, $location, $rootScope, $mdToast, $state, $settings) {
  
  return {
    status: function() {
      var deferred = $q.defer();
      var promise = $http.get(settings.prefix + 'status');

      if ($rootScope.userData != undefined) {
        
        if ($rootScope.userData.role_id === 1) $settings.outlookServiceStatus();
        
        deferred.resolve();
        
      } else {
        promise.then(function(response) {
          if (response.data) {
            
            $rootScope.userData = response.data;
            
            if ($rootScope.userData.role_id === 1) $settings.outlookServiceStatus();
            
            deferred.resolve();
            
          } else { $location.path("/login") }
          
        }, function() { $location.path("/login") });
      }

      return deferred.promise;
    },

    login: function(email, password) {
      var deferred = $q.defer();
      var promise = $http({method: "POST", url: settings.prefix+"login", data: { email: email, password: password } })
      
      promise.then(function(response) {
        
        $rootScope.userData = response.data.user;
        
  			if($rootScope.userData.role_id === 1) $rootScope.showOutlookAccountWarning = true;
        
        deferred.resolve(response);
  		}, function (response){ 
  		  deferred.reject(response);
  		});
  		return deferred.promise;
    },

    logout: function() {
      $http({ method: 'PUT', url: settings.prefix + 'logout'}).then(function( response ){
        $mdToast.hide(); //remove any toasts
  			
  			$state.go('login');
  		
  		}, function(response){ console.log(response) });
    }
    
  }; //End of returned object
  
});