App.factory('$verifyProjectData', function($http, $location, $rootScope) {
    
	return {
		isDataComplete: function(project_id){
		  return new Promise(function(resolve, reject){
		    if(!project_id) return resolve(false);
		  
    		$http.get(settings.prefix + 'projects/' + project_id +'/status').then(function(response){
          
          return resolve(response.data);
          
        }, function(error){
          
          return reject(error);
          
        });
		  });
		 
		}
	};
	
});