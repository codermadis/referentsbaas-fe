App.config(function($stateProvider) {
  $stateProvider
    .state({
      name: "exported_cv",
      url: "/exported_cv",
      templateUrl: "cv.view.tpl",
      controller: "cv.view",
      label: 'cv',
      resolve: settings.resolveAll,
    });
});
