<script type="text/ng-template" id="cv.view.tpl">
   <div class="cv">
        <section class="container-fluid" style="background-color: #FFFFFF; padding: 1.25rem;">
            <table class="table table-bordered table-striped table-sm">
                <tbody>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Töötaja nimi:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">Mirell Krain</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">1. Projekti nimetus:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">Patendiameti infosüsteemi analüüs</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Projekti tellija ja kontaktisik:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">Patendiamet (süsteemi tellija), Fujitsu (peatöövõtja), Test Kasutaja, test.kasutaja@fujitsu.ee</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Projekti maksumus <span style="font-weight: normal">(eurodes)</span>:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">Üle 75 000</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Projekti tööde kogumaht <span style="font-weight: normal">(h)</span>:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">2 000</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Rollitäitja poolt teostatud tööde maht <span style="font-weight: normal">(h)</span>:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">Üle 800</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Rollitäitja tööde teostamise aeg:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">12.2014 - 09.2015</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Roll projektis:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">Analüütik</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Teostatud töö sisu lühikirjeldus:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">Protsesside kaardistamine, kasutajanõuete väljaselgitamine, süsteemianalüüsi dokumentatsiooni koostamine (kasutuslood).</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Projektis kasutatud tehnoloogia:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">Java, Oracle, Axure RP, Angular2, HTML5, CSS3, JS jms</td>
                    </tr>
                    <tr>
                       <td style="background-color:#3A3D57; border: 1px solid #3A3D57"></td>
                       <td style="background-color:#3A3D57; border: 1px solid #3A3D57"></td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">2. Projekti nimetus:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">Patendiameti infosüsteemi analüüs</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Projekti tellija ja kontaktisik:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">Patendiamet (süsteemi tellija), Fujitsu (peatöövõtja), Test Kasutaja, test.kasutaja@fujitsu.ee</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Projekti maksumus <span style="font-weight: normal">(eurodes)</span>:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">Üle 75 000</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Projekti tööde kogumaht <span style="font-weight: normal">(h)</span>:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">2 000</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Rollitäitja poolt teostatud tööde maht <span style="font-weight: normal">(h)</span>:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">Üle 800</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Rollitäitja tööde teostamise aeg:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">12.2014 - 09.2015</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Roll projektis:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">Analüütik</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Teostatud töö sisu lühikirjeldus:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">Protsesside kaardistamine, kasutajanõuete väljaselgitamine, süsteemianalüüsi dokumentatsiooni koostamine (kasutuslood).</td>
                    </tr>
                    <tr>
                       <td class="align-middle" style="border: 1px solid #c6c6c6; width: 30%; font-weight: bold;">Projektis kasutatud tehnoloogia:</td>
                       <td class="align-middle" style="border: 1px solid #c6c6c6;">Java, Oracle, Axure RP, Angular2, HTML5, CSS3, JS jms</td>
                    </tr>
                </tbody>
            </table>
        </section>
    </div><!--/cv-->
</script>