App.controller("dashboard", function($scope, $rootScope, $http, $location, $filter, $timeout, $q, $log){
    $scope.searchTabs = [
        {_id: 0, min_role_id: 3, name:'Otsi projekti', hash:'otsi_projekti', template:"search_projects.tpl"},
        {_id: 1, min_role_id: 3, name:'Otsi töötajat',hash:'otsi_tootajat', template:"search_users.tpl"}
    ];
    var secondMenu_min_role_id =  3;
    $scope.showSecondMenu = $rootScope.userData.role_id < secondMenu_min_role_id ? true: false;
    
    $scope.setHash = function(tab) {
      if (tab._id === 0) {
        $location.hash(tab.hash);
      } else {
        $location.hash(tab.hash);
      }
    };
    
    if($location.hash() === 'otsi_projekti' || !$location.hash() ) {
      $location.hash('otsi_projekti');
      //active tab = projects
    } else {
      $location.hash('otsi_tootajat');
      //active tab = employees
    }
    
    $scope.isActiveTab = function(tab) {
      if( $location.hash() == tab.hash ){ return true; }
      else{ return false;}
    };
//console.log($rootScope.userData);
});