App.config(function ($stateProvider, $urlRouterProvider){
  
  $urlRouterProvider.otherwise("/toolaud");
   
  $stateProvider.state({
    name: "dashboard",
    url: "/toolaud",
    templateUrl: "dashboard.tpl",
		controller: "dashboard",
		data: {
      label: 'Töölaud',
    },
		resolve: settings.resolveAll,
		reloadOnSearch: false
  })
  /*
  .state({
    name: "dashboard-my_projects-edit",
    url: "/dashboard/my_projects/projects/edit/:id",
    label: 'Muuda',
    templateUrl: "projects.add.tpl",
    controller: "projects.add",
    resolve: settings.resolveAll,
		reloadOnSearch: false
  })
  .state({
    name: "dashboard-my_projects-edit",
    url: "/dashboard/projects/edit/:id",
    label: 'Muuda',
    templateUrl: "projects.add.tpl",
    controller: "projects.add",
    resolve: settings.resolveAll,
		reloadOnSearch: false
  }) */
  .state({
    name: "dashboard-my_projects",
    url: "/toolaud/minu_projektid",
		label: 'Puudulike andmetega projektid',
    templateUrl: "dashboard.my_projects.tpl",
		controller: "dashboard.my_projects",
		resolve: settings.resolveAll,
		reloadOnSearch: false
  })
  
  .state({
    name: "dashboard-project-create",
    url: "/toolaud/projektid/loo",
    label: 'Loo uus projekt',
    templateUrl: "projects.add.tpl",
    controller: "projects.add",
    resolve: settings.resolveAll,
		reloadOnSearch: false
		
  })
  /*
  .state({
    name: "dashboard-my_projects-settings",
    url: "/dashboard/my_projects/settings",
    templateUrl: "settings.tpl",
    controller: "settings",
    label: "Seaded",
    resolve: settings.resolveAll
  })
  
  .state({
    name: "dashboard-settings-projects-edit",
    url: "/dashboard/settings/projects/edit/:id",
    label: 'Muuda',
    templateUrl: "projects.add.tpl",
    controller: "projects.add",
    resolve: settings.resolveAll,
		reloadOnSearch: false
  })*/;
      
      
});