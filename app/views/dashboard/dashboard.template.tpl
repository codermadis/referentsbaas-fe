<script type="text/ng-template" id="dashboard.tpl">

   <ng-include ng-if="showSecondMenu" src="'second-menu.tpl'"></ng-include>
   
   <section class="container">
      <div class="main-page">
         
        
            <div class="row justify-content-center">
               <md-tabs md-dynamic-height md-border-bottom class="col-lg-9 col-md-12 search-project">
                  <md-tab label="{{tab.name}}" ng-repeat="tab in searchTabs" ng-click="setHash(tab)" md-active="isActiveTab(tab)">
                  
                     <ng-include src="tab.template"></ng-include>
                  
                  </md-tab>
               </md-tabs>
            </div><!--/row-->
            
            <div class="row">
               <div class="col-md-12 display-project recent-project">
                   
                  <ng-include src="'recent_projects.tpl'"></ng-include>
                  
               </div>
            </div><!--/row-->
      
      </div><!--/main-page-->
   </section><!--/container-->
</script>