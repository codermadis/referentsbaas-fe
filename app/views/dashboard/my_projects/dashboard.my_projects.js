App.controller("dashboard.my_projects", function($scope, $rootScope, $http, $location, $dummyData, $filter, $converter, $window){
  const PAGE_SIZE = 10; 
  $scope.dummyPlaceholderData = $dummyData.table(PAGE_SIZE, 6, 1, 6);
  $scope.participantDisplayLimit = 4;
  
  $scope.sort = {
    key: 'imported_at',
    direction: 'DESC'
  };
  
  $scope.$watch(function(){
    return $location.search();
  }, function(){
    $scope.fetchProjects($location.search());
  }, true);
  
  $scope.orderList = function(key){
    if($scope.sort.key == key) {
      $scope.sort.direction =  $scope.sort.direction === 'ASC' ? 'DESC': 'ASC';
    } else {
      $scope.sort.key = key;
      $scope.sort.direction = 'DESC';
    }
    $scope.fetchProjects($location.search());
  };
  
  $scope.sortArrowClass = function(key){
      if($scope.sort.key === key) return $scope.sort.direction === 'DESC' ? 'fa fa-sort-down' : 'fa fa-sort-up';
      return 'fa fa-sort';
  };
  
  $scope.fetchProjects = function(opts){
    $scope.loader = true;
    $scope.projects = false;
    var string = "";
    for(var i in opts){
      string += i +'='+ opts[i] + '&';
    }
    string += 'data_complete=false&order_by='+ $scope.sort.key +'&direction='+ $scope.sort.direction +'&account_id='+ $rootScope.userData.id;
    $http.get(settings.prefix + 'projects?pageSize='+ PAGE_SIZE + '&' + string).then(function(response) {
      
      $scope.projects = response.data.list;
      $scope.pagination = response.data.pagination;
      $scope.loader = false;
      if ($scope.projects.length > 0) {
        $scope.areProjects = true;
      } else {
        $scope.areProjects = false;
      }
    }, function (err) {
      $scope.loader = false;
      console.error(err);
    });
    $scope.areProjects = true;
  };
  
  $scope.hide = function (project_id) {
    $http({url: settings.prefix + 'projects/' + project_id + '/hide', method:'put'}).then(function(response){
      $scope.projects = $scope.projects.filter(function(project) {return project.id != project_id });
      if ($scope.projects.length > 0) {
        $scope.areProjects = true;
      } else {
        $scope.areProjects = false;
      }
    }, function (err) {
      console.error(err);
    });
  };
  
  $scope.getWindowWidth = function() {
    var windowWidth = $window.innerWidth - 30;
    var tableWidth = document.querySelector('.table-l-layout').clientWidth;
    console.log(tableWidth);
    if (tableWidth > windowWidth) {
      $scope.tableScroll = true;
    } else {
      $scope.tableScroll = false;
    }
  };
  
  $scope.getWindowWidth();
  
  angular.element($window).on('resize', function(){
    $scope.getWindowWidth();
  });
  
});