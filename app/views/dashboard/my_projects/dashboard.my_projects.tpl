<script type="text/ng-template" id="dashboard.my_projects.tpl">
  <ng-include src="'second-menu.tpl'"></ng-include>
  <section class="container">
    <div class="main-page">
      <div class="row">
        <div class="col-md-12 display-project">

          <div ng-if="loader" class="loader-box">
            <md-progress-circular md-mode="indeterminate"></md-progress-circular>
          </div>
          <div ng-class="{'table-overflow': tableScroll}">
            <table class="table table-striped table-bordered table-l-layout">
              <thead class="thead-default">
                <tr>
                  <th style="width: 30%; min-width: 150px;" ng-click="orderList('freckle_name')">
                    Projekti Freckle nimetus
                    <i ng-class="sortArrowClass('freckle_name')"></i>
                  </th>
                  <th style="width: 25%; min-width: 250px;">Projektis osalenud töötajad</th>
                  <th style="width: 10%;" class="text-center" ng-click="orderList('real_capacity')">
                    Töömaht
                    <i ng-class="sortArrowClass('real_capacity')"></i>
                  </th>
                  <th style="width: 25%; min-width: 150px;">Viimati muutis</th>
                  <th style="width: 5;"></th>
                  <th style="width: 5;"></th>
                </tr>
              </thead>
              <tbody>
                <tr ng-repeat="item in dummyPlaceholderData" ng-if="!projects" class="blokk-font">
                  <td ng-repeat="col in item">{{ col }}</td>
                </tr>
                <tr ng-if="!areProjects" class="no-results">
                  <td colspan="99" class="p-4">
                    <h4 class="mb-0">Projektid puuduvad</h4>
                  </td>
                </tr>
                <tr ng-repeat="project in projects" ng-if="projects">
                  <td class="name">
                    <a ui-sref="projects-view.view({id:{{project.id}}})"> {{ project.name || project.freckle_name  }}</a>
                  </td>
                  <td style="width: 20%">
                    <span ng-repeat="participant in project.participants">
                      <span ng-if="!$last">{{participant.user.first_name}} {{participant.user.last_name}},</span>
                      <span ng-if="$last">{{participant.user.first_name}} {{participant.user.last_name}}</span>
                    </span>
                  </td>
                  <td class="text-center">{{ project.real_capacity | minutesTohours}}</td>
                  <td>
                    <span ng-if="project.history_recent.account.first_name && project.history_recent.account.last_name">
                      {{ project.history_recent.account.first_name }} {{ project.history_recent.account.last_name }}
                    </span>
                    <span ng-if="!project.history_recent.account.first_name || !project.history_recent.account.last_name">
                      {{ project.history_recent.account.email }}
                    </span>
                    <b>{{ project.history_recent.timestamp | relativeTimestamp }}</b>
                  </td>
                  <td class="text-center"><a class="red" href="javascript:void(0)" ng-click="hide(project.id)"><b>Peida</b></a></td>
                  <td><a class="mt-2 d-inline-block" ui-sref="projects.general({id:{{project.id}}})"><i class="material-icons edit-icon">edit</i></a></td>
                </tr>

              </tbody>
            </table>
          </div>
          <paginator data="{{ pagination }}"></paginator>
        </div>
      </div>
    </div>
    <!--/main-page-->
  </section>
  <!--/container-->
  </div>
</script>
