App.controller("recent_projects", function($scope, $rootScope, $http, $location, $dummyData, $window){
  const PAGE_SIZE = 10;
  const NOT_SEARCH_KEYS = ['page', 'data_complete', 'order_by', 'direction'];
  $scope.userIsEmployee = $rootScope.userData.role_id > 2 ? true: false;
  $scope.sort = {};
  $scope.dummyPlaceholderData = $dummyData.table(PAGE_SIZE, $scope.userIsEmployee ? 9: 10, 1, 3);
  $scope.participantDisplayLimit = 4;
  
  $scope.$watch(function(){
    return $location.search();
  }, function(){
    if($scope.userIsEmployee){
      if(!$location.search().data_complete){
        var tempSearch = $location.search();
        tempSearch['data_complete'] = true;
        $location.search(tempSearch);
      }
    }
    $scope.fetchProjects($location.search());
  }, true);
  
  $scope.sortAccounts = function(key){
        if($scope.sort.key == key) {
            $scope.sort.direction =  $scope.sort.direction === 'ASC' ? 'DESC': 'ASC';
        } else {
          $scope.sort.key = key;
          $scope.sort.direction = 'DESC';
        }
        var search = $location.search();
        search.order_by = $scope.sort.key;
        search.direction = $scope.sort.direction;
        $location.search(search);
       
    };
  $scope.sortArrowClass = function(key){
      if($scope.sort.key === key) return $scope.sort.direction === 'DESC' ? 'fa fa-sort-down' : 'fa fa-sort-up';
      return 'fa fa-sort';
  };
  
  $scope.resetSearch = function(){
    $location.search({});
  };
  
  const USER_SEARCH_PARAMETERS = ['user_id', 'user_task','user_technology','user_role','min_minutes'];
  $scope.highlightParticipant = function (participant){
    var highlight_class = 'highlight-participant';
    var search_strings = $location.search();
    
    for(var attribute in search_strings){
      if(USER_SEARCH_PARAMETERS.includes(attribute)){
        switch(attribute){
          case 'user_id':
            if(search_strings[attribute] == participant.user.id) return highlight_class;
            break;
          case 'user_task':
            if(participant.tasks.some(function(task){ return task.id == search_strings[attribute]})) return highlight_class;
            break;
          case 'user_technology':
            if(participant.technologies.some(function(technology){ return technology.id == search_strings[attribute]})) return highlight_class;
            break;
          case 'user_role':
            if(participant.roles.some(function(role){ return role.id == search_strings[attribute]})) return highlight_class;
            break;
          case 'min_minutes': 
            if(participant.minutes >= search_strings[attribute]) return highlight_class;
            break;
        }
      }
    }
    
    return '';
  };
  
  $scope.fetchProjects = function (opts){
      
      $scope.projects = false;
      $scope.loader = true;
      var params = [];
      for(var i in opts){
        params.push(i + '=' + opts[i]);
      }
      
      var string = params.join("&");
      $http.get(settings.prefix + 'projects?pageSize=' + PAGE_SIZE + '&' + string).then(function(response) {

      $scope.projects = response.data.list;
      $scope.pagination = response.data.pagination;
      //console.log($scope.projects);
      $scope.isSearchResult = Object.keys($location.search()).filter(function(key){return !NOT_SEARCH_KEYS.includes(key) }).length ? true : false;
      $scope.loader = false;
      if ($scope.projects.length > 0) {
        $scope.areProjects = true;
      } else {
        $scope.areProjects = false;
      }
    }, function(err){
      $scope.loader = false; 
      console.log(err);
    });
    
    $scope.areProjects = true;
  };
  
  $scope.getWindowWidth = function() {
    var windowWidth = $window.innerWidth - 30;
    var tableWidth = document.querySelector('.table-xl-layout').clientWidth;
    if (tableWidth > windowWidth) {
      $scope.tableScroll = true;
    } else {
      $scope.tableScroll = false;
    }
  };
  
  $scope.getWindowWidth();
  
  angular.element($window).on('resize', function(){
    $scope.getWindowWidth();
  });
});