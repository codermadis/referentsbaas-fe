<script type="text/ng-template" id="recent_projects.tpl">
  <div ng-controller="recent_projects">
    <h4>
      <span ng-if="!isSearchResult">Projektid{{ pagination.rowCount ? ' ('+ pagination.rowCount +')' : ' (0)'}}</span>
      <span ng-if="isSearchResult">Otsingutulemused ({{pagination.rowCount}})</span>
    </h4>
    <md-divider></md-divider>

    <ng-include ng-if="!userIsEmployee" src="'search_filters.tpl'"></ng-include>
    
    <div ng-if="loader" class="loader-box">
        <md-progress-circular md-mode="indeterminate"></md-progress-circular>
    </div>
    
    <div ng-class="{'table-overflow': tableScroll}">
      <table class="table table-striped table-bordered table-xl-layout">
        <thead class="thead-default">
           <tr>
              <!--<th></th> Teise etapi teema-->
              <th style="width: 5%" ng-click="sortAccounts('finished')">
                Projekti seis
                <i ng-class="sortArrowClass('finished')"></i>
              </th>
              <th style="width: 15%" ng-click="sortAccounts('name')">
                Projekti nimetus
                <i ng-class="sortArrowClass('name')"></i>
              </th>
              <th style="width: 10%">Tellija</th>
              <th style="width: 8%" ng-click="sortAccounts('real_capacity')">
                Töömaht
                <i ng-class="sortArrowClass('real_capacity')"></i>
              </th>
              <th style="width: 9%" ng-click="sortAccounts('cost')">
                Maksumus
                <i ng-class="sortArrowClass('cost')"></i>
              </th>
              <th style="width: 9%" ng-click="sortAccounts('start_date')">
                Periood
                <i ng-class="sortArrowClass('start_date')"></i>
              </th>
              <th style="width: 21%">Töötajad</th>
              <th style="width: 10%">Teostatud tööd</th>
              <th style="width: 8%" ng-click="sortAccounts('data_complete')">
                Andmete täielikkus
                <i ng-class="sortArrowClass('data_complete')"></i>
              </th>
              <th style="width: 5%" ng-if="!userIsEmployee"></th>
           </tr>
        </thead>
        <tbody>
          <tr ng-repeat="item in dummyPlaceholderData" ng-if="!projects.length && loader" class="blokk-font">
                 <td ng-repeat="col in item">{{ col }}</td>
          </tr>
          <tr ng-if="isSearchResult && !areProjects" class="no-results">
            <td colspan="99" class="p-4">
              <h2 class="mb-3"><i class="fa fa-search"></i></h2>
              <h4 class="mb-3">Otsingutulemused puuduvad</h4>
              <h5 class="m-0">Lähtesta otsing <a href="javascript:void(0);" ng-click="resetSearch()">siit</a></h5>
            </td>
          </tr>
          <tr ng-if="!isSearchResult && !areProjects" class="no-results">
            <td colspan="99" class="p-4">
              <h4 class="mb-0">Projektid puuduvad</h4>
            </td>
          </tr>
          <tr ng-repeat="project in projects" ng-if="projects.length">
          <!-- teise etapi teema
            <td>
               <label class="custom-control custom-checkbox d-block">
                  <input type="checkbox" class="custom-control-input">
                  <span class="custom-control-indicator"></span>
               </label>
            </td>
          -->
            <td ng-if="!project.finished">Töös</td>
            <td ng-if="project.finished">Lõpetatud</td>
            <td class="name">
              <a ui-sref="projects-view.view({id:{{project.id}}})">{{project.name}}</a>
            </td>
            <td>
                <span ng-repeat="client in project.clients">
                  <span ng-if="!$last"> {{client.name}},</span>
                  <span ng-if="$last"> {{client.name}}</span>
                </span>
            </td>
            <td >{{project.real_capacity | minutesTohours}}</td>
            <td >{{project.cost}}</td>
            <td>{{project.start_date | date : 'dd.MM.yyyy' }} - {{project.finish_date | date : 'dd.MM.yyyy' }}</td>
            <td>
                <span ng-repeat="participant in project.participants">
                  <span ng-class="highlightParticipant(participant)">
                    <span ng-if="!$last">{{participant.user.first_name}} {{participant.user.last_name}},</span>
                    <span ng-if="$last">{{participant.user.first_name}} {{participant.user.last_name}}</span>
                  </span>
                </span>
                <!--
                <div class="badge" ng-if="participantDisplayLimit < project.participants.length" >
                  +{{project.participants.length - participantDisplayLimit}}
                </div>
                -->
            </td>
            <td>
                <span ng-repeat="task in project.project_tasks">
                  <span ng-if="!$last">{{task.value}},</span>
                  <span ng-if="$last" >{{task.value}}</span>
                </span>
            </td>
            <td>
              <span ng-if="!project.data_complete">Puudulik</span>
              <span ng-if="project.data_complete">Lõplik</span>
            </td>
            <td ng-if="!userIsEmployee">
              <a class="mt-2 d-inline-block" ui-sref="projects.general({id:{{project.id}}})">
                <i class="material-icons edit-icon">edit</i>
              </a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
     
    <paginator data="{{ pagination }}"></paginator>
    <!-- 
    <a  href="" class="btn btn-primary mr-2 mt-4">Ekspordi projekt</a>   
    <a href="" class="btn btn-primary mt-4">Ekspordi CV</a>
    -->
  </div>
</script>