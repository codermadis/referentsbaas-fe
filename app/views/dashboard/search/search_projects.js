App.controller("search_projects", function($scope, $rootScope, $http, $location, $filter, $timeout, $converter){
  const SEARCH_IGNORE = ['page', 'data_complete'];
  const DEFINITION = {
    'project_names':{type: 'list', src: 'projects?project_names='},
    'project_ids': {type: 'async', src: 'projects?ids='},
    'client_ids': {type: 'async', src: 'clients?ids='},
    'participants': {type: 'async', src: 'users?ids='},
    'system_type': {type: 'class', category: 'system_type'},
    'project_task': {type: 'class', category: 'task'},
    'project_technology': {type: 'class', category: 'technology'},
    'client_field': {type: 'class', category: 'field'},
    'min_capacity': {type: 'minutes' },
    'min_cost': {type: 'value'},
    'start_date': {type: 'value'},
    'finish_date': {type: 'value'},
    'outsourced': {type: 'value'},
    'finished': {type: 'value'},
    'client_sector': {type: 'checkbox'}
  };
  const EXPANDED_SEARCH_KEYS = ['outsourced', 'finished', 'client_ids', 'participants', 'client_sector','client_field'];
  
  $scope.expansion = function() {
    if ($rootScope.showExpansion) {
      $rootScope.showExpansion = false;
    } else {
      $rootScope.showExpansion = true;
    }
  };
  
  $scope.autocomplete = function(input, resource) {
    if(input.length < 3) return [];
    return $http.get(settings.prefix + resource + '/autocomplete?name=' + input)
      .then(function(response) { return response.data }, function(err){ console.log(err) });
  };
    
  $scope.tags = function(input, category){
    var tags = $scope.class[category];
    var output = [];
    if(typeof(input) == 'string'){
      for(var i in tags){
        if(tags[i].value.toLowerCase().includes(input.toLowerCase())) output.push(tags[i]);
      }
    } else if(typeof(input) == 'number'){
      for(var ii in tags){
        if(tags[ii].id == input) output = tags[ii];
      }
    }
    return output;
  };
  
  $scope.transformProjectName = function(input){
    if(input.id) return input.name;
    else return input;
  };
  
  function initialize () {
    /**
     * Populate search form with data from $location.search();
     *
     **/
    console.log()
    $scope.class = $rootScope.classRegistry;
    
    $scope.model = {
      project_names: [],
      client_sector: {},
      finished: "",
      start_date: null,
      finish_date: null
    };
    
    var searchUrl = $location.search();
    
    for(var field in DEFINITION){
      if(DEFINITION[field].type == 'class') $scope.model[field] = [];
      else if(DEFINITION[field].type == 'async' && field != 'project_ids') $scope.model[field] = [];
    }
    
    for(var attr in searchUrl){
   
      if(EXPANDED_SEARCH_KEYS.includes(attr) && !$rootScope.showExpansion){
        $rootScope.showExpansion = true;
      }
      
      if(DEFINITION[attr]){
        switch(DEFINITION[attr].type){
          case 'class': 
            var tag_array =  searchUrl[attr].split(',');
            for(var ci in tag_array){
              $scope.model[attr].push($scope.tags(Number(tag_array[ci]), DEFINITION[attr].category));
            }
            break;
          case 'minutes':
            $scope.model[attr] = $converter.minutesTohours(searchUrl[attr]);
            break;
          case 'value':
            if(attr == 'min_cost')  $scope.model[attr] = parseInt(searchUrl[attr], 10);
            else $scope.model[attr] = searchUrl[attr];
            break;
          case 'checkbox':
            var boxes = searchUrl[attr].split(',');
            for(var box in boxes){
              $scope.model[attr][boxes[box]] = true;
            }
            break;
          case 'list': $scope.model[attr] = searchUrl[attr].split(',');
        }
      }
    }
    
    if(searchUrl['project_ids']) $http.get(settings.prefix + 'projects?ids=' +  searchUrl['project_ids']).then(function(response){ 
      $scope.model.project = response.data[0];
    }).catch(function(err){ console.log(err)});
    
    if(searchUrl['client_ids']) $http.get(settings.prefix + 'clients?ids=' +  searchUrl['client_ids']).then(function(response){ 
      $scope.model.client_ids = response.data;
    }).catch(function(err){ console.log(err)});
    
    if(searchUrl['participants']) $http.get(settings.prefix + 'users?ids=' +  searchUrl['participants']).then(function(response){ 
      $scope.model.participants = response.data;
    }).catch(function(err){ console.log(err)});
    //console.log($scope.model);
    
  }// END OF initialize
  
  var mustBeCSVofIds = ['system_type','project_task','client_field','project_technology','participants','client_ids'];
  var mustBeMinutes = ['min_capacity'];
  var mustBeCSV = ['project_names'];
  
  $scope.search = function(){
    //console.log($scope.model);
    var strings = {};
    
    for(var attr in $scope.model){
      
      if($scope.model[attr]){
        //Must be CSV of Ids
        if(mustBeCSVofIds.includes(attr)){ 
          for(var i in $scope.model[attr]) strings[attr] ? strings[attr] += ','+ $scope.model[attr][i].id : strings[attr] = $scope.model[attr][i].id;
        }
        //Project
        else if(mustBeCSV.includes(attr)){ 
          for(var index in $scope.model[attr]){
            strings[attr] ? strings[attr] += ','+ $scope.model[attr][index] : strings[attr] = $scope.model[attr][index];
          }
        }
        else if(attr === 'project') strings.project_ids = $scope.model[attr].id;
        
        else if (attr === 'finished'){
         if($scope.model[attr] == 'true') strings.finished = 'true';
         else if($scope.model[attr] == 'false') strings.finished = 'false';
        }
        //Client sector
        else if(attr === 'client_sector'){
          if($scope.model[attr].public === true) strings[attr] ? strings[attr] += ',public' : strings[attr] = 'public';
          if($scope.model[attr].private === true) strings[attr] ? strings[attr] += ',private' : strings[attr] = 'private';
          if($scope.model[attr].nonprofit === true) strings[attr] ? strings[attr] += ',nonprofit' : strings[attr] = 'nonprofit';
        }
        //Format dates
        else if(attr.includes('date')) strings[attr] = $filter('date')($scope.model[attr], 'yyyy-MM-dd');
        //Values must be in minutes
        else if(mustBeMinutes.includes(attr)){
          strings[attr] = $converter.hoursTominutes($scope.model[attr]);
          $scope.model[attr] = $converter.minutesTohours(strings[attr]);
        }
        //All the rest
        else strings[attr] = $scope.model[attr];
      }
      
    }
    
    $location.search( strings );
    
  };
  
  $scope.$watch(function(){
    return $location.search();
  }, function(){
    var count = 0;
    for(var i in $location.search()){
      if(!SEARCH_IGNORE.includes(i)) count ++;
    }
    
    if(count === 0){
      if($scope.class) {
        initialize();
      }
    }
    
  }, true);
  
  $scope.placeholder = function(text, arrayLength){
    return arrayLength > 0 ? '' : text;
  };
  
  $scope.resetSearch = function(){
    $location.search({});
    initialize();
  };
  
  initialize();
});