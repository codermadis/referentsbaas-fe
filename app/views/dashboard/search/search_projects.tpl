<script type="text/ng-template" id="search_projects.tpl">
<div ng-controller="search_projects">
      <form ng-submit="search()">
           
           <div class="form-group">
              <label for="projectClient">Projekti nimetus</label>
              <md-chips ng-model="model.project_names"
                 md-autocomplete-snap
                 md-separator-keys="[188,13]"
                 md-transform-chip="transformProjectName($chip)">
                  <md-autocomplete
                    md-search-text="searchProject"
                    md-selected-item="model.project"
                    md-items="item in autocomplete(searchProject, 'projects')"
                    md-item-text="item.name"
                    md-clear-button="false"
                    placeholder="Sisesta projekti nimetus või märksõna">
                 <md-item-template>
                    <span md-highlight-text="searchProject">{{item.name}}</span>
                 </md-item-template>
              </md-autocomplete>
                <md-chip-template>
                  <span>
                    <strong>{{$chip}}</strong>
                  </span>
                </md-chip-template>
            </div>
           
           <div class="form-group">
              <label for="projectJobs">Süsteemi liigid</label>
              <md-chips ng-model="model.system_type" ng-class="{'no-placeholder': model.system_type.length}"
                 md-autocomplete-snap
                 md-require-match=true>
                 <md-autocomplete
                       md-search-text="searchSystemType"
                       md-items="item in tags(searchSystemType, 'system_type')"
                       md-item-text="item.value"
                       md-min-length="0"
                       placeholder="{{placeholder('E-pood, veebileht, infosüsteem vms', model.system_type.length)}}">
                    <span md-highlight-text="searchSystemType">{{item.value}}</span>
                 </md-autocomplete>
                 
                 <md-chip-template>
                    <span>
                       <strong>{{$chip.value}}</strong>
                    </span>
                 </md-chip-template>
                 
              </md-chips>
              
           </div>
           <div class="form-group">
              <label for="projectJobs">Teostatud tööd</label>
              <md-chips ng-model="model.project_task" ng-class="{'no-placeholder': model.project_task.length}"
                 md-autocomplete-snap
                 md-separator-keys="[188,13]"
                 md-require-match=true>
                 <md-autocomplete
                       md-search-text="searchProjectTask"
                       md-items="item in tags(searchProjectTask, 'task')"
                       md-item-text="item.value"
                       placeholder="{{placeholder('Ärianalüüs, programmeerimine, kasutajaliidese disain vms', model.project_task.length)}}">
                    <span md-highlight-text="searchProjectTask">{{item.value}}</span>
                 </md-autocomplete>
                 
                 <md-chip-template>
                    <span>
                       <strong>{{$chip.value}}</strong>
                    </span>
                 </md-chip-template>
                 
              </md-chips>
              
           </div>
           <div class="d-flex justify-content-between flex-wrap">
               <div class="form-group next-to">
                  <label>Töömaht tundides</label>
                  <input type="text" class="form-control" ng-model="model.min_capacity">
               </div>
               <div class="form-group next-to">
                  <label for="projectPrice">Maksumus eurodes</label>
                  <input type="text" class="form-control" id="projectPrice" ng-model="model.min_cost">
               </div>
           </div>
           <div class="d-flex justify-content-between flex-wrap">
               <div class="form-group next-to">
                  <label for="projectBeginningDate">Algus</label>
                  <md-datepicker md-placeholder="kuupäev" ng-model="model.start_date" ></md-datepicker>
               </div>
               <div class="form-group next-to">
                  <label for="projectEndDate">Lõpp</label>
                  <md-datepicker md-placeholder="kuupäev" ng-model="model.finish_date"></md-datepicker>
               </div>
           </div>
           <div class="form-group">
              <label for="projectTech">Tehnoloogia</label>
              <md-chips ng-model="model.project_technology" ng-class="{'no-placeholder': model.project_technology.length}"
               md-autocomplete-snap
               md-separator-keys="[188,13]"
               md-require-match=true>
                <md-autocomplete
                     md-search-text="searchProjectTech"
                     md-items="item in tags(searchProjectTech, 'technology')"
                     md-item-text="item.value"
                     placeholder="{{placeholder('Node.js, AngularJS, CSS3, HTML5 vms', model.project_technology.length)}}">
                  <span md-highlight-text="searchProjectTech">{{item.value}}</span>
                </md-autocomplete>
               
                <md-chip-template>
                  <span>
                     <strong>{{$chip.value}}</strong>
                  </span>
                </md-chip-template>
               
              </md-chips>
             
            </div>
           
           <div ng-show="showExpansion">
              <div class="form-check form-check-inline">
                  <label class="custom-control custom-checkbox">
                     <input type="checkbox" class="custom-control-input" value="true" ng-model="model.outsourced">
                     <span class="custom-control-indicator"></span>
                     <span class="custom-control-description">Allhanke projekt</span>
                  </label>
              </div>
              <p class="mb-2">Projekti seis</p>
              <label class="custom-control custom-radio d-block mb-1">
                 <input type="radio" class="custom-control-input" value="" ng-model="model.finished">
                 <span class="custom-control-indicator"></span>
                 <span class="custom-control-description">Kõik</span>
              </label>
              <label class="custom-control custom-radio d-block mb-1">
                 <input type="radio" class="custom-control-input" value="false" ng-model="model.finished">
                 <span class="custom-control-indicator"></span>
                 <span class="custom-control-description">Töös</span>
              </label>
              <label class="custom-control custom-radio d-block mb-2">
                 <input type="radio" class="custom-control-input" value="true" ng-model="model.finished">
                 <span class="custom-control-indicator"></span>
                 <span class="custom-control-description">Lõpetatud</span>
              </label>
              
              <div class="form-group">
                <label for="projectClient">Tellija</label>
                <md-chips ng-model="model.client_ids"
                   md-autocomplete-snap
                   md-separator-keys="[188,13]"
                   md-require-match=true>
                    <md-autocomplete
                      md-search-text="searchClient"
                      md-items="item in autocomplete(searchClient, 'clients')"
                      md-item-text="item.name"
                      placeholder="Lisa Tellija">
                   <md-item-template>
                      <span md-highlight-text="searchClient">{{item.name}}</span>
                   </md-item-template>
                </md-autocomplete>
                  <md-chip-template>
                    <span>
                      <strong>{{$chip.name}}</strong>
                    </span>
                  </md-chip-template>
              </div>
              
              <div class="form-group">
                 <label for="projectEmployee">Töötaja projektis</label>
                <md-chips ng-model="model.participants"
                 md-autocomplete-snap
                 md-separator-keys="[188,13]"
                 md-require-match=true>
                  <md-autocomplete
                   md-search-text="searchParticipant"
                   md-items="item in autocomplete(searchParticipant, 'users')"
                   md-item-text="item.email"
                   placeholder="Lisa töötaja">
                    <span md-highlight-text="searchParticipant">{{item.first_name}} {{item.last_name}}</span>
                  </md-autocomplete>
                  <md-chip-template>
                    <span>
                      <strong>{{$chip.first_name}} {{$chip.last_name}}</strong>
                    </span>
                  </md-chip-template>
              </div>
              
              <p class="mb-2">Kliendisektor</p>
              <label class="custom-control custom-checkbox d-block mb-1">
                 <input type="checkbox" class="custom-control-input" value="true" ng-model="model.client_sector.public">
                 <span class="custom-control-indicator"></span>
                 <span class="custom-control-description">Avalik sektor</span>
              </label>
              <label class="custom-control custom-checkbox d-block mb-2">
                 <input type="checkbox" class="custom-control-input" value="true" ng-model="model.client_sector.private">
                 <span class="custom-control-indicator"></span>
                 <span class="custom-control-description">Erasektor</span>
              </label>
              <div class="form-group">
                 <label for="projectField">Valdkond</label>
                 <md-chips ng-model="model.client_field" ng-class="{'no-placeholder': model.client_field.length}"
                 md-autocomplete-snap
                 md-separator-keys="[188,13]"
                 md-require-match=true>
                 <md-autocomplete
                       md-search-text="searchField"
                       md-items="item in tags(searchField, 'field')"
                       md-item-text="item.value"
                       placeholder="{{placeholder('Finants, IT-arendus, telekommunikatsioon vms', model.client_field.length)}}">
                    <span md-highlight-text="searchField">{{item.value}}</span>
                 </md-autocomplete>
                 
                 <md-chip-template>
                    <span>
                       <strong>{{$chip.value}}</strong>
                    </span>
                 </md-chip-template>
                </md-chips>
                
              </div>
              
           </div>
           <div class="d-flex justify-content-between align-items-start align-items-sm-center mt-2 flex-column flex-sm-row">
              <a href="javascript:void(0);" class="expand-search" ng-click="expansion()">
                Täiendav otsing
                <i class="material-icons" ng-show="!showExpansion">keyboard_arrow_down</i>
                <i class="material-icons" ng-show="showExpansion">keyboard_arrow_up</i>
              </a>
              <div class="d-flex align-items-start align-items-sm-center flex-column-reverse flex-sm-row mt-3 mt-sm-0">
                <a href="javascript:void(0);" ng-click="resetSearch()" class="mr-3">Lähtesta otsing</a>
                <button type="submit" class="btn btn-primary mb-3 mb-sm-0">Otsi</button>
              </div>
           </div>
      </form>
</div>
</script>