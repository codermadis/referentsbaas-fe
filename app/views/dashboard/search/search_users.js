App.controller("search_users", function($scope, $rootScope, $http, $location, $filter, $timeout, $converter){
  var userAppliedSearchParams = false;
  const LISTS = ['user_role', 'user_task', 'user_technology'];
  const CSV_OF_IDS = ['user_role', 'user_task', 'user_technology'];
  const MINUTES = ['min_minutes'];
  const DEFINITION = {
    'user_id': {type: 'async', src: 'users?ids='},
    'user_task': {type: 'class', category: 'task'},
    'user_technology': {type: 'class', category: 'technology'},
    'user_role': {type: 'class', category: 'role'},
    'min_minutes': {type: 'minutes' },
  };
  
  function initialize(){
    /**
     * Populate search form with data from $location.search();
     *
     **/
    $scope.model = {}; 
    for(var item in LISTS){ 
      $scope.model[LISTS[item]] = [];
    }
    
    var searchUrl = $location.search();
    for(var attribute in searchUrl){
      if(DEFINITION[attribute]){
        switch(DEFINITION[attribute].type){
          case 'class': 
            var tag_array = [];
            if(typeof searchUrl[attribute] == 'number') tag_array.push( searchUrl[attribute]);
            else tag_array = searchUrl[attribute].split(',');
              $scope.model[attribute] = tag_array.map(function(item){
               return $scope.tags(Number(item), DEFINITION[attribute].category)[0];
              });

            break;
          case 'minutes':
            $scope.model[attribute] = $converter.minutesTohours(searchUrl[attribute]);
            break;
        }
      }
    }
    if(searchUrl['user_id']) {
      $http.get(settings.prefix + 'users?ids=' +  searchUrl['user_id']).then(function(response){ 
        $scope.model.user = response.data[0];
      }).catch(function(err){ console.log(err)});
    }
  }

  $scope.autocomplete = function(searchText, resource) {
    if(searchText.length < 3) return [];
    return $http.get(settings.prefix + resource + '/autocomplete?name=' + searchText)
      .then(function(response) {
        return response.data;
      },function(error){ console.log(error.data) });
  };
  
  $scope.tags = function(input, category){
    if(typeof(input) == 'string'){
      return $rootScope.classRegistry[category].filter(function(item) {
        return item.value.toLowerCase().includes(input.toLowerCase());
      });
    } else if(typeof(input) == 'number'){
      return $rootScope.classRegistry[category].filter(function(item) {
        return item.id == input;
      });
    }
  };
 
  $scope.search = function(){
    var strings = {};
      for(var parameter in $scope.model){
        if($scope.model[parameter]){
        
          if(CSV_OF_IDS.includes(parameter)){ 
            for(var i in $scope.model[parameter]) strings[parameter] ? strings[parameter] += ','+ $scope.model[parameter][i].id : strings[parameter] = $scope.model[parameter][i].id;
          }
          
          else if(parameter === 'user') {
            strings.user_id = $scope.model[parameter].id;
          }
         
          else if(MINUTES.includes(parameter)){
            strings[parameter] = $converter.hoursTominutes($scope.model[parameter]);
          }
          
          else {
            strings[parameter] = $scope.model[parameter];
          }
        }
      }
    userAppliedSearchParams = true;
    
    $location.search( strings );
    
    $timeout(function(){
      userAppliedSearchParams = false;
    },0);
  };
  
  $scope.$watch(function(){
    return $location.search();
  }, function(){
    if(!userAppliedSearchParams){
      initialize();
    }
  }, true);
  
  $scope.resetSearch = function(){
    $location.search({});
    initialize();
  };
  
  $scope.placeholder = function(text, arrayLength){
    return arrayLength > 0 ? '' : text;
  };
  
  $scope.fullname = function(item){
    return item.first_name + ' ' + item.last_name;
  };
  
  initialize();
});