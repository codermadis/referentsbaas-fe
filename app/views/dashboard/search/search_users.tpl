<script type="text/ng-template" id="search_users.tpl">
<div ng-controller="search_users">
      <form ng-submit="search()">
           <div class="form-group">
              <label for="projectEmployee">Töötaja nimi</label>
              <md-autocomplete
                    md-match-case-insensitive=true
                    md-search-text="searchUser"
                    md-selected-item="model.user"
                    md-items="item in autocomplete(searchUser, 'users')"
                    md-item-text="fullname(item)"
                    placeholder="Sisesta töötaja nimi">
                 <md-item-template>
                    <span md-highlight-text="searchProject">{{item.first_name}} {{item.last_name}}</span>
                 </md-item-template>
              </md-autocomplete>
           </div>
           <div class="form-group" style="width: 25%">
              <label for="projectHours">Töötunnid</label>
              <input type="text" class="form-control" id="projectHours" ng-model="model.min_minutes">
           </div>
           <div class="form-group">
              <label for="projectRole">Roll</label>
              <md-chips ng-model="model.user_role"
                 md-autocomplete-snap
                 md-separator-keys="[188,13]"
                 md-require-match=true>
                 <md-autocomplete
                       md-search-text="searchRole"
                       md-items="item in tags(searchRole, 'role')"
                       md-item-text="item.value"
                       placeholder="{{placeholder('Analüütik, front-end arendaja, projektijuht vms', model.user_role.length)}}">
                    <span md-highlight-text="searchRole">{{item.value}}</span>
                 </md-autocomplete>
                 
                 <md-chip-template>
                    <span>
                       <strong>{{$chip.value}}</strong>
                    </span>
                 </md-chip-template>
              </md-chips>
           </div>
           <div class="form-group">
              <label for="projectAssignment">Teostatud tööd</label>
              <md-chips ng-model="model.user_task"
                 md-autocomplete-snap
                 md-separator-keys="[188,13]"
                 md-require-match=true>
                 <md-autocomplete
                       md-search-text="searchTask"
                       md-items="item in tags(searchTask, 'task')"
                       md-item-text="item.value"
                       placeholder="{{placeholder('Dokumenteerimine, ärianalüüs, programmeerimine vms', model.user_task.length)}}">
                    <span md-highlight-text="searchTask">{{item.value}}</span>
                 </md-autocomplete>
                 
                 <md-chip-template>
                    <span>
                       <strong>{{$chip.value}}</strong>
                    </span>
                 </md-chip-template>
              </md-chips>
           </div>
           <div class="form-group">
              <label for="projectTechUse">Tehnoloogia</label>
              <md-chips ng-model="model.user_technology"
                 md-autocomplete-snap
                 md-separator-keys="[188,13]"
                 md-require-match=true>
                 <md-autocomplete
                       md-search-text="searchTechnology"
                       md-items="item in tags(searchTechnology, 'technology')"
                       md-item-text="item.value"
                       placeholder="{{placeholder('Node.js, AngularJS, CSS3, HTML5 vms', model.user_technology.length)}}">
                    <span md-highlight-text="searchTechnology">{{item.value}}</span>
                 </md-autocomplete>
                 
                 <md-chip-template>
                    <span>
                       <strong>{{$chip.value}}</strong>
                    </span>
                 </md-chip-template>
              </md-chips>
           </div>
           <div class="d-flex justify-content-start justify-content-sm-end align-items-center mt-2">
              <div class="d-flex align-items-start align-items-sm-center flex-column-reverse flex-sm-row mt-3 mt-sm-0">
                <a href="javascript:void(0);" ng-click="resetSearch()" class="mr-3">Lähtesta otsing</a>
                <button type="submit" class="btn btn-primary mb-3 mb-sm-0">Otsi</button>
              </div>
           </div>
      </form>
</div>
</script>