App.controller("second-menu", function($scope, $rootScope, $location){
    
    $scope.currentLocation = $location.path();
    
    $scope.mainTabs = [
        {_id: 0, min_role_id: 3, name:'Töölaud',  ui_sref:"dashboard", url:"/toolaud"},
        {_id: 1, min_role_id: 2, name:'Minu projektid',  ui_sref:"dashboard-my_projects", url:"/toolaud/minu_projektid"},
    ];
    
    $scope.setActiveMainTab = function (id){
       $scope.activeMainTab = id;
    };
    
    
    $scope.setActiveMainTab($scope.mainTabs[0]._id);
});