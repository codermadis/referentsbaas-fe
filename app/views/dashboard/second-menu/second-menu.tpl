<script type="text/ng-template" id="second-menu.tpl">
<div ng-controller="second-menu">
    <section class="front-tabs">
          <div class="container">
             <div class="tab-wrapper">
                <a href="{{tab.url}}" ng-class="currentLocation == tab.url ? 'single-tab active' : 'single-tab'"
                    ng-repeat="tab in mainTabs">
                    {{tab.name}}
                </a>
             </div>
          </div>
    </section>
</div> 
</script>