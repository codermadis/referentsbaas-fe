App.controller("search_filters", function($scope, $rootScope, $location){
    $scope.model = {filter: $location.search().data_complete || 'all'};
    
    $scope.$watch(function(){
      return $location.search();
    }, function(){
      $scope.model = {filter: $location.search().data_complete || 'all'};
      $scope.setFilter();
    }, true);
    
    $scope.setFilter = function () {
      var searchParams = $location.search();
        switch($scope.model.filter){
          case 'all': 
              if(searchParams.data_complete) {
                searchParams.page = 1;
                delete searchParams.data_complete;
              }
              break;
          case 'true': 
              if(searchParams.data_complete != 'true') searchParams.page = 1;
              searchParams.data_complete = 'true';
              break;
          case 'false':
              if(searchParams.data_complete != 'false') searchParams.page = 1;
              searchParams.data_complete = 'false';
              break;
        }
      $location.search( searchParams );
    };
});
   