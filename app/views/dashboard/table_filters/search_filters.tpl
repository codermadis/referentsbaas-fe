<script type="text/ng-template" id="search_filters.tpl">
<div ng-controller="search_filters">
    <div class="my-4" >
        <label class="custom-control custom-radio d-block mb-1">
             <input type="radio" class="custom-control-input" ng-model="model.filter" value="all" ng-click="setFilter()">
             <span class="custom-control-indicator"></span>
             <span class="custom-control-description">Kõik</span>
        </label>
        <label class="custom-control custom-radio d-block mb-1">
             <input type="radio" class="custom-control-input" ng-model="model.filter" value="true" ng-click="setFilter()">
             <span class="custom-control-indicator"></span>
             <span class="custom-control-description">Lõplike andmetega projektid</span>
        </label>
        <label class="custom-control custom-radio d-block mb-1">
             <input type="radio" class="custom-control-input" ng-model="model.filter" value="false" ng-click="setFilter()">
             <span class="custom-control-indicator"></span>
             <span class="custom-control-description">Puudulike andmetega projektid</span>
        </label>
    </div>
</div>
</script>