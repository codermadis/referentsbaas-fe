App.controller('login', function($state, $user, $scope, $http, $rootScope, $location, $cookies, $crypto, $mdToast) {
	
	var expireDate = new Date();
	expireDate.setDate(expireDate.getDate() + 180);
	var cookieOptions = {
		expires: expireDate
	};
	$scope.model = {};
	$scope.opts = {};
	$rootScope.headerless = $state.current.headerless ? true : false;
	
	if($cookies.get('rememberMe')) {
		$scope.opts.remembered = true;
		$scope.model.email = JSON.parse($cookies.get('email'));
		$scope.model.password = $crypto.decrypt(JSON.parse($cookies.get('password'))); 
	} else {
		$scope.opts.remembered = false;
	}
	
  
  $scope.messages = { errorMessage: false, alertMessage: false };
  
	$scope.login = function() {
		$scope.messages.alertMessage = false;
  	$scope.messages.errorMessage = false;
  	
		$user.login($scope.model.email, $scope.model.password).then(function(response) {
			
			if($scope.opts.remembered == true) {
				$cookies.putObject('rememberMe', true, cookieOptions);
				$cookies.putObject('email', $scope.model.email, cookieOptions);
				$cookies.putObject('password',  $crypto.encrypt($scope.model.password), cookieOptions);
			} else {
				$cookies.remove('rememberMe');
				$cookies.remove('email');
				$cookies.remove('password');
			}
		
			$location.path('/');
		
		}, function (response){
			if(response.data.message) {
				$scope.messages.errorMessage = response.data.message;
			} else { 
				$scope.messages.errorMessage = "Logimine ebaõnnestus";
			}
		});
  };
  
  $scope.switchForm = function(key){
  	$scope.forgotPassword =	(key =='login') ?  false: true;
  	// var errorMsg = document.querySelector('.item-update');
  	// //console.log(errorMsg);
  	// if (errorMsg) {
  	// 	console.log(errorMsg);
			// errorMsg.remove();
  	// }
  	$scope.messages.alertMessage = false;
  	$scope.messages.errorMessage = false;
  }
  $scope.sendRecoveryEmail = function(email){
  	$scope.messages.alertMessage = false;
  	$scope.messages.errorMessage = false;
  	$http({
			method: "POST",
			url: settings.prefix + "recovery",
			data: {'email': email} 
		}).then(function(response) {
			
		$scope.messages.alertMessage = 'Email on saadetud';
		
		}, function (response){
			 $scope.messages.errorMessage = "Emaili saatmine ebaõnnestus";
		});
  	
  }
})