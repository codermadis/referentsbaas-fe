App.config(function ($stateProvider){
   
   $stateProvider
      .state({
         name: "login",
         url: "/login",
         templateUrl: "login.tpl",
			controller: "login",
			headerless: true
      });
});