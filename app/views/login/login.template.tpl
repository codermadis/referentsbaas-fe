<script type="text/ng-template" id="login.tpl">
    <section class="container-fluid d-flex justify-content-center align-items-center">
       <div class="login">
          <div class="d-flex justify-content-center">
              <img src="assets/imgs/logo.svg" class="img-fluid" />
          </div>
          <h1 class="text-center">Referentsbaas</h1>
          <form ng-if="!forgotPassword">
             <p ng-if="messages.errorMessage" class="text-danger item-update" id="login_validation_errors">{{messages.errorMessage}}</p>
             <div class="form-group row">
                <label class="col-md-3 col-form-label" for="email">E-mail:</label>
                <div class="col-md-9">
                   <input type="email" ng-model="model.email" class="form-control" id="email">
                </div>
             </div>
             <div class="form-group row">
                <label class="col-md-3 col-form-label" for="password">Salasõna:</label>
                <div class="col-md-9">
                   <input type="password" ng-model="model.password" class="form-control" id="password">
                </div>
             </div>
             <p class="text-right"><a href="javascript:void(0);" ng-click="switchForm('forgotPassword')">Unustasid salasõna?</a></p>
             <div class="form-check">
                <label class="custom-control custom-checkbox">
                   <input type="checkbox" class="custom-control-input" ng-model="opts.remembered">
                   <span class="custom-control-indicator"></span>
                   <span class="custom-control-description">Jäta mind meelde</span>
                </label>
             </div>
             <div class="form-group text-center">
                <button type="submit" ng-click="login()" class="btn btn-primary">Logi sisse</button>
             </div>
          </form>
          <form id="enter_email_form" name="enter_email_form" ng-if="forgotPassword" ng-submit="sendRecoveryEmail(set_email)">
  					<h5>Sisesta e-mail uue salasõna saamiseks</h5>
  					<p ng-if="messages.errorMessage" class="text-danger item-update" id="login_validation_errors">{{messages.errorMessage}}</p>
  					<p ng-if="messages.alertMessage" class="well item-update" id="login_validation_errors">{{messages.alertMessage}}</p>
  					<!--<p ng-if="!messages.errorMessage && !messages.alertMessage" class="well text-danger" id="login_validation_errors"></p>-->
						<div class="form-group text-center">
							<input id="email" class="form-control" ng-model="set_email" type="email" name="email" value="">
		        </div>
  					<div class="form-group text-center">
                <button type="submit" class="btn btn-primary">Saada uus salasõna</button>
            </div>
            <div class="form-group text-center">
                <a ng-click="switchForm('login')" href="javascript:void(0);">Tagasi</a>
            </div>
		  </form>
       </div>
    </section>
</script>