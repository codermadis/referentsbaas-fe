App.controller("project_client", function($scope, $http, $rootScope, $stateParams, $filter, Upload, $timeout, $converter, $window, $location){
  $scope.redirect_path = $location.path();
  $scope.model = {
    clients: []
  };
  $scope.model.id = $stateParams.id || null;
  $scope.settings_prefix = settings.prefix; //for file download link
  
  $scope.setActiveClient = function(client_id){
    $scope.activeClient_id = client_id;
    $scope.tabScroll();
  };
  
  $scope.bindSelectedItem = function(selectedItem, model){
    if(selectedItem == null) return;
    model.disableNameInput = true;
    model.updateAttributes(selectedItem);
    
    model.attach();
  };
  
  $scope.fetchData = function(){
    $scope.loader = true;
    $http.get(settings.prefix + 'projects/' + $scope.model.id ).then(function(response){
      $scope.loader = false;
      $scope.model = response.data.data;
      $scope.$parent.model = response.data.data;
      
      $scope.tabScroll();
      
      for(var c_index in $scope.model.clients){
        $scope.model.clients[c_index] = new Client($scope.model.clients[c_index], randomId(), true);
      }
      
      if($scope.model.clients.length) $scope.setActiveClient($scope.model.clients[0]._id);
      else {
        var client = $scope.addClient();
        $scope.setActiveClient(client._id);
      }
    }, function(error){
     console.error(error);
    });
  };
  $scope.fetchData();
  
  function Client (data, _id, disableNameInput){
    this.attributes = {};
    this._id = _id;
    this.disableNameInput = disableNameInput || false;
    this.updateAttributes = function(data){
      
      if(!data.files) this.attributes.files ? data.files = this.attributes.files: data.files = [];
      
      for(var i in Object.keys(data)){
        var key = Object.keys(data)[i];
        this.attributes[key] = data[key];
      }
      
      var self = this;
      
      if(data.delegates) {
        //populate delegates
        if( data.delegates.length ){
          this.delegates =  data.delegates.map(function(delegate){
           
            return new Delegate(delegate, randomId(), self.attributes, self._id, true)
          });
         
        } else {
          this.delegates =  [ new Delegate({}, randomId(), this.attributes, this._id, false)];
        }
         
      }
      else {
        //update delegates' client data
        this.delegates.map(function(delegate){ delegate.client = { _id: self._id, attributes: self.attributes}});
      }
     
    };
    this.allCaps = function(key){
      return this.attributes[key].toUpperCase();
    };
    this.updateAttributes(data);
  }
  Client.prototype.attach = function(){
    var _self = this;
    $http({url: settings.prefix + 'projects/' + $scope.model.id + '/clients/' +  _self.attributes.id, method: "PUT"})
        .then(function(response){
          _self.delegates.map(function(delegate){ delegate.disableNameInput = false });
        }, function(error){ 
          console.error(error)
    });
  }
  Client.prototype.detach = function(){
    $scope.tabScroll();
    var _self = this;
    if(!_self.attributes.id) {
      removeClientFromArray();
    }
    else {
      $http({url: settings.prefix + 'projects/' +  $scope.model.id + '/clients/'+ _self.attributes.id, method: 'delete'}).then(function(res){
        removeClientFromArray();
        
      }, function(err){ console.error(err.data)});
    }
    function removeClientFromArray(){
      var index = $scope.model.clients.indexOf(_self);
      $scope.model.clients.splice(index, 1);
      
      if($scope.model.clients.length){
        var selectedTab;
        if($scope.model.clients.length > index)  selectedTab = $scope.model.clients[index]._id;
        else selectedTab = $scope.model.clients[$scope.model.clients.length - 1]._id;
    
        $scope.setActiveClient(selectedTab);
      }
      else $scope.setActiveClient($scope.addClient()._id);
    }
  };
  Client.prototype.save = function (key){
    var _self = this;
    if(!_self.attributes.name) return;
    
    var payload = angular.copy(_self.attributes);
    
    var requestParameters;
    
    if(_self.attributes.hasOwnProperty('id')) requestParameters = {url: settings.prefix + 'clients/' + _self.attributes.id, method: "PUT", data : payload}
    else requestParameters = {url: settings.prefix + 'clients/', method: "POST", data: payload};
    
    $http(requestParameters)
    .then(function(res){
      _self.updateAttributes(res.data.data);
      if(requestParameters.method == 'POST') _self.attach();
      displayCheckDone(key, _self._id);
    }, function(err){ console.error(err.data)});
  };
  Client.prototype.submitFiles = function(files){
    var _self = this;
    for(var i in files){
      
      files[i].upload = Upload.upload({
        url: settings.prefix + 'projects/' + $scope.model.id + '/clients/'+ _self.attributes.id +'/files',
        data: {file: files[i]}
      });
      files[i].upload.then(function (response) {
        _self.attributes.files.push(response.data.data);
      }, function (response) {
        console.error(response.data);
        $scope.messages.errorMessage = response.data.code;    
      });
    }
  };
  Client.prototype.deleteFile = function(file){
    var _self = this;
    $http({url:settings.prefix+ 'projects/' +  $scope.model.id + '/files/' + file.id, method: 'DELETE'}).then(function(response){
        _self.attributes.files.splice(_self.attributes.files.indexOf(file), 1);
    }, function(error){
      console.error(error);
    });
  };
  Client.prototype.downloadFile = function(file){
     window.open(settings.prefix + 'projects/' +  $scope.model.id  + '/files/' + file.id, '_self');
  };
  
  function Delegate (data, _id, client_attributes, clientLocalID, disableNameInput){
    this.attributes = {};
    this._id = _id;
    this.disableNameInput = disableNameInput || false;
    this.client = { _id:  clientLocalID, attributes: client_attributes};
    this.updateAttributes = function(data){
      var splitPhoneNumber = $converter.phoneNumberSplit(data.phone);
      this.phone_number = splitPhoneNumber.number;
      this.phone_prefix = splitPhoneNumber.prefix;
      
      for(var i in Object.keys(data)){
        var key = Object.keys(data)[i];
        this.attributes[key] = data[key];
      }
    };
    this.updateAttributes(data);
  
  }
  Delegate.prototype.attach = function(){
    var _self = this;
    if(_self.client.attributes.id == undefined || _self.attributes.id == undefined) return console.error(_self.client.attributes.id, _self.attributes.id);
    
    $http({url: settings.prefix + 'projects/' + $scope.model.id + '/clients/' +  _self.client.attributes.id + '/delegates/' + _self.attributes.id, method: "PUT"})
        .then(function(response){

        }, function(error){ 
          console.error(error);
    });
  };
  
  Delegate.prototype.detach = function(){
    var _self = this;
    
    if(!_self.attributes.hasOwnProperty('id')) {
      removeFromArray();
    }
    else {
      var url = settings.prefix + 'projects/'+  $scope.model.id + '/clients/'+ _self.client.attributes.id + '/delegates/'+ _self.attributes.id;
      $http({url: url, method: 'delete'}).then(function(res){
        removeFromArray();
      }, function(err){ console.error(err.data)});
    }
    function removeFromArray() {
      for(var i in $scope.model.clients){
        if($scope.model.clients[i]._id == _self.client._id){
          $scope.model.clients[i].delegates = $scope.model.clients[i].delegates.filter(function(delegate){
              return delegate._id != _self._id;
          });
        }
      }
    }
  }; 
  Delegate.prototype.save = function (key){
    
    var _self = this;
    if(!_self.attributes.name) return console.error('Delegate name missing');
    var payload = angular.copy(_self.attributes);
    payload.phone = _self.phone_prefix + _self.phone_number;
    
    var requestParameters;
    if(_self.attributes.hasOwnProperty('id')) requestParameters = {url: settings.prefix + 'delegates/' + _self.attributes.id, method: "PUT", data : payload}
    else requestParameters = {url: settings.prefix + 'delegates/', method: "POST", data: payload};
    
    $http(requestParameters)
    .then(function(res){
      _self.updateAttributes(res.data.data);
      if(requestParameters.method == 'POST') _self.attach();
      displayCheckDone(key, _self._id);
    }, function(err){ console.error(err.data)});
  };
  $scope.addClient = function(){
    
    var client = new Client({delegates: [], files: [], field: []}, randomId(), false);
    $scope.model.clients.push(client);
    //$scope.addDelegate(client);
    return client;
  };
  $scope.addDelegate = function(client){
  
    var delegate = new Delegate({}, randomId(), client.attributes, client._id, false);
    //client.attributes.delegates.push(delegate);
    client.delegates.push(delegate);
    return delegate;
  };
  
  function randomId(){
    //Do differentiate between clients for tabbing
    return Math.random().toString(36).substr(2, 5);
  }
  
  function displayCheckDone(key, localID){
    $scope.checkDataVerification();
    
    if(!$scope.updatedFields) $scope.updatedFields = {};
    
    if(!$scope.updatedFields[key]) $scope.updatedFields[key] = {};
    
   
    if($scope.updatedFields[key][localID]) delete $scope.updatedFields[key][localID];
    
    $timeout(function(){
      if(!$scope.updatedFields[key][localID]) $scope.updatedFields[key][localID] = true;
    },0);
  }
  
  $scope.tabScroll = function(){
    $timeout(function(){
      
      var wrapper = document.querySelector(".tabs-wrapper") || {};
      var children = wrapper.children || [];
      var childrenWidth = 0;
      for(var i = 0; i < children.length; i++) {
        childrenWidth += children[i].clientWidth;
      }
      if (wrapper.clientWidth < childrenWidth) {
        wrapper.classList.add('overflow-x');
      } else {
       if(wrapper.classList) wrapper.classList.remove('overflow-x');
      }
    }, 0);
  };
  
  $scope.updateProjectData =  function (client, data){
    $http({url: settings.prefix + 'projects/'+ $scope.model.id + '/clients/' + client.attributes.id , method:'put', data: data }).then(function(response){
      displayCheckDone('description', client._id);
    }, function(error){
      console.log(error);
    });
  }
  
  angular.element($window).bind('resize', function(){
    $scope.tabScroll();
  });
});