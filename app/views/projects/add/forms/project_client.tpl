<script type="text/ng-template" id="project_client.tpl">
  <div ng-if="loader" class="d-flex justify-content-center pt-4">
    <md-progress-circular md-mode="indeterminate"></md-progress-circular>
  </div>
  <div ng-if="!loader">
    <form>
      <div class="tabs">
        <div class="tabs-wrapper">
          <div class="single-tab" ng-class="{'active': activeClient_id == client._id}" ng-click="setActiveClient(client._id)" ng-repeat="client in model.clients">
            <span ng-if="!client.attributes.name">Tellija andmed</span>
            <span ng-if="client.attributes.name">{{client.allCaps('name') | truncate:15}}</span>
          </div>
          <div class="single-tab add-tab" ng-click="setActiveClient(addClient()._id)">+ Lisa veel tellijaid</div>
        </div>
        <div class="tab-content margin-sides" ng-if="activeClient_id == client._id" ng-repeat="client in model.clients">
          <div class="d-flex flex-wrap align-items-end">
            <div class="form-group col-12 form-half-width">
              <label><strong>Ärinimi <span class="red">*</span></strong>
                <checkdone ng-if="updatedFields['name'][client._id] == true" timeout="3000"></checkdone>
              
              </label>
              <md-autocomplete
                md-no-cache="true"
                md-clear-button="false"
                md-delay=300
                md-match-case-insensitive=true
                ng-disabled="client.disableNameInput"
                
                md-selected-item="selectedClient"
                md-selected-item-change="bindSelectedItem(selectedClient, client)"
                
                md-search-text="client.attributes.name"
                
                md-items="item in autocomplete(client.attributes.name, 'clients', model.clients)"
                md-item-text="item.name">
                
                <md-item-template>
                  <span>{{item.name}}</span>
                </md-item-template>
              </md-autocomplete>
            </div>
            <div class="form-group col-12 form-half-width">
              <label><strong>Aadress</strong>
                <checkdone ng-if="updatedFields['address'][client._id] == true" timeout="3000"></checkdone>
              </label>
              <input type="text" class="form-control" ng-model="client.attributes.address" ng-blur="client.save('address')" />
            </div>
            <button class="btn btn-primary mb-3 ml-3" type="button" ng-click="client.detach()">Eemalda tellija</button>
          </div>
          
          <div class="form-group col-12">
            <label><strong>Märkused</strong> <span class="optional-field">(valikuline)</span></label>
            <checkdone ng-if="updatedFields['description'][client._id] == true" timeout="3000"></checkdone>
            <textarea class="form-control" rows="4" ng-model="client.attributes.description" ng-blur="updateProjectData(client, { description: client.attributes.description })" maxlength="500"></textarea>
            <span class="word-count">{{ client.attributes.description.length ? client.attributes.description.length : 0 }} / 500</span>
          </div>
          <label class="mb-2 pp col-12"><strong>Kliendisektor <span class="red">*</span></strong>
            <checkdone ng-if="updatedFields['sector'][client._id] == true" timeout="3000"></checkdone>
          </label>
          <div class="form-check col-12">
            <label class="custom-control custom-radio">
              <input type="radio" class="custom-control-input" ng-model="client.attributes.sector" value="public" ng-change="client.save('sector')" />
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">Avalik sektor</span>
            </label>
          </div>
          <div class="form-check col-12">
            <label class="custom-control custom-radio">
              <input type="radio" class="custom-control-input" ng-model="client.attributes.sector" value="private" ng-change="client.save('sector')" />
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">Erasektor</span>
            </label>
          </div>
          <div class="form-group col-12">
            <label><strong>Valdkond <span class="red">*</span></strong>
              <checkdone ng-if="updatedFields['field'][client._id] == true" timeout="3000"></checkdone>
            </label>
            <md-chips ng-model="client.attributes.field"  ng-class="{'no-placeholder': client.attributes.field.length}"
                md-autocomplete-snap
                md-separator-keys="[188,13]"
                md-require-match=true
                md-on-add="client.save('field')"
                md-on-remove="client.save('field')">
              <md-autocomplete
                  md-search-text="searchField"
                  md-items="item in tags(searchField, 'field')"
                  md-item-text="item.value"
                  placeholder="{{client.attributes.field.length < 1 ? 'Finants, IT-arendus, telekommunikatsioon vms': ''}}">
                <span md-highlight-text="searchField">{{item.value}}</span>
                <md-not-found>
                  <a md-truncate flex href="/seaded/sildid?redirect={{redirect_path}}#valdkonnad">Silti ei leitud. Kliki, et luua uus silt.</a>
                </md-not-found>
              </md-autocomplete>
                       
              <md-chip-template>
                <span>
                  <strong>{{$chip.value}}</strong>
                </span>
              </md-chip-template>
            </md-chips>
           
          </div>
          <div class="form-group col-12">
            <label>
              <strong>Tellija kinnituskirjad</strong>
            </label>
            <p class="font-style-italic" ng-if="!client.attributes.files.length">Puuduvad</p>
            <p ng-repeat="file in client.attributes.files">
              <span>
                <a class="fa fa-download" href="{{settings_prefix}}projects/{{model.id}}/files/{{file.id}}" download></a>
                {{file.originalname}}
              </span>
            <a href="javascript:void(0);" class="ml-2" ng-click="client.deleteFile(file)">Eemalda</a>
            
            </p>
            <div class="file-upload" ngf-drag-over-class ngf-drop="client.submitFiles($files)" ngf-multiple="true" ng-class="{'upload-disabled': !client.attributes.id}">
              <h5 class="mb-0">
                <span ng-if="client.attributes.id">
                  Üleslaadimiseks lohista failid siia või
                  <a href="javascript:void(0);" ngf-select="client.submitFiles($files)" multiple ngf-max-size="5MB">vali kettalt</a>
                </span>
                <span ng-if="!client.attributes.id">Üleslaadimiseks lisa esmalt tellija</span>
              </h5>
            </div>
          </div>
          <div class="col-12">
            <h5><strong>Kontaktisikud</strong></h5>
          </div>
          <md-list class="pb-0">
            <md-list-item class="list-item-content list-item-contact flex-wrap" ng-repeat="delegate in client.delegates">
              <div class="d-flex w-100 align-items-end flex-wrap">
                <div class="col-lg-3 col-12">
                  <label><b>Nimi <span class="red">*</span></b></label>
                  
                  <md-autocomplete
                    md-no-cache="true"
                    md-clear-button="false"
                    md-delay=300
                    md-match-case-insensitive=true
                    ng-disabled="delegate.disableNameInput"
                    
                    md-selected-item="selectedDelegate"
                    md-selected-item-change="bindSelectedItem(selectedDelegate, delegate)"
                    
                    md-search-text="delegate.attributes.name"
                    
                    md-items="item in autocomplete(delegate.attributes.name, 'delegates', client.delegates)"
                    md-item-text="item.name">
                    
                    <md-item-template>
                      <span>{{item.name}}</span>
                    </md-item-template>
                  </md-autocomplete>
                </div>
                <div class="col-lg-3 col-12 mt-3 mt-lg-0">
                  <label><b>Telefoninumber <span class="red">*</span></b></label>
                  <checkdone ng-if="updatedFields['phone'][delegate._id] == true" timeout="3000"></checkdone>
                  <div class="d-flex">
                    <input type="text" class="form-control number-prefix" ng-model="delegate.phone_prefix" ng-blur="delegate.save('phone')"/>
                    <input type="text" class="form-control number-perifix" ng-model="delegate.phone_number" ng-blur="delegate.save('phone')"/>
                  </div>
                </div>
                <div class="col-lg-3 col-12 mt-3 mt-lg-0">
                  <label><b>E-post <span class="red">*</span></b></label>
                  <checkdone ng-if="updatedFields['email'][delegate._id] == true" timeout="3000"></checkdone>
                  <input type="text" class="form-control list-input" ng-model="delegate.attributes.email" ng-blur="delegate.save('email')" ng-pattern="EMAIL_PATTERN" />
                </div>
                <div class="col-lg-2 col-12 mt-3 mt-lg-0">
                  <label><b>Suhtluskeel <span class="red">*</span></b></label>
                  <checkdone ng-if="updatedFields['language'][delegate._id] == true" timeout="3000"></checkdone>
                  <select class="form-control" ng-model="delegate.attributes.language" ng-change="delegate.save('language')" >
                    <option hidden value="">Vali keel</option>
                    <option value="et">Eesti keel</option>
                    <option value="en">Inglise keel</option>
                    <option value="ru">Vene keel</option>
                  </select>
                </div>
                <div class="col-lg-1 col-12 d-flex justify-content-end mt-3 mt-lg-0" ng-if="client.delegates.length > 1">
                  <button class="btn btn-primary btn-icon" type="button" ng-click="delegate.detach()"><i class="material-icons">close</i></button>
                </div>
              </div>
            </md-list-item>
          </md-list>
          <a class="add-list-item add-contact-item mb-3" href="javascript:void(0);" ng-click="addDelegate(client)">
            <b><i class="material-icons">add</i> Lisa kontaktisik</b>
          </a>
        </div>
      </div>
      <div class="d-flex justify-content-between mt-4 px-3 flex-wrap flex-column flex-lg-row">
        <div class="d-flex flex-wrap align-items-md-center align-items-start flex-column flex-md-row">
          <a class="btn btn-primary btn-preview mr-2 mb-2 mb-md-0" ui-sref="projects-view.view({id: model.id })">
            <md-tooltip md-direction="top">Eelvaade</md-tooltip>
            <i class="material-icons">remove_red_eye</i>
          </a>
          <a ui-sref="dashboard" class="btn btn-light mb-2 mb-md-0">Katkesta ja jätka hiljem</a>
          <a href="javascript:void(0);" ng-if="model.data_complete === false && isDataComplete" ng-click="confirmDataComplete()" class="btn btn-primary ml-md-2" ng-class="isDataComplete ? '': 'btn-disabled'">Kinnita projekti andmed</a>
        </div>
        <div class="d-flex mt-2 mt-lg-0 flex-wrap flex-column flex-md-row align-items-start">
          <a class="btn btn-light" ui-sref="projects.general({id: model.id})">Tagasi</a>
          <a class="btn btn-primary ml-0 ml-md-2 mt-2 mt-md-0" ui-sref="projects.contractor({id: model.id})">Edasi</a>
        </div>
      </div>
    </form>
  </div>
</script>