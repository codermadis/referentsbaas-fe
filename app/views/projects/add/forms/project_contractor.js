App.controller("project_contractor", function($scope, $http, $rootScope, $stateParams, $filter, Upload, $timeout, $converter){
  /* CONTRACTOR */
  $scope.model.id = $stateParams.id || null;
  $scope.settings_prefix = settings.prefix; //for file download link
  var hoursAndMinutes = ['real_capacity'];
  
  $scope.fetchData = function(){
    $scope.loader = true;
    $http.get(settings.prefix + 'projects/' + $scope.model.id ).then(function(response){
      $scope.checkDataVerification();
      $scope.loader = false;
      $scope.model = response.data.data;
      $scope.$parent.model = response.data.data;
      
      if($scope.model.outsource){
        if($scope.model.outsource['real_capacity'])  $scope.model.outsource['real_capacity'] = $converter.minutesTohours($scope.model.outsource['real_capacity']);
      
        if($scope.model.outsource.start_date === null || $scope.model.outsource.start_date === undefined) {
          $scope.model.outsource.start_date = null;
        }
        if($scope.model.outsource.finish_date === null || $scope.model.outsource.finish_date === undefined) {
          $scope.model.outsource.finish_date = null;
        }
        //name input fields will be disabled, when contractor and/or delegate is already linked
        if($scope.model.contractor.id){
          $scope.disableContractorNameInput = true;
          $scope.disableDelegateNameInput = false;
        } else {
          $scope.disableContractorNameInput = false;
          $scope.disableDelegateNameInput = true;
        }
      
        if($scope.model.contractor.delegates){
          if(typeof($scope.model.contractor.delegates[0]) == 'object'){
            $scope.disableDelegateNameInput = $scope.model.contractor.delegates[0].id ? true : false;
            
            var splitPhoneNumber = $converter.phoneNumberSplit($scope.model.contractor.delegates[0].phone);
            $scope.model.contractor.delegates[0].phone  = splitPhoneNumber.number;
            $scope.model.contractor.delegates[0].phone_prefix =  splitPhoneNumber.prefix;
            
          }
        }
      }
      
    }, function(error){
     console.error(error);
    });
  };
  $scope.fetchData();
  
  $scope.bindDelegate = function(delegate){
    if(!$scope.model.contractor.id) return console.log('Missing contractor');
    if(delegate == null) return;
    
    for(var key in delegate){
      $scope.model.contractor.delegates[0][key] = delegate[key];
    }
    var splitPhoneNumber = $converter.phoneNumberSplit($scope.model.contractor.delegates[0].phone);
    $scope.model.contractor.delegates[0].phone  = splitPhoneNumber.number;
    $scope.model.contractor.delegates[0].phone_prefix =  splitPhoneNumber.prefix;
    
    attachDelegate($scope.model.contractor.delegates[0]);
  };
  
  $scope.saveDelegate = function (key){
    var delegate = $scope.model.contractor.delegates[0];
    
    if(!delegate.name) return console.log('Delegate name missing');
    
    var payload = angular.copy(delegate);
    payload.phone = payload.phone_prefix + payload.phone;
    delete payload.phone_prefix;
    
    var requestParameters;
    if(delegate.hasOwnProperty('id')) requestParameters = {url: settings.prefix + 'delegates/' + delegate.id, method: "PUT", data : payload}
    else requestParameters = {url: settings.prefix + 'delegates/', method: "POST", data: payload};
    
    $http(requestParameters)
    .then(function(response){
      $scope.model.contractor.delegates[0].id = response.data.data.id;
     
      var splitPhoneNumber = $converter.phoneNumberSplit(response.data.data.phone);
      $scope.model.contractor.delegates[0].phone  = splitPhoneNumber.number;
      $scope.model.contractor.delegates[0].phone_prefix =  splitPhoneNumber.prefix;
      
      if(requestParameters.method == 'POST') attachDelegate(delegate);
      displayCheckDone(key);
    }, function(err){ console.error(err.data)});
  };
  
  function attachDelegate(delegate){
    if(!$scope.model.contractor.id) return;
    if(!delegate.id) return;
    
    $http({url: settings.prefix + 'projects/' + $scope.model.id + '/contractors/' +  $scope.model.contractor.id + '/delegates/' + delegate.id, method: "PUT"})
        .then(function(response){
          $scope.disableDelegateNameInput = true;
        }, function(error){ 
          console.error(error);
    });
  }
  
  $scope.detachDelegate = function(delegate){
    if(!$scope.model.contractor.id) return;
    if(!delegate.id) return;
    $http({url: settings.prefix + 'projects/' + $scope.model.id + '/contractors/' +  $scope.model.contractor.id + '/delegates/' + delegate.id, method: "DELETE"})
        .then(function(response){
          $scope.model.contractor.delegates = [];
          $scope.disableDelegateNameInput = false;
        }, function(error){ 
          console.error(error);
    });
  };
  
  $scope.bindContractor = function(contractor){
    
    if(!contractor){
       $scope.model.contractor = {files:[]};
    } else {
      $scope.model.contractor = contractor;
      $scope.model.contractor.files = [];
    }
    
    if($scope.model.contractor.id != undefined) attachContractor(contractor);
    
  };
  
  $scope.saveContractor = function(key){
    var contractor = $scope.model.contractor;
    
    if(!contractor.name) return;

    var requestParameters;
    
    if(contractor.hasOwnProperty('id')) requestParameters = {url: settings.prefix + 'clients/' + contractor.id, method: "PUT", data : contractor}
    else requestParameters = {url: settings.prefix + 'clients/', method: "POST", data: contractor};
    
    $http(requestParameters)
    .then(function(res){
      contractor.id = res.data.data.id;
      if(requestParameters.method == 'POST') attachContractor(contractor);
      displayCheckDone(key);
    }, function(err){ console.error(err.data)});
  };
  
  function attachContractor(contractor) {
    if(!contractor.id) return;
    console.log(contractor);
    $http({url: settings.prefix + 'projects/' + $scope.model.id + '/contractors/' +  contractor.id, method: "PUT"})
      .then(function(res){ 
        $scope.disableContractorNameInput = true;  
        $scope.disableDelegateNameInput = false;
      }, function(err){ console.error(err.data)});
  }
  $scope.detachContractor = function(contractor) {
    if(!contractor.id) return;
    $http({url: settings.prefix + 'projects/' + $scope.model.id + '/contractors/' +  contractor.id, method: "DELETE"})
      .then(function(res){ 
        $scope.model.contractor = {files: []};
        $scope.disableContractorNameInput = false;
        $scope.disableDelegateNameInput = false;
      }, function(err){ console.error(err.data)});
  }
  $scope.saveChange = function(key){
    var data = {};
    data[key] = $scope.model[key];
    
    $http({url: settings.prefix + 'projects/'+ $scope.model.id , method:'put', data: data })
      .then(function (response){
        $scope.model.contractor = {};
        $scope.disableContractorNameInput = false;
        $scope.disableDelegateNameInput = true;
        displayCheckDone(key);
    }, function(err){ console.log(err); $scope.messages.errorMessage = err.data });
  };
  
  $scope.saveOutsource = function(key){
    
    var data = {};
    if(key.includes('date')) data[key] = $filter('date')($scope.model.outsource[key], 'yyyy-MM-dd');
    else if (hoursAndMinutes.includes(key) ) {
      data[key] = $converter.hoursTominutes($scope.model.outsource[key]);
      $scope.model.outsource[key] = $converter.minutesTohours(data[key]);
    }
    else data[key] = $scope.model.outsource[key];
    
    $http({url: settings.prefix + 'projects/'+ $scope.model.id + '/outsource', method:'put', data: data }).then(function (response){
      displayCheckDone(key);
    }, function(err){ console.log(err); $scope.messages.errorMessage = err.data });
  };
  $scope.submitFiles = function(files){
    
    for(var i in files){
      files[i].upload = Upload.upload({
        url: settings.prefix + 'projects/' + $scope.model.id + '/contractors/'+ $scope.model.contractor.id +'/files',
        data: {file: files[i]}
      });
      
      files[i].upload.then(function (response) {
        
        $scope.model.contractor.files.push(response.data.data);
      }, function (response) {
        console.error(response.data);
        $scope.messages.errorMessage = response.data.code;    
      });
    }
  };
  $scope.deleteFile = function(file){
   
    $http({url:settings.prefix+ 'projects/' +  $scope.model.id + '/files/' + file.id, method: 'DELETE'}).then(function(response){
        $scope.model.contractor.files.splice($scope.model.contractor.files.indexOf(file), 1);
    }, function(error){
      console.error(error);
    });
  };
  $scope.downloadFile = function(file){
     window.open(settings.prefix + 'projects/' +  $scope.model.id  + '/files/' + file.id, '_self');
  };
  
  $scope.updateProjectData =  function (client_id, data){

    $http({url: settings.prefix + 'projects/'+ $scope.model.id + '/contractors/' + client_id , method:'put', data: data })
    .then(function(response){
      displayCheckDone('description');
    }, function(error){
      console.log(error);
    });
  };
  
  function displayCheckDone(key){
    $scope.checkDataVerification();
    
    if(!$scope.updatedFields) $scope.updatedFields = {};
    
    if(!$scope.updatedFields[key]) $scope.updatedFields[key] = {};
    
   
    if($scope.updatedFields[key]) delete $scope.updatedFields[key];
    
    $timeout(function(){
      if(!$scope.updatedFields[key]) $scope.updatedFields[key] = true;
    },0);
  }
})