<script  type="text/ng-template" id="project_contractor.tpl">
  <div ng-if="loader" class="d-flex justify-content-center pt-4">
    <md-progress-circular md-mode="indeterminate"></md-progress-circular>
  </div>
  <div ng-if="!loader">
    <form id="contractorForm" name="contractorForm">
      
      <label class="mb-2 pp col-12"><strong>Kas on allhanke projekt? <span class="red">*</span></strong>
        <checkdone timeout="3000" ng-if="updatedFields['outsourced']"></checkdone>
      </label>
      <div class="form-check col-12">
        <label class="custom-control custom-radio">
          <input type="radio" class="custom-control-input" ng-model="model.outsourced" ng-value="true" ng-change="saveChange('outsourced')">
          <span class="custom-control-indicator"></span>
          <span class="custom-control-description">Jah</span>
        </label>
      </div>
      <div class="form-check col-12">
        <label class="custom-control custom-radio">
          <input type="radio" class="custom-control-input" ng-model="model.outsourced" ng-value="false" ng-change="saveChange('outsourced')">
          <span class="custom-control-indicator"></span>
          <span class="custom-control-description">Ei</span>
        </label>
      </div>
      
      <div ng-if="model.outsourced">
        <div class="d-flex flex-wrap align-items-end">
          <div class="form-group col-12 form-half-width">
            <label><strong>Peatöövõtja ärinimi <span class="red">*</span></strong></label>
            <md-autocomplete
              md-no-cache="true"
              md-delay=300
              md-clear-button="false"
              ng-disabled="disableContractorNameInput"
              
              md-selected-item="contractor"
              md-selected-item-change="bindContractor(contractor)";
              md-search-text="model.contractor.name"
              md-items="item in autocomplete(model.contractor.name, 'clients')"
              md-item-text="item.name">
              <md-item-template>
                <span md-highlight-text="model.contractor.name">{{item.name}}</span>
              </md-item-template>
            </md-autocomplete>
          </div>
          <div class="form-group col-12 form-half-width">
            <label for="cont2"><strong>Aadress</strong>
              <checkdone timeout="3000" ng-if="updatedFields['address']"></checkdone>
            </label>
            <input id="cont2" type="text" class="form-control" ng-model="model.contractor.address" ng-blur="saveContractor('address')">
          </div>
          <button class="btn btn-primary mb-3 ml-3" type="button" ng-if="model.contractor.id" ng-click="detachContractor(model.contractor)">Eemalda peatöövõtja</button>
        </div>
        
        <div class="form-group col-lg-12 col-md-12 col-sm-12 myfade">
          <label for="outsourceComments"><strong>Märkused</strong> <span class="lighter">(valikuline)</span>
            <checkdone timeout="3000" ng-if="updatedFields['description']"></checkdone>
          </label>
          <textarea class="form-control" id="outsourceComments" rows="3" maxlength="500" ng-model="model.contractor.description" ng-blur="updateProjectData(model.contractor.id, { description: model.contractor.description })"></textarea>
          <span class="word-count">{{ model.contractor.description.length ? model.contractor.description.length : 0 }} / 500</span>
        </div>
        
        <div class="d-flex myfade flex-wrap">
          <div class="form-group col-md-6 col-12">
            <label class="d-block"><strong>Projekti algus <span class="red">*</span></strong>
              <checkdone timeout="3000" ng-if="updatedFields['start_date']"></checkdone>
            </label>
            <md-datepicker ng-model="model.outsource.start_date" ng-change="saveOutsource('start_date')" md-placeholder="Vali kuupäev"></md-datepicker>
          </div>
          <div class="form-group col-md-6 col-12">
            <label class="d-block"><strong>Projekti lõpp <span class="red">*</span></strong>
              <checkdone timeout="3000" ng-if="updatedFields['finish_date']"></checkdone>
            </label>
            <md-datepicker ng-model="model.outsource.finish_date" ng-change="saveOutsource('finish_date')" md-placeholder="Vali kuupäev"></md-datepicker>
          </div>
        </div><!--/d-flex-->
        
        <div class="d-flex myfade flex-wrap">
          <div class="form-group col-md-6 col-12">
            <label for="outsourceWholeTime"><strong>Kogu projekti töömaht <span class="red">*</span></strong>
              <checkdone timeout="3000" ng-if="updatedFields['real_capacity']"></checkdone>
            </label>
            <input type="text" id="outsourceWholeTime" class="form-control" ng-model="model.outsource.real_capacity" ng-blur="saveOutsource('real_capacity')" min="0">
          </div>
          <div class="form-group col-md-6 col-12">
            <label for="outsourceWholePrice"><strong>Kogu projekti maksumus eurodes <span class="red">*</span></strong>
              <checkdone timeout="3000" ng-if="updatedFields['cost']"></checkdone>
            </label>
            <input class="form-control" type="number" id="outsourceWholePrice" ng-model="model.outsource.cost" ng-blur="saveOutsource('cost')" min="0">
          </div>
        </div><!--/d-flex-->
        
        <div class="form-group col-12">
          <label>
            <strong>Peatöövõtja kinnituskirjad</strong>
          </label>
          
          <p class="font-style-italic" ng-if="!model.contractor.files.length">
            Puuduvad
          </p>
          <p ng-repeat="file in model.contractor.files">
            <span>
              <a class="fa fa-download" href="{{settings_prefix}}projects/{{model.id}}/files/{{file.id}}" download></a>
              {{file.originalname}}
            </span>
          <a href="javascript:void(0);" class="ml-2" ng-click="deleteFile(file)">Eemalda</a>
          
          </p>
          <div class="file-upload" ngf-drag-over-class ngf-drop="submitFiles($files)" ngf-multiple="true" ng-class="{'upload-disabled': !model.contractor.id }">
            <h5 class="mb-0">
              <span ng-if="model.contractor.id">
                Üleslaadimiseks lohista failid siia või 
                <a href="javascript:void(0);" ngf-select="submitFiles($files)" multiple ngf-max-size="5MB">
                  vali kettalt
                </a>
              </span>
              <span ng-if="!model.contractor.id">Üleslaadimiseks lisa esmalt peatöövõtja</span>
            </h5>
          </div>
        </div>
        <hr>
        <div class="d-flex flex-wrap align-items-end">
          <div class="form-group col-12 form-half-width">
            <label for="contpoc1"><strong>Kontaktisiku nimi <span class="red">*</span></strong></label>
            <md-autocomplete
              md-no-cache="true"
              md-clear-button="false"
              md-delay=300
              md-match-case-insensitive=true
              ng-disabled="disableDelegateNameInput"
              
              md-selected-item="selectedDelegate"
              md-selected-item-change="bindDelegate(selectedDelegate)"
              
              md-search-text="model.contractor.delegates[0].name"
              
              md-items="item in autocomplete(model.contractor.delegates[0].name, 'delegates')"
              md-item-text="item.name">
              
              <md-item-template>
                <span>{{item.name}}</span>
              </md-item-template>
            </md-autocomplete>
          </div>
          <div class="form-group col-12 form-half-width">
            <label for="contpoc3"><strong>E-mail <span class="red">*</span></strong>
              <checkdone timeout="3000" ng-if="updatedFields['email']"></checkdone>
            </label>
            <input id="contpoc3" type="text" class="form-control" ng-model="model.contractor.delegates[0].email" ng-blur="saveDelegate('email')" ng-pattern="EMAIL_PATTERN">
          </div>
        </div>
        <div class="d-flex flex-wrap align-items-end">
          <div class="form-group col-12 form-half-width">
            <label><strong>Telefon <span class="red">*</span></strong>
              <checkdone timeout="3000" ng-if="updatedFields['phone']"></checkdone>
            </label>
            <div class="d-flex">
              <input class="form-control number-prefix" type="text" ng-model="model.contractor.delegates[0].phone_prefix" ng-blur="saveDelegate('phone')" />
              <input type="text" class="form-control number-perifix" ng-model="model.contractor.delegates[0].phone" ng-blur="saveDelegate('phone')">
            </div>
          </div>
          <div class="form-group col-12 form-half-width">
            <label for="outsourceLanguage"><strong>Suhtluskeel <span class="red">*</span></strong>
              <checkdone timeout="3000" ng-if="updatedFields['language']"></checkdone>
            </label>
            <select class="form-control" id="outsourceLanguage" ng-model="model.contractor.delegates[0].language" ng-change="saveDelegate('language')">
            <option hidden value="">Vali keel</option>
            <option  value="et">eesti keel</option>
            <option value="en">inglise keel</option>
            <option value="ru">vene keel</option>
            </select>
          </div>
          <button class="btn btn-primary mb-3 ml-3" type="button" ng-if="model.contractor.delegates[0].id" ng-click="detachDelegate(model.contractor.delegates[0])">Eemalda kontaktisik</button>
        </div>
        <hr>
      </div>
      <div class="d-flex justify-content-between mt-4 px-3 flex-wrap flex-column flex-lg-row">
        <div class="d-flex flex-wrap align-items-md-center align-items-start flex-column flex-md-row">
          <a class="btn btn-primary btn-preview mr-2 mb-2 mb-md-0" ui-sref="projects-view.view({id: model.id })">
            <md-tooltip md-direction="top">Eelvaade</md-tooltip>
            <i class="material-icons">remove_red_eye</i>
          </a>
          <a ui-sref="dashboard" class="btn btn-light mb-2 mb-md-0">Katkesta ja jätka hiljem</a>
          <a href="javascript:void(0);" ng-if="model.data_complete === false && isDataComplete" ng-click="confirmDataComplete()" class="btn btn-primary ml-md-2" ng-class="isDataComplete ? '': 'btn-disabled'">Kinnita projekti andmed</a>
        </div>
        <div class="d-flex mt-2 mt-lg-0 flex-wrap flex-column flex-md-row align-items-start">
          <a class="btn btn-light" ui-sref="projects.client({id: model.id})">Tagasi</a>
          <a class="btn btn-primary ml-0 ml-md-2 mt-2 mt-md-0" ui-sref="projects.participation({id: model.id})">Edasi</a>
        </div>
      </div>
    </form>
  </div>
</script>