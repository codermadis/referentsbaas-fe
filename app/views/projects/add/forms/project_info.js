App.controller("project_info", function($scope, $http, $rootScope, $state, $stateParams, $filter, Upload, $timeout, $converter, $location){
  
  initialize();
  var hoursAndMinutes = ['real_capacity', 'planned_capacity'];
  const TYPE_INTEGER = ['cost', 'billing_rate'];
  $scope.redirect_path = $location.path();
  
  function getOrCreateProject(id, data) {
    if(id) return $http.get(settings.prefix + 'projects/' + id );
    else if (data) return $http({url: settings.prefix + 'projects/', method:'post', data: data});
  }
  
  function updateProject (id, data){
    return $http({url: settings.prefix + 'projects/'+ id , method:'put', data: data });
  }
  function initialize (){
    $scope.loader = true;
    var _id = $stateParams.id || undefined;
    if(!isNaN(_id)) {
      getOrCreateProject(_id).then(function (response){
        preCompile(response.data.data);
        if($scope.model.start_date === null || $scope.model.start_date === undefined) {
          $scope.model.start_date = null;
        }
        if($scope.model.finish_date === null || $scope.model.finish_date === undefined) {
          $scope.model.finish_date = null;
        }
      }, function(err){ console.log(err); $scope.messages.errorMessage = err });
    } else {
      preCompile({});
    }
  }

  function preCompile (data){
      $scope.$parent.model = data; //response.data.data;
      
      $scope.model = data;
      if(!data.name) return $scope.loader = false;
     
      $scope.model.procurement_code ? $scope.isProcurement = true : $scope.isProcurement = false;
       
      
      for(var key in $scope.model){
        if(hoursAndMinutes.includes(key)) $scope.model[key] = $converter.minutesTohours($scope.model[key]);
      }
      
      if(!$scope.model.contractor.name) $scope.model.contractor = {name: "", address: ""};
      //console.log($scope.model);
      if(data.outsource) $scope.outsource = data.outsource;
     
      //console.log($scope.model);
      $scope.loader = false;
    }
    
  
 
  $scope.saveChange = function(key){
    //$filter('date')(date, 'yyyy-MM-dd')
    //var parsedUnixTime = new Date('Mon, 25 Dec 1995 13:30:00 GMT').getUnixTime();
    var data = {};
    if (key == 'procurement_code') {
      if ($scope.model[key] === undefined) {
        var cantoast = true;
        var message = "Hanke viitenumber peab koosnema vähemalt kuuest märgist!";
        if(cantoast) {
          cantoast = false;
          $rootScope.toastNotification({ type: "simple", message: message, class: 'toast-error', hideDelay: 6000, position: 'bottom right'});
          $timeout(function() {
            cantoast = true;
          }, 6000);
        }
      }
    }
    $scope.savedFields = {};
    if(key.includes('date')) data[key] = $filter('date')($scope.model[key], 'yyyy-MM-dd');
    else if (key == 'name') {
      if($scope.model[key].length == 0){
        $scope.model[key] =  $scope.model.freckle_name ?  angular.copy($scope.model.freckle_name) : 'Nimetuseta projekt' ;
       
      }
      $scope.$parent.model.name = $scope.model[key];
      data[key] = $scope.model[key];
    }
    else if (hoursAndMinutes.includes(key)) {
      data[key] = $converter.hoursTominutes($scope.model[key]);
      $scope.model[key] = $converter.minutesTohours(data[key]);
      if(key == 'planned_capacity' && $scope.model.billing_rate){
        $scope.model.cost = Math.floor(($scope.model.billing_rate/ 60) * $converter.hoursTominutes($scope.model.planned_capacity));
        data['cost'] = $scope.model.cost;
      }
    }
    else if (key == 'billing_rate') {
      data[key] = $scope.model[key];
      var capacity = false;
      if( $converter.hoursTominutes($scope.model.planned_capacity) ) capacity =  $converter.hoursTominutes($scope.model.planned_capacity);
      else if( $converter.hoursTominutes($scope.model.real_capacity) ) capacity = $converter.hoursTominutes($scope.model.real_capacity);
      if(capacity){
        $scope.model.cost  = Math.floor(($scope.model.billing_rate / 60) * capacity);
        data['cost'] = $scope.model.cost;
      }
      
    }
    else data[key] = $scope.model[key];
    
    for(var dkey in data){
      if(TYPE_INTEGER.includes(dkey)) data[dkey] = parseInt(data[dkey], 10);
    }
    
    if($scope.model.id === undefined && $scope.model.name != undefined){
      //create new project
       getOrCreateProject(null, {name: $scope.model.name}).then(function (response){
        $scope.savedFields = data;
        
        $state.go('.', {id: response.data.data.id}, {reload: false, notify: false});
        preCompile(response.data.data);
      }, function(err){ console.log(err) });
    } else if ($scope.model.id){
      //update existing project
      updateProject($scope.model.id, data).then(function (response){
        $scope.savedFields = data;
        $scope.checkDataVerification();
      }, function(err){ console.log(err); $scope.messages.errorMessage = err.data });
    }else return;
    
  };
  
  $scope.switchProcurement = function (value){
   if(value === false && $scope.model.procurement_code != null){
     console.log('$scope.model.procurement_code:' + $scope.model.procurement_code);
      $scope.model.procurement_code = null;
      $scope.saveChange('procurement_code');
   }
   else $scope.savedFields = {};
  };
  
  $scope.debounceCollection = {};
  
  $scope.debounceBeforeSaving = function(key, debounceTime) {
    
    if($scope.debounceCollection[key]) {
      
      clearTimeout($scope.debounceCollection[key]);
    }
    
    $scope.debounceCollection[key] = setTimeout(function(){
      
      $scope.saveChange(key);
      
    }, debounceTime);
    
  };
  
})