<script  type="text/ng-template" id="project_info.tpl">
  <div ng-if="loader" class="d-flex justify-content-center pt-4">
    <md-progress-circular md-mode="indeterminate"></md-progress-circular>
  </div>
  <div ng-if="!loader">
    <form ng-submit="save()">
      <div class="d-flex flex-wrap">
        <div class="form-group col-12 form-half-width">
          <label><strong>Projekti nimetus <span class="red">*</span></strong>
          <checkdone timeout="3000" ng-if="savedFields.name !== undefined" ></checkdone>
          </label>
          <input type="text" ng-blur="saveChange('name')" ng-model="model.name" class="form-control" maxlength="255" />
        </div>
        <div class="form-group col-12 form-half-width form-freckle-name" ng-if="model.name">
          <label><strong>Freckle nimetus</strong></label>
          <p ng-if="model.freckle_name">{{model.freckle_name}}</p>
          <p ng-if="!model.freckle_name"><i>puudub</i></p>
        </div>
      </div>
      <div ng-class="{'blur-layer': !model.name}">
        <div class="form-check col-6" >
          <p class="mb-2">
            <strong>Projekti staatus <span class="red">*</span></strong>
            <checkdone timeout="3000" ng-if="savedFields.finished !== undefined" ></checkdone>
          </p>
          <label class="custom-control custom-radio">
            <input id="projectCountry" ng-change="saveChange('finished')" ng-model="model.finished" ng-value="true" type="radio" class="custom-control-input">
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">Lõpetatud</span>
          </label>
          <label class="custom-control custom-radio">
            <input id="projectCountry" ng-change="saveChange('finished')" ng-model="model.finished" ng-value="false"  type="radio" class="custom-control-input">
            <span class="custom-control-indicator"></span>
            <span class="custom-control-description">Töös</span>
          </label>
        </div>
        
        <div class="form-group col-12">
          <label>
            <strong>Süsteemi liigid</strong>
            
            <checkdone timeout="3000" ng-if="savedFields.system_types !== undefined"></checkdone>
          </label>
          <md-chips ng-model="model.system_types" ng-class="{'no-placeholder': model.system_types.length}"
            md-separator-keys="[188,13]"
            md-transform-chip="transformTag($chip)"
            md-on-add="saveChange('system_types')"
            md-require-match="true"
            md-on-remove="saveChange('system_types')">
            <md-autocomplete
                   md-search-text="searchSystemType"
                   md-items="item in tags(searchSystemType, 'system_type')"
                   md-item-text="item.value"
                   md-min-length="0"
                   placeholder="{{showPlaceholder('E-pood, veebileht, infosüsteem vms', model.system_types.length)}}">
                   <span md-highlight-text="searchSystemType">{{item.value}}</span>
                <md-not-found>
                  <a md-truncate flex href="/seaded/sildid?redirect={{redirect_path}}#systeemi_liigid">Silti ei leitud. Kliki, et luua uus silt.</a>
                </md-not-found>
            </md-autocomplete>
             <md-chip-template>
                <span>
                   <strong>{{$chip.value}}</strong>
                </span>
             </md-chip-template>
          </md-chips>
        </div>
        
        <div class="d-flex flex-wrap">
          <div class="form-group col-md-6 col-sm-12">
            <p class="mb-2 pp">
              <strong>Kas on riigihange? <span class="red">*</span></strong>
              <checkdone timeout="3000" ng-if="savedFields.procurement_code !== undefined && savedFields.procurement_code === null"></checkdone>
            </p>
            <div class="form-check">
              <label class="custom-control custom-radio">
              <input ng-model="isProcurement" ng-value="true" ng-change="switchProcurement(isProcurement)" name="procurement" type="radio" class="custom-control-input">
              <span class="custom-control-indicator"></span>
              <span class="custom-control-description">Jah</span>
            </label>
            </div>
            <div class="form-check">
              <label class="custom-control custom-radio">
                <input  ng-model="isProcurement" ng-value="false" ng-change="switchProcurement(isProcurement)" name="procurement" type="radio" class="custom-control-input">
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Ei</span>
              </label>
            </div>
          </div>
          <div ng-if="isProcurement" class="form-group col-md-6 col-sm-12 fastfade">
            <label>
              <strong>Hanke viitenumber <span class="red">*</span></strong>
              <checkdone timeout="3000" ng-if="savedFields.procurement_code !== undefined && savedFields.procurement_code"></checkdone>
            </label>
            <input class="form-control" ng-change="debounceBeforeSaving('procurement_code', 1000)" type="text" 
              ng-model="model.procurement_code"  maxlength="25" ng-pattern="/^.{6,25}$/">
              <span class="word-count">{{ model.procurement_code.length ? model.procurement_code.length : 0 }} / 25</span>
          </div>
        </div><!--/d-flex-->
        
        <div class="form-group col-lg-12 col-md-12 col-sm-12">
          <label><strong>Projekti lühikirjeldus (eesti k.) <span class="red">*</span></strong>
            <checkdone timeout="3000" ng-if="savedFields.description_et !== undefined"></checkdone>
          </label>
          <textarea ng-model="model.description_et" ng-change="debounceBeforeSaving('description_et', 1000)" class="form-control" id="estDesc" rows="3" maxlength="300" ng-trim="false"></textarea>
          <span class="word-count">{{ model.description_et.length ? model.description_et.length : 0 }} / 300</span>
        </div>
        <div class="form-group col-lg-12 col-md-12 col-sm-12">
          <label for="engDesc"><strong>Projekti lühikirjeldus (ingl. k.) <span class="red">*</span></strong>
            <checkdone timeout="3000" ng-if="savedFields.description_en !== undefined"></checkdone>
          </label>
          <textarea ng-model="model.description_en" ng-change="debounceBeforeSaving('description_en', 1000)" class="form-control" ng-maxlength="300" maxlength="300" ng-trim="false" id="engDesc" rows="3"></textarea>
          <span class="word-count" >{{ model.description_en.length ? model.description_en.length : 0 }} / 300</span>
        </div>
        
        <div class="d-flex flex-wrap">
          <div class="form-group col-md-6 col-sm-12">
            <label for="beginningDate" class="mr-3 d-block"><strong>Tööde teostamise algus <span class="red">*</span></strong>
              <checkdone timeout="3000" ng-if="savedFields.start_date !== undefined"></checkdone>
            </label>
            <md-datepicker ng-model="model.start_date" ng-change="saveChange('start_date')" md-placeholder="Vali kuupäev"></md-datepicker>
          </div>
          <div class="form-group col-md-6 col-sm-12">
            <label for="endDate" class="mr-3 d-block"><strong>Tööde teostamise lõpp</strong>
              <checkdone timeout="3000" ng-if="savedFields.finish_date !== undefined"></checkdone>
            </label>
            <md-datepicker ng-model="model.finish_date" ng-change="saveChange('finish_date')" md-placeholder="Vali kuupäev"></md-datepicker>
          </div>
        </div><!--/d-flex-->
        
        <div class="form-group col-lg-12 col-md-12 col-sm-12" ng-if="model.project_tasks.length">
          <label><strong>Projektis teostatud tööd</strong>
            <checkdone timeout="3000" ng-if="savedFields.project_tasks !== undefined"></checkdone>
          </label>
          <md-chips ng-model="model.project_tasks" ng-class="{'no-placeholder': model.project_tasks.length}"
              md-on-add="saveChange('project_tasks')"
              md-on-remove="saveChange('project_tasks')"
              md-separator-keys="[188,13]"
              md-require-match="true"
              readonly="true"
              md-transform-chip="transformTag($chip)">
            <md-autocomplete
              md-search-text="searchTasks"
              md-items="item in tags(searchTasks, 'task')"
              md-item-text="item.value"
              placeholder="{{showPlaceholder('Ärianalüüs, programmeerimine, kasutajaliidese disain vms', model.project_tasks.length)}}">
            <span md-highlight-text="searchTasks">{{item.value}}</span>
              <md-not-found>
                <a md-truncate flex href="/seaded/sildid#teostatud_tood">Silti ei leitud. Kliki, et luua uus silt.</a>
              </md-not-found>
            </md-autocomplete>
            
            <md-chip-template>
              <span>
                <strong>{{$chip.value}}</strong>
              </span>
            </md-chip-template>
          </md-chips>
        </div>
        
        <div class="d-flex flex-wrap">
          <div class="form-group col-md-6 col-sm-12">
            <label for="plannedTime" style="white-space:nowrap;"><strong>Algne planeeritud töömaht <span class="red">*</span></strong>
              <checkdone timeout="3000" ng-if="savedFields.planned_capacity !== undefined"></checkdone>
            </label>
            <input class="form-control" type="text" ng-blur="saveChange('planned_capacity')"  ng-model="model.planned_capacity">
          </div>
          <div class="form-group col-md-6 col-sm-12">
            <label for="actualTime"><strong>Tegelik töömaht <span class="red">*</span></strong>
              <checkdone timeout="3000" ng-if="savedFields.real_capacity !== undefined"></checkdone>
            </label>
            <input class="form-control" type="text" ng-blur="saveChange('real_capacity')" ng-model="model.real_capacity">
          </div>
        </div><!--/d-flex-->
        
        <div class="d-flex flex-wrap">
          <div class="form-group col-md-6 col-sm-12">
            <label for="hourPrice"><strong>Tunnihind eurodes</strong>
              <checkdone timeout="3000" ng-if="savedFields.billing_rate !== undefined"></checkdone>
            </label>
            <input class="form-control" type="number" ng-blur="saveChange('billing_rate')" ng-model="model.billing_rate" min="0">
          </div>
          <div class="form-group col-md-6 col-sm-12">
            <label for="jobPrice"><strong>Tööde maksumus km-ta <span class="red">*</span></strong>
              <checkdone timeout="3000" ng-if="savedFields.cost !== undefined"></checkdone>
            </label>
            <input class="form-control" type="number" ng-blur="saveChange('cost')" ng-model="model.cost" min="0">
          </div>
        </div><!--/d-flex-->
        
        
        <div class="form-group col-lg-12 col-md-12 col-sm-12" ng-if="model.technologies.length">
          <label for="technology"><strong>Tehnoloogia</strong>
            <checkdone timeout="3000" ng-if="savedFields.technologies !== undefined"></checkdone>
          </label>
          <md-chips ng-model="model.technologies" ng-class="{'no-placeholder': model.technologies.length}"
              md-on-add="saveChange('technologies')"
              md-on-remove="saveChange('technologies')",
              readonly="true"
              md-separator-keys="[188,13]"
              md-require-match="true"
              md-transform-chip="transformTag($chip)">
            <md-autocomplete
              md-search-text="searchTechnologies"
              md-items="item in tags(searchTechnologies, 'technology')"
              md-item-text="item.value"
              placeholder="{{showPlaceholder('Node.js, AngularJS, CSS3, HTML5 vms', model.technologies.length)}}">
            <span md-highlight-text="searchTechnologies">{{item.value}}</span>
            <md-not-found>
              <a md-truncate flex href="/seaded/sildid#tehnoloogiad">Silti ei leitud. Kliki, et luua uus silt.</a>
            </md-not-found>
            </md-autocomplete>
            
            <md-chip-template>
              <span>
                <strong>{{$chip.value}}</strong>
              </span>
            </md-chip-template>
          </md-chips>
        </div>
        
        <div class="form-group col-12">
          <label for="otherComments"><strong>Täiendav info projekti kohta</strong> <span class="lighter">(valikuline)</span>
            <checkdone timeout="3000" ng-if="savedFields.additional !== undefined"></checkdone>
          </label>
          <textarea class="form-control" rows="3" id="otherComments" ng-change="debounceBeforeSaving('additional', 1000)"  ng-model="model.additional" maxlength="500"></textarea>
          <span class="word-count">{{ model.additional.length ? model.additional.length : 0 }} / 500</span>
        </div>
      
        <div class="d-flex justify-content-between mt-4 px-3 flex-wrap flex-column flex-lg-row">
          <div class="d-flex flex-wrap align-items-md-center align-items-start flex-column flex-md-row">
            <a class="btn btn-primary btn-preview mr-2 mb-2 mb-md-0" ui-sref="projects-view.view({id: model.id })">
              <md-tooltip md-direction="top">Eelvaade</md-tooltip>
              <i class="material-icons">remove_red_eye</i>
            </a>
            <a ui-sref="dashboard" class="btn btn-light mb-2 mb-md-0">Katkesta ja jätka hiljem</a>
            <a href="javascript:void(0);" ng-if="model.data_complete === false && isDataComplete" ng-click="confirmDataComplete()" class="btn btn-primary ml-md-2" ng-class="isDataComplete ? '': 'btn-disabled'">Kinnita projekti andmed</a>
          </div>
          <div class="d-flex mt-2 mt-lg-0">
            <a class="btn btn-primary" ui-sref="projects.client({id: model.id})">Edasi</a>
          </div>
        </div>
      </div>
    </form>
  </div>
</script>