App.controller("project_participant", function($scope, $http, $rootScope, $stateParams, $filter, $location, Upload, $timeout, $converter, $state){
  const PER_PAGE = 5;
  $scope.per_page = PER_PAGE;
  var project_id = $stateParams.id;
  $scope.redirect_path = $location.path();
 
  getData(project_id);
  
  function Participant (data, array) { 
      this.attributes = data;
      this._previousAttributes = angular.copy(data);
      this.array = array;
      this.fullname = this.attributes.user ? this.attributes.user.first_name + ' ' +this.attributes.user.last_name: "";
      this.hours = $converter.minutesTohours(this.attributes.minutes);
  }
  
  $scope.createParticipant = function(){
    var defaultParticipant = {
      minutes: 0,
      tasks: [],
      technologies: [],
      roles: [],
      additional: "",
      start_date: null,
      finish_date: null
    };
    var newParticipant = new Participant(defaultParticipant, $scope.data);
    $scope.data.push(newParticipant);
    renderContent();
    $location.search().page = $scope.pagination.pageCount;
  };
  
  function showSavedObject (id, category){
    //Display check-done to saved result
    $scope.checkDataVerification();
    
    $scope.savedFields = {};
    
    $timeout(function(){$scope.savedFields[id] = true },0);
  }
  
  $scope.$watch(function(){
    return $location.search();
  }, function(){
    if($scope.pagination){
      renderContent();
    }
  }, true);
  function sortByKey(array, key) {
    return array.sort(function(a, b) {
       
          var x =  (typeof(a.attributes.user[key]) == 'string') ? a.attributes.user[key].toLowerCase() : a.attributes.user[key];
          var y = (typeof(b.attributes.user[key]) == 'string') ? b.attributes.user[key].toLowerCase() : b.attributes.user[key];
          return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    
   
    });
  }
  function renderContent (){
    //order list alphabetically
    var list = $scope.data;
    $scope.savedFields = {}; //reset savedFields
    
    $scope.pagination = {
      pageCount: Math.ceil(list.length / PER_PAGE)
    };
    $scope.pagination.page = validActivePage(),
    
    $scope.participants = pageContent(list, new contentLimit(list.length, $scope.pagination.page, PER_PAGE));
   
    function validActivePage(){
      if(!$location.search().page) return 1;
      if($location.search().page){
        if($location.search().page > $scope.pagination.pageCount) return $scope.pagination.pageCount;
        else return $location.search().page;
      }
    }
    function pageContent(list, limit){
      var output = [];
      for(var i = limit.start; i < limit.end; i++){
        output.push(list[i]);
      }
      return output;
    }
    function contentLimit (length, page, per_page) {
      this.start = page * per_page - per_page;
      this.end =  (this.start + per_page > length) ? length : this.start + per_page;
    }
    
  }
  
  function getData(id) {
    $scope.loader = true;
     $http.get(settings.prefix + 'projects/' + id ).then(function(response){
      $scope.model = response.data.data;
      $scope.$parent.model = response.data.data;
      $scope.data = response.data.data.participants;
      for(var p = 0; p < $scope.data.length; p++) $scope.data[p] = new Participant($scope.data[p], $scope.data );
      
      $scope.data = sortByKey($scope.data, 'first_name');;
      renderContent($scope.data);
      $scope.loader = false;
     }, function(error){
       console.log(error);
     });
  };
  
  Participant.prototype.minutes = function(){
        this.attributes.minutes = $converter.hoursTominutes(this.hours);
        this.hours = $converter.minutesTohours(this.attributes.minutes);
        if(this._previousAttributes.minutes != this.attributes.minutes ) this.save();
      }
  Participant.prototype.destroy = function(){
    var _self = this;
    if(!_self.attributes.user) {
      
      _self.array.splice(_self.array.indexOf(_self), 1);
      renderContent();
    }
    //else if(!_self.attributes.user.hasOwnProperty('id')) return _self.array.splice(_self.array.indexOf(_self), 1);
    else $http({url: settings.prefix + 'projects/' + project_id + '/participants/'+ _self.attributes.user.id, method: 'delete'})
            .then(function(res){
              _self.array.splice(_self.array.indexOf(_self), 1)
              renderContent();
            }, function(err){ console.error(err.data)});
  };
  Participant.prototype.save = function (){
    var _self = this;
    if(!_self.attributes.user) return;
    var payload = angular.copy(_self.attributes);
    
    payload.start_date = $filter('date')(payload.start_date, 'yyyy-MM-dd');
    payload.finish_date = $filter('date')(payload.finish_date, 'yyyy-MM-dd');
    
    var requestParameters;
    
    if(_self.attributes.hasOwnProperty('id')) requestParameters = {url: settings.prefix + 'projects/' + project_id + '/participants/' + _self.attributes.user.id, method: "PUT", data : payload}
    else requestParameters = {url: settings.prefix + 'projects/' + project_id + '/participants/', method: "POST", data: payload};
    
    $http(requestParameters)
    .then(function(res){
      _self.attributes = res.data.data;
      _self._previousAttributes = angular.copy(res.data.data);
      showSavedObject(_self.attributes.id);
    }, function(err){ console.error(err.data)});
  };
})