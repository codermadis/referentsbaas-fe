<script  type="text/ng-template" id="project_participant.tpl">
  <div ng-if="loader" class="d-flex justify-content-center pt-4">
    <md-progress-circular md-mode="indeterminate"></md-progress-circular>
  </div>
  <div ng-if="!loader">
    <form>
      <md-list>
        <md-list-item class="list-item-content flex-wrap" ng-repeat="participant in participants">
          <div class="col-12 d-flex justify-content-between">
            <div class="mr-4 list-participant-name">
              <md-autocomplete class="" ng-if="!participant.attributes.user.id"
                      md-search-text="searchUser"
                      md-selected-item-change="participant.save()"
                      md-selected-item="participant.attributes.user"
                      md-items="item in autocomplete(searchUser, 'users', participants)"
                      md-item-text="participant.fullname"
                      md-clear-button="false"
                      md-no-cache="true"
                      placeholder="Ees- ja perekonnanimi">
                  <md-item-template>
                    <span md-highlight-text="searchProject">{{item.first_name}} {{item.last_name}}</span>
                  </md-item-template>
              </md-autocomplete>
              <h4 ng-if="participant.attributes.user.id">
                {{participant.attributes.user.first_name}} {{participant.attributes.user.last_name}}
                <checkdone timeout="3000" ng-if="savedFields[participant.attributes.id]"></checkdone>
              </h4>
            </div>
            <div>
              <button class="btn btn-primary btn-icon" type="button" ng-click="participant.destroy()"><i class="material-icons">close</i></button>
            </div>
          </div>
          
          <div class="d-flex w-100 align-items-center flex-wrap">
            <div class="col-lg-4 col-xl-3 col-12">
              <div class="d-flex align-items-lg-center flex-column flex-lg-row mt-2 mt-lg-0">
                <div class="list-label">
                  <p class="mb-2 mb-lg-0 mr-3"><b>Töötunnid <span class="red">*</span></b></p>
                </div>
                <input type="text" class="form-control list-hours" ng-blur="participant.minutes()" ng-model="participant.hours"/>
              </div>
            </div>
            <div class="col-lg-8 col-xl-9 col-12">
              <div class="d-flex align-items-lg-center flex-column flex-lg-row mt-2 mt-lg-0">
                <div class="list-label list-label-xl">
                  <p class="mb-0 mr-3"><b>Roll <span class="red">*</span></b></p>
                </div>
                <md-chips class="w-100" 
                  ng-model="participant.attributes.roles" ng-class="{'no-placeholder': participant.attributes.roles.length}"
                  md-separator-keys="[188,13]"
                  md-on-add="participant.save()"
                  md-require-match=true
                  md-on-remove="participant.save()">
                  <md-autocomplete
                   md-search-text="searchUserRole"
                   md-items="item in tags(searchUserRole, 'role')"
                   md-min-length="0"
                   md-item-text="item.value"
                   placeholder="{{showPlaceholder('Analüütik, arendaja, projektijuht vms', participant.attributes.roles.length)}}">
                    <span md-highlight-text="searchUserRole">{{item.value}}</span>
                    <md-not-found>
                      <a md-truncate flex href="/seaded/sildid?redirect={{redirect_path}}#tootajate_rollid">Silti ei leitud. Kliki, et luua uus silt.</a>
                    </md-not-found>
                  </md-autocomplete>
                   <md-chip-template>
                      <span>
                         <strong>{{$chip.value}}</strong>
                      </span>
                   </md-chip-template>
                </md-chips>
                
              </div>
            </div>
          </div>
          <div class="d-flex w-100 align-items-center flex-wrap">
            <div class="col-lg-4 col-xl-3 col-12">
              <div class="d-flex align-items-lg-center flex-column flex-lg-row mt-2 mt-lg-0">
                <div class="list-label">
                  <p class="mb-0 mr-3"><b>Töö algus <span class="red">*</span></b></p>
                </div>
                <md-datepicker class="list-datepicker" md-placeholder="Vali kuupäev" ng-model="participant.attributes.start_date" ng-change="participant.save()"></md-datepicker>
              </div>
            </div>
            <div class="col-lg-8 col-xl-9 col-12">
              <div class="d-flex align-items-lg-center flex-column flex-lg-row mt-2 mt-lg-0">
                <div class="list-label list-label-xl">
                  <p class="mb-0 mr-3"><b>Teostatud tööd <span class="red">*</span></b></p>
                </div>
                <md-chips class="w-100" 
                  ng-model="participant.attributes.tasks" ng-class="{'no-placeholder': participant.attributes.tasks.length}"
                  md-separator-keys="[188,13]"
                  md-require-match=true
                  md-on-add="participant.save()"
                  md-on-remove="participant.save()">
                  <md-autocomplete
                         md-search-text="searchUserTasks"
                         md-items="item in tags(searchUserTasks, 'task')"
                         md-min-length="0"
                         md-item-text="item.value"
                         placeholder="{{showPlaceholder('Ärianalüüs, programmeerimine, kasutajaliidese disain vms', participant.attributes.tasks.length)}}">
                      <span md-highlight-text="searchUserTasks">{{item.value}}</span>
                      <md-not-found>
                        <a md-truncate flex href="/seaded/sildid?redirect={{redirect_path}}#teostatud_tood">Silti ei leitud. Kliki, et luua uus silt.</a>
                      </md-not-found>
                  </md-autocomplete>
                   <md-chip-template>
                      <span>
                         <strong>{{$chip.value}}</strong>
                      </span>
                   </md-chip-template>
                </md-chips>
                
              </div>
            </div>
          </div>
          <div class="d-flex w-100 align-items-center flex-wrap">
            <div class="col-lg-4 col-xl-3 col-12">
              <div class="d-flex align-items-lg-center flex-column flex-lg-row mt-2 mt-lg-0">
                <div class="list-label">
                  <p class="mb-0 mr-3"><b>Töö lõpp <span class="red">*</span></b></p>
                </div>
                <md-datepicker class="list-datepicker" md-placeholder="Vali kuupäev" ng-model="participant.attributes.finish_date"  ng-change="participant.save()"></md-datepicker>
              </div>
            </div>
            <div class="col-lg-8 col-xl-9 col-12">
              <div class="d-flex align-items-lg-center flex-column flex-lg-row mt-2 mt-lg-0">
                <div class="list-label list-label-xl">
                  <p class="mb-0 mr-3"><b>Tehnoloogia <span class="red">*</span></b></p>
                </div>
                <md-chips class="w-100" ng-model="participant.attributes.technologies" ng-class="{'no-placeholder': participant.attributes.technologies.length}"
                  md-separator-keys="[188,13]"
                  md-require-match=true
                  md-on-add="participant.save()"
                  md-on-remove="participant.save()">
                  <md-autocomplete
                    md-search-text="searchUserTech"
                    md-items="item in tags(searchUserTech, 'technology')"
                    md-min-length="0"
                    md-item-text="item.value"
                    placeholder="{{showPlaceholder('Node.js, AngularJS, CSS3, HTML5 vms', participant.attributes.technologies.length)}}">
                    <span md-highlight-text="searchUserTech">{{item.value}}</span>
                    <md-not-found>
                      <a md-truncate flex href="/seaded/sildid?redirect={{redirect_path}}#tehnoloogiad">Silti ei leitud. Kliki, et luua uus silt.</a>
                    </md-not-found>
                  </md-autocomplete>
                   <md-chip-template>
                      <span>
                         <strong>{{$chip.value}}</strong>
                      </span>
                   </md-chip-template>
                </md-chips>
                
              </div>
            </div>
          </div>
          <div class="d-flex w-100 mt-2">
            <div class="form-group col-12">
              <label><strong>Täiendav info</strong> (valikuline)</label>
              <textarea class="form-control" rows="3" ng-model="participant.attributes.additional" ng-blur="participant.save()" maxlength="500"></textarea>
              <span class="word-count">{{ participant.attributes.additional.length ? participant.attributes.additional.length : 0 }} / 500</span>
            </div>
          </div>
        </md-list-item>
    
      </md-list>
      
      <paginator data="{{ pagination }}" ng-if="data.length > per_page"></paginator>
      <a class="add-list-item" href="javascript:void(0);" ng-click="createParticipant()">
        <b><i class="material-icons">add</i> Lisa töötaja</b>
      </a>
      <div class="d-flex justify-content-between mt-4 px-3 flex-wrap flex-column flex-lg-row">
        <div class="d-flex flex-wrap align-items-md-center align-items-start flex-column flex-md-row">
          <a class="btn btn-primary btn-preview mr-2 mb-2 mb-md-0" ui-sref="projects-view.view({id: model.id })">
            <md-tooltip md-direction="top">Eelvaade</md-tooltip>
            <i class="material-icons">remove_red_eye</i>
          </a>
          <a ui-sref="dashboard" class="btn btn-light mb-2 mb-md-0">Katkesta ja jätka hiljem</a>
          <a href="javascript:void(0);" ng-if="model.data_complete === false && isDataComplete" ng-click="confirmDataComplete()" class="btn btn-primary ml-md-2" ng-class="isDataComplete ? '': 'btn-disabled'">Kinnita projekti andmed</a>
        </div>
        <div class="d-flex mt-2 mt-lg-0 flex-wrap flex-column flex-md-row align-items-start">
          <a class="btn btn-light" ui-sref="projects.contractor({id: model.id})">Tagasi</a>
          <a class="btn btn-primary ml-0 ml-md-2 mt-2 mt-md-0" ui-sref="projects.results({id: model.id})">Edasi</a>
        </div>
      </div>
    </form>
  </div>
</script>