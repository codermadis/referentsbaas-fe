App.controller("project_result", function($scope, $http, $rootScope, $stateParams, $filter, Upload, $timeout, $converter, $location){
  $scope.redirect_path = $location.path();
  var project_id = $stateParams.id;
  
  fetchData(project_id);
  
  function Result (data, array) { 
      this.attributes = data;
      this.array = array;
      this.share = this.attributes.private === true ? false : true; // Kas tahad jagada teistele?
  }
  
  $scope.createResult = function(){
    $scope.results.push(new Result({private: false, categories:[]}, $scope.results));
  };
  
  function showSavedObject (id){
    $scope.checkDataVerification();
    // Display check-done to saved result
    $scope.savedFields = {};
  
    $timeout(function(){
      $scope.savedFields[id] = true;
    },0);
  }
  
  function fetchData(id) {
    $scope.loader = true;
     $http.get(settings.prefix + 'projects/' + id ).then(function(response){
      $scope.model = response.data.data;
      $scope.results = response.data.data.results;
      $scope.$parent.model = response.data.data;
      if(!$scope.results.length) $scope.results.push({private: true, categories:[]});
      for(var i = 0; i < $scope.results.length; i++) $scope.results[i] = new Result($scope.results[i], $scope.results);
      $scope.loader = false;
       
     }, function(error){
       console.log(error);
     });
  }
  
  Result.prototype.destroy = function () {
    var _self = this;
    if(!_self.attributes.hasOwnProperty('id')) return _self.array.splice(_self.array.indexOf(_self), 1);
    else $http({url: settings.prefix + 'projects/' + project_id + '/results/'+ _self.attributes.id, method: 'delete'})
            .then(function(res){ 
              _self.array.splice(_self.array.indexOf(_self), 1);
              
            }, function(err){ console.error(err.data)});
  };
  
  Result.prototype.save = function (){
    var _self = this;
    console.log(this);
    
    this.attributes.private = (this.share === true) ?  false : true;
    
    if(_self.attributes.hasOwnProperty('id')){ 
      $http({url: settings.prefix + 'projects/' + project_id + '/results/' + _self.attributes.id, method: "PUT", data : _self.attributes})
            .then(function(res){ 
              showSavedObject(_self.attributes.id);
            }, function(err){ console.error(err.data)});
    } else { 
      $http({url: settings.prefix + 'projects/' + project_id + '/results/', method: "POST", data: _self.attributes})
            .then(function(res){
              _self.attributes.id = res.data.data.id;
              showSavedObject(_self.attributes.id);
            }, function(err){ console.error(err.data)});
    }
  };
  
})