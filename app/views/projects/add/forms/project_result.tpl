<script  type="text/ng-template" id="project_result.tpl">
  <div ng-if="loader" class="d-flex justify-content-center pt-4">
    <md-progress-circular md-mode="indeterminate"></md-progress-circular>
  </div>
  <div ng-if="!loader">
    <form>
      <md-list ng-if="results.length">
        <md-list-item class="list-item-content flex-wrap" ng-repeat="result in results | orderBy: 'attributes.id'">
        
          <div class="d-flex w-100 align-items-center flex-wrap">
            <div class="col-12 col-lg-5">
              <div class="d-flex align-items-sm-center flex-column flex-sm-row">
                <div class="list-label list-label-xl">
                  <p class="mb-1 mb-sm-0 mr-3"><b>Töötulem</b></p>
                </div>
                <label class="custom-control custom-checkbox d-block m-0">
                  <input class="custom-control-input" type="checkbox" ng-change="result.save()" ng-model="result.share">
                  <span class="custom-control-indicator"></span>
                  <span class="custom-control-description">Kas võib teistele klientidele näidata?</span>
                </label>
              </div>
            </div>
            <div class="col-12 col-lg-7">
              <div class="d-flex align-items-sm-center mt-3 mt-lg-0 flex-column flex-sm-row">
                <div class="list-label list-label-xl">
                  <p class="mb-0 mr-3"><b>Töötulemi liik</b></p>
                </div>
                <md-chips class="w-100" 
                  ng-model="result.attributes.categories" ng-class="{'no-placeholder': result.attributes.categories.length}"
                  md-separator-keys="[188,13]"
                  md-transform-chip="transformTag($chip)"
                  md-require-match=true
                  md-on-add="result.save()"
                  md-on-remove="result.save()">
                  <md-autocomplete
                         md-search-text="searchResult"
                         md-items="item in tags(searchResult, 'result')"
                         md-item-text="item.value"
                         placeholder="{{result.attributes.categories.length ? '': 'Prototüüp, kujundusfailid, HTML-kood, ärianalüüsi dokument, loogiline andmemudel vms'}}">
                      <span md-highlight-text="searchResult">{{item.value}}</span>
                      <md-not-found>
                        <a md-truncate flex href="/seaded/sildid?redirect={{redirect_path}}#tootulemid">Silti ei leitud. Kliki, et luua uus silt.</a>
                      </md-not-found>
                  </md-autocomplete>
                   <md-chip-template>
                      <span>
                         <strong>{{$chip.value}}</strong>
                      </span>
                   </md-chip-template>
                </md-chips>
                
              </div>
            </div>
          </div>
          <div class="d-flex w-100 align-items-center flex-wrap">
            <div class="col-12 col-lg-5">
              <div class="d-flex align-items-sm-center mt-3 mt-lg-0 flex-column flex-sm-row">
                <div class="list-label list-label-xl">
                  <p class="mb-2 mb-sm-0 mr-3"><b>Töötulemi URL</b></p>
                </div>
                <input type="text" class="form-control" ng-change="result.save()" ng-model-options='{ debounce: 1000 }' maxlength="500" ng-model="result.attributes.url" />
              </div>
            </div>
            <div class="col-12 col-lg-7">
              <div class="d-flex align-items-sm-center my-3 flex-column flex-sm-row">
                <div class="list-label list-label-xl">
                  <p class="mb-2 mb-sm-0 mr-3"><b>Töötulemi kirjeldus</b></p>
                </div>
                <div class="w-100">
                  <textarea class="form-control" 
                  placeholder='Siia kirjuta, millise töötulemiga täpsemalt tegemist on. Näiteks võid prototüübi puhul kirjutada "kliendi uue veebilehe Axure RP prototüübi link".' 
                  rows="3" ng-blur="result.save()" maxlength="500" ng-model="result.attributes.description"></textarea>
                  <span class="word-count">{{ result.attributes.description.length ? result.attributes.description.length : 0 }} / 500</span>
                </div>
              </div>
            </div>
          </div>
          <div class="d-flex w-100">
            <div class="col-12 d-flex justify-content-end">
              <checkdone timeout="3000" ng-if="savedFields[result.attributes.id]" class="static-position"></checkdone>
              <button ng-click="result.destroy()" class="btn btn-primary btn-icon" type="button"><i class="material-icons">close</i></button>
            </div>
          </div>
        </md-list-item>
        
      </md-list>
      <a class="add-list-item" href="javascript:void(0);" ng-click="createResult()" ><i class="material-icons">add</i><b>Lisa töötulem</b></a>
      <div class="d-flex justify-content-between mt-4 px-3 flex-wrap flex-column flex-lg-row">
        <div class="d-flex flex-wrap align-items-md-center align-items-start flex-column flex-md-row">
          <a class="btn btn-primary btn-preview mr-2 mb-2 mb-md-0" ui-sref="projects-view.view({id: model.id })">
            <md-tooltip md-direction="top">Eelvaade</md-tooltip>
            <i class="material-icons">remove_red_eye</i>
          </a>
          <a ui-sref="dashboard" class="btn btn-light mb-2 mb-md-0">Katkesta ja jätka hiljem</a>
          <a href="javascript:void(0);" ng-if="model.data_complete === false && isDataComplete" ng-click="confirmDataComplete()" class="btn btn-primary ml-md-2" ng-class="isDataComplete ? '': 'btn-disabled'">Kinnita projekti andmed</a>
        </div>
        <div class="d-flex mt-2 mt-lg-0">
          <a class="btn btn-light" ui-sref="projects.participation({id: model.id})">Tagasi</a>
        </div>
      </div>
    </form>
  </div>
</script>