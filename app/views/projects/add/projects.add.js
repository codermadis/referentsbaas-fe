App.controller("projects.add", function($scope, $http, $rootScope, $stateParams, $filter, Upload, $timeout, $converter, $location, $transitions, $verifyProjectData){
  $scope.EMAIL_PATTERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const DATA_COMPLETE_SUCCESS = 'Projekti andmed kinnitatud!';
  $scope.messages = {};
  $scope.model = {};
  $scope.tabs = [
    {_id: 0, label:'Projekti üldinfo',  state: "projects.general", url: "/projektid/:id/projekti_yldinfo"},
    {_id: 1, label:'Tellija',           state: "projects.client", url: "/projektid/:id/tellija"},
    {_id: 2, label:'Allhanke info',     state: "projects.contractor", url: "/projektid/:id/allhanke_info"},
    {_id: 3, label:'Töötajad',          state: "projects.participation", url: "/projektid/:id/tootajad"},
    {_id: 4, label:'Töötulemid',        state: "projects.results", url: "/projektid/:id/tootulemid"}
  ];
  $scope.isActiveTab = function(tab){
    
    if( $location.path().replace($stateParams.id, ":id") == tab.url ){ return true; }
    else{ return false;}
  };
  
  $http.get(settings.prefix + 'class').then(function(response) {
    $scope.class =  response.data;
  }, function(err){ console.log(err) });
  
  $scope.tags = function(searchText, category){
    var tags = $scope.class[category];
    var output = [];
    for(var i in tags){
      if(tags[i].value.toLowerCase().includes(searchText.toLowerCase())) output.push(tags[i]);
    }
    return output;
  };
  
  $scope.transformTag = function(input){
    if(input.id) return input;
    else return { value: input };
  };
  
  $scope.autocomplete = function(input, resource, existing) {
    // existing will be transformed to an array of ids of existing resources
    var attributes_id = ['clients', 'delegates'];
    if(resource == 'users') {
      if(existing) existing = existing.map(function(participation){  if(participation.attributes.user) return participation.attributes.user.id}); 
    }
    else if (attributes_id.includes(resource)){
       if(existing){
        existing = existing.map(function(obj){  if(obj.attributes.id) return obj.attributes.id});
       }
    }
    return $http.get(settings.prefix + resource + '/autocomplete?name=' + input)
      .then(function(response) { 
        if(existing){
          return response.data.filter(function(record) {
            return !existing.includes(record.id);
          });
        }
        else return response.data;
        
      }, function(err){ console.log(err) });
  };
  
  $scope.showPlaceholder = function(text, arrayLength){
    return arrayLength > 0 ? '' : text;
  };
  $scope.isDataComplete = false;
  $scope.checkDataVerification = function () {
    
      $verifyProjectData.isDataComplete($stateParams.id).then(function (data) {
        $scope.validationMessages = data.messages;
        $scope.isDataComplete = data.required_fields_filled;
        $scope.$apply();
      });
  
  };
  
  $scope.checkDataVerification();
  
  $scope.confirmDataComplete = function(){
    $http({url: settings.prefix + 'projects/'+ $stateParams.id +'/data_complete', method:'put' }).then(function(res){
      $scope.giveToast(DATA_COMPLETE_SUCCESS,'toast-success');
    }, function(err){
      console.log(err.data);
    });
  };
  
  $scope.giveToast = function(message, className){
    $rootScope.toastNotification({ type: "simple", message: message, class: className, hideDelay: 3000, position: 'bottom right'});
  };

}); 