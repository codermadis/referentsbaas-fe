<script type="text/ng-template" id="projects.add.tpl">
  <div ng-show="loader"  class="d-flex justify-content-center pt-4">
      <md-progress-circular md-mode="indeterminate"></md-progress-circular>  
  </div>
  <div ng-show="!loader">
    <section class="container myfade">
      <ui-breadcrumb></ui-breadcrumb>
      <div class="add-project-manual">
          <div class="row">
             <div class="col-md-12">
                <div class="d-flex justify-content-between align-items-center flex-wrap">
                  <div class="mr-3">
                    <h4>
                      <span ng-if="!model.name">Lisa projekti andmed</span>
                      <span ng-if="model.name">{{model.name}}</span>
                    </h4>
                    <p class="last-modified mb-2" ng-if="model.history_recent"> 
                      Viimati muutis {{ model.history_recent.account.first_name }} {{ model.history_recent.account.last_name }}
                      <b>{{ model.history_recent.timestamp | relativeTimestamp }}</b>
                    </p>
                  </div>
                </div>
                
                <md-tabs md-dynamic-height md-border-bottom >
                  <md-tab md-active="isActiveTab(tab)"  label="{{tab.label}}" ui-sref="{{ tab.state }}" ng-repeat="tab in tabs" ng-disabled="!model.name && !isActiveTab(tab)">
  
                  </md-tab>
               </md-tabs>
               <ui-view></ui-view>

             </div>
          </div>
       </div><!--/add-project-manual-->
    </section><!--/container-->
  </div>
</script>