App.config(function ($stateProvider, $urlRouterProvider){
 //$urlRouterProvider.when("/projects/:id", "/projects/:id/general");
 
  
  $stateProvider
  .state({
    abstract: true,
    name: "projects",
    url: "/projektid/:id",
    label: 'Muuda',
    templateUrl: "projects.add.tpl",
    controller: "projects.add",
    resolve: settings.resolveAll,
    reloadOnSearch: false,
    params: {
      id: {
         dynamic: true
      }
    }
  }).state({
    name: "projects.general",
    url: "/projekti_yldinfo",
    data: {
      label: 'Projekti üldinfo',
    },
    templateUrl: "project_info.tpl",
    controller: "project_info"
  }).state({
    name: "projects.client",
    url: "/tellija",
    data: {
      label: 'Tellija',
    },
    templateUrl: "project_client.tpl",
    controller: "project_client"
  }).state({
    name: "projects.contractor",
    url: "/allhanke_info",
    data: {
      label: 'Allhanke info',
    },
    templateUrl: "project_contractor.tpl",
    controller: "project_contractor"
  }).state({
    name: "projects.participation",
    url: "/tootajad",
    data: {
      label: 'Töötajad',
    },
    templateUrl: "project_participant.tpl",
    controller: "project_participant"
  }).state({
    name: "projects.results",
    url: "/tootulemid",
    data: {
      label: 'Töötulemid',
    },
    templateUrl: "project_result.tpl",
    controller: "project_result"
    
  });
  
  $stateProvider
  .state({
    abstract: true,
    name: "projects-view",
    url: "/projektid/:id",
    data: {
      label: 'Vaata abstract',
    },
    params: {
      id: {
         dynamic: true
      }
    },
    controller: "projects.view.abstract",
    templateUrl: "projects.view.abstract.tpl",
    resolve: settings.resolveAll,
    reloadOnSearch: false
    
  }).state({
    name: "projects-view.view",
    url: "/vaata",
    data: {
      label: 'Projekti eelvaade',
    },
    templateUrl: "projects.view.tpl",
    controller: "projects.view", 
    
  });
  
      
});