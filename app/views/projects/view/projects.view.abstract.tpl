<script type="text/ng-template" id="projects.view.abstract.tpl">
  <section class="container">
    <ui-breadcrumb></ui-breadcrumb>
    <ui-view></ui-view>
  </section>
</script>