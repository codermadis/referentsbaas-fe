App.controller('projects.view', function($scope, $rootScope, $http, $stateParams, $mdDialog, $verifyProjectData, $timeout){
  const DATA_COMPLETE_SUCCESS = 'Projekti andmed kinnitatud!';
  $scope.validationMessages = [];
  $scope.changeProject = $rootScope.userData.role_id > 2 ? true : false;
  
  $scope.settings_prefix = settings.prefix; //for file download link
  $scope.getProject = function() {
    $scope.loader = true;
    $http.get(settings.prefix + 'projects/' + $stateParams.id).then(function(response){
      
      $scope.projectData = response.data.data;
      
      //Check whether the project's "data_complete" could be set to true
      $verifyProjectData.isDataComplete(response.data.data.id).then(function (data) {
       
        $timeout(function(){
          console.log(data);
          $scope.validationMessages = data.messages;
          $scope.isDataComplete = data.required_fields_filled;
        }, 0);
        
      });
     
      $scope.loader = false;
      
    }, function(error){
      $scope.loader = false;
      $scope.projectData = false;
    });
  };
  $scope.getProject();
  
  $scope.confirmDataComplete = function(){
    
    $http({url: settings.prefix + 'projects/'+ $stateParams.id +'/data_complete', method:'put' }).then(function(res){
      console.log(res.data);
      $scope.projectData.data_complete = true;
      $scope.giveToast(DATA_COMPLETE_SUCCESS,'toast-success');
    }, function(err){
      console.log(err.data);
    });
  };
  
  $scope.giveToast = function(message, className){
    $rootScope.toastNotification({ type: "simple", message: message, class: className, hideDelay: 3000, position: 'bottom right'});
  };
  
  $scope.isFieldValid = function (object, field) {
    if(!$scope.validationMessages.length) return true;
    
    var messages = $scope.validationMessages;
    var valid = true;
    for(var i = 0; i < messages.length; i++) {
      if(typeof messages[i].field === 'string') continue;
      
      if(messages[i].field[0] !== field) continue;
      
      if(object.id === messages[i].field[1]) {
        valid = false;
        break;
      }
    }
    return valid;
  };
  
});

App.filter('clientSector', function() {
  return function(sector) {
    
    switch (sector) {
      case 'public':
        return "Avalik sektor";
        break;
      case 'private':
        return "Erasektor";
        break;
    }
  }
});