<script type="text/ng-template" id="projects.view.tpl">
  <div ng-if="loader" class="d-flex justify-content-center">
    <md-progress-circular md-mode="indeterminate"></md-progress-circular>
  </div>
  <div ng-if="!loader">
    <div ng-if="projectData">
      <div class="display-project-page">
     
        <div class="d-flex flex-wrap justify-content-between align-items-lg-center mb-4 flex-column flex-lg-row">
          <h4 class="mb-0">{{ projectData.name }}</h4>
          <a href="javascript:history.back()" class="btn btn-light ml-3 mt-3 mt-lg-0 align-self-end">Tagasi</a>
        </div>
        <div class="outside-border p-3">
          <div class="row">
            <div class="col-12">
              <div class="section-heading d-flex justify-content-between align-items-start align-items-sm-center flex-column flex-sm-row">
                <h4 class="mb-0"><b>Projekti üldinfo</b></h4>
                <a ng-if="!changeProject" class="mt-1 mt-md-0 d-inline-block" ui-sref="projects.general({id:{{projectData.id}}})">Muuda<i class="material-icons view-icon">edit</i></a>
              </div>
              <div class="row">
                <div class="d-flex flex-wrap col-12 col-md-6 mt-3">
                  <p class="field-label"><b>Projekti staatus:</b></p>
                  <p>
                    <span ng-if="projectData.finished">Lõpetatud</span>
                    <span ng-if="!projectData.finished">Töös</span>
                  </p>
                </div>
                <div class="d-flex flex-wrap col-12 col-md-6 mt-3" ng-if="projectData.procurement_code">
                  <p class="field-label"><b>Riigihanke viitenumber:</b></p>
                  <p>{{ projectData.procurement_code }}</p>
                </div>
              </div>
              <div class="d-flex flex-wrap mt-3">
                <p class="field-label"><b>Süsteemi liigid:</b></p>
                <p>
                  <span class="no-info" ng-if="!projectData.system_types.length">Vajab täiendamist!</span>
                  <span ng-repeat="type in projectData.system_types">
                    <span ng-if="!$last">{{type.value}},</span>
                    <span ng-if="$last">{{type.value}}</span>
                  </span>
                </p>
              </div>
              <div class="d-flex flex-wrap flex-xl-nowrap mt-3">
                <p class="field-label"><b>Projekti lühikirjeldus (eesti k.):</b></p>
                <p>{{ projectData.description_et }}<span class="no-info" ng-if="!projectData.description_et">Vajab täiendamist!</span></p>
              </div>
              <div class="d-flex flex-wrap flex-xl-nowrap mt-3">
                <p class="field-label"><b>Projekti lühikirjeldus (inglise k.):</b></p>
                <p>{{ projectData.description_en }}<span class="no-info" ng-if="!projectData.description_en">Vajab täiendamist!</span></p>
              </div>
              <div class="d-flex flex-wrap mt-3">
                <p class="field-label"><b>Projektis teostatud tööd:</b></p>
                <p>
                  <span class="no-info" ng-if="!projectData.project_tasks.length">Vajab täiendamist!</span>
                  <span ng-repeat="task in projectData.project_tasks">
                    <span ng-if="!$last">{{task.value}},</span>
                    <span ng-if="$last">{{task.value}}</span>
                  </span>
                </p>
              </div>
              <div class="row">
                <div class="d-flex flex-wrap mt-3 col-12 col-md-6">
                  <p class="field-label"><b>Tööde algus:</b></p>
                  <p> {{ projectData.start_date | date : 'dd.MM.yyyy' }}<span class="no-info" ng-if="!projectData.start_date">Vajab täiendamist!</span></p>
                </div>
                <div class="d-flex flex-wrap mt-3 col-12 col-md-6">
                  <p class="field-label"><b>Tööde lõpp:</b></p>
                  <p> {{ projectData.finish_date | date : 'dd.MM.yyyy' }}<span class="no-info" ng-if="!projectData.finish_date">Vajab täiendamist!</span></p>
                </div>
              </div>
              <div class="row">
                <div class="d-flex flex-wrap mt-3 col-12 col-md-6">
                  <p class="field-label"><b>Algne planeeritud töömaht (h):</b></p>
                  <p>{{ projectData.planned_capacity | minutesTohours }}
                    <!--<span class="no-info" ng-if="!projectData.planned_capacity"> / Vajab täiendamist!</span>-->
                  </p>
                </div>
                <div class="d-flex flex-wrap mt-3 col-12 col-md-6">
                  <p class="field-label"><b>Tegelik töömaht (h):</b></p>
                  <p>{{ projectData.real_capacity | minutesTohours }}
                    <!--<span class="no-info" ng-if="!projectData.real_capacity"> / Vajab täiendamist!</span>-->
                  </p>
                </div>
              </div>
              <div class="row">
                <div class="d-flex flex-wrap mt-3 col-12 col-md-6">
                  <p class="field-label"><b>Tunnihind (EUR):</b></p>
                  <p>{{ projectData.billing_rate }}<span class="no-info" ng-if="!projectData.billing_rate">Vajab täiendamist!</span></p>
                </div>
                <div class="d-flex flex-wrap mt-3 col-12 col-md-6">
                  <p class="field-label"><b>Tööde maksumus (EUR):</b></p>
                  <p>{{ projectData.cost }}
                    <!--<span class="no-info" ng-if="!projectData.cost">Vajab täiendamist!</span>-->
                  </p>
                </div>
              </div>
              <div class="d-flex flex-wrap mt-3">
                <p class="field-label"><b>Tehnoloogia:</b></p>
                <p>
                  <span class="no-info" ng-if="!projectData.technologies.length">Vajab täiendamist!</span>
                  <span ng-repeat="technology in projectData.technologies">
                    <span ng-if="!$last">{{technology.value}},</span>
                    <span ng-if="$last">{{technology.value}}</span>
                  </span>
                </p>
              </div>
              <div class="d-flex flex-wrap flex-xl-nowrap mt-3" ng-if="projectData.additional">
                <p class="field-label"><b>Täiendav info projekti kohta</b></p>
                <p>{{projectData.additional}}</p>
              </div>
            </div>
            <div class="col-md-6 col-12 mt-4">
              <div class="section-heading d-flex justify-content-between align-items-start align-items-sm-center flex-column flex-sm-row">
                <h4 class="mb-0"><b>Töötajad</b></h4>
                <a ng-if="!changeProject" class="mt-1 mt-sm-0 d-inline-block" ui-sref="projects.participation({id:{{projectData.id}}})">Muuda<i class="material-icons view-icon">edit</i></a>
              </div>
              <div class="d-flex flex-wrap mt-3">
                <span class="no-info" ng-if="!projectData.participants.length">Vajab täiendamist!</span>
                <p class="mb-2 field-label-l" ng-repeat="participant in projectData.participants">
                  <a modal template-url="projects_workerModal.tpl" controller="projects.view.participant" ng-click="openModal($event)" href="javascript:void(0);" locals="{{participant}}">
                    <b>{{ participant.user.first_name }} {{ participant.user.last_name }}</b>
                    <span ng-if="!isFieldValid(participant, 'participants')" class="fa fa-exclamation-circle red ml-1"></span>
                  </a>
                </p>
              </div>
            </div>
            <div class="col-md-6 col-12 mt-4">
              <div class="section-heading d-flex justify-content-between align-items-start align-items-sm-center flex-column flex-sm-row">
                <h4 class="mb-0"><b>Töötulemid</b></h4>
                <a ng-if="!changeProject" class="mt-1 mt-sm-0 d-inline-block" ui-sref="projects.results({id:{{projectData.id}}})">Muuda<i class="material-icons view-icon">edit</i></a>
              </div>
              <div class="d-flex flex-wrap result-example">
                <div class="d-flex align-items-center mr-3 mt-2">
                  <span class="result-badge result-badge-public">A</span>
                  <p>- Avalik töötulem</p>
                </div>
                <div class="d-flex align-items-center mt-2">
                  <span class="result-badge result-badge-private">S</span>
                  <p>- Sisemine töötulem</p>
                </div>
              </div>
              <div class="row">
                <div class="col-12" ng-if="!projectData.results.length">
                  <div class="d-flex align-items-center mt-3">
                    <span class="no-info">Vajab täiendamist!</span>
                  </div>
                </div>
                <div class="col-md-6 col-12" ng-repeat="result in projectData.results">
                  <div class="d-flex align-items-center mt-3">
                    <span class="result-badge result-badge-private" ng-if="result.private">S</span>
                    <span class="result-badge result-badge-public" ng-if="!result.private">A</span>
                    <a modal template-url="projects_resultModal.tpl" controller="projects.view.result" ng-click="openModal($event)" href="javascript:void(0);" locals="{{result}}">
                      <b>
                        <span ng-repeat="category in result.categories">
                          <span ng-if="!$last">{{category.value}},</span>
                          <span ng-if="$last">{{category.value}}</span>
                        </span>
                      </b>
                      <span ng-if="!isFieldValid(result, 'results')" class="fa fa-exclamation-circle red ml-1"></span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 mt-4">
              <div class="section-heading d-flex justify-content-between align-items-start align-items-sm-center flex-column flex-sm-row">
                <h4 class="mb-0" ng-if="projectData.clients.length <= 1"><b>Tellija</b></h4>
                <h4 class="mb-0" ng-if="projectData.clients.length > 1"><b>Tellijad</b></h4>
                <a ng-if="!changeProject" class="mt-1 mt-sm-0 d-inline-block" ui-sref="projects.client({id:{{projectData.id}}})">Muuda<i class="material-icons view-icon">edit</i></a>
              </div>
              <div class="d-flex flex-wrap mt-3" ng-if="!projectData.clients.length">
                <span class="no-info">Vajab täiendamist!</span>
              </div>
              <div ng-repeat="client in projectData.clients">
                <div class="d-flex flex-wrap mt-3">
                  <h5 class="mb-0"><b>{{client.name}}</b></h5>
                </div>
                <div class="d-flex flex-wrap mt-3">
                  <p class="field-label"><b>Aadress:</b></p>
                  <p>{{client.address}}<span class="no-info" ng-if="!client.address">Vajab täiendamist!</span></p>
                </div>
                <div class="d-flex flex-wrap mt-3">
                  <p class="field-label"><b>Kliendisektor:</b></p>
                  <p>{{client.sector | clientSector}}<span class="no-info" ng-if="!client.sector">Vajab täiendamist!</span></p>
                </div>
                <div class="d-flex flex-wrap mt-3">
                  <p class="field-label"><b>Valdkond:</b></p>
                  <p>
                    <span class="no-info" ng-if="!client.field.length">Vajab täiendamist!</span>
                    <span ng-repeat="field in client.field">
                      <span ng-if="!$last">{{field.value}},</span>
                      <span ng-if="$last">{{field.value}}</span>
                    </span>
                  </p>
                </div>
                <div class="d-flex flex-wrap flex-xl-nowrap mt-3" ng-if="client.description">
                  <p class="field-label"><b>Märkused:</b></p>
                  <p>{{client.description}}</p>
                </div>
                <div class="d-flex flex-wrap mt-3 align-items-center">
                  <p class="field-label mb-2"><b>Süsteemi tellija kinnitused:</b></p>
                  <div class="d-flex flex-wrap">
                    <span class="no-info" ng-if="!client.files.length">Vajab täiendamist!</span>
                    <span ng-repeat="file in client.files" class="fa fa-file-pdf-o file-icon">
                      <md-tooltip md-direction="top">{{file.originalname}}</md-tooltip>
                      <a href="{{settings_prefix}}projects/{{projectData.id}}/files/{{file.id}}" class="file-link"></a>
                    </span>
                  </div>
                </div>
                <div ng-repeat="delegate in client.delegates">
                  <hr class="hr-separator" />
                  <div class="d-flex flex-wrap mt-3">
                    <p class="field-label"><b>Kontaktisiku nimi:</b></p>
                    <p>{{delegate.name}}<span class="no-info" ng-if="!delegate.name">Vajab täiendamist!</span></p>
                  </div>
                  <div class="d-flex flex-wrap mt-3">
                    <p class="field-label"><b>Telefon:</b></p>
                    <p>{{delegate.phone}}<span class="no-info" ng-if="delegate.phone.length < 5"> / Vajab täiendamist!</span></p>
                  </div>
                  <div class="d-flex flex-wrap mt-3">
                    <p class="field-label"><b>E-mail:</b></p>
                    <p>{{delegate.email}}<span class="no-info" ng-if="!delegate.email">Vajab täiendamist!</span></p>
                  </div>
                  <div class="d-flex flex-wrap mt-3">
                    <p class="field-label"><b>Suhtluskeel:</b></p>
                    <p>{{delegate.language | language}}<span class="no-info" ng-if="!delegate.language">Vajab täiendamist!</span></p>
                  </div>
                </div>
                <hr ng-if="!$last" />
              </div>
            </div>
            <div class="col-12 mt-4" ng-if="projectData.outsourced">
              <div class="section-heading d-flex justify-content-between align-items-start align-items-sm-center flex-column flex-sm-row">
                <h4 class="mb-0"><b>Allhanke info</b></h4>
                <a ng-if="!changeProject" class="mt-1 mt-sm-0 d-inline-block" ui-sref="projects.contractor({id:{{projectData.id}}})">Muuda<i class="material-icons view-icon">edit</i></a>
              </div>
              <div class="row">
                <div class="col-md-6 col-12">
                  <div class="d-flex flex-wrap mt-3">
                    <p class="field-label"><b>Peatöövõtja ärinimi:</b></p>
                    <p>{{projectData.contractor.name}}<span class="no-info" ng-if="!projectData.contractor.name">Vajab täiendamist!</span></p>
                  </div>
                  <div class="d-flex flex-wrap mt-3">
                    <p class="field-label"><b>Aadress:</b></p>
                    <p>{{projectData.contractor.address}}<span class="no-info" ng-if="!projectData.contractor.address">Vajab täiendamist!</span></p>
                  </div>
                  <div class="d-flex flex-wrap mt-3">
                    <p class="field-label"><b>Kontaktisiku nimi:</b></p>
                    <p>{{projectData.contractor.delegates[0].name}}<span class="no-info" ng-if="!projectData.contractor.delegates[0].name">Vajab täiendamist!</span></p>
                  </div>
                  <div class="d-flex flex-wrap mt-3">
                    <p class="field-label"><b>Telefon:</b></p>
                    <p>{{projectData.contractor.delegates[0].phone}}<span class="no-info" ng-if="!projectData.contractor.delegates[0].phone">Vajab täiendamist!</span></p>
                  </div>
                  <div class="d-flex flex-wrap mt-3">
                    <p class="field-label"><b>E-mail:</b></p>
                    <p>{{projectData.contractor.delegates[0].email}}<span class="no-info" ng-if="!projectData.contractor.delegates[0].email">Vajab täiendamist!</span></p>
                  </div>
                  <div class="d-flex flex-wrap mt-3">
                    <p class="field-label"><b>Suhtluskeel:</b></p>
                    <p>{{projectData.contractor.delegates[0].language | language}}<span class="no-info" ng-if="!projectData.contractor.delegates[0].language">Vajab täiendamist!</span></p>
                  </div>
                </div>
                <div class="col-md-6 col-12">
                  <div class="d-flex flex-wrap mt-3">
                    <p class="field-label-xl"><b>Projekti alguskuupäev:</b></p>
                    <p>{{projectData.outsource.start_date | date : 'dd.MM.yyyy'}}<span class="no-info" ng-if="!projectData.outsource.start_date">Vajab täiendamist!</span></p>
                  </div>
                  <div class="d-flex flex-wrap mt-3">
                    <p class="field-label-xl"><b>Projekti lõppkuupäev:</b></p>
                    <p>{{projectData.outsource.finish_date | date : 'dd.MM.yyyy'}}<span class="no-info" ng-if="!projectData.outsource.finish_date">Vajab täiendamist!</span></p>
                  </div>
                  <div class="d-flex flex-wrap mt-3">
                    <p class="field-label-xl"><b>Kogu projekti maksumus (EUR):</b></p>
                    <p>{{projectData.outsource.cost}}<span class="no-info" ng-if="!projectData.outsource.cost">Vajab täiendamist!</span></p>
                  </div>
                  <div class="d-flex flex-wrap mt-3">
                    <p class="field-label-xl"><b>Kogu projekti töömaht (h):</b></p>
                    <p>{{projectData.outsource.real_capacity | minutesTohours}}
                      <!--<span class="no-info" ng-if="!projectData.outsource.real_capacity"> / Vajab täiendamist!</span>-->
                    </p>
                  </div>
                  <div class="d-flex flex-wrap mt-3 align-items-center">
                    <p class="field-label-xl mb-2"><b>Peatöövõtja kinnitused:</b></p>
                    <div class="d-flex flex-wrap">
                      <span class="no-info" ng-if="!projectData.contractor.files.length">Vajab täiendamist!</span>
                      <span ng-repeat="file in projectData.contractor.files" class="fa fa-file-pdf-o file-icon">
                        <md-tooltip md-direction="top">{{file.originalname}}</md-tooltip>
                        <a href="{{settings_prefix}}projects/{{projectData.id}}/files/{{file.id}}" class="file-link"></a>
                      </span>
                    </div>
                  </div>
                  <div class="d-flex flex-wrap flex-xl-nowrap mt-3" ng-if="projectData.contractor.description">
                    <p class="field-label-xl"><b>Märkused:</b></p>
                    <p>{{projectData.contractor.description}}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div><!--/display-project-page-->
      
      <div class="d-flex mt-4 justify-content-end" ng-if="projectData.data_complete === false">
        <a href="javascript:void(0);" ng-click="confirmDataComplete()" class="btn btn-primary" ng-class="isDataComplete ? '': 'btn-disabled'">Kinnita projekti andmed</a>
      </div>
      
    </div>
    <div ng-if="!projectData">
      <h4>Projekti ei leitud!</h4>
      <a ui-sref="dashboard">Tagasi töölauale</a>
    </div>
  </div>
</script>