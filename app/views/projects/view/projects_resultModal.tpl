<script type="text/ng-template" id="projects_resultModal.tpl">
  <md-dialog aria-label="dialog" style="height: auto;" class="participant-modal">
    <div class="d-flex py-3 justify-content-between align-items-center flex-shrink-0">
      <h4 class="m-0">
        <span ng-repeat="category in result.categories">
          <span ng-if="!$last">{{category.value}},</span>
          <span ng-if="$last">{{category.value}}</span>
        </span>
      </h4>
      <md-button aria-label="close modal" ng-click="hide()" class="md-primary modal-close" md-autofocus><i class="material-icons">close</i></md-button>
    </div>

    <md-dialog-content style="overflow-x: hidden;">
      <div class="row">
        <div class="col-md-6 col-12 d-flex flex-wrap">
          <p class="field-label"><b>Töötulem:</b></p>
          <p>{{result.private | workResult}}</p>
        </div>
        <div class="col-md-6 col-12 d-flex flex-wrap">
          <p class="field-label mb-3"><b>Töötulemi URL:</b></p>
          <a ng-if="result.url" href="//{{result.url}}" target="_blank">{{result.url | truncate:25}}<span class="fa fa-external-link ml-2"></span></a>
          <span class="no-info" ng-if="!result.url">Vajab täiendamist!</span>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <p class="field-label"><b>Töötulemi kirjeldus:</b></p>
          <p>{{result.description}}</p>
        </div>
      </div>
    </md-dialog-content>

    <md-dialog-actions class="justify-content-end">
      <md-button class="btn btn-primary" ng-click="hide()">
        Sulge
      </md-button>
    </md-dialog-actions>
  </md-dialog>
</script>