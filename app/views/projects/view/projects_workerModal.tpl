<script type="text/ng-template" id="projects_workerModal.tpl">
  <md-dialog aria-label="dialog" style="height: auto;" class="participant-modal">
    <div class="d-flex py-3 justify-content-between align-items-center flex-shrink-0">
      <h4 class="m-0">{{participant.user.first_name}} {{participant.user.last_name}}</h4>
      <md-button aria-label="close modal" ng-click="hide()" class="md-primary modal-close" md-autofocus><i class="material-icons">close</i></md-button>
    </div>

    <md-dialog-content style="overflow-x: hidden;">
      <div class="row">
        <div class="col-md-6 col-12 d-flex flex-wrap">
          <p class="field-label"><b>Töötunnid projektis:</b></p>
          <p>{{participant.minutes | minutesTohours}}<span class="no-info" ng-if="!participant.minutes"> / Vajab täiendamist!</span></p>
        </div>
        <div class="col-md-6 col-12 d-flex flex-wrap">
          <p class="field-label"><b>Roll projektis:</b></p>
          <p>
            <span class="no-info" ng-if="!participant.roles.length">Vajab täiendamist!</span>
            <span ng-repeat="role in participant.roles">
              <span ng-if="!$last">{{role.value}},</span>
              <span ng-if="$last">{{role.value}}</span>
            </span>
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-12 d-flex flex-wrap">
          <p class="field-label"><b>Töötamise algus:</b><p>
          <p>{{participant.start_date | date : 'dd.MM.yyyy'}}<span class="no-info" ng-if="!participant.start_date">Vajab täiendamist!</span></p>
        </div>
        <div class="col-md-6 col-12 d-flex flex-wrap">
          <p class="field-label"><b>Töötamise lõpp:</b><p>
          <p>{{participant.finish_date | date : 'dd.MM.yyyy'}}<span class="no-info" ng-if="!participant.finish_date">Vajab täiendamist!</span></p>
        </div>
      </div>
      <div class="row">
        <div class="col-12 d-flex flex-wrap">
          <p class="field-label"><b>Teostatud tööd</b></p>
          <p>
            <span class="no-info" ng-if="!participant.tasks.length">Vajab täiendamist!</span>
            <span ng-repeat="task in participant.tasks">
              <span ng-if="!$last">{{task.value}},</span>
              <span ng-if="$last">{{task.value}}</span>
            </span>
          </p>          
        </div>
        <div class="col-12 d-flex flex-wrap">
          <p class="field-label"><b>Tehnoloogia</b></p>
          <p>
            <span class="no-info" ng-if="!participant.technologies.length">Vajab täiendamist!</span>
            <span ng-repeat="technology in participant.technologies">
              <span ng-if="!$last">{{technology.value}},</span>
              <span ng-if="$last">{{technology.value}}</span>
            </span>
          </p>          
        </div>
      </div>
    </md-dialog-content>

    <md-dialog-actions class="justify-content-end">
      <md-button class="btn btn-primary" ng-click="hide()">
        Sulge
      </md-button>
    </md-dialog-actions>
  </md-dialog>
</script>