App.controller('recover.controller', function($scope, $rootScope, $http, $location, $timeout) {
  $rootScope.headerless = true;
  const MESSAGE_DURATION = 3000;
  const QUERY_STRINGS = '?email=' + $location.search().email + '&code=' + $location.search().code;
  $scope.currentEmail = $location.search().email;
  $scope.loading;
  $scope.isValidLink;
  $scope.model = {};
  $scope.messages = {};
  $scope.passwordSuccess = false;
  
  $scope.submitPassword = function(new_pw, repeat_new_pw){
    if(new_pw != repeat_new_pw) {
      $scope.messages.errorMessage = 'Salasõnad ei ühti. Palun proovi uuesti.';
      $timeout(function(){
        $scope.messages.errorMessage = false;
      }, MESSAGE_DURATION);
      return;
    }
    
    $scope.loading = true;
    
    $http({url: settings.prefix + 'resetpassword' + QUERY_STRINGS , method: 'PUT', data: { password: new_pw } }).then(function(response) {
      $scope.loading = false;
      $scope.messages.alertMessage = 'Parool edukalt vahetatud!';
      $scope.passwordSuccess = true;
      //$location.path('/login');
    },function (error) {
      $scope.isValidLink = false;
      //$scope.loading = false;
      console.error(error);
    });
  };
  
  var validateLink = function (){
    $scope.loading = true;
    $http.get(settings.prefix + 'resetpassword' + QUERY_STRINGS).then(function(response) {
      $scope.loading = false;
      $scope.isValidLink = response.data.success;
    },function (error) {
      $scope.isValidLink = false;
      $scope.loading = false;
      console.error(error);
    });
  };
  
  validateLink();
  
})