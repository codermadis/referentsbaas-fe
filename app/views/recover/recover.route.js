App.config(function ($stateProvider){
   
  $stateProvider.state('resetpassword',{
    url: "/resetpassword?email&code",
    templateUrl: "recover.tpl",
    controller: "recover.controller"
  });
  
});