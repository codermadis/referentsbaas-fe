<script id="recover.tpl" type="text/ng-template">
	<div ng-if="loading" class="container-fluid"></div>
	
	<div class="container-fluid d-flex justify-content-center align-items-center" ng-if="!loading">
		<div ng-if="!isValidLink" class="login reset-password">
			<div class="d-flex justify-content-center">
				<img src="assets/imgs/logo.svg" class="img-fluid" />
			</div>
			<h1 class="text-center">Referentsbaas</h1>
			<h4 class="text-center mb-0 not-valid">Kehtetu link</h4>
		</div>
		
		<section ng-if="isValidLink">
			<div class="login reset-password">
				<div class="d-flex justify-content-center">
					<img src="assets/imgs/logo.svg" class="img-fluid" />
				</div>
				<h1 class="text-center">Referentsbaas</h1>
				<form ng-submit="submitPassword(model.new_password, model.repeat_new_password)">
					<p ng-if="messages.errorMessage" class="well text-danger item-update" id="login_validation_errors">{{messages.errorMessage}}</p>
					<p ng-if="messages.alertMessage" class="well item-update" id="login_validation_errors">{{messages.alertMessage}}</p>
					<h4 class="text-center">{{currentEmail}}</h4>
					<div ng-if="!passwordSuccess">
						<div class="form-group row">
	          	<label class="col-12 col-form-label">Loo uus salasõna:</label>
	            <div class="col-md-12">
	            	<input id="new_password" class="form-control recovery" ng-model="model.new_password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$" type="password" name="new_password" value="" required />
	          	</div>
	          </div>
	          <div class="form-group row">
	          	<label class="col-12 col-form-label">Kinnita uus salasõna:</label>
	            <div class="col-12">
	            	<input id="repeat_new_password" class="form-control recovery" ng-model="model.repeat_new_password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$" type="password" name="repeat_new_password" value="" required />
	          	</div>
	          </div>
						<div class="password-helper mb-2">Parool peab olema vähemalt 6 tähemärki pikk ning sisaldama ühte suurt tähte ja ühte numbrit.</div>
					</div>
					<div class="d-flex flex-column align-items-center">
						<button type="submit" class="btn btn-primary mt-2" ng-if="!passwordSuccess">Kinnita</button>
						<a class="mt-2" ui-sref="login">Logi sisse</a>
					</div>
				</form>
			</div> 
		</section>
	</div><!--/container-->
</script>