App.controller("settings", function($scope, $rootScope, $http, $location, $transitions, $timeout){
    $scope.accountRoles = [
      {label: "Admin", _id:1},
      {label : "Projektijuht", _id:2},
      {label : "Töötaja", _id:3},
    ];
    
    $scope.tabs = [
      {_id: 0, label:'Konto seaded',       min_role_id: 3, state: "settings.account", url: "/seaded/konto_seaded" },
      {_id: 1, label:'Kasutajad',          min_role_id: 1, state: "settings.users", url: "/seaded/kasutajad" },
      {_id: 2, label:'Sildid',             min_role_id: 2, state: "settings.tags", url: "/seaded/sildid" },
      {_id: 3, label:'Töötajad',           min_role_id: 2, state: "settings.employees", url: "/seaded/tootajad"},
      {_id: 4, label:'Peidetud projektid', min_role_id: 2, state: "settings.hidden", url: "/seaded/peidetud_projektid"},
      {_id: 5, label:'Süsteem',            min_role_id: 1, state: "settings.system", url: "/seaded/systeem"}
    ];
    
    $scope.isActiveTab = function(tab){
      if( $location.path() == tab.url ){ return true; }
      else{ return false;}
    }

    $scope.roleLabel = function(role_id){
        for(var i = 0; i < $scope.accountRoles.length; i++){
            if($scope.accountRoles[i]._id == role_id) return $scope.accountRoles[i].label;
        }
        return role_id;
    };
    
    
    $scope.giveToast = function(message, className){
        $rootScope.toastNotification({ type: "simple", message: message, class: className, hideDelay: 3000, position: 'bottom right'});
    };
    
});