App.config(function ($stateProvider, $urlRouterProvider){
  
  //$urlRouterProvider.otherwise("/settings/account");
  
  $urlRouterProvider.when("/seaded", "/seaded/konto_seaded");
  $stateProvider.state({
    abstract: true,
    name: "settings",
    url: "/seaded",
    templateUrl: "settings.tpl",
    controller: "settings",
    data: {
      label: 'Seaded',
    },
    resolve: settings.resolveAll,
    reloadOnSearch: false
  })
  .state({
    name: "settings.account",
    url: "/konto_seaded",
    data: {
      label: 'Konto seaded',
    },
    templateUrl: "account_settings.tpl",
    controller: "account_settings"
  })
  .state({
    name: "settings.users",
    url: "/kasutajad",
    data: {
      label: 'Kasutajad',
    },
    templateUrl: "manage_users.tpl",
    controller: "manage_users"
  })
  .state({
    name: "settings.hidden",
    url: "/peidetud_projektid",
    data: {
      label: 'Peidetud projektid',
    },
    templateUrl: "hidden_projects.tpl",
    controller: "hidden_projects"
  })
  .state({
    name: "settings.employees",
    url: "/tootajad",
    data: {
      label: 'Töötajad',
    },
    templateUrl: "manage_employees.tpl",
    controller: "manage_employees"
  })
  .state({
    name: "settings.tags",
    url: "/sildid",
    data: {
      label: 'Sildid',
    },
    templateUrl: "manage_tags.tpl",
    controller: "manage_tags"
  })
  .state({
    name: "settings.system",
    url: "/systeem",
    data: {
      label: 'Süsteem',
    },
    templateUrl: "system_settings.tpl",
    controller: "system_settings"
  });
});