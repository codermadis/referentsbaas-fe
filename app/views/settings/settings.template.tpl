<script type="text/ng-template" id="settings.tpl">
  <div ng-show="loader" class="d-flex justify-content-center pt-4"> 
     <md-progress-circular md-mode="indeterminate"></md-progress-circular>
  </div>  
  <div ng-show="!loader">  
    <section class="container">
    
      <ui-breadcrumb></ui-breadcrumb>
  
        <div ng-show="err">{{err}}</div>
        <div class="profile">
            <!--
            <div class="row animate-if">
                <div class="col-md-12 header main-header mb-3">
                    <h4>Seaded</h4>
                </div>
            </div>
            -->
            <div class="row animate-if">
               
                <md-tabs md-dynamic-height md-border-bottom class="col-md-12 search-project" ng-if="!loader">
                   <md-tab md-active="isActiveTab(tab)" ng-disabled="userData.role_id > tab.min_role_id" label="{{tab.label}}" ui-sref="{{ tab.state }}" ng-repeat="tab in tabs">
                   </md-tab>
                </md-tabs>
                <div class="col-lg-12 col-md-12 col-sm-12 fade-element-in mt-3">
                  <ui-view></ui-view>
                </div> <!--col-12-->
            </div><!--inside row-->
        </div><!--/container-->
    </section><!--/container-->
  </div>  
</script>