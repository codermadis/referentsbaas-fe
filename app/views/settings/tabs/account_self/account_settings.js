App.controller("account_settings", function($scope, $rootScope, $http, $location, $timeout){
  const SELF_UPDATE_ERROR = 'Nime salvestamisel läks midagi valesti!';
  const SELF_UPDATE_SUCCESS = 'Nimi salvestatud!';
  const PASSWORDS_DO_NOT_MATCH = 'Salasõnad ei ühti, palun proovi uuesti!';
  const PASSWORD_SAVED = 'Salasõna uuendatud';
  
  var fullname = $rootScope.userData.first_name + ' ' + $rootScope.userData.last_name;
  $scope.account = {
    name: fullname.trim()
  };
  
  $scope.checkdone = {
    password: false
  };
  
  $scope.pw_model;
  
  $scope.changeUserData = function(data){
    var first_name, last_name;
    var arr = data.split(" ");
    
    if(arr.length < 2){
      first_name = data;
      last_name = "";
    } else {
      first_name = arr.splice(0, arr.length-1).join(' ');
      last_name = arr.pop();
    }
    
    if(first_name ===  $rootScope.userData.first_name && last_name === $rootScope.userData.last_name) return;
    
    $rootScope.userData.first_name = angular.copy(first_name);
    $rootScope.userData.last_name = angular.copy(last_name);
    $http({url: settings.prefix + 'accounts/'+ $rootScope.userData.id+ '/self',
          method: 'put',
          data: {'first_name': $rootScope.userData.first_name, 'last_name': $rootScope.userData.last_name}
    }).then(function(response){
        $scope.loader = false;
        $scope.giveToast(SELF_UPDATE_SUCCESS,'toast-success');
      }, function(err){
        $scope.loader = false;
        $scope.giveToast(SELF_UPDATE_ERROR,'toast-error');
    });
    
    
  };
 
  $scope.changePassword = function (password){
    if(!password.new_password || !password.confirm_new_password) {
      return;
    }
    $scope.err = false;
   
    if(password.new_password != password.confirm_new_password) {
      $scope.noMatch = true;
      if (password.confirm_new_password.length > 0) {
        return $scope.giveToast(PASSWORDS_DO_NOT_MATCH, 'toast-error');
      }
    }
    $scope.noMatch = false;
   
    $scope.loader = true;
     $http({url: settings.prefix + 'accounts/'+ $rootScope.userData.id+ '/password',
          method: 'put',
          data: {'password': password.new_password} })
     .then(function(response){
          $scope.loader = false;
          $scope.giveToast(PASSWORD_SAVED, 'toast-success');
      }, function(err){
          $scope.err = err;
          $scope.loader = false;
      });
       
   };
});
