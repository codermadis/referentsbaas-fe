<script type="text/ng-template" id="account_settings.tpl">

  <div class="container py-4">
    <h5 class="mb-3 accountsHeader">Sinu konto</h5>
    <div class="row align-items-center">
        <div class="col-md-3">
            <div class="bold">Nimi</div>
        </div>
        <div class="col-md-4 col-lg-3">
          <input class="form-control" type="text" maxLength="25" placeholder="Lisa enda nimi" ng-blur="changeUserData(account.name)" ng-model="account.name" />
        </div>
    </div>
    <div class="row mt-2">
        <div class="col-md-3">
            <div class="bold">E-mail</div>
        </div>
        <div class="col-md-9">{{userData.email}}</div>
    </div>
    <div class="row mt-3">
        <div class="col-md-3">
            <div class="bold">Roll</div>
        </div>
        <div class="col-md-9">{{roleLabel(userData.role_id)}}</div>
    </div>
    <hr>
    <h5 class="accountsHeader">Salasõna muutmine</h5>
    <form id="password_form" name="password_form">
      <div class="row mb-2">
          <div class="col-md-6">
              <span class="password-helper">Parool peab olema vähemalt 6 tähemärki pikk ning sisaldama ühte suurt tähte ja ühte numbrit.</span>
          </div>
      </div>
      <div class="row mb-3 align-items-center">
          <div class="col-md-3">
              <p class="mb-2 mb-md-0"><b>Uus salasõna</b></p>
          </div>
          <div class="col-md-4 col-lg-3">
              <input class="form-control" ng-class="{'error-input' : noMatch}" type="password" id="new_password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$" ng-model="pw_model.new_password" required>
          </div>
      </div>
      <div class="row align-items-center">
          <div class="col-md-3">
              <p class="mb-2 mb-md-0"><b>Kinnita uus salasõna</b></p>
          </div>
          <div class="col-md-4 col-lg-3">
              <input class="form-control" ng-class="{'error-input' : noMatch}" type="password" id="confirm_new_password" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$" ng-model="pw_model.confirm_new_password" required>
          </div>
      </div>
      <div class="row mt-3">
        <div class="col-md-3">
          
        </div>
        <div class="col-md-4 col-lg-3 d-flex justify-content-end align-items-start">
          <button type="submit" ng-class="{'d-none': loader}" ng-click="changePassword(pw_model)" class="btn btn-primary">Salvesta</button>
          <md-button aria-label="Saving data" ng-class="{'d-none': !loader}" class="btn btn-primary md-raised-loader loader-white" ng-click="null">
            <md-progress-circular md-mode="indeterminate" md-diameter="24px"></md-progress-circular>
          </md-button>
          <checkdone ng-if="checkdone.password" class="no-absolute" timeout="3000"></checkdone>
        </div>
      </div>
    </form>
  </div>

</script>