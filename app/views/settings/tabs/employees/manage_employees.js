App.controller("manage_employees", function($scope, $rootScope, $http, $location, $timeout, $window){
  const EMAIL_PATTERN = settings.pattern.email;
  const NAME_PATTERN = settings.pattern.name;
  
  const EMPLOYEE_UPDATED = 'Töötaja andmed uuendatud!';
  const EMPLOYEE_ADDED = 'Uus töötaja lisatud';
  const EMPLOYEE_EMAIL_DUPLICATE = 'Sellise e-mailiga töötaja on juba lisatud!';
  const PLEASE_FILL_ALL_FIELDS = 'Palun täida kõik väljad!';
  const PLEASE_PROVIDE_VALID_FIRST_NAME = 'Palun sisesta sobilik eesnimi!';
  const PLEASE_PROVIDE_VALID_LAST_NAME = 'Palun sisesta sobilik perekonnanimi!';
  const PLEASE_PROVIDE_VALID_EMAIL = "Palun sisesta sobilik e-mail!";
  
  $scope.submitLoader = {};
  $scope.sort = {};
  
  function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x =  (typeof(a.attributes[key]) == 'string') ? a.attributes[key].toLowerCase() : a.attributes[key];
        var y = (typeof(b.attributes[key]) == 'string') ? b.attributes[key].toLowerCase() : b.attributes[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }
  $scope.sortArrowClass = function(key){
    if($scope.sort.key === key) return $scope.sort.order === 'desc' ? 'fa fa-sort-down' : 'fa fa-sort-up';
    return 'fa fa-sort';
  };
  $scope.sortAccounts = function(key){
    if($scope.sort.key == key) {
      $scope.sort.order =  $scope.sort.order === 'asc' ? 'desc': 'asc';
      return $scope.employees = $scope.employees.reverse(); 
    }
    
    $scope.sort.key = key;
    $scope.sort.order = 'desc';
    $scope.employees = sortByKey($scope.employees, key);
  };
  $scope.createEmployee = function(data){
    if(!data.first_name || !data.last_name || !data.email ) return $scope.giveToast(PLEASE_FILL_ALL_FIELDS, 'toast-error');
    
    if(NAME_PATTERN.test(data.first_name) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_FIRST_NAME, 'toast-error');
    
    if(NAME_PATTERN.test(data.last_name) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_LAST_NAME, 'toast-error');
    
    if(EMAIL_PATTERN.test(data.email) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_EMAIL,'toast-error');
    
    if(checkForExistingEmail(data.email,  $scope.employees)) return $scope.giveToast(EMPLOYEE_EMAIL_DUPLICATE, 'toast-error');
    
    var employee = new Employee(data);
    employee.save();
    $scope.employees.push(employee);
    $scope.newRow = false;
  }
  function Employee (obj) {
    this.attributes = obj;
    this._previousAttributes = angular.copy(obj);
  }
  Employee.prototype.isModified = function(){
    if(JSON.stringify(this.attributes)  != JSON.stringify(this._previousAttributes)) return true;
    else if (!this.hasAttribute('id')) return true;
    else false;
  };
  Employee.prototype.undo = function(){
    this.attributes = angular.copy(this._previousAttributes);
  };
  Employee.prototype.hasAttribute = function (attr) { return  this.attributes[attr] === undefined ? false : true };
  Employee.prototype.save = function (){
    if(NAME_PATTERN.test(this.attributes.first_name) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_FIRST_NAME,'toast-error');
    if(NAME_PATTERN.test(this.attributes.last_name) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_LAST_NAME, 'toast-error');
    if(EMAIL_PATTERN.test(this.attributes.email) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_EMAIL, 'toast-error');
    
    var _self = this;
    $scope.submitLoader[_self.attributes.email] = true;
    
    var requestAttributes;
    
    if(this.hasAttribute('id')) requestAttributes =  {url: settings.prefix + 'users/' + this.attributes.id, method: "PUT", data : this.attributes};
    else requestAttributes =  {url: settings.prefix + 'users', method: "POST", data: this.attributes};
    
    return $http(requestAttributes)
    .then(function(response){
      _self.attributes = response.data.data;
      _self._previousAttributes = angular.copy(response.data.data);
      $scope.submitLoader[_self.attributes.email]  = false;
      
      if(requestAttributes.method === 'POST'){
        $scope.giveToast(EMPLOYEE_ADDED, 'toast-success');
      } else {
        $scope.giveToast(EMPLOYEE_UPDATED, 'toast-success');
      }
    },function(err){
      console.error(err);
      $scope.submitLoader[_self.attributes.email]  = false;
    });
  };
  
  function getEmployees (){
    $scope.loader = true;
    $http({url: settings.prefix + 'users', method: "get"}).then(function(response) {
      $scope.loader = false;
      var data = response.data;
      for(var i = 0; i < data.length; i++) data[i] = new Employee(data[i]);
      
      $scope.employees = data;
    }, function(err) {
        console.error(err);
    });
  }
  getEmployees();
  
  $scope.sortArrowClass = function(key){
    if($scope.sort.key === key) return $scope.sort.order === 'desc' ? 'fa fa-sort-down' : 'fa fa-sort-up';
    return 'fa fa-sort';
  };
  $scope.toggleNewRow = function(){
    $scope.newRow = $scope.newRow ? false : {};
  };
  
  function checkForExistingEmail(email, existing ){
    var duplicate = false;
          
    for(var ii = 0; ii < existing.length; ii++){
      if(existing[ii].attributes.email.toLowerCase() === email.toLowerCase() ) {
        duplicate = true;
        break;
      }
    }
    return duplicate;
  }
  
  $scope.sortIcon = "keyboard_arrow_down";
  $scope.sortLink = function(){
    if ($scope.sortList) {
      $scope.sortList = false;
      $scope.sortIcon = "keyboard_arrow_down";
    } else {
      $scope.sortList = true;
      $scope.sortIcon = "keyboard_arrow_up";
    }
  };
  if ($window.innerWidth > 767) {
    $scope.sortList = true;
  }
  angular.element($window).on('resize', function(){
    if ($window.innerWidth > 767) {
      $scope.sortList = true;
    } else {
      $scope.sortList = false;
      $scope.sortIcon = "keyboard_arrow_down";
    }
  });
});