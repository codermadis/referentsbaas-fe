<script type="text/ng-template" id="manage_employees.tpl">
  <div ng-if="loader" class="d-flex justify-content-center">
    <md-progress-circular md-mode="indeterminate"></md-progress-circular>
  </div>
  <div class="row" ng-if="!loader">
  
    <div class="col-md-12">
      <md-list>
        <div class="d-flex align-items-center col-12 sort-link mb-2">
          <a href="javascript:void(0);" ng-click="sortLink()" class="expand-search">Sorteeri<i class="material-icons">{{sortIcon}}</i>
          </a>
        </div>
        <md-list-item class="row accountsHeader mb-3 mb-md-3" ng-if="sortList">
          <div class="col-12 col-xl-3 col-md-4">
            <div ng-click="sortAccounts('first_name')">
              EESNIMI
              <i ng-class="sortArrowClass('first_name')" class="ml-1"></i>
            </div>
          </div>
          <div class="col-12 col-xl-3 col-md-4 mt-2 mt-md-0">
            <div ng-click="sortAccounts('last_name')">
              PEREKONNANIMI
              <i ng-class="sortArrowClass('last_name')" class="ml-1"></i>
            </div>
          </div>
          <div class="col-12 col-xl-3 col-md-4 mt-2 mt-md-0">
            <div ng-click="sortAccounts('email')">
              EMAIL
              <i ng-class="sortArrowClass('email')" class="ml-1"></i>
            </div>
          </div>
        </md-list-item>
        
        <md-list-item ng-repeat="employee in employees" class="accountsRow accountsContent row my-2 my-md-0 py-2 py-md-0" ng-class="{'modifiedRow' : employee.isModified(), 'unmodifiedRow': !employee.isModified(), 'loadingRow': submitLoader[employee.attributes.email] }">
          <div class="col-12 col-xl-3 col-md-4">
            <input class="form-control" ng-model="employee.attributes.first_name"></input>
          </div>
          <div class="col-12 col-xl-3 col-md-4">
            <input class="form-control mt-2 mt-md-0" ng-model="employee.attributes.last_name"></input>
          </div>
          <div class="col-12 col-xl-3 col-md-4">
            <input class="form-control mt-2 mt-md-0" ng-model="employee.attributes.email"></input>
          </div>
          <div class="col-12 col-xl-3">
            <div class="d-flex flex-column-reverse flex-md-row align-items-start">
              <md-button class="btn btn-light saveButton mb-md-2 mb-xl-0 mt-2 mt-xl-0" ng-click="employee.undo()" >Katkesta</md-button>
              <md-button class="btn btn-primary saveButton ml-md-3 ml-0 mb-xl-0 mt-2 mt-xl-0"  ng-click="employee.save()" >
                Salvesta
              </md-button>
              
              <md-button aria-label="Saving data" class="btn btn-primary md-raised-loader loader-white" ng-click="null">
                <md-progress-circular md-mode="indeterminate" md-diameter="24px"></md-progress-circular>
              </md-button>
            </div>
          </div>
        </md-list-item>
        
        <md-list-item ng-if="newRow" class="row">
          <div class="col-12 col-xl-3 col-md-4">
            <input class="form-control" ng-model="newRow.first_name"></input>
          </div>
          <div class="col-12 col-xl-3 col-md-4">
            <input class="form-control" ng-model="newRow.last_name"></input>
          </div>
          <div class="col-12 col-xl-3 col-md-4">
            <input class="form-control" ng-model="newRow.email"></input>
          </div>
          <div class="col-12 col-xl-3">
            <div class="d-flex flex-column-reverse flex-md-row align-items-start">
              <md-button class="btn btn-light mb-md-2 mb-xl-0 mt-2 mt-xl-0" ng-click="toggleNewRow()">Katkesta</md-button>
              <md-button class="btn btn-primary ml-md-3 ml-0 mb-xl-0 mt-2 mt-xl-0" ng-click="createEmployee(newRow)">
                Salvesta
              </md-button>
            </div>
          </div>
        </md-list-item>
      </md-list>
      <a class="add-list-item" href="javascript:void(0);" ng-if="!newRow"  ng-click="toggleNewRow()">
        <b ><i class="material-icons">add</i> Lisa töötaja</b>
      </a>
    </div>
  </div>
    
  
</script>