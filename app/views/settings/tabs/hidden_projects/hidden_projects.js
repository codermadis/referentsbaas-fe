App.controller("hidden_projects", function($scope, $rootScope, $http, $location, $filter){
    var orderBy = $filter('orderBy');
    $scope.propertyName = 'name';
    $scope.reverse = true;
    $scope.sortArrow = 'fa fa-sort';
    $scope.sortBy = function(propertyName){
        $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
        $scope.hiddenProjects = orderBy($scope.hiddenProjects, $scope.propertyName, $scope.reverse);
        $scope.sortArrow = ($scope.reverse) ? 'fa fa-sort-up' : 'fa fa-sort-down';
    };
 
    $scope.deleteProject = function(item){
        $http({
            url: settings.prefix + 'projects/' + item.id,
            method: "DELETE",
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).then(function(response){
            var index = $scope.hiddenProjects.indexOf(item);
            $scope.hiddenProjects.splice(index,1);
        }, function(err) {
            $scope.err = err;
        });
    };
    $scope.toggleProject = function(project) {
        $scope.hiddenProjectLoader = project.id;
        $http({ method: "PUT",
                url: settings.prefix + 'projects/' + project.id + '/show'
        }).then(function(response){
            var index = $scope.hiddenProjects.indexOf(project);
            $scope.hiddenProjects.splice(index,1);
            
            $scope.hiddenProjectLoader = false;
        }, function(response){
            $scope.hiddenProjectLoader = false;
            $scope.messages.errorMessage = response.data.code;
        });    
    };
    $scope.getHiddenprojects = function(){
        $scope.loader = true;
        $http({
            url: settings.prefix + 'projects?visible=false&pageSize=9999',
            method: "GET",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
        }).then(function(response){
            $scope.loader = false;
            $scope.hiddenProjects = response.data.list;
            
        }, function(response){
            $scope.loader = false;
            return
        });
    };
    $scope.getHiddenprojects();
    
});
