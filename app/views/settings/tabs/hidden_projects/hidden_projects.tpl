<script type="text/ng-template" id="hidden_projects.tpl">
  <div ng-if="loader" class="d-flex justify-content-center">
    <md-progress-circular md-mode="indeterminate"></md-progress-circular>
  </div> 
  <div ng-if="!loader">  
    <div style="margin:15px;" ng-if="!hiddenProjects.length">
      Peidetud projektid puuduvad
    </div>
    
    <div class="row" ng-if="hiddenProjects.length">
        <div class="col-md-12">
            <div style="margin:15px;">
                Muuda puudulike andmetega projektid töölaual uuesti nähtavaks või kustuta ebavajalikud projektid.
            </div>
        </div>
        <div class="col-md-12">
            <md-list>
              <md-list-item class="accountsHeader">
                <div ng-click="sortBy('name')">
                  PROJEKTI NIMETUS
                  <i class="{{sortArrow}} ml-1"></i>
                </div>
              </md-list-item>
              <md-list-item ui-sref="projects-view.view({id: project.id})" ng-repeat="project in hiddenProjects" class="modifiedRow clickable" ng-class="{'loadingRow': hiddenProjectLoader == project.id}">
                <p class="pr-3 bootleg-link font-weight-bold">{{project.name}}</p>
                
                <div class="d-flex mb-2 mb-md-0 flex-column-reverse flex-sm-row">
                  <md-button class="btn btn-secondary ml-0" ng-click="deleteProject(project)">
                      Kustuta
                  </md-button>
                  <md-button class="btn btn-primary saveButton ml-sm-3 ml-0 my-2 my-sm-0" ng-click="toggleProject(project)" >
                    Muuda nähtavaks
                  </md-button>
                  
                  <md-button class="btn btn-primary md-raised-loader loader-white ml-sm-3 ml-0 my-2 my-sm-0"  aria-label="Saving data"  ng-click="null" >
                    <md-progress-circular md-mode="indeterminate" md-diameter="24px"></md-progress-circular>
                  </md-button>
                  
                 
                  
                </div>
              </md-list-item>
            </md-list>
          
        </div>
    </div>
  </div>
</script>