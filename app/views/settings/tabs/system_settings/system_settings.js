App.controller("system_settings", function($scope, $http){
  
  const EMAIL_PATTERN = settings.pattern.email;
  
  const PLEASE_PROVIDE_VALID_EMAIL = 'Palun sisesta sobilik e-mail!';
  const PLEASE_FILL_ALL_FIELDS = 'Palun täida kõik väljad!';
  const SYSTEM_SAVED = 'Süsteemi konto on salvestatud!';
  
  var PW_TEXT = 'PLACEHOLDER';
  $scope.model = {
    outlook: {}
  };
  function getOutlookCredentials(){
    $scope.loader = true;
    $http({url: settings.prefix + 'settings/outlook', method: "get"}).then(function(response) {
      $scope.loader = false;
      $scope.model.outlook.username = response.data.username;
      $scope.model.outlook.password = response.data.password == true ? PW_TEXT : undefined;
    }, function(err) {
      console.error(err);
    });
  }
  
  $scope.saveOutlookCredentials = function(){
    var data = {
      username: $scope.model.outlook.username,
      password: $scope.model.outlook.password == PW_TEXT? undefined : $scope.model.outlook.password
    };
    
    if(EMAIL_PATTERN.test(data.username) === false && data.username.length) return $scope.giveToast(PLEASE_PROVIDE_VALID_EMAIL,'toast-error');
    //if(!data.password || !data.username) return $scope.giveToast(PLEASE_FILL_ALL_FIELDS,'toast-error');
    
    $http({url: settings.prefix + 'settings/outlook', method: "PUT", data : data}).then(function(response){
      $scope.giveToast(SYSTEM_SAVED, 'toast-success');
    },
    function(err){
      console.error(err);
    });
    
  };
  getOutlookCredentials();
});