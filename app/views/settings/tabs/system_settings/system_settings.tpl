<script type="text/ng-template" id="system_settings.tpl">
  <div ng-if="loader" class="d-flex justify-content-center">
    <md-progress-circular md-mode="indeterminate"></md-progress-circular>
  </div> 
  <div ng-if="!loader">  
    <div class="container py-4">
      <h5 class="accountsHeader">Office 365</h5>
      <p>E-mailide saatmine salasõna ununemisel ja konto loomisel.</p>
      <div class="row mb-2 align-items-center">
        <div class="col-md-3">
            <p class="mb-2 mb-md-0"><b>E-mail</b></p>
        </div>
        <div class="col-md-5 col-lg-3">
          <input type="text" class="form-control" placeholder="Sisesta e-maili aadress" ng-model="model.outlook.username"/>
         
        </div>
      </div>
      <div class="row mb-2 align-items-center">
          <div class="col-md-3">
              <p class="mb-2 mb-md-0"><b>Salasõna</b></p>
          </div>
          <div class="col-md-5 col-lg-3">
            <input type="password" class="form-control" placeholder="Sisesta salasõna" ng-model="model.outlook.password"/>
          </div>
      </div>
      <div class="row mt-3">
          <div class="col-md-8 col-lg-6">
            <md-button class="btn btn-primary float-right" ng-click="saveOutlookCredentials()" >
              Salvesta
            </md-button>
          </div>
      </div>
    </div> 
  </div>
</script>