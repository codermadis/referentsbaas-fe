App.controller("manage_tags", function($scope, $rootScope, $http, $location, $mdDialog, $filter, $window, $timeout){
  const PLEASE_ADD_VALUE = 'Palun lisa sildi väärtus!';
  const DUPLICATE_VALUE = 'Selline silt on juba loodud!';
  const TAG_ADDED = 'Uus silt lisatud!';
  const TAG_UPDATED = 'Silt uuendatud!';
  const TAG_REMOVED = "Silt eemaldatud!";
  
  $scope.loader = true;
  $scope.tagMergeBtn = true;
  
  $scope.classNameBook = {
    system_type: 'Süsteemi liigid',
    technology: 'Tehnoloogiad',
    role: 'Töötajate rollid',
    task: 'Teostatud tööd',
    result: 'Töötulemid',
    field: 'Valdkonnad'
  };
  
  $scope.submitLoader = {};
  
  var orderBy = $filter('orderBy');
  $scope.propertyName = 'attributes.count';
  $scope.reverse = true;
  $scope.sortBy = function(propertyName){
      $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName) ? !$scope.reverse : false;
      $scope.propertyName = propertyName;
      $scope.class = orderBy($scope.class, $scope.propertyName, $scope.reverse);
  };
  $scope.sortArrow = function(propertyName){
    if ($scope.propertyName === propertyName) {
      return !$scope.reverse ? 'fa fa-sort-down' : 'fa fa-sort-up';
    } else {
      return 'fa fa-sort';
    }
  };
  
  $scope.class = [];
  $scope.activeTab = function(category){
    $scope.activeCategory = category;
    switch (category) {
      case 'system_type':
        $location.hash("systeemi_liigid");
        break;
      case 'technology':
        $location.hash("tehnoloogiad");
        break;
      case 'role':
        $location.hash("tootajate_rollid");
        break;
      case 'task':
        $location.hash("teostatud_tood");
        break;
      case 'result':
        $location.hash("tootulemid");
        break;
      case 'field':
        $location.hash("valdkonnad");
        break;
    }
    $scope.reverse = false;
    $scope.propertyName = 'attributes.count';
    $scope.sortBy('attributes.count');
  };
  
  $scope.bindTag = function(input){
    //value validation
    
    if(!input.value) return $scope.giveToast(PLEASE_ADD_VALUE, 'toast-error');
    
    if(checkForDuplicate(input, $scope.class)) return $scope.giveToast(DUPLICATE_VALUE, 'toast-error');
    
    var tag = new Tag(input);
    
    $scope.class.push(tag);
    tag.save();
    
    $scope.newTag = false;
  };
  
  function checkForDuplicate(tag, existing) {
    var duplicate = false;
    
    for(var ii = 0; ii < existing.length; ii++){
      console.log(tag);
      console.log(existing[ii]);
      if(existing[ii].attributes.value.toLowerCase() === tag.value.toLowerCase()
        && existing[ii].attributes.group === tag.group) {
        duplicate = true;
        break;
      }
    }
    return duplicate;
  }
  
  function Tag(obj) {
    this.attributes = obj;
    this._previousAttributes = angular.copy(obj);
  }
  
  Tag.prototype.isModified = function(){
    if(JSON.stringify(this.attributes)  != JSON.stringify(this._previousAttributes)) return true;
    else if (!this.hasAttribute('id')) return true;
    else false;
  };
 
  Tag.prototype.hasAttribute = function (attr) { return  this.attributes[attr] === undefined ? false : true };
  
  Tag.prototype.save = function (){
    var _self = this;
    $scope.submitLoader[_self.attributes.value] = true;
    var requestAttributes;
    var payload = angular.copy(this.attributes);
    delete payload.count;
    
    if(this.hasAttribute('id')) requestAttributes =  {url: settings.prefix + 'class/' + this.attributes.id, method: "PUT", data: payload};
    else requestAttributes =  {url: settings.prefix + 'class', method: "POST", data: payload};
    
    return $http(requestAttributes)
    .then(function(response){
    
      for(var i in response.data){
        if(i == 'id') {
           _self.attributes[i] = response.data[i];
        } else if (_self.attributes[i] != undefined) {
          _self.attributes[i] = response.data[i];
        }
      }
      _self._previousAttributes = angular.copy(_self.attributes);
      $scope.submitLoader[_self.attributes.value] = false;
      
      if(requestAttributes.method === 'POST'){
        $scope.giveToast(TAG_ADDED, 'toast-success');
      } else {
        $scope.giveToast(TAG_UPDATED, 'toast-success');
      }
    },function(err){
      console.error(err);
      $scope.submitLoader[_self.attributes.value] = false;
    });
  };
  
  Tag.prototype.remove = function(){
    var _self = this;
    $http({url: settings.prefix + 'class/' + this.attributes.id, method: "DELETE" }).then(function(response){
      $scope.class.splice($scope.class.indexOf(_self), 1);
      $scope.giveToast(TAG_REMOVED, 'toast-success');
    },function(err){
      
      console.error(err);
    });
  };
  
  Tag.prototype.removeFromList = function(){
    $scope.class.splice($scope.class.indexOf(this), 1);
  };
  
  Tag.prototype.undo = function(){
    this.attributes = angular.copy(this._previousAttributes);
  };
  $scope.getData = function(locationHash){
    $scope.loader = true;
    
    $http({url: settings.prefix + 'class', method: "get"})
    .then(function(response) {
      var data = response.data;
      var list = [];
      for(var i = 0; i < Object.keys(data).length; i++){
      for(var x = 0; x <  data[Object.keys(data)[i]].length; x++){
        
        var obj = data[Object.keys(data)[i]][x];
        obj.group = Object.keys(data)[i];
         
        list.push(obj);
      }
      }
      data = angular.copy(list);
      for(var i = 0; i < data.length; i++) {
        data[i] = new Tag(data[i], $scope.class);
      }
      
      $scope.class = data;
      
      $scope.keysArray = Object.keys(response.data);
     
     
      if($scope.keysArray.length){
        
        if(!locationHash){
          $location.hash('systeemi_liigid');
          $scope.activeTab('system_type');
        } else {
          var activeKey = $scope.keysArray.find(function(key){
            return $scope.hashList[key] === locationHash;
          });
          $scope.activeTab(activeKey);
        }
      }
      $scope.tabScroll();
      $scope.loader = false;
    }, function(err) {
      console.error(err);
    });
  };
  $scope.redirect_link = false;
  
  if($location.search().redirect) {
    $scope.redirect_link = $location.search().redirect;
  }
  $scope.getData(angular.copy($location.hash()));
  
  $scope.hashList = {
    'system_type': 'systeemi_liigid',
    'technology': 'tehnoloogiad',
    'role': 'tootajate_rollid',
    'task': 'teostatud_tood',
    'result': 'tootulemid',
    'field': 'valdkonnad'
  };
  
  $scope.toggleNewTag = function(){
    $scope.newTag = $scope.newTag ? false : {};
  };
  
  $scope.tabScroll = function(){
    $timeout(function(){
      
      var wrapper = document.querySelector(".tabs-wrapper") || {};
      var children = wrapper.children || [];
      var childrenWidth = 0;
      for(var i = 0; i < children.length; i++) {
        childrenWidth += children[i].clientWidth;
      }
      if (wrapper.clientWidth < childrenWidth) {
        wrapper.classList.add('overflow-x');
      } else {
       if(wrapper.classList) wrapper.classList.remove('overflow-x');
      }
    }, 500);
  };
  $scope.sortIcon = "keyboard_arrow_down";
  $scope.sortLink = function(){
    if ($scope.sortList) {
      $scope.sortList = false;
      $scope.sortIcon = "keyboard_arrow_down";
    } else {
      $scope.sortList = true;
      $scope.sortIcon = "keyboard_arrow_up";
    }
  };
  if ($window.innerWidth > 767) {
    $scope.sortList = true;
  }
  angular.element($window).on('resize', function(){
    $scope.tabScroll();
    if ($window.innerWidth > 767) {
      $scope.sortList = true;
    } else {
      $scope.sortList = false;
      $scope.sortIcon = "keyboard_arrow_down";
    }
  });
});

