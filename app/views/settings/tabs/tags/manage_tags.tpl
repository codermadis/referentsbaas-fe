<script type="text/ng-template" id="manage_tags.tpl">
  <div ng-if="loader" class="d-flex justify-content-center">
    <md-progress-circular md-mode="indeterminate"></md-progress-circular>
  </div> 
  <div ng-if="!loader">
    <div class="container py-4">
      <div class="row">
        <div class="col-12">
          <div class="tabs px-0">
            <div class="tabs-wrapper">
              <div class="single-tab" ng-class="{'active': activeCategory == category}" ng-click="activeTab(category)" ng-repeat="category in keysArray">{{classNameBook[category]}}</div>
            </div>
            <div class="tab-content">
              <div class="d-flex align-items-center sort-link mb-2">
                <a href="javascript:void(0);" ng-click="sortLink()" class="expand-search">Sorteeri<i class="material-icons">{{sortIcon}}</i>
                </a>
              </div>
              <div class="row accountsHeader" ng-if="sortList">
                <div class="col-12 col-md-4">
                  <div ng-click="sortBy('attributes.value')">
                    SILT
                    <i class="ml-1" ng-class="sortArrow('attributes.value')"></i>
                  </div>
                </div>
                <div class="col-12 col-md-5 mt-2 mt-md-0">
                  <div ng-click="sortBy('attributes.count')">
                    KASUTUSKORDADE ARV
                    <i class="ml-1" ng-class="sortArrow('attributes.count')"></i>
                  </div>
                </div>
              </div>
              <div class="row row-adjuster mt-3 align-items-center" ng-repeat="item in class" ng-if="item.attributes.group === activeCategory">
                <div class="col-12 col-md-4">
                  <div class="form-group mb-0">
                    <input type="text" class="form-control" ng-model="item.attributes.value" />
                  </div>
                </div>
                <div class="col-12 col-md-3 mt-2 mt-md-0">
                  <span class="badge badge-preview">
                    <md-tooltip class="tag-preview" md-direction="top">
                      <div class="d-flex flex-column align-items-center">
                        <span class="pb-1">Eelvaade</span>
                        <md-chips class="m-0 px-2 py-0">
                          <md-chip class="m-0"><b>{{item.attributes.value}}</b></md-chip>
                        </md-chips>
                      </div>
                    </md-tooltip>
                    <span class="fa fa-tags"></span>
                    <b>{{item.attributes.count}}</b>
                  </span>
                </div>
                <div class="col-md-5 mt-2 mt-md-0">
                  <div class="d-flex flex-column-reverse flex-xl-row align-items-start aling-items-xl-center">
                    <md-button ng-if="tagMergeBtn" ng-class="{'d-none': submitLoader[item.attributes.value]}" class="btn btn-light mr-md-3" modal ng-click="mergeTag($event, item, class)" locals="{{ { 'item': item, 'list': class } }}">Ühtlusta</md-button>
                    <md-button ng-if="item.attributes.count == 0" class="btn btn-secondary mb-2 mb-xl-0" modal template-url="manage_tags_delete.tpl" controller="manage_tags_delete" ng-click="openTagDelete($event, item)" locals="{{item}}">Kustuta</md-button>
                    <md-button ng-class="{'d-inline-block': item.isModified(), 'd-none': !item.isModified() || submitLoader[item.attributes.value], 'ml-xl-3': item.attributes.count == 0}" class="btn btn-primary ml-0 mb-2 mb-xl-0" modal template-url="manage_tags_save.tpl" controller="manage_tags_save" ng-click="openTagConfirm($event, item)" locals="{{item}}">Salvesta</md-button>
                    <md-button aria-label="Saving data" ng-class="{'d-block': submitLoader[item.attributes.value], 'd-none': !submitLoader[item.attributes.value], 'ml-md-3': item.attributes.count == 0}" class="btn btn-primary md-raised-loader loader-white ml-0" ng-click="null">
                      <md-progress-circular md-mode="indeterminate" md-diameter="24px"></md-progress-circular>
                    </md-button>
                  </div>
                </div>
              </div>
              
              <div class="row row-adjuster mt-3 align-items-center" ng-if="newTag">
                <div class="col-12 col-md-4">
                  <div class="form-group mb-0">
                    <input type="text" class="form-control" ng-model="newTag.value" />
                  </div>
                </div>
                <div class="col-md-3 mt-2 mt-md-0">
                  <span class="badge badge-preview">
                    <md-tooltip class="tag-preview" md-direction="top">
                      <div class="d-flex flex-column align-items-center">
                        <span class="pb-1">Eelvaade</span>
                        <md-chips class="m-0 px-2 py-0">
                          <md-chip class="m-0"><b>{{newTag.value}}</b></md-chip>
                        </md-chips>
                      </div>
                    </md-tooltip>
                    <span class="fa fa-tags"></span>
                    <b>0</b>
                  </span>
                </div>
                <div class="col-md-5 mt-2 mt-md-0">
                  <div class="d-flex flex-column-reverse flex-md-row align-items-start aling-items-md-center">
                    <md-button class="btn btn-light" ng-click="toggleNewTag()">Katkesta</md-button>
                    <md-button class="btn btn-primary ml-md-3 ml-0 mb-2 mb-md-0" ng-click="bindTag({value: newTag.value, group: activeCategory, count: 0})">Salvesta</md-button>
                  </div>
                </div>
              </div>
              
              <a href="javascript:void(0);" ng-if="!newTag" class="add-list-item ml-0 mt-3" ng-click="toggleNewTag()">
                <b><i class="material-icons">add</i> Lisa silt</b>
              </a>
              
              <a ng-if="redirect_link" href="{{redirect_link}}" class="mt-4 add-list-item d-block p-0"><b>Tagasi projekti juurde</b></a>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</script>