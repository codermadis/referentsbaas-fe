App.controller('manage_tags_delete', function($scope, $http, $stateParams, $mdDialog, parentData){
  $scope.item = JSON.parse(parentData);
  
  $scope.cancel = function() {
      $mdDialog.cancel();
  };
  $scope.hide = function(ans) {
    $mdDialog.hide(ans);
  };
})