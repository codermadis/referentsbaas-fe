<script type="text/ng-template" id="manage_tags_delete.tpl">
  <md-dialog aria-label="dialog" style="height: auto;">
    <div class="d-flex py-3 justify-content-between align-items-center flex-shrink-0">
      <h4 class="m-0">Kustuta silt "{{item.attributes.value}}"</h4>
      <md-button aria-label="close modal" ng-click="cancel()" class="md-primary modal-close" md-autofocus><i class="material-icons">close</i></md-button>
    </div>

    <md-dialog-content>
      <p>Oled kindel, et soovid kustutada silti? <span ng-if="item.attributes.count > 0">See on kasutuses {{item.attributes.count}} <span ng-if="item.attributes.count == 1">korra</span><span ng-if="item.attributes.count > 1">korda</span>.</span></p>
    </md-dialog-content>

    <md-dialog-actions class="justify-content-end">
      <md-button class="btn btn-light" ng-click="hide(false)">
        Katkesta
      </md-button>
      <md-button class="btn btn-secondary" ng-click="hide(true)">
        Kustuta
      </md-button>
    </md-dialog-actions>
  </md-dialog>
</script>