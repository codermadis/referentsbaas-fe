App.controller('manage_tags_merge', function($scope, $rootScope, $http, $stateParams, $mdDialog, parentData){
  var data = JSON.parse(parentData);
  $scope.showSummary = false;
  $scope.model = {
    subject: data.item
  };
  
  $scope.mergeTags = function () {
    if(!$scope.model.target || !$scope.model.subject) return alert('no target or no subject');
    var data = {
      subject_id: $scope.model.subject.attributes.id,
      target_id: $scope.model.target.attributes.id
    };
    
    $http({url: settings.prefix + 'class/merge', method: "PUT", data: data}).then(function(response){
      $scope.model.target.attributes.count = response.data.targetCount;
      $scope.hide({ 'target': $scope.model.target, 'error': false, 'message': response.data.message || '' });
    }, function(response){
      $scope.hide({ 'error': true, 'message': response.data.message || '' });
    });
  };
  
  $scope.toggleSummary = function () {
    $scope.showSummary = $scope.showSummary === true ? false : true;
  };
  
  $scope.autocomplete = function (searchText, subject) {
    return data.list.filter(function(candidate){
      return candidate.attributes.value.toLowerCase().includes(searchText.toLowerCase())
      && candidate.attributes.group === subject.attributes.group
      && candidate.attributes.id !== subject.attributes.id;
    });
  };
  
  $scope.cancel = function() { 
    $mdDialog.cancel();
  };
  $scope.hide = function(data) { $mdDialog.hide(data) };
})