<script type="text/ng-template" id="manage_tags_merge.tpl">
  <md-dialog aria-label="dialog" style="height: auto;">
    <div class="d-flex py-3 justify-content-between align-items-center flex-shrink-0">
      <h4 class="m-0">"{{ model.subject.attributes.value }}" ühtlustamine</h4>
      <md-button aria-label="close modal" ng-click="cancel()" class="md-primary modal-close" md-autofocus><i class="material-icons">close</i></md-button>
    </div>

    <md-dialog-content>
      <div style="height: 45px;" ng-if="showSummary === false">
         <md-autocomplete
            ng-model="candidates"
            placeholder="Vali silt millega {{ model.subject.attributes.value }} ühtlustada"
            md-clear-button="false"
            md-min-length="0"
            md-selected-item="model.target"
            md-selected-item-change="toggleSummary()"
            md-require-match=true
            md-item-text="candidate.attributes.value"
            md-search-text="searchText"
            md-items="candidate in autocomplete(searchText, model.subject)">
          <md-item-template>
            <span md-highlight-text="searchText">{{candidate.attributes.value}}</span>
          </md-item-template>
          <md-not-found>
            Silt peab olemas olema duplikaadiga samas kategoorias!
          </md-not-found>
        </md-autocomplete>
      </div>
      <div class="mb-2" ng-if="showSummary === true">
        <p>
          <ul>
            <li>Silt <span class="bold">{{ model.subject.attributes.value }}</span> seotakse jäädavalt kokku sildiga <span class="bold">{{ model.target.attributes.value }}</span>.</li>
            <li>Silt <span class="bold">{{ model.subject.attributes.value }}</span> kustutakse süsteemist.</li>
          </ul>
          <span class="bold red"> <i class="fa fa-exclamation-triangle"></i> Seda toimingut ei saa tagasi võtta! <span>
        </p>
      </div>
    </md-dialog-content>

    <md-dialog-actions class="justify-content-end">
      <md-button class="btn btn-light" ng-click="cancel()">
        Katkesta
      </md-button>
      <md-button ng-if="model.target" class="btn btn-primary" ng-click="mergeTags()">
        Ühtlusta sildid (pöördumatu)
      </md-button>
    </md-dialog-actions>
  </md-dialog>
</script>