App.controller('manage_tags_save', function($scope, $rootScope, $http, $stateParams, $mdDialog, parentData){
  $scope.item = JSON.parse(parentData);
  
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.hide = function(ans) {
    $mdDialog.hide(ans);
  };
})