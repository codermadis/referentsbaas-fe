App.controller("manage_users", function($scope, $rootScope, $http, $location, $timeout, $window) {
  const EMAIL_PATTERN = settings.pattern.email;

  const USER_UPDATED = 'Kasutaja andmed uuendatud!';
  const USER_ADDED = 'Uus kasutaja lisatud';
  const USER_EMAIL_DUPLICATE = 'Sellise e-mailiga kasutaja on juba lisatud!';
  const PLEASE_FILL_ALL_FIELDS = 'Palun täida kõik väljad!';
  const PLEASE_PROVIDE_VALID_FIRST_NAME = 'Palun sisesta sobilik eesnimi!';
  const PLEASE_PROVIDE_VALID_LAST_NAME = 'Palun sisesta sobilik perekonnanimi!';
  const PLEASE_PROVIDE_VALID_EMAIL = "Palun sisesta sobilik e-mail!";
  const MESSAGE_NOT_SENT_ERROR = "E-maili väljastamine kasutajale ebaõnnestus!";
  const EMAIL_NOT_UNIQUE = "Sellise emailiga kasutaja juba eksisteerib!";
  
  $scope.rePositioning = function() {
    $timeout(function() {
      var openContainer = document.querySelector(".top-select.md-active");
      var windowHeight = $(window).height();
      $timeout(function() {
        var openContainerSize = openContainer.offsetTop + openContainer.offsetHeight;
        if (openContainerSize > windowHeight) {
          openContainer.style.top = openContainer.offsetTop - (openContainerSize - windowHeight) - 20 + "px";
        }
      }, 100);
    }, 100);
  };
  $scope.submitLoader = {};
  $scope.newAccount = false;
  $scope.sort = {};
  $scope.searchText = "";
  $scope.accountStatus = [
    { label: "Aktiveeritud", active: true },
    { label: "Deaktiveeritud", active: false },
  ];

  function sortByKey(array, key) {
    return array.sort(function(a, b) {
      var x = (typeof(a.attributes[key]) == 'string') ? a.attributes[key].toLowerCase() : a.attributes[key];
      var y = (typeof(b.attributes[key]) == 'string') ? b.attributes[key].toLowerCase() : b.attributes[key];
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }
  $scope.sortArrowClass = function(key) {
    if ($scope.sort.key === key) return $scope.sort.order === 'desc' ? 'fa fa-sort-down' : 'fa fa-sort-up';
    return 'fa fa-sort';
  };
  $scope.sortAccounts = function(key) {
    if ($scope.sort.key == key) {
      $scope.sort.order = $scope.sort.order === 'asc' ? 'desc' : 'asc';
      return $scope.users = $scope.users.reverse();
    }

    $scope.sort.key = key;
    $scope.sort.order = 'desc';
    $scope.users = sortByKey($scope.users, key);
  };
  $scope.getUserRole = function(id) {
    return $scope.accountRoles.filter(function(item) {
      return parseInt(id, 10) === item._id;
    });
  };

  $scope.bindUser = function(input) {
    if (!input.email || input.active === undefined || !input.role_id) return $scope.giveToast(PLEASE_FILL_ALL_FIELDS, 'toast-error');

    if (EMAIL_PATTERN.test(input.email) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_EMAIL, 'toast-error');

    if (checkForExistingEmail(input.email, $scope.users)) return $scope.giveToast(USER_EMAIL_DUPLICATE, 'toast-error');

    var attrs = {
      email: input.email,
      active: input.active,
      role_id: input.role_id
    };

    var user = new Account(attrs, $scope.users);
    user.save();
    user.array.push(user);

    $scope.newAccount = false;

  };

  function checkForExistingEmail(email, existing) {
    var duplicate = false;

    for (var ii = 0; ii < existing.length; ii++) {
      if (existing[ii].attributes.email.toLowerCase() === email.toLowerCase()) {
        duplicate = true;
        break;
      }
    }
    return duplicate;
  }

  $scope.autocomplete = function(searchText, existing) {

    return $http.get(settings.prefix + 'users/autocomplete?email=' + searchText)
      .then(function(response) {
        var output = [];
        if (existing) {
          for (var i = 0; i < response.data.length; i++) {
            var emailExists = checkForExistingEmail(response.data[i].email, existing)
            if (emailExists === false) output.push(response.data[i]);
          }

        }
        else {
          output = response.data;
        }
        return output;
      });
  };

  function Account(obj, array) {
    this.attributes = obj;
    this._previousAttributes = angular.copy(obj);
    this.array = array;
    this._id = randomId();
  }
  Account.prototype.isModified = function() {
    if (JSON.stringify(this.attributes) != JSON.stringify(this._previousAttributes)) return true;
    else if (!this.hasAttribute('id')) return true;
    else false;
  };
  Account.prototype.hasAttribute = function(attr) { return this.attributes[attr] === undefined ? false : true };
  Account.prototype.save = function() {
    if (EMAIL_PATTERN.test(this.attributes.email) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_EMAIL, 'toast-error');
    if (!this.isUnique('email', $scope.users)) return $scope.giveToast(EMAIL_NOT_UNIQUE, 'toast-error');
    
    var _self = this;
    $scope.submitLoader[_self.attributes.email] = true;
    var requestAttributes;

    if (this.hasAttribute('id')) requestAttributes = { url: settings.prefix + 'accounts/' + this.attributes.id, method: "PUT", data: this.attributes };
    else requestAttributes = { url: settings.prefix + 'accounts', method: "POST", data: this.attributes };

    return $http(requestAttributes)
      .then(function(response) {
        $scope.submitLoader[_self.attributes.email] = false;
        
        if(response.data.errors){
          //EMAIL SERVICE ERROR HANDLING
          if(response.data.account) {
             _self.attributes = response.data.account;
             _self._previousAttributes = angular.copy(response.data.account);
          }
          
          return $scope.giveToast(MESSAGE_NOT_SENT_ERROR, 'toast-warning');
        } else {
          //EMAIL SERVICE WAS NOT USED
          _self.attributes = response.data.account;
          _self._previousAttributes = angular.copy(response.data.account);
  
          if (requestAttributes.method === 'POST') {
            $scope.giveToast(USER_ADDED, 'toast-success');
          }
          else {
            $scope.giveToast(USER_UPDATED, 'toast-success');
          }
        }
      }, function(response) {
        $scope.submitLoader[_self.attributes.email] = false;
        console.error(response.data)
        
      });
  };
  Account.prototype.undo = function() {
    this.attributes = angular.copy(this._previousAttributes);
  };
  
  Account.prototype.isUnique = function (key, list){
    for(var i in list){
      if(this._id != list[i]._id){
        if(list[i].attributes[key].toLowerCase() == this.attributes[key].toLowerCase()) {
          return false;
        }
      }
    }
    return true;
  };
  
  $scope.getData = function() {
    $scope.loader = true;
    $http({ url: settings.prefix + 'accounts', method: "get" })
      .then(function(response) {

        var data = response.data;
        for (var i = 0; i < data.length; i++) data[i] = new Account(data[i], $scope.users);
        $scope.users = data;
        $scope.loader = false;
      }, function(err) {
        console.error(err);
      });
  };

  $scope.getData();

  $scope.toggleNewAccount = function() {
    $scope.newAccount = $scope.newAccount ? false : { active: true };
  };

  $scope.sortIcon = "keyboard_arrow_down";
  $scope.sortLink = function() {
    if ($scope.sortList) {
      $scope.sortList = false;
      $scope.sortIcon = "keyboard_arrow_down";
    }
    else {
      $scope.sortList = true;
      $scope.sortIcon = "keyboard_arrow_up";
    }
  };
  if ($window.innerWidth > 767) {
    $scope.sortList = true;
  }
  angular.element($window).on('resize', function() {
    if ($window.innerWidth > 767) {
      $scope.sortList = true;
    }
    else {
      $scope.sortList = false;
      $scope.sortIcon = "keyboard_arrow_down";
    }
  });
  
  function randomId(){
    //Do differentiate between clients for tabbing
    return Math.random().toString(36).substr(2, 5);
  }

});
