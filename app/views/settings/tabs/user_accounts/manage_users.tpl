<script type="text/ng-template" id="manage_users.tpl">

  <div ng-if="loader" class="d-flex justify-content-center">
    <md-progress-circular md-mode="indeterminate"></md-progress-circular>
  </div> 
  
  <div class="row" ng-if="!loader">
    
    <div class="col-md-12">
      <md-list>
        <div class="d-flex align-items-center col-12 sort-link mb-2">
          <a href="javascript:void(0);" ng-click="sortLink()" class="expand-search">Sorteeri<i class="material-icons">{{sortIcon}}</i>
          </a>
        </div>
        <md-list-item class="row accountsHeader mb-3 mb-md-0" ng-if="sortList">
          <div class="col-12 col-lg-3 col-md-4">
            <div ng-click="sortAccounts('email')">
              EMAIL
              <i ng-class="sortArrowClass('email')" class="ml-1"></i>
            </div>
          </div>
          <div class="col-12 col-lg-3 col-md-4 mt-2 mt-md-0">
            <div ng-click="sortAccounts('role_id')">
              ROLL
              <i ng-class="sortArrowClass('role_id')" class="ml-1"></i>
            </div>
          </div>
          <div class="col-12 col-lg-3 col-md-4 mt-2 mt-md-0">
            <div ng-click="sortAccounts('active')">
              STAATUS
              <i ng-class="sortArrowClass('active')" class="ml-1"></i>
            </div>
          </div>
        </md-list-item>
        <md-list-item ng-repeat="user in users" class="row accountsContent pt-2 pt-md-0" ng-class="{'modifiedRow' : user.isModified(), 'unmodifiedRow': !user.isModified(), 'loadingRow': submitLoader[user.attributes.email] }">
          <div class="col-12 col-lg-3 col-md-4">
            <input class="form-control "ng-model="user.attributes.email" type="text" placeholder="Lisa e-mail" />
          </div>
          <div class="col-12 col-lg-3 col-md-4">
            <md-select aria-label="role" ng-model="user.attributes.role_id" md-container-class="top-select" required md-on-open="rePositioning()">
              <md-option ng-repeat="role in accountRoles" ng-value="role._id">
                {{role.label}}
              </md-option>
            </md-select>
          </div>
          <div class="col-12 col-lg-3 col-md-4">
            <md-select aria-label="status" ng-model="user.attributes.active" md-container-class="top-select" required md-on-open="rePositioning()">
              <md-option ng-repeat="status in accountStatus" ng-value="status.active">
                {{status.label}}
              </md-option>
            </md-select>
          </div>
          <div class="col-12 col-xl-3">
            <div class="d-flex mb-3 mb-xl-0">
              <md-button class="btn btn-light ml-0 saveButton" ng-click="user.undo()" >Katkesta</md-button>
              <md-button class="btn btn-primary ml-3 saveButton"  ng-click="user.save()" >
                Salvesta
              </md-button>
            
              <md-button  aria-label="Saving data" class="btn btn-primary md-raised-loader loader-white" ng-click="null" >
                <md-progress-circular md-mode="indeterminate" md-diameter="24px"></md-progress-circular>
              </md-button>
            </div>
          </div>
        </md-list-item>
        <md-list-item ng-if="newAccount" class="row accountsContent newContent">
          <div class="col-3">
            <md-autocomplete placeholder="Lisa e-mail"
                md-delay=300
                md-clear-button="false"
                md-item-text="item.email"
                md-search-text="searchText"
                md-items="item in autocomplete(searchText, users)">
              <md-item-template>
                  <span md-highlight-text="searchText">{{item.email}}</span>
              </md-item-template>
            </md-autocomplete>
          </div>
          <div class="col-3">
            <md-select placeholder="Vali roll" aria-label="role" ng-model="newAccount.role_id" md-container-class="top-select" required md-on-open="rePositioning()">
              <md-option ng-repeat="role in accountRoles" ng-value="role._id">
                {{role.label}}
              </md-option>
            </md-select>
          </div>
          <div class="col-3">
            <md-select placeholder="Vali staatus" aria-label="status" ng-model="newAccount.active" md-container-class="top-select" required md-on-open="rePositioning()">
              <md-option ng-repeat="status in accountStatus" ng-value="status.active">
                {{status.label}}
              </md-option>
            </md-select>
          </div>
          <div class="col-3">
            <div class="d-flex">
              <md-button ng-click="toggleNewAccount()" class="btn btn-light ml-0">Katkesta</md-button>
              <md-button ng-click="bindUser({email: searchText, role_id: newAccount.role_id, active: newAccount.active})" class="btn btn-primary ml-3">
                Salvesta
              </md-button>
              
            </div>
          </div>
        </md-list-item>
           
      </md-list>
        
      <a href="javascript:void(0);" ng-if="!newAccount" class="add-list-item" ng-click="toggleNewAccount()">
        <b><i class="material-icons">add</i> Lisa kasutaja</b>
      </a>
        
    </div>
  </div>


</script>