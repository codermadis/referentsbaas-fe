var App = angular.module("App", ['mdo-angular-cryptography','ngCookies','ui.router', 'ngAnimate', 'ngSanitize', 'ng-breadcrumbs', 'ngMaterial', 'ngTagsInput', 'ngFileUpload', 'uiBreadcrumbs']);

var BE_URL = window.location.host.includes('c9users') ? "https://twn-referentsbaas-be-raimoj.c9users.io/api/v1/" : "https://referentsid.twn.ee/api/v1/";
const EMAIL_PATTERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const NAME_PATTERN = /^([^0-9]{2,30})$/;

moment.locale('et');

var settings = {
	template: "/dist/templates.html",
	prefix: BE_URL,
	pattern: {
		email: EMAIL_PATTERN,
		name: NAME_PATTERN
	},
	resolveAll: {
		status: function($user) {
			return $user.status();
		},
		tags: function($settings) {
			return $settings.tags();
		},
		translations: function($settings){
			return;
		}
	}
};

App.factory('$settings', function($http, $q, $location, $rootScope, $mdToast, $state) {
    
	return {
		outlookServiceStatus: function(){
			
			if($rootScope.showOutlookAccountWarning === false) return;
			
			$http({url: settings.prefix + 'settings/outlook', method: "get"})
			.then(function(response) {
      	if(response.data.password == false || !response.data.username.length){
      		
	      		if($state.$current.name != 'settings.system'){
	      			$mdToast.show({
			          hideDelay   : false,
			          position    : 'top right',
			          controller  : 'outlook_toast_service',
			          templateUrl : 'outlook_toast_service.tpl',
			          toastClass	: 'toast-warning'
			        });
	      		}
		        $rootScope.showOutlookAccountWarning = true;
	      	} else {
	      		$rootScope.showOutlookAccountWarning = false;
	      		$mdToast.hide();
	      	}
	    }, function(err) {
	      console.error(err);
	    });
		},
		tags: function(){
			var deferred = $q.defer();
			var promise = $http.get(settings.prefix + 'class');

			promise.then(function(response){
			if(response.data){
			  $rootScope.classRegistry = response.data;	
				deferred.resolve(response);
				
			} else { $location.path("/login") }
			
			}, function(error){
				console.log(error.data);
			});
			
			return deferred.promise;
		}
	};
});
App.factory('$converter', function($filter) {
    
	return {
		minutesTohours: function(minutes){
  		var hours = Math.floor( minutes / 60) + "";
      var remaining = (minutes % 60);
      var min = remaining < 10 ? "0" + remaining : "" + remaining;
      return hours+ ':' + min;
		},
		hoursTominutes: function(hours){
		  // 834:45 = 50085
		  var h, min;
      if(!hours.includes(":")) {
        hours = hours.replace(/\D+/g, '');
      	if(!hours.length) return null;
        
        h =  hours;
        min = 0;
      }
      else{
	      h = hours.split(":")[0].replace(/\D+/g, '');
        min = hours.split(":")[1].replace(/\D+/g, '');
        if(!h) h = 0;
        if(!min) min = 0;
      }
    
      return (parseInt(h) * 60) + parseInt(min) ;
		},
		dateToDatabase: function(date){
			return $filter('date')(date, 'yyyy-MM-dd')
		},
		phoneNumberSplit: function(number){
		  //var supported_prefixes = ['+372','+371','+370','+358'];
		  
		  if(!number) return {prefix:"+372", number: ""};
		  
		  var country = SUPPORTED_PREFIXES.find(function(country){
		  	return number.includes(country.dial_code);
		  });
		  
		  if(country) return { prefix: number.substring(0,country.dial_code.length), number: number.substring(country.dial_code.length)};
		  else return {prefix: "", number: number};
		},
		relativeTimestamp: function(timestamp){
			var now = new Date();
	    var date = new Date(timestamp);
	   
	    //Current MINUTE
	    var diff_in_seconds = Math.floor( (now.getTime() - date.getTime()) / 1000);
	    if(diff_in_seconds < 60){
	      if(diff_in_seconds < 2) return diff_in_seconds + ' sekund tagasi';
	      else return diff_in_seconds + ' sekundit tagasi';
	    }
	   
	    //Current HOUR
	    var diff_in_minutes = Math.floor(diff_in_seconds / 60);
	    if(diff_in_minutes < 60){
	      if(diff_in_minutes < 2) return diff_in_minutes + ' minut tagasi';
	      else return diff_in_minutes + ' minutit tagasi';
	    }
	    
	    //Current DAY
	    if((date.getDate() === now.getDate() && date.getMonth() === now.getMonth() && date.getFullYear() === now.getFullYear())){
	      return 'Täna ' + $filter('date')(timestamp, 'HH:mm');
	    }
	    
	    //Current WEEK
	    if(date.getFullYear() === now.getFullYear()){
  			
	    	if($filter('date')(now, "w") === $filter('date')(date, "w")){
	    		return $filter('date')(timestamp, 'EEE HH:mm');
	    	}
	    	
	    } 
	    	
	    //Current MONTH
	    if((date.getMonth() === now.getMonth() && date.getFullYear() === now.getFullYear())){
	      return $filter('date')(timestamp, 'EEE dd.MM');
	    }
	    
	    //ALL THE REST
	    return $filter('date')(timestamp, 'dd.MM.yyyy');
			
		}
	};
});
const SUPPORTED_PREFIXES = [
{
"name": "Afghanistan",
"dial_code": "+93",
"code": "AF"
},
{
"name": "Aland Islands",
"dial_code": "+358",
"code": "AX"
},
{
"name": "Albania",
"dial_code": "+355",
"code": "AL"
},
{
"name": "Algeria",
"dial_code": "+213",
"code": "DZ"
},
{
"name": "AmericanSamoa",
"dial_code": "+1684",
"code": "AS"
},
{
"name": "Andorra",
"dial_code": "+376",
"code": "AD"
},
{
"name": "Angola",
"dial_code": "+244",
"code": "AO"
},
{
"name": "Anguilla",
"dial_code": "+1264",
"code": "AI"
},
{
"name": "Antarctica",
"dial_code": "+672",
"code": "AQ"
},
{
"name": "Antigua and Barbuda",
"dial_code": "+1268",
"code": "AG"
},
{
"name": "Argentina",
"dial_code": "+54",
"code": "AR"
},
{
"name": "Armenia",
"dial_code": "+374",
"code": "AM"
},
{
"name": "Aruba",
"dial_code": "+297",
"code": "AW"
},
{
"name": "Australia",
"dial_code": "+61",
"code": "AU"
},
{
"name": "Austria",
"dial_code": "+43",
"code": "AT"
},
{
"name": "Azerbaijan",
"dial_code": "+994",
"code": "AZ"
},
{
"name": "Bahamas",
"dial_code": "+1242",
"code": "BS"
},
{
"name": "Bahrain",
"dial_code": "+973",
"code": "BH"
},
{
"name": "Bangladesh",
"dial_code": "+880",
"code": "BD"
},
{
"name": "Barbados",
"dial_code": "+1246",
"code": "BB"
},
{
"name": "Belarus",
"dial_code": "+375",
"code": "BY"
},
{
"name": "Belgium",
"dial_code": "+32",
"code": "BE"
},
{
"name": "Belize",
"dial_code": "+501",
"code": "BZ"
},
{
"name": "Benin",
"dial_code": "+229",
"code": "BJ"
},
{
"name": "Bermuda",
"dial_code": "+1441",
"code": "BM"
},
{
"name": "Bhutan",
"dial_code": "+975",
"code": "BT"
},
{
"name": "Bolivia, Plurinational State of Bolivia",
"dial_code": "+591",
"code": "BO"
},
{
"name": "Bonaire, Sint Eustatius and Saba",
"dial_code": "+599",
"code": "BQ"
},
{
"name": "Bosnia and Herzegovina",
"dial_code": "+387",
"code": "BA"
},
{
"name": "Botswana",
"dial_code": "+267",
"code": "BW"
},
{
"name": "Bouvet Island",
"dial_code": "+55",
"code": "BV"
},
{
"name": "Brazil",
"dial_code": "+55",
"code": "BR"
},
{
"name": "British Indian Ocean Territory",
"dial_code": "+246",
"code": "IO"
},
{
"name": "Brunei Darussalam",
"dial_code": "+673",
"code": "BN"
},
{
"name": "Bulgaria",
"dial_code": "+359",
"code": "BG"
},
{
"name": "Burkina Faso",
"dial_code": "+226",
"code": "BF"
},
{
"name": "Burundi",
"dial_code": "+257",
"code": "BI"
},
{
"name": "Cambodia",
"dial_code": "+855",
"code": "KH"
},
{
"name": "Cameroon",
"dial_code": "+237",
"code": "CM"
},
{
"name": "Canada",
"dial_code": "+1",
"code": "CA"
},
{
"name": "Cape Verde",
"dial_code": "+238",
"code": "CV"
},
{
"name": "Cayman Islands",
"dial_code": "+1345",
"code": "KY"
},
{
"name": "Central African Republic",
"dial_code": "+236",
"code": "CF"
},
{
"name": "Chad",
"dial_code": "+235",
"code": "TD"
},
{
"name": "Chile",
"dial_code": "+56",
"code": "CL"
},
{
"name": "China",
"dial_code": "+86",
"code": "CN"
},
{
"name": "Christmas Island",
"dial_code": "+61",
"code": "CX"
},
{
"name": "Cocos (Keeling) Islands",
"dial_code": "+61",
"code": "CC"
},
{
"name": "Colombia",
"dial_code": "+57",
"code": "CO"
},
{
"name": "Comoros",
"dial_code": "+269",
"code": "KM"
},
{
"name": "Congo",
"dial_code": "+242",
"code": "CG"
},
{
"name": "Congo, The Democratic Republic of Congo",
"dial_code": "+243",
"code": "CD"
},
{
"name": "Cook Islands",
"dial_code": "+682",
"code": "CK"
},
{
"name": "Costa Rica",
"dial_code": "+506",
"code": "CR"
},
{
"name": "Cote d'Ivoire",
"dial_code": "+225",
"code": "CI"
},
{
"name": "Croatia",
"dial_code": "+385",
"code": "HR"
},
{
"name": "Cuba",
"dial_code": "+53",
"code": "CU"
},
{
"name": "Curaçao",
"dial_code": "+5999",
"code": "CW"
},
{
"name": "Cyprus",
"dial_code": "+357",
"code": "CY"
},
{
"name": "Czech Republic",
"dial_code": "+420",
"code": "CZ"
},
{
"name": "Denmark",
"dial_code": "+45",
"code": "DK"
},
{
"name": "Djibouti",
"dial_code": "+253",
"code": "DJ"
},
{
"name": "Dominica",
"dial_code": "+1767",
"code": "DM"
},
{
"name": "Dominican Republic",
"dial_code": "+1849",
"code": "DO"
},
{
"name": "Ecuador",
"dial_code": "+593",
"code": "EC"
},
{
"name": "Egypt",
"dial_code": "+20",
"code": "EG"
},
{
"name": "El Salvador",
"dial_code": "+503",
"code": "SV"
},
{
"name": "Equatorial Guinea",
"dial_code": "+240",
"code": "GQ"
},
{
"name": "Eritrea",
"dial_code": "+291",
"code": "ER"
},
{
"name": "Estonia",
"dial_code": "+372",
"code": "EE"
},
{
"name": "Ethiopia",
"dial_code": "+251",
"code": "ET"
},
{
"name": "Falkland Islands (Malvinas)",
"dial_code": "+500",
"code": "FK"
},
{
"name": "Faroe Islands",
"dial_code": "+298",
"code": "FO"
},
{
"name": "Fiji",
"dial_code": "+679",
"code": "FJ"
},
{
"name": "Finland",
"dial_code": "+358",
"code": "FI"
},
{
"name": "France",
"dial_code": "+33",
"code": "FR"
},
{
"name": "French Guiana",
"dial_code": "+594",
"code": "GF"
},
{
"name": "French Polynesia",
"dial_code": "+689",
"code": "PF"
},
{
"name": "French Southern and Antarctic Lands",
"dial_code": "+262",
"code": "TF"
},
{
"name": "Gabon",
"dial_code": "+241",
"code": "GA"
},
{
"name": "Gambia",
"dial_code": "+220",
"code": "GM"
},
{
"name": "Georgia",
"dial_code": "+995",
"code": "GE"
},
{
"name": "Germany",
"dial_code": "+49",
"code": "DE"
},
{
"name": "Ghana",
"dial_code": "+233",
"code": "GH"
},
{
"name": "Gibraltar",
"dial_code": "+350",
"code": "GI"
},
{
"name": "Greece",
"dial_code": "+30",
"code": "GR"
},
{
"name": "Greenland",
"dial_code": "+299",
"code": "GL"
},
{
"name": "Grenada",
"dial_code": "+1473",
"code": "GD"
},
{
"name": "Guadeloupe",
"dial_code": "+590",
"code": "GP"
},
{
"name": "Guam",
"dial_code": "+1671",
"code": "GU"
},
{
"name": "Guatemala",
"dial_code": "+502",
"code": "GT"
},
{
"name": "Guernsey",
"dial_code": "+44",
"code": "GG"
},
{
"name": "Guinea",
"dial_code": "+224",
"code": "GN"
},
{
"name": "Guinea-Bissau",
"dial_code": "+245",
"code": "GW"
},
{
"name": "Guyana",
"dial_code": "+592",
"code": "GY"
},
{
"name": "Haiti",
"dial_code": "+509",
"code": "HT"
},
{
"name": "Heard Island and McDonald Islands",
"dial_code": "+672",
"code": "HM"
},
{
"name": "Holy See (Vatican City State)",
"dial_code": "+379",
"code": "VA"
},
{
"name": "Honduras",
"dial_code": "+504",
"code": "HN"
},
{
"name": "Hong Kong",
"dial_code": "+852",
"code": "HK"
},
{
"name": "Hungary",
"dial_code": "+36",
"code": "HU"
},
{
"name": "Iceland",
"dial_code": "+354",
"code": "IS"
},
{
"name": "India",
"dial_code": "+91",
"code": "IN"
},
{
"name": "Indonesia",
"dial_code": "+62",
"code": "ID"
},
{
"name": "Iran, Islamic Republic of Persian Gulf",
"dial_code": "+98",
"code": "IR"
},
{
"name": "Iraq",
"dial_code": "+964",
"code": "IQ"
},
{
"name": "Ireland",
"dial_code": "+353",
"code": "IE"
},
{
"name": "Isle of Man",
"dial_code": "+44",
"code": "IM"
},
{
"name": "Israel",
"dial_code": "+972",
"code": "IL"
},
{
"name": "Italy",
"dial_code": "+39",
"code": "IT"
},
{
"name": "Jamaica",
"dial_code": "+1876",
"code": "JM"
},
{
"name": "Japan",
"dial_code": "+81",
"code": "JP"
},
{
"name": "Jersey",
"dial_code": "+44",
"code": "JE"
},
{
"name": "Jordan",
"dial_code": "+962",
"code": "JO"
},
{
"name": "Kazakhstan",
"dial_code": "+77",
"code": "KZ"
},
{
"name": "Kenya",
"dial_code": "+254",
"code": "KE"
},
{
"name": "Kiribati",
"dial_code": "+686",
"code": "KI"
},
{
"name": "Korea, Democratic People's Republic of Korea",
"dial_code": "+850",
"code": "KP"
},
{
"name": "Korea, Republic of South Korea",
"dial_code": "+82",
"code": "KR"
},
{
"name": "Kosovo, Republic of Kosovo",
"dial_code": "+383",
"code": "XK"
},
{
"name": "Kuwait",
"dial_code": "+965",
"code": "KW"
},
{
"name": "Kyrgyzstan",
"dial_code": "+996",
"code": "KG"
},
{
"name": "Laos",
"dial_code": "+856",
"code": "LA"
},
{
"name": "Latvia",
"dial_code": "+371",
"code": "LV"
},
{
"name": "Lebanon",
"dial_code": "+961",
"code": "LB"
},
{
"name": "Lesotho",
"dial_code": "+266",
"code": "LS"
},
{
"name": "Liberia",
"dial_code": "+231",
"code": "LR"
},
{
"name": "Libyan Arab Jamahiriya",
"dial_code": "+218",
"code": "LY"
},
{
"name": "Liechtenstein",
"dial_code": "+423",
"code": "LI"
},
{
"name": "Lithuania",
"dial_code": "+370",
"code": "LT"
},
{
"name": "Luxembourg",
"dial_code": "+352",
"code": "LU"
},
{
"name": "Macao",
"dial_code": "+853",
"code": "MO"
},
{
"name": "Macedonia",
"dial_code": "+389",
"code": "MK"
},
{
"name": "Madagascar",
"dial_code": "+261",
"code": "MG"
},
{
"name": "Malawi",
"dial_code": "+265",
"code": "MW"
},
{
"name": "Malaysia",
"dial_code": "+60",
"code": "MY"
},
{
"name": "Maldives",
"dial_code": "+960",
"code": "MV"
},
{
"name": "Mali",
"dial_code": "+223",
"code": "ML"
},
{
"name": "Malta",
"dial_code": "+356",
"code": "MT"
},
{
"name": "Marshall Islands",
"dial_code": "+692",
"code": "MH"
},
{
"name": "Martinique",
"dial_code": "+596",
"code": "MQ"
},
{
"name": "Mauritania",
"dial_code": "+222",
"code": "MR"
},
{
"name": "Mauritius",
"dial_code": "+230",
"code": "MU"
},
{
"name": "Mayotte",
"dial_code": "+262",
"code": "YT"
},
{
"name": "Mexico",
"dial_code": "+52",
"code": "MX"
},
{
"name": "Micronesia, Federated States of Micronesia",
"dial_code": "+691",
"code": "FM"
},
{
"name": "Moldova",
"dial_code": "+373",
"code": "MD"
},
{
"name": "Monaco",
"dial_code": "+377",
"code": "MC"
},
{
"name": "Mongolia",
"dial_code": "+976",
"code": "MN"
},
{
"name": "Montenegro",
"dial_code": "+382",
"code": "ME"
},
{
"name": "Montserrat",
"dial_code": "+1664",
"code": "MS"
},
{
"name": "Morocco",
"dial_code": "+212",
"code": "MA"
},
{
"name": "Mozambique",
"dial_code": "+258",
"code": "MZ"
},
{
"name": "Myanmar",
"dial_code": "+95",
"code": "MM"
},
{
"name": "Namibia",
"dial_code": "+264",
"code": "NA"
},
{
"name": "Nauru",
"dial_code": "+674",
"code": "NR"
},
{
"name": "Nepal",
"dial_code": "+977",
"code": "NP"
},
{
"name": "Netherlands",
"dial_code": "+31",
"code": "NL"
},
{
"name": "Netherlands Antilles",
"dial_code": "+599",
"code": "AN"
},
{
"name": "New Caledonia",
"dial_code": "+687",
"code": "NC"
},
{
"name": "New Zealand",
"dial_code": "+64",
"code": "NZ"
},
{
"name": "Nicaragua",
"dial_code": "+505",
"code": "NI"
},
{
"name": "Niger",
"dial_code": "+227",
"code": "NE"
},
{
"name": "Nigeria",
"dial_code": "+234",
"code": "NG"
},
{
"name": "Niue",
"dial_code": "+683",
"code": "NU"
},
{
"name": "Norfolk Island",
"dial_code": "+672",
"code": "NF"
},
{
"name": "Northern Mariana Islands",
"dial_code": "+1670",
"code": "MP"
},
{
"name": "Norway",
"dial_code": "+47",
"code": "NO"
},
{
"name": "Oman",
"dial_code": "+968",
"code": "OM"
},
{
"name": "Pakistan",
"dial_code": "+92",
"code": "PK"
},
{
"name": "Palau",
"dial_code": "+680",
"code": "PW"
},
{
"name": "Palestinian Territory, Occupied",
"dial_code": "+970",
"code": "PS"
},
{
"name": "Panama",
"dial_code": "+507",
"code": "PA"
},
{
"name": "Papua New Guinea",
"dial_code": "+675",
"code": "PG"
},
{
"name": "Paraguay",
"dial_code": "+595",
"code": "PY"
},
{
"name": "Peru",
"dial_code": "+51",
"code": "PE"
},
{
"name": "Philippines",
"dial_code": "+63",
"code": "PH"
},
{
"name": "Pitcairn",
"dial_code": "+870",
"code": "PN"
},
{
"name": "Poland",
"dial_code": "+48",
"code": "PL"
},
{
"name": "Portugal",
"dial_code": "+351",
"code": "PT"
},
{
"name": "Puerto Rico",
"dial_code": "+1939",
"code": "PR"
},
{
"name": "Qatar",
"dial_code": "+974",
"code": "QA"
},
{
"name": "Romania",
"dial_code": "+40",
"code": "RO"
},
{
"name": "Russia",
"dial_code": "+7",
"code": "RU"
},
{
"name": "Rwanda",
"dial_code": "+250",
"code": "RW"
},
{
"name": "Reunion",
"dial_code": "+262",
"code": "RE"
},
{
"name": "Saint Barthelemy",
"dial_code": "+590",
"code": "BL"
},
{
"name": "Saint Helena, Ascension and Tristan Da Cunha",
"dial_code": "+290",
"code": "SH"
},
{
"name": "Saint Kitts and Nevis",
"dial_code": "+1869",
"code": "KN"
},
{
"name": "Saint Lucia",
"dial_code": "+1758",
"code": "LC"
},
{
"name": "Saint Martin",
"dial_code": "+590",
"code": "MF"
},
{
"name": "Saint Pierre and Miquelon",
"dial_code": "+508",
"code": "PM"
},
{
"name": "Saint Vincent and the Grenadines",
"dial_code": "+1784",
"code": "VC"
},
{
"name": "Samoa",
"dial_code": "+685",
"code": "WS"
},
{
"name": "San Marino",
"dial_code": "+378",
"code": "SM"
},
{
"name": "Sao Tome and Principe",
"dial_code": "+239",
"code": "ST"
},
{
"name": "Saudi Arabia",
"dial_code": "+966",
"code": "SA"
},
{
"name": "Senegal",
"dial_code": "+221",
"code": "SN"
},
{
"name": "Serbia",
"dial_code": "+381",
"code": "RS"
},
{
"name": "Seychelles",
"dial_code": "+248",
"code": "SC"
},
{
"name": "Sierra Leone",
"dial_code": "+232",
"code": "SL"
},
{
"name": "Singapore",
"dial_code": "+65",
"code": "SG"
},
{
"name": "Sint Maarten",
"dial_code": "+1721",
"code": "SX"
},
{
"name": "Slovakia",
"dial_code": "+421",
"code": "SK"
},
{
"name": "Slovenia",
"dial_code": "+386",
"code": "SI"
},
{
"name": "Solomon Islands",
"dial_code": "+677",
"code": "SB"
},
{
"name": "Somalia",
"dial_code": "+252",
"code": "SO"
},
{
"name": "South Africa",
"dial_code": "+27",
"code": "ZA"
},
{
"name": "South Sudan",
"dial_code": "+211",
"code": "SS"
},
{
"name": "South Georgia and the South Sandwich Islands",
"dial_code": "+500",
"code": "GS"
},
{
"name": "Spain",
"dial_code": "+34",
"code": "ES"
},
{
"name": "Sri Lanka",
"dial_code": "+94",
"code": "LK"
},
{
"name": "Sudan",
"dial_code": "+249",
"code": "SD"
},
{
"name": "Suriname",
"dial_code": "+597",
"code": "SR"
},
{
"name": "Svalbard and Jan Mayen",
"dial_code": "+47",
"code": "SJ"
},
{
"name": "Swaziland",
"dial_code": "+268",
"code": "SZ"
},
{
"name": "Sweden",
"dial_code": "+46",
"code": "SE"
},
{
"name": "Switzerland",
"dial_code": "+41",
"code": "CH"
},
{
"name": "Syrian Arab Republic",
"dial_code": "+963",
"code": "SY"
},
{
"name": "Taiwan",
"dial_code": "+886",
"code": "TW"
},
{
"name": "Tajikistan",
"dial_code": "+992",
"code": "TJ"
},
{
"name": "Tanzania, United Republic of Tanzania",
"dial_code": "+255",
"code": "TZ"
},
{
"name": "Thailand",
"dial_code": "+66",
"code": "TH"
},
{
"name": "Timor-Leste",
"dial_code": "+670",
"code": "TL"
},
{
"name": "Togo",
"dial_code": "+228",
"code": "TG"
},
{
"name": "Tokelau",
"dial_code": "+690",
"code": "TK"
},
{
"name": "Tonga",
"dial_code": "+676",
"code": "TO"
},
{
"name": "Trinidad and Tobago",
"dial_code": "+1868",
"code": "TT"
},
{
"name": "Tunisia",
"dial_code": "+216",
"code": "TN"
},
{
"name": "Turkey",
"dial_code": "+90",
"code": "TR"
},
{
"name": "Turkmenistan",
"dial_code": "+993",
"code": "TM"
},
{
"name": "Turks and Caicos Islands",
"dial_code": "+1649",
"code": "TC"
},
{
"name": "Tuvalu",
"dial_code": "+688",
"code": "TV"
},
{
"name": "Uganda",
"dial_code": "+256",
"code": "UG"
},
{
"name": "Ukraine",
"dial_code": "+380",
"code": "UA"
},
{
"name": "United Arab Emirates",
"dial_code": "+971",
"code": "AE"
},
{
"name": "United Kingdom",
"dial_code": "+44",
"code": "GB"
},
{
"name": "United States",
"dial_code": "+1",
"code": "US"
},
{
"name": "United States Minor Outlying Islands",
"dial_code": "+1581",
"code": "UM"
},
{
"name": "Uruguay",
"dial_code": "+598",
"code": "UY"
},
{
"name": "Uzbekistan",
"dial_code": "+998",
"code": "UZ"
},
{
"name": "Vanuatu",
"dial_code": "+678",
"code": "VU"
},
{
"name": "Venezuela, Bolivarian Republic of Venezuela",
"dial_code": "+58",
"code": "VE"
},
{
"name": "Viet Nam",
"dial_code": "+84",
"code": "VN"
},
{
"name": "Virgin Islands, British",
"dial_code": "+1284",
"code": "VG"
},
{
"name": "Virgin Islands, U.S.",
"dial_code": "+1340",
"code": "VI"
},
{
"name": "Wallis and Futuna",
"dial_code": "+681",
"code": "WF"
},
{
"name": "Western Sahara",
"dial_code": "+212",
"code": "EH"
},
{
"name": "Yemen",
"dial_code": "+967",
"code": "YE"
},
{
"name": "Zambia",
"dial_code": "+260",
"code": "ZM"
},
{
"name": "Zimbabwe",
"dial_code": "+263",
"code": "ZW"
}
];
App.factory('$dummyData', function() {
    
	return {
		table: function(row, col, wordCount, wordLength){
		  var wc = wordCount || 1;
		  var wl = wordLength || 4;
		  var word = "";
		  for(var w = 0; w < wc; w++){
		    word += '#'.repeat(wl) + " ";
		  }
		  var output = [];
  		for(var r = 0; r < row; r++){
        var obj = {};
        for(var c = 0; c < col; c++){
          obj[ 'key' + c ] = word;
        }
        output.push(obj);
      }
      return output;
		}
		
	};
});
App.factory('$templateCache', function($cacheFactory, $http, $injector) {
  var cache = $cacheFactory('templates');
  var allTplPromise;

  return {
    get: function(url) {
      var fromCache = cache.get(url);

      // already have required template in the cache
      if (fromCache) {
        return fromCache;
      }

      // first template request ever - get the all tpl file
      if (!allTplPromise) {
        allTplPromise = $http.get(settings.template).then(function(response) {
          // compile the response, which will put stuff into the cache
          $injector.get('$compile')(response.data);
          return response;
        });
      }
      
      // return the all-tpl promise to all template requests
      return allTplPromise.then(function(response) {
        return {
          status: response.status,
          data: cache.get(url),
          headers: response.headers
        };
      });
    },

    put: function(key, value) {
      cache.put(key, value);
    }
  };
});
App.factory("$user", function($http, $q, $location, $rootScope, $mdToast, $state, $settings) {
  
  return {
    status: function() {
      var deferred = $q.defer();
      var promise = $http.get(settings.prefix + 'status');

      if ($rootScope.userData != undefined) {
        
        if ($rootScope.userData.role_id === 1) $settings.outlookServiceStatus();
        
        deferred.resolve();
        
      } else {
        promise.then(function(response) {
          if (response.data) {
            
            $rootScope.userData = response.data;
            
            if ($rootScope.userData.role_id === 1) $settings.outlookServiceStatus();
            
            deferred.resolve();
            
          } else { $location.path("/login") }
          
        }, function() { $location.path("/login") });
      }

      return deferred.promise;
    },

    login: function(email, password) {
      var deferred = $q.defer();
      var promise = $http({method: "POST", url: settings.prefix+"login", data: { email: email, password: password } })
      
      promise.then(function(response) {
        
        $rootScope.userData = response.data.user;
        
  			if($rootScope.userData.role_id === 1) $rootScope.showOutlookAccountWarning = true;
        
        deferred.resolve(response);
  		}, function (response){ 
  		  deferred.reject(response);
  		});
  		return deferred.promise;
    },

    logout: function() {
      $http({ method: 'PUT', url: settings.prefix + 'logout'}).then(function( response ){
        $mdToast.hide(); //remove any toasts
  			
  			$state.go('login');
  		
  		}, function(response){ console.log(response) });
    }
    
  }; //End of returned object
  
});
App.factory('$verifyProjectData', function($http, $location, $rootScope) {
    
	return {
		isDataComplete: function(project_id){
		  return new Promise(function(resolve, reject){
		    if(!project_id) return resolve(false);
		  
    		$http.get(settings.prefix + 'projects/' + project_id +'/status').then(function(response){
          
          return resolve(response.data);
          
        }, function(error){
          
          return reject(error);
          
        });
		  });
		 
		}
	};
	
});
App.directive("toalertMessage", ['$timeout', function($timeout) {
    return {
        restrict: "E",
        replace: true,
        scope: {
            'toalert': '='
        },
        template: 
            '<div class="row fade-element-in">' +
                '<div class="col-12">' +
                    '<div class="alert alert-success" role="alert"> {{ toalert }} </div>' +
                '</div>' +
            '</div>',
        link: function (scope, elem, attrs) {
            scope.$watch('toalert', function (newValue, oldValue, scope) {
                $timeout(function () {
                    scope.$parent.messages.alertMessage = false;
                    scope.$parent.messages.generalAlertMessage = false;
                    scope.$parent.updatedWorkdays = false;
                }, 5000);       
            });
        }
    };
}]);
App.directive("checkdone", function($timeout) {
    return {
        restrict: "E",
        replace: false,
        scope: true,
        template: '<svg ng-if="displaySvg" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2">' +
                    '<polyline class="path check" fill="none" stroke="#2E9F63" stroke-width="20" stroke-linecap="round" stroke-miterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 "/>' +
                  '</svg>',
        link: function($scope, $elem, $attrs){
            
            
            $scope.displaySvg = true;
            
            if( $attrs.timeout !== undefined ){
                //console.log("hiding");
                $timeout(function(){
                    //console.log("time out");
                    $scope.displaySvg = false;
                    $scope.$apply();
                }, parseInt( $attrs.timeout ) );   
            }
            /*
            if ($scope.saveDoneDebounce) {
                clearTimeout($scope.saveDoneDebounce);
            }
        
            $scope.saveDoneDebounce = setTimeout(function() {
                $scope.displaySvg = false;
                $scope.$apply();
            }, $attrs.timeout);
            */
        }
    };
});
App.directive("errorMessage", ['$timeout', function($timeout){
    return {
        restrict: "E",
        replace: true,
        scope: {
            'error': '=',
            'fadeout': '='
        },
        template: 
            '<div class="my-error animate-if-error">' +
                '<div>' +
                    '<div class="alert alert-danger" role="alert"> {{ error }} </div>' +
                '</div>' +
            '</div>',
        link: function (scope, elem, attrs) {
            if(scope.fadeout) {
                scope.$watch('error', function (newValue, oldValue, scope) {
                    $timeout(function () {
                        scope.$parent.messages.errorMessage = false;
                    }, 5000);       
                });
            };
        }
    };
}]);

App.directive("modal", function($mdDialog, $location, $timeout, $rootScope) {
  return {
    restrict: "EA",
    scope: true,
    link: function ($scope, $elem, $attrs) {
      const TAG_MERGE_SUCCESS = 'Duplikaat-silt ühtlustatud!';
      
      $scope.openModal = function ($event){
        $rootScope.importDialog = true;
        $mdDialog.show({
          controller: $attrs.controller,
          templateUrl:  $attrs.templateUrl,
          parent: angular.element(document.querySelector( 'body' )),
          targetEvent: $event,
          clickOutsideToClose:false,
          locals: {parentData : $attrs.locals || {}},
          bindToController: $attrs.locals ? true : false
        }).finally(function(){
          $rootScope.importDialog = false;
        });
      };
      
      $scope.openTagConfirm = function ($event, item){
      
        $mdDialog.show({
          controller: $attrs.controller,
          templateUrl:  $attrs.templateUrl,
          parent: angular.element(document.querySelector( 'body' )),
          targetEvent: $event,
          clickOutsideToClose:false,
          locals: {parentData : $attrs.locals || {}},
          bindToController: $attrs.locals ? true : false
        }).then(function(boolean) {
          if(boolean === true){
            item.save();
          } else {
            item.undo();
          }
        }, function() {
          item.undo();
        });
      };
      
      $scope.mergeTag = function ($event, subject, list) {
        $mdDialog.show({
          controller: 'manage_tags_merge',
          templateUrl: 'manage_tags_merge.tpl',
          parent: angular.element(document.querySelector( 'body' )),
          targetEvent: $event,
          clickOutsideToClose: false,
          locals: { parentData : $attrs.locals || {} },
          bindToController: $attrs.locals ? true : false
        }).then(function(data){
          if(!data.error && data.target) {
            var target = list.find(function(tag){ return tag.attributes.id === data.target.attributes.id });
            if(target) {
              console.log('target found!');
              target.attributes.count = data.target.attributes.count;
              target._previousAttributes = angular.copy(target.attributes);
              $scope.giveToast(TAG_MERGE_SUCCESS, 'toast-success');
            }
            subject.removeFromList();
            
          } else {
            console.log(data);
          }
        });
      };
      
      $scope.openTagDelete = function ($event, item){
        $mdDialog.show({
          controller: $attrs.controller,
          templateUrl:  $attrs.templateUrl,
          parent: angular.element(document.querySelector( 'body' )),
          targetEvent: $event,
          clickOutsideToClose:false,
          locals: {parentData : $attrs.locals || {}},
          bindToController: $attrs.locals ? true : false
        }).then(function(boolean) {
          if(boolean === true){
            item.remove();
          } else {
            item.undo();
          }
        }, function() {
          item.undo();
        });
      };
      
    }
  };
});
App.directive('myLink', function() {
  return {
    restrict: 'A',
    scope: {
      enabled: '=myLink'
    },
    link: function(scope, element, attrs) {
      element.bind('click', function(event) {
        if(!scope.enabled) {
          event.preventDefault();
        }
      });
    }
  };
});

App.filter('naturalSort',function(){
    function naturalSort (a, b) {
       var re = /(^-?[0-9]+(\.?[0-9]*)[df]?e?[0-9]?$|^0x[0-9a-f]+$|[0-9]+)/gi,
            sre = /(^[ ]*|[ ]*$)/g,
            dre = /(^([\w ]+,?[\w ]+)?[\w ]+,?[\w ]+\d+:\d+(:\d+)?[\w ]?|^\d{1,4}[\/\-]\d{1,4}[\/\-]\d{1,4}|^\w+, \w+ \d+, \d{4})/,
            hre = /^0x[0-9a-f]+$/i,
            ore = /^0/,
            i = function(s) { return naturalSort.insensitive && (''+s).toLowerCase() || ''+s },
            // convert all to strings strip whitespace
            x = i(a).replace(sre, '') || '',
            y = i(b).replace(sre, '') || '',
            // chunk/tokenize
            xN = x.replace(re, '\0$1\0').replace(/\0$/,'').replace(/^\0/,'').split('\0'),
            yN = y.replace(re, '\0$1\0').replace(/\0$/,'').replace(/^\0/,'').split('\0'),
            // numeric, hex or date detection
            xD = parseInt(x.match(hre)) || (xN.length != 1 && x.match(dre) && Date.parse(x)),
            yD = parseInt(y.match(hre)) || xD && y.match(dre) && Date.parse(y) || null,
            oFxNcL, oFyNcL;
        // first try and sort Hex codes or Dates
        if (yD)
            if ( xD < yD ) return -1;
        else if ( xD > yD ) return 1;
        // natural sorting through split numeric strings and default strings
        for(var cLoc=0, numS=Math.max(xN.length, yN.length); cLoc < numS; cLoc++) {
            // find floats not starting with '0', string or 0 if not defined (Clint Priest)
            oFxNcL = !(xN[cLoc] || '').match(ore) && parseFloat(xN[cLoc]) || xN[cLoc] || 0;
            oFyNcL = !(yN[cLoc] || '').match(ore) && parseFloat(yN[cLoc]) || yN[cLoc] || 0;
            // handle numeric vs string comparison - number < string - (Kyle Adams)
            if (isNaN(oFxNcL) !== isNaN(oFyNcL)) { return (isNaN(oFxNcL)) ? 1 : -1; }
            // rely on string comparison if different types - i.e. '02' < 2 != '02' < '2'
            else if (typeof oFxNcL !== typeof oFyNcL) {
                oFxNcL += '';
                oFyNcL += '';
            }
            if (oFxNcL < oFyNcL) return -1;
            if (oFxNcL > oFyNcL) return 1;
        }
        return 0;
    }
    return function(arrInput) {
        var arr = arrInput.sort(function(a, b) {
            return naturalSort(a.name,b.name);
        });
        return arr;
    }
});
/*
    @input
    {
        pageCount: Int,
        page: Int
    }
*/
App.directive("paginator", function($timeout, $location) {
    return {
        restrict: "AEC",
        scope: true,
        templateUrl: 'paginator.tpl',
        link: function ($scope, $elem, $attrs) {
            
            
            $scope.changePage = function(params){
                
                if( $attrs.scope !== undefined ){
                    $scope.$parent.changePage( params.page );
                }else{
                    var searchParams = $location.search();
                    if(params.page != undefined && params.page > 0) searchParams.page = params.page;
                    else delete searchParams.page;   
                    $location.search( searchParams );
                }
                
            }
            
            $scope.construct = function(){

                var pagination = JSON.parse( $attrs.data );
                
                var pages = [];
        
                if(pagination.pageCount > 1){
                var range = 5;
                var before = 2, after = 2;
                var start, stop;
                if(pagination.pageCount > range){
                    var page = pagination.page;
                
                    start = page - before > 0 ? page - before : 1;
                    
                    stop = page + after < pagination.pageCount ? page + after : pagination.pageCount;
                    
                    
                    if( stop - page < after ){
                        var detuct = (stop - page - after)*(-1);
                        start-= detuct;
                    }
                    
                    if( page - before <= 0 ){
                       var add = (page - before - start)*(-1); 
                       stop+= add;
                    }  
                } else {
                    start = 1;
                    stop = pagination.pageCount
                }
                    
                
                for(var i = start; i <= stop; i++){pages.push(i)}
                $scope.startPage = start;
                $scope.stopPage = stop;
                }
                
                $scope.pages = pages;
                
            }
            
            $scope.$watch(function(){
                return $attrs.data
            }, function(){
                if( $attrs.data !== ""){
                    $scope.construct(); 
                }
            }, false);
            
        }
    };
});
/*!
 * @module ui-router-breadcrumbs
 * @description A Simple directive that creates breadcrumbs on the fly for AngularJs pages using angular-ui-router
 * @version v1.2.2
 * @link https://github.com/Sibiraj-S/ui-router-breadcrumbs#readme
 * @licence MIT License, https://opensource.org/licenses/MIT
 */
(function() {
    "use strict";
    var r, a, t;
    r = function() {
        var a;
        return a = !1, {
            setAbstract: function(r) {
                r = JSON.parse(r), a = r
            },
            $get: function() {
                return a
            }
        }
    }, a = function(c, u) {
        var a;
        return a = function(r) {
            var a, t, e, n;
            for (t in r = r || u, a = [], n = [], e = [], c.$current.includes) n.push(t);
            for (t in n) "" !== n[t] && "" !== c.get(n[t]).$$state().parent.self.name && e.push(c.get(n[t]).$$state().parent.self);
            if (r) a = e;
            else
                for (t in e) e[t].abstract || a.push(e[t]);
            return a.push(c.current), a
        }, {
            getbreadcrumbs: function(r) {
                return a(r)
            }
        }
    }, (t = function(r, a, c) {
        return {
            restrict: "E",
            transclude: !0,
            template: '<nav aria-label="breadcrumb">  <ol class="breadcrumb">  <li class="breadcrumb-item"><a ui-sref="dashboard">Töölaud</a></li>  <li class="breadcrumb-item" ng-repeat="data in $breadcrumbs" ui-sref-active="active">      <a ui-sref="{{data.abstract || data.name}}" ng-class="{\'last-item\': data.abstract || $last }">{{data.data.label || data.name}}</a>    </li>  </ol></nav>',
            link: function(r, a, t) {
                var e, n;
                t.abstract = !!t.abstract && t.abstract, e = JSON.parse(t.abstract), n = function() {
                    r.$breadcrumbs !== c.getbreadcrumbs(e) && (r.$breadcrumbs = c.getbreadcrumbs(e))
                }, r.$on("$viewContentLoaded", function() {
                    n()
                })
            }
        }
    }).$inject = ["$state", "$rootScope", "breadcrumbsService"], a.$inject = ["$state", "breadcrumbconfig"], angular.module("uiBreadcrumbs", ["ui.router", "ngSanitize"]).directive("uiBreadcrumb", t).provider("breadcrumbconfig", r).factory("breadcrumbsService", a)
}).call(this);
//# sourceMappingURL=ui-router-breadcrumbs.min.js.map

App.config(function ($compileProvider, $locationProvider, $httpProvider){
   
   $locationProvider.html5Mode({
  		enabled: true,
  		requireBase: false
	});
	
	
   $httpProvider.defaults.withCredentials = true;
   
   $httpProvider.interceptors.push(function($q, $location, $rootScope, $interval) {
      return {
         'responseError': function(response) {
            if(response.status === 404) {
               return $q.reject(response);
            } 
            if(response.status === 401) {
               $location.path('/login');
               return $q.reject(response);
            } 
            if(response.status === 412) {
               return $q.reject(response);
            } 
            if(response.status === 400) {
               return $q.reject(response);
            } 
            if(response.status === 403) {
               return $q.reject(response);
            } 
         }
      };       
   });

});
App.config(['$cryptoProvider', function($cryptoProvider){
    $cryptoProvider.setCryptographyKey('zuperSecretKey4C00kies');
}]);
App.config(function($mdDateLocaleProvider){
   $mdDateLocaleProvider.firstDayOfWeek = 1;
   $mdDateLocaleProvider.formatDate = function(date) {
      var m = moment(date);
      return m.isValid() ? m.format('L') : '';
   };
   $mdDateLocaleProvider.parseDate = function(dateString) {
      var m = moment(dateString, 'L', true);
      return m.isValid() ? m.toDate() : new Date(NaN);
   };
});
App.controller('mainCtrl', function ($scope, $http, $rootScope, $state, $sanitize, $location, $mdDateLocale, $filter, $transitions, $mdToast, $user) {   
	 
	//$rootScope.stateLabel = { "finished": "Lõpetatud", "unfinished": "Töös" };
	// $mdDateLocale.formatDate = function(date) {
	// 	if(date) { return $filter('date')(date, 'd MMM, y'); }
	// 	else return "";
	// };
	$rootScope.importStatus = { importInProgress : false, projectsImported : false };
	
	
	$rootScope.headerless = true;
	
	$transitions.onEnter({ }, function(trans) {
    $rootScope.headerless =  false;
 		window.scrollTo(0, 0);
	});
  
	$rootScope.toastNotification = function(opts) {
		switch(opts.type){
			case 'simple': {
				$mdToast.show(
					$mdToast.simple().textContent(opts.message ||'').position(opts.position || 'bottom right')
					  .hideDelay(opts.hideDelay || 3000).toastClass(opts.class || '')
				); break;
			}
			case 'custom': {
				alert('Custom toast coming soon...');
			}
		}
	}
	
	$rootScope.getClasses = function() {
		$http.get(settings.prefix + 'class').then(function(response){
			$rootScope.classRegistry = response.data;	
		});
	};
	
	$rootScope.logOut = function(){
		$user.logout();
	};
	
});
App.filter('hoursTominutes', function($converter) {
    return function(input) { return $converter.hoursTominutes(input) };
});
App.filter('language', function() {
  return function(language) {
    
    switch (language) {
      case 'et':
        return "eesti keel";
        break;
      case 'en':
        return "inglise keel";
        break;
      case 'ru':
        return "vene keel";
        break;
    }
    
  }
});
App.filter('minutesTohours', function($converter) {
    return function(input) { return $converter.minutesTohours(input) };
});
App.filter('relativeTimestamp', function($converter) {
    return function(input) { return $converter.relativeTimestamp(input) };
});
App.filter('truncate', function() {
  return function(text, length, end) {
    if (isNaN(length))
      length = 10;

    if (end === undefined)
      end = "...";

    if (text.length <= length || text.length - end.length <= length) {
      return text;
    } else {
      return String(text).substring(0, length - end.length) + end;
    }

  };
});
App.controller('header', function($scope, $mdDialog, $http, $rootScope, $location) {
  $scope.menuToggle = function(){
    $scope.menuContent = !$scope.menuContent;
  };
  if($rootScope.userData){
    $scope.projectAdd = $rootScope.userData.role_id > 2 ? true : false;
  }
  
});
App.controller('help-modal', function($scope, $http, $stateParams, $mdDialog){
  $scope.hide = function() {
    $mdDialog.hide();
  };
});
App.controller('import', function($scope, $mdDialog, $anchorScroll, $http, $rootScope, $location, $state, $dummyData, $timeout) {
  var PER_PAGE = 20;
  $scope.import_list = {};
  $scope.project_group = {};
  
  
  $scope.dummyPlaceholderData = $dummyData.table(PER_PAGE, 5, 1, 8);
  $scope.search_attributes = {
    archived: false,
    unbillable: false
  };
  
  $scope.hide = function() {
    $mdDialog.hide();
  };
  
  $scope.changePage = function(page_nr){$scope.search(page_nr)};

  $scope.search = function (page) {
    $scope.loader = true;
    $scope.showData = false;
    var str = "";
    
    if($scope.search_attributes.archived == true) str += '&enabled=false';
    if($scope.search_attributes.unbillable == true) str += '&billable=false';
    if($scope.search_attributes.project_group_name)  str += '&project_group_name=' + $scope.search_attributes.project_group_name;
    
    if($scope.search_attributes.name) str += '&name=' + $scope.search_attributes.name;
    if(page) str += '&page=' + page;
    
    $http({ url: settings.prefix + 'projects/import/preview?per_page='+ PER_PAGE + str, method: 'get' })
    .then(function(response){
      $scope.loader = false;
      $scope.showData = true;
      $scope.projects = response.data.list;
      $scope.pagination = {
        page: response.data.page,
        pageCount: response.data.last_page
      };
      //console.log(response.data);
    }, function(err){
      $scope.loader = false;
      console.error(err);
    });
  };
  $scope.search(); //initial list
  
 
  
  $scope.import = function (){
    if($scope.importLoader === true) return console.log('Import already underway');
   
    //console.log($scope.import_list);
    var filtered_ids = [];
    for(var id in $scope.import_list){
      if($scope.import_list[id] === true) filtered_ids.push(id);
    }
   
    if(!filtered_ids.length) return console.log('No projects selected');
    
    var projectIdsCsv = "";//'billable='+ $scope.freckle.billable + '&enabled='+ enabled;
      for(var i in filtered_ids){
         //str += '&' + key + '=' + $scope.freckle[key];
        if(i == 0) projectIdsCsv +=  filtered_ids[i];
        else projectIdsCsv += ',' + filtered_ids[i];

    }
    
    $http({url: settings.prefix + 'projects/import?project_ids=' + projectIdsCsv, method: 'get' })
      .then(function(response){
        $scope.import_list = {};
        initImportState();
      }, function(err){
        console.error(err);
      });
  }; //import
  
  function initImportState (){
      $scope.importProgress = 0;
      $scope.importLoader = true;
      $scope.showData = false;
  }
  
  function watchImportState (){
    $http.get(settings.prefix + 'projects/import/status').then(function(response){
      
      if($rootScope.importDialog) {
        
        if(response.data.status == 'UNDERWAY'){
          if($scope.importLoader == false || $scope.importLoader == undefined){
             initImportState();
          }
          
          $scope.importProgress = response.data.progress;
        } else if(response.data.status == 'READY' &&  $scope.importLoader == true) {
          $scope.importProgress = 100;
          
          $timeout(function(){
            $scope.showData = true;
            $scope.importLoader = false;
            $location.search({});
            if($location.path() == "/toolaud/minu_projektid") $state.reload();
            else $location.path("/toolaud/minu_projektid");
            
            $scope.hide();
          }, 1000);
        }
        $timeout(function() {
            watchImportState();
        }, 1000);
      }
    }, function(err){
      console.error(err);
    });
  }
  watchImportState();
  
  $scope.createProjectManually = function(){
    
    if($state.$current.name === 'projects.general'){
      $state.go($state.current, {id:'loo'}, {reload: true});
    } else {
      $state.go('projects.general', {id:'loo'});
    }
    $scope.hide();
  }
});
App.controller("outlook_toast_service", function($scope, $rootScope, $http, $location, $mdToast){
  $scope.closeToast = function() {
    $mdToast.hide();
  };
});
App.config(function($stateProvider) {
  $stateProvider
    .state({
      name: "exported_cv",
      url: "/exported_cv",
      templateUrl: "cv.view.tpl",
      controller: "cv.view",
      label: 'cv',
      resolve: settings.resolveAll,
    });
});

App.controller('cv.view', function($scope){
    $('.modal-backdrop').removeClass('modal-backdrop fade show');
})
App.controller("dashboard", function($scope, $rootScope, $http, $location, $filter, $timeout, $q, $log){
    $scope.searchTabs = [
        {_id: 0, min_role_id: 3, name:'Otsi projekti', hash:'otsi_projekti', template:"search_projects.tpl"},
        {_id: 1, min_role_id: 3, name:'Otsi töötajat',hash:'otsi_tootajat', template:"search_users.tpl"}
    ];
    var secondMenu_min_role_id =  3;
    $scope.showSecondMenu = $rootScope.userData.role_id < secondMenu_min_role_id ? true: false;
    
    $scope.setHash = function(tab) {
      if (tab._id === 0) {
        $location.hash(tab.hash);
      } else {
        $location.hash(tab.hash);
      }
    };
    
    if($location.hash() === 'otsi_projekti' || !$location.hash() ) {
      $location.hash('otsi_projekti');
      //active tab = projects
    } else {
      $location.hash('otsi_tootajat');
      //active tab = employees
    }
    
    $scope.isActiveTab = function(tab) {
      if( $location.hash() == tab.hash ){ return true; }
      else{ return false;}
    };
//console.log($rootScope.userData);
});
App.config(function ($stateProvider, $urlRouterProvider){
  
  $urlRouterProvider.otherwise("/toolaud");
   
  $stateProvider.state({
    name: "dashboard",
    url: "/toolaud",
    templateUrl: "dashboard.tpl",
		controller: "dashboard",
		data: {
      label: 'Töölaud',
    },
		resolve: settings.resolveAll,
		reloadOnSearch: false
  })
  /*
  .state({
    name: "dashboard-my_projects-edit",
    url: "/dashboard/my_projects/projects/edit/:id",
    label: 'Muuda',
    templateUrl: "projects.add.tpl",
    controller: "projects.add",
    resolve: settings.resolveAll,
		reloadOnSearch: false
  })
  .state({
    name: "dashboard-my_projects-edit",
    url: "/dashboard/projects/edit/:id",
    label: 'Muuda',
    templateUrl: "projects.add.tpl",
    controller: "projects.add",
    resolve: settings.resolveAll,
		reloadOnSearch: false
  }) */
  .state({
    name: "dashboard-my_projects",
    url: "/toolaud/minu_projektid",
		label: 'Puudulike andmetega projektid',
    templateUrl: "dashboard.my_projects.tpl",
		controller: "dashboard.my_projects",
		resolve: settings.resolveAll,
		reloadOnSearch: false
  })
  
  .state({
    name: "dashboard-project-create",
    url: "/toolaud/projektid/loo",
    label: 'Loo uus projekt',
    templateUrl: "projects.add.tpl",
    controller: "projects.add",
    resolve: settings.resolveAll,
		reloadOnSearch: false
		
  })
  /*
  .state({
    name: "dashboard-my_projects-settings",
    url: "/dashboard/my_projects/settings",
    templateUrl: "settings.tpl",
    controller: "settings",
    label: "Seaded",
    resolve: settings.resolveAll
  })
  
  .state({
    name: "dashboard-settings-projects-edit",
    url: "/dashboard/settings/projects/edit/:id",
    label: 'Muuda',
    templateUrl: "projects.add.tpl",
    controller: "projects.add",
    resolve: settings.resolveAll,
		reloadOnSearch: false
  })*/;
      
      
});
App.controller("dashboard.my_projects", function($scope, $rootScope, $http, $location, $dummyData, $filter, $converter, $window){
  const PAGE_SIZE = 10; 
  $scope.dummyPlaceholderData = $dummyData.table(PAGE_SIZE, 6, 1, 6);
  $scope.participantDisplayLimit = 4;
  
  $scope.sort = {
    key: 'imported_at',
    direction: 'DESC'
  };
  
  $scope.$watch(function(){
    return $location.search();
  }, function(){
    $scope.fetchProjects($location.search());
  }, true);
  
  $scope.orderList = function(key){
    if($scope.sort.key == key) {
      $scope.sort.direction =  $scope.sort.direction === 'ASC' ? 'DESC': 'ASC';
    } else {
      $scope.sort.key = key;
      $scope.sort.direction = 'DESC';
    }
    $scope.fetchProjects($location.search());
  };
  
  $scope.sortArrowClass = function(key){
      if($scope.sort.key === key) return $scope.sort.direction === 'DESC' ? 'fa fa-sort-down' : 'fa fa-sort-up';
      return 'fa fa-sort';
  };
  
  $scope.fetchProjects = function(opts){
    $scope.loader = true;
    $scope.projects = false;
    var string = "";
    for(var i in opts){
      string += i +'='+ opts[i] + '&';
    }
    string += 'data_complete=false&order_by='+ $scope.sort.key +'&direction='+ $scope.sort.direction +'&account_id='+ $rootScope.userData.id;
    $http.get(settings.prefix + 'projects?pageSize='+ PAGE_SIZE + '&' + string).then(function(response) {
      
      $scope.projects = response.data.list;
      $scope.pagination = response.data.pagination;
      $scope.loader = false;
      if ($scope.projects.length > 0) {
        $scope.areProjects = true;
      } else {
        $scope.areProjects = false;
      }
    }, function (err) {
      $scope.loader = false;
      console.error(err);
    });
    $scope.areProjects = true;
  };
  
  $scope.hide = function (project_id) {
    $http({url: settings.prefix + 'projects/' + project_id + '/hide', method:'put'}).then(function(response){
      $scope.projects = $scope.projects.filter(function(project) {return project.id != project_id });
      if ($scope.projects.length > 0) {
        $scope.areProjects = true;
      } else {
        $scope.areProjects = false;
      }
    }, function (err) {
      console.error(err);
    });
  };
  
  $scope.getWindowWidth = function() {
    var windowWidth = $window.innerWidth - 30;
    var tableWidth = document.querySelector('.table-l-layout').clientWidth;
    console.log(tableWidth);
    if (tableWidth > windowWidth) {
      $scope.tableScroll = true;
    } else {
      $scope.tableScroll = false;
    }
  };
  
  $scope.getWindowWidth();
  
  angular.element($window).on('resize', function(){
    $scope.getWindowWidth();
  });
  
});
App.controller("recent_projects", function($scope, $rootScope, $http, $location, $dummyData, $window){
  const PAGE_SIZE = 10;
  const NOT_SEARCH_KEYS = ['page', 'data_complete', 'order_by', 'direction'];
  $scope.userIsEmployee = $rootScope.userData.role_id > 2 ? true: false;
  $scope.sort = {};
  $scope.dummyPlaceholderData = $dummyData.table(PAGE_SIZE, $scope.userIsEmployee ? 9: 10, 1, 3);
  $scope.participantDisplayLimit = 4;
  
  $scope.$watch(function(){
    return $location.search();
  }, function(){
    if($scope.userIsEmployee){
      if(!$location.search().data_complete){
        var tempSearch = $location.search();
        tempSearch['data_complete'] = true;
        $location.search(tempSearch);
      }
    }
    $scope.fetchProjects($location.search());
  }, true);
  
  $scope.sortAccounts = function(key){
        if($scope.sort.key == key) {
            $scope.sort.direction =  $scope.sort.direction === 'ASC' ? 'DESC': 'ASC';
        } else {
          $scope.sort.key = key;
          $scope.sort.direction = 'DESC';
        }
        var search = $location.search();
        search.order_by = $scope.sort.key;
        search.direction = $scope.sort.direction;
        $location.search(search);
       
    };
  $scope.sortArrowClass = function(key){
      if($scope.sort.key === key) return $scope.sort.direction === 'DESC' ? 'fa fa-sort-down' : 'fa fa-sort-up';
      return 'fa fa-sort';
  };
  
  $scope.resetSearch = function(){
    $location.search({});
  };
  
  const USER_SEARCH_PARAMETERS = ['user_id', 'user_task','user_technology','user_role','min_minutes'];
  $scope.highlightParticipant = function (participant){
    var highlight_class = 'highlight-participant';
    var search_strings = $location.search();
    
    for(var attribute in search_strings){
      if(USER_SEARCH_PARAMETERS.includes(attribute)){
        switch(attribute){
          case 'user_id':
            if(search_strings[attribute] == participant.user.id) return highlight_class;
            break;
          case 'user_task':
            if(participant.tasks.some(function(task){ return task.id == search_strings[attribute]})) return highlight_class;
            break;
          case 'user_technology':
            if(participant.technologies.some(function(technology){ return technology.id == search_strings[attribute]})) return highlight_class;
            break;
          case 'user_role':
            if(participant.roles.some(function(role){ return role.id == search_strings[attribute]})) return highlight_class;
            break;
          case 'min_minutes': 
            if(participant.minutes >= search_strings[attribute]) return highlight_class;
            break;
        }
      }
    }
    
    return '';
  };
  
  $scope.fetchProjects = function (opts){
      
      $scope.projects = false;
      $scope.loader = true;
      var params = [];
      for(var i in opts){
        params.push(i + '=' + opts[i]);
      }
      
      var string = params.join("&");
      $http.get(settings.prefix + 'projects?pageSize=' + PAGE_SIZE + '&' + string).then(function(response) {

      $scope.projects = response.data.list;
      $scope.pagination = response.data.pagination;
      //console.log($scope.projects);
      $scope.isSearchResult = Object.keys($location.search()).filter(function(key){return !NOT_SEARCH_KEYS.includes(key) }).length ? true : false;
      $scope.loader = false;
      if ($scope.projects.length > 0) {
        $scope.areProjects = true;
      } else {
        $scope.areProjects = false;
      }
    }, function(err){
      $scope.loader = false; 
      console.log(err);
    });
    
    $scope.areProjects = true;
  };
  
  $scope.getWindowWidth = function() {
    var windowWidth = $window.innerWidth - 30;
    var tableWidth = document.querySelector('.table-xl-layout').clientWidth;
    if (tableWidth > windowWidth) {
      $scope.tableScroll = true;
    } else {
      $scope.tableScroll = false;
    }
  };
  
  $scope.getWindowWidth();
  
  angular.element($window).on('resize', function(){
    $scope.getWindowWidth();
  });
});
App.controller("search_projects", function($scope, $rootScope, $http, $location, $filter, $timeout, $converter){
  const SEARCH_IGNORE = ['page', 'data_complete'];
  const DEFINITION = {
    'project_names':{type: 'list', src: 'projects?project_names='},
    'project_ids': {type: 'async', src: 'projects?ids='},
    'client_ids': {type: 'async', src: 'clients?ids='},
    'participants': {type: 'async', src: 'users?ids='},
    'system_type': {type: 'class', category: 'system_type'},
    'project_task': {type: 'class', category: 'task'},
    'project_technology': {type: 'class', category: 'technology'},
    'client_field': {type: 'class', category: 'field'},
    'min_capacity': {type: 'minutes' },
    'min_cost': {type: 'value'},
    'start_date': {type: 'value'},
    'finish_date': {type: 'value'},
    'outsourced': {type: 'value'},
    'finished': {type: 'value'},
    'client_sector': {type: 'checkbox'}
  };
  const EXPANDED_SEARCH_KEYS = ['outsourced', 'finished', 'client_ids', 'participants', 'client_sector','client_field'];
  
  $scope.expansion = function() {
    if ($rootScope.showExpansion) {
      $rootScope.showExpansion = false;
    } else {
      $rootScope.showExpansion = true;
    }
  };
  
  $scope.autocomplete = function(input, resource) {
    if(input.length < 3) return [];
    return $http.get(settings.prefix + resource + '/autocomplete?name=' + input)
      .then(function(response) { return response.data }, function(err){ console.log(err) });
  };
    
  $scope.tags = function(input, category){
    var tags = $scope.class[category];
    var output = [];
    if(typeof(input) == 'string'){
      for(var i in tags){
        if(tags[i].value.toLowerCase().includes(input.toLowerCase())) output.push(tags[i]);
      }
    } else if(typeof(input) == 'number'){
      for(var ii in tags){
        if(tags[ii].id == input) output = tags[ii];
      }
    }
    return output;
  };
  
  $scope.transformProjectName = function(input){
    if(input.id) return input.name;
    else return input;
  };
  
  function initialize () {
    /**
     * Populate search form with data from $location.search();
     *
     **/
    console.log()
    $scope.class = $rootScope.classRegistry;
    
    $scope.model = {
      project_names: [],
      client_sector: {},
      finished: "",
      start_date: null,
      finish_date: null
    };
    
    var searchUrl = $location.search();
    
    for(var field in DEFINITION){
      if(DEFINITION[field].type == 'class') $scope.model[field] = [];
      else if(DEFINITION[field].type == 'async' && field != 'project_ids') $scope.model[field] = [];
    }
    
    for(var attr in searchUrl){
   
      if(EXPANDED_SEARCH_KEYS.includes(attr) && !$rootScope.showExpansion){
        $rootScope.showExpansion = true;
      }
      
      if(DEFINITION[attr]){
        switch(DEFINITION[attr].type){
          case 'class': 
            var tag_array =  searchUrl[attr].split(',');
            for(var ci in tag_array){
              $scope.model[attr].push($scope.tags(Number(tag_array[ci]), DEFINITION[attr].category));
            }
            break;
          case 'minutes':
            $scope.model[attr] = $converter.minutesTohours(searchUrl[attr]);
            break;
          case 'value':
            if(attr == 'min_cost')  $scope.model[attr] = parseInt(searchUrl[attr], 10);
            else $scope.model[attr] = searchUrl[attr];
            break;
          case 'checkbox':
            var boxes = searchUrl[attr].split(',');
            for(var box in boxes){
              $scope.model[attr][boxes[box]] = true;
            }
            break;
          case 'list': $scope.model[attr] = searchUrl[attr].split(',');
        }
      }
    }
    
    if(searchUrl['project_ids']) $http.get(settings.prefix + 'projects?ids=' +  searchUrl['project_ids']).then(function(response){ 
      $scope.model.project = response.data[0];
    }).catch(function(err){ console.log(err)});
    
    if(searchUrl['client_ids']) $http.get(settings.prefix + 'clients?ids=' +  searchUrl['client_ids']).then(function(response){ 
      $scope.model.client_ids = response.data;
    }).catch(function(err){ console.log(err)});
    
    if(searchUrl['participants']) $http.get(settings.prefix + 'users?ids=' +  searchUrl['participants']).then(function(response){ 
      $scope.model.participants = response.data;
    }).catch(function(err){ console.log(err)});
    //console.log($scope.model);
    
  }// END OF initialize
  
  var mustBeCSVofIds = ['system_type','project_task','client_field','project_technology','participants','client_ids'];
  var mustBeMinutes = ['min_capacity'];
  var mustBeCSV = ['project_names'];
  
  $scope.search = function(){
    //console.log($scope.model);
    var strings = {};
    
    for(var attr in $scope.model){
      
      if($scope.model[attr]){
        //Must be CSV of Ids
        if(mustBeCSVofIds.includes(attr)){ 
          for(var i in $scope.model[attr]) strings[attr] ? strings[attr] += ','+ $scope.model[attr][i].id : strings[attr] = $scope.model[attr][i].id;
        }
        //Project
        else if(mustBeCSV.includes(attr)){ 
          for(var index in $scope.model[attr]){
            strings[attr] ? strings[attr] += ','+ $scope.model[attr][index] : strings[attr] = $scope.model[attr][index];
          }
        }
        else if(attr === 'project') strings.project_ids = $scope.model[attr].id;
        
        else if (attr === 'finished'){
         if($scope.model[attr] == 'true') strings.finished = 'true';
         else if($scope.model[attr] == 'false') strings.finished = 'false';
        }
        //Client sector
        else if(attr === 'client_sector'){
          if($scope.model[attr].public === true) strings[attr] ? strings[attr] += ',public' : strings[attr] = 'public';
          if($scope.model[attr].private === true) strings[attr] ? strings[attr] += ',private' : strings[attr] = 'private';
          if($scope.model[attr].nonprofit === true) strings[attr] ? strings[attr] += ',nonprofit' : strings[attr] = 'nonprofit';
        }
        //Format dates
        else if(attr.includes('date')) strings[attr] = $filter('date')($scope.model[attr], 'yyyy-MM-dd');
        //Values must be in minutes
        else if(mustBeMinutes.includes(attr)){
          strings[attr] = $converter.hoursTominutes($scope.model[attr]);
          $scope.model[attr] = $converter.minutesTohours(strings[attr]);
        }
        //All the rest
        else strings[attr] = $scope.model[attr];
      }
      
    }
    
    $location.search( strings );
    
  };
  
  $scope.$watch(function(){
    return $location.search();
  }, function(){
    var count = 0;
    for(var i in $location.search()){
      if(!SEARCH_IGNORE.includes(i)) count ++;
    }
    
    if(count === 0){
      if($scope.class) {
        initialize();
      }
    }
    
  }, true);
  
  $scope.placeholder = function(text, arrayLength){
    return arrayLength > 0 ? '' : text;
  };
  
  $scope.resetSearch = function(){
    $location.search({});
    initialize();
  };
  
  initialize();
});
App.controller("search_users", function($scope, $rootScope, $http, $location, $filter, $timeout, $converter){
  var userAppliedSearchParams = false;
  const LISTS = ['user_role', 'user_task', 'user_technology'];
  const CSV_OF_IDS = ['user_role', 'user_task', 'user_technology'];
  const MINUTES = ['min_minutes'];
  const DEFINITION = {
    'user_id': {type: 'async', src: 'users?ids='},
    'user_task': {type: 'class', category: 'task'},
    'user_technology': {type: 'class', category: 'technology'},
    'user_role': {type: 'class', category: 'role'},
    'min_minutes': {type: 'minutes' },
  };
  
  function initialize(){
    /**
     * Populate search form with data from $location.search();
     *
     **/
    $scope.model = {}; 
    for(var item in LISTS){ 
      $scope.model[LISTS[item]] = [];
    }
    
    var searchUrl = $location.search();
    for(var attribute in searchUrl){
      if(DEFINITION[attribute]){
        switch(DEFINITION[attribute].type){
          case 'class': 
            var tag_array = [];
            if(typeof searchUrl[attribute] == 'number') tag_array.push( searchUrl[attribute]);
            else tag_array = searchUrl[attribute].split(',');
              $scope.model[attribute] = tag_array.map(function(item){
               return $scope.tags(Number(item), DEFINITION[attribute].category)[0];
              });

            break;
          case 'minutes':
            $scope.model[attribute] = $converter.minutesTohours(searchUrl[attribute]);
            break;
        }
      }
    }
    if(searchUrl['user_id']) {
      $http.get(settings.prefix + 'users?ids=' +  searchUrl['user_id']).then(function(response){ 
        $scope.model.user = response.data[0];
      }).catch(function(err){ console.log(err)});
    }
  }

  $scope.autocomplete = function(searchText, resource) {
    if(searchText.length < 3) return [];
    return $http.get(settings.prefix + resource + '/autocomplete?name=' + searchText)
      .then(function(response) {
        return response.data;
      },function(error){ console.log(error.data) });
  };
  
  $scope.tags = function(input, category){
    if(typeof(input) == 'string'){
      return $rootScope.classRegistry[category].filter(function(item) {
        return item.value.toLowerCase().includes(input.toLowerCase());
      });
    } else if(typeof(input) == 'number'){
      return $rootScope.classRegistry[category].filter(function(item) {
        return item.id == input;
      });
    }
  };
 
  $scope.search = function(){
    var strings = {};
      for(var parameter in $scope.model){
        if($scope.model[parameter]){
        
          if(CSV_OF_IDS.includes(parameter)){ 
            for(var i in $scope.model[parameter]) strings[parameter] ? strings[parameter] += ','+ $scope.model[parameter][i].id : strings[parameter] = $scope.model[parameter][i].id;
          }
          
          else if(parameter === 'user') {
            strings.user_id = $scope.model[parameter].id;
          }
         
          else if(MINUTES.includes(parameter)){
            strings[parameter] = $converter.hoursTominutes($scope.model[parameter]);
          }
          
          else {
            strings[parameter] = $scope.model[parameter];
          }
        }
      }
    userAppliedSearchParams = true;
    
    $location.search( strings );
    
    $timeout(function(){
      userAppliedSearchParams = false;
    },0);
  };
  
  $scope.$watch(function(){
    return $location.search();
  }, function(){
    if(!userAppliedSearchParams){
      initialize();
    }
  }, true);
  
  $scope.resetSearch = function(){
    $location.search({});
    initialize();
  };
  
  $scope.placeholder = function(text, arrayLength){
    return arrayLength > 0 ? '' : text;
  };
  
  $scope.fullname = function(item){
    return item.first_name + ' ' + item.last_name;
  };
  
  initialize();
});
App.controller("second-menu", function($scope, $rootScope, $location){
    
    $scope.currentLocation = $location.path();
    
    $scope.mainTabs = [
        {_id: 0, min_role_id: 3, name:'Töölaud',  ui_sref:"dashboard", url:"/toolaud"},
        {_id: 1, min_role_id: 2, name:'Minu projektid',  ui_sref:"dashboard-my_projects", url:"/toolaud/minu_projektid"},
    ];
    
    $scope.setActiveMainTab = function (id){
       $scope.activeMainTab = id;
    };
    
    
    $scope.setActiveMainTab($scope.mainTabs[0]._id);
});
App.controller("search_filters", function($scope, $rootScope, $location){
    $scope.model = {filter: $location.search().data_complete || 'all'};
    
    $scope.$watch(function(){
      return $location.search();
    }, function(){
      $scope.model = {filter: $location.search().data_complete || 'all'};
      $scope.setFilter();
    }, true);
    
    $scope.setFilter = function () {
      var searchParams = $location.search();
        switch($scope.model.filter){
          case 'all': 
              if(searchParams.data_complete) {
                searchParams.page = 1;
                delete searchParams.data_complete;
              }
              break;
          case 'true': 
              if(searchParams.data_complete != 'true') searchParams.page = 1;
              searchParams.data_complete = 'true';
              break;
          case 'false':
              if(searchParams.data_complete != 'false') searchParams.page = 1;
              searchParams.data_complete = 'false';
              break;
        }
      $location.search( searchParams );
    };
});
   
App.controller('login', function($state, $user, $scope, $http, $rootScope, $location, $cookies, $crypto, $mdToast) {
	
	var expireDate = new Date();
	expireDate.setDate(expireDate.getDate() + 180);
	var cookieOptions = {
		expires: expireDate
	};
	$scope.model = {};
	$scope.opts = {};
	$rootScope.headerless = $state.current.headerless ? true : false;
	
	if($cookies.get('rememberMe')) {
		$scope.opts.remembered = true;
		$scope.model.email = JSON.parse($cookies.get('email'));
		$scope.model.password = $crypto.decrypt(JSON.parse($cookies.get('password'))); 
	} else {
		$scope.opts.remembered = false;
	}
	
  
  $scope.messages = { errorMessage: false, alertMessage: false };
  
	$scope.login = function() {
		$scope.messages.alertMessage = false;
  	$scope.messages.errorMessage = false;
  	
		$user.login($scope.model.email, $scope.model.password).then(function(response) {
			
			if($scope.opts.remembered == true) {
				$cookies.putObject('rememberMe', true, cookieOptions);
				$cookies.putObject('email', $scope.model.email, cookieOptions);
				$cookies.putObject('password',  $crypto.encrypt($scope.model.password), cookieOptions);
			} else {
				$cookies.remove('rememberMe');
				$cookies.remove('email');
				$cookies.remove('password');
			}
		
			$location.path('/');
		
		}, function (response){
			if(response.data.message) {
				$scope.messages.errorMessage = response.data.message;
			} else { 
				$scope.messages.errorMessage = "Logimine ebaõnnestus";
			}
		});
  };
  
  $scope.switchForm = function(key){
  	$scope.forgotPassword =	(key =='login') ?  false: true;
  	// var errorMsg = document.querySelector('.item-update');
  	// //console.log(errorMsg);
  	// if (errorMsg) {
  	// 	console.log(errorMsg);
			// errorMsg.remove();
  	// }
  	$scope.messages.alertMessage = false;
  	$scope.messages.errorMessage = false;
  }
  $scope.sendRecoveryEmail = function(email){
  	$scope.messages.alertMessage = false;
  	$scope.messages.errorMessage = false;
  	$http({
			method: "POST",
			url: settings.prefix + "recovery",
			data: {'email': email} 
		}).then(function(response) {
			
		$scope.messages.alertMessage = 'Email on saadetud';
		
		}, function (response){
			 $scope.messages.errorMessage = "Emaili saatmine ebaõnnestus";
		});
  	
  }
})
App.config(function ($stateProvider){
   
   $stateProvider
      .state({
         name: "login",
         url: "/login",
         templateUrl: "login.tpl",
			controller: "login",
			headerless: true
      });
});
App.controller("project_client", function($scope, $http, $rootScope, $stateParams, $filter, Upload, $timeout, $converter, $window, $location){
  $scope.redirect_path = $location.path();
  $scope.model = {
    clients: []
  };
  $scope.model.id = $stateParams.id || null;
  $scope.settings_prefix = settings.prefix; //for file download link
  
  $scope.setActiveClient = function(client_id){
    $scope.activeClient_id = client_id;
    $scope.tabScroll();
  };
  
  $scope.bindSelectedItem = function(selectedItem, model){
    if(selectedItem == null) return;
    model.disableNameInput = true;
    model.updateAttributes(selectedItem);
    
    model.attach();
  };
  
  $scope.fetchData = function(){
    $scope.loader = true;
    $http.get(settings.prefix + 'projects/' + $scope.model.id ).then(function(response){
      $scope.loader = false;
      $scope.model = response.data.data;
      $scope.$parent.model = response.data.data;
      
      $scope.tabScroll();
      
      for(var c_index in $scope.model.clients){
        $scope.model.clients[c_index] = new Client($scope.model.clients[c_index], randomId(), true);
      }
      
      if($scope.model.clients.length) $scope.setActiveClient($scope.model.clients[0]._id);
      else {
        var client = $scope.addClient();
        $scope.setActiveClient(client._id);
      }
    }, function(error){
     console.error(error);
    });
  };
  $scope.fetchData();
  
  function Client (data, _id, disableNameInput){
    this.attributes = {};
    this._id = _id;
    this.disableNameInput = disableNameInput || false;
    this.updateAttributes = function(data){
      
      if(!data.files) this.attributes.files ? data.files = this.attributes.files: data.files = [];
      
      for(var i in Object.keys(data)){
        var key = Object.keys(data)[i];
        this.attributes[key] = data[key];
      }
      
      var self = this;
      
      if(data.delegates) {
        //populate delegates
        if( data.delegates.length ){
          this.delegates =  data.delegates.map(function(delegate){
           
            return new Delegate(delegate, randomId(), self.attributes, self._id, true)
          });
         
        } else {
          this.delegates =  [ new Delegate({}, randomId(), this.attributes, this._id, false)];
        }
         
      }
      else {
        //update delegates' client data
        this.delegates.map(function(delegate){ delegate.client = { _id: self._id, attributes: self.attributes}});
      }
     
    };
    this.allCaps = function(key){
      return this.attributes[key].toUpperCase();
    };
    this.updateAttributes(data);
  }
  Client.prototype.attach = function(){
    var _self = this;
    $http({url: settings.prefix + 'projects/' + $scope.model.id + '/clients/' +  _self.attributes.id, method: "PUT"})
        .then(function(response){
          _self.delegates.map(function(delegate){ delegate.disableNameInput = false });
        }, function(error){ 
          console.error(error)
    });
  }
  Client.prototype.detach = function(){
    $scope.tabScroll();
    var _self = this;
    if(!_self.attributes.id) {
      removeClientFromArray();
    }
    else {
      $http({url: settings.prefix + 'projects/' +  $scope.model.id + '/clients/'+ _self.attributes.id, method: 'delete'}).then(function(res){
        removeClientFromArray();
        
      }, function(err){ console.error(err.data)});
    }
    function removeClientFromArray(){
      var index = $scope.model.clients.indexOf(_self);
      $scope.model.clients.splice(index, 1);
      
      if($scope.model.clients.length){
        var selectedTab;
        if($scope.model.clients.length > index)  selectedTab = $scope.model.clients[index]._id;
        else selectedTab = $scope.model.clients[$scope.model.clients.length - 1]._id;
    
        $scope.setActiveClient(selectedTab);
      }
      else $scope.setActiveClient($scope.addClient()._id);
    }
  };
  Client.prototype.save = function (key){
    var _self = this;
    if(!_self.attributes.name) return;
    
    var payload = angular.copy(_self.attributes);
    
    var requestParameters;
    
    if(_self.attributes.hasOwnProperty('id')) requestParameters = {url: settings.prefix + 'clients/' + _self.attributes.id, method: "PUT", data : payload}
    else requestParameters = {url: settings.prefix + 'clients/', method: "POST", data: payload};
    
    $http(requestParameters)
    .then(function(res){
      _self.updateAttributes(res.data.data);
      if(requestParameters.method == 'POST') _self.attach();
      displayCheckDone(key, _self._id);
    }, function(err){ console.error(err.data)});
  };
  Client.prototype.submitFiles = function(files){
    var _self = this;
    for(var i in files){
      
      files[i].upload = Upload.upload({
        url: settings.prefix + 'projects/' + $scope.model.id + '/clients/'+ _self.attributes.id +'/files',
        data: {file: files[i]}
      });
      files[i].upload.then(function (response) {
        _self.attributes.files.push(response.data.data);
      }, function (response) {
        console.error(response.data);
        $scope.messages.errorMessage = response.data.code;    
      });
    }
  };
  Client.prototype.deleteFile = function(file){
    var _self = this;
    $http({url:settings.prefix+ 'projects/' +  $scope.model.id + '/files/' + file.id, method: 'DELETE'}).then(function(response){
        _self.attributes.files.splice(_self.attributes.files.indexOf(file), 1);
    }, function(error){
      console.error(error);
    });
  };
  Client.prototype.downloadFile = function(file){
     window.open(settings.prefix + 'projects/' +  $scope.model.id  + '/files/' + file.id, '_self');
  };
  
  function Delegate (data, _id, client_attributes, clientLocalID, disableNameInput){
    this.attributes = {};
    this._id = _id;
    this.disableNameInput = disableNameInput || false;
    this.client = { _id:  clientLocalID, attributes: client_attributes};
    this.updateAttributes = function(data){
      var splitPhoneNumber = $converter.phoneNumberSplit(data.phone);
      this.phone_number = splitPhoneNumber.number;
      this.phone_prefix = splitPhoneNumber.prefix;
      
      for(var i in Object.keys(data)){
        var key = Object.keys(data)[i];
        this.attributes[key] = data[key];
      }
    };
    this.updateAttributes(data);
  
  }
  Delegate.prototype.attach = function(){
    var _self = this;
    if(_self.client.attributes.id == undefined || _self.attributes.id == undefined) return console.error(_self.client.attributes.id, _self.attributes.id);
    
    $http({url: settings.prefix + 'projects/' + $scope.model.id + '/clients/' +  _self.client.attributes.id + '/delegates/' + _self.attributes.id, method: "PUT"})
        .then(function(response){

        }, function(error){ 
          console.error(error);
    });
  };
  
  Delegate.prototype.detach = function(){
    var _self = this;
    
    if(!_self.attributes.hasOwnProperty('id')) {
      removeFromArray();
    }
    else {
      var url = settings.prefix + 'projects/'+  $scope.model.id + '/clients/'+ _self.client.attributes.id + '/delegates/'+ _self.attributes.id;
      $http({url: url, method: 'delete'}).then(function(res){
        removeFromArray();
      }, function(err){ console.error(err.data)});
    }
    function removeFromArray() {
      for(var i in $scope.model.clients){
        if($scope.model.clients[i]._id == _self.client._id){
          $scope.model.clients[i].delegates = $scope.model.clients[i].delegates.filter(function(delegate){
              return delegate._id != _self._id;
          });
        }
      }
    }
  }; 
  Delegate.prototype.save = function (key){
    
    var _self = this;
    if(!_self.attributes.name) return console.error('Delegate name missing');
    var payload = angular.copy(_self.attributes);
    payload.phone = _self.phone_prefix + _self.phone_number;
    
    var requestParameters;
    if(_self.attributes.hasOwnProperty('id')) requestParameters = {url: settings.prefix + 'delegates/' + _self.attributes.id, method: "PUT", data : payload}
    else requestParameters = {url: settings.prefix + 'delegates/', method: "POST", data: payload};
    
    $http(requestParameters)
    .then(function(res){
      _self.updateAttributes(res.data.data);
      if(requestParameters.method == 'POST') _self.attach();
      displayCheckDone(key, _self._id);
    }, function(err){ console.error(err.data)});
  };
  $scope.addClient = function(){
    
    var client = new Client({delegates: [], files: [], field: []}, randomId(), false);
    $scope.model.clients.push(client);
    //$scope.addDelegate(client);
    return client;
  };
  $scope.addDelegate = function(client){
  
    var delegate = new Delegate({}, randomId(), client.attributes, client._id, false);
    //client.attributes.delegates.push(delegate);
    client.delegates.push(delegate);
    return delegate;
  };
  
  function randomId(){
    //Do differentiate between clients for tabbing
    return Math.random().toString(36).substr(2, 5);
  }
  
  function displayCheckDone(key, localID){
    $scope.checkDataVerification();
    
    if(!$scope.updatedFields) $scope.updatedFields = {};
    
    if(!$scope.updatedFields[key]) $scope.updatedFields[key] = {};
    
   
    if($scope.updatedFields[key][localID]) delete $scope.updatedFields[key][localID];
    
    $timeout(function(){
      if(!$scope.updatedFields[key][localID]) $scope.updatedFields[key][localID] = true;
    },0);
  }
  
  $scope.tabScroll = function(){
    $timeout(function(){
      
      var wrapper = document.querySelector(".tabs-wrapper") || {};
      var children = wrapper.children || [];
      var childrenWidth = 0;
      for(var i = 0; i < children.length; i++) {
        childrenWidth += children[i].clientWidth;
      }
      if (wrapper.clientWidth < childrenWidth) {
        wrapper.classList.add('overflow-x');
      } else {
       if(wrapper.classList) wrapper.classList.remove('overflow-x');
      }
    }, 0);
  };
  
  $scope.updateProjectData =  function (client, data){
    $http({url: settings.prefix + 'projects/'+ $scope.model.id + '/clients/' + client.attributes.id , method:'put', data: data }).then(function(response){
      displayCheckDone('description', client._id);
    }, function(error){
      console.log(error);
    });
  }
  
  angular.element($window).bind('resize', function(){
    $scope.tabScroll();
  });
});
App.controller("project_contractor", function($scope, $http, $rootScope, $stateParams, $filter, Upload, $timeout, $converter){
  /* CONTRACTOR */
  $scope.model.id = $stateParams.id || null;
  $scope.settings_prefix = settings.prefix; //for file download link
  var hoursAndMinutes = ['real_capacity'];
  
  $scope.fetchData = function(){
    $scope.loader = true;
    $http.get(settings.prefix + 'projects/' + $scope.model.id ).then(function(response){
      $scope.checkDataVerification();
      $scope.loader = false;
      $scope.model = response.data.data;
      $scope.$parent.model = response.data.data;
      
      if($scope.model.outsource){
        if($scope.model.outsource['real_capacity'])  $scope.model.outsource['real_capacity'] = $converter.minutesTohours($scope.model.outsource['real_capacity']);
      
        if($scope.model.outsource.start_date === null || $scope.model.outsource.start_date === undefined) {
          $scope.model.outsource.start_date = null;
        }
        if($scope.model.outsource.finish_date === null || $scope.model.outsource.finish_date === undefined) {
          $scope.model.outsource.finish_date = null;
        }
        //name input fields will be disabled, when contractor and/or delegate is already linked
        if($scope.model.contractor.id){
          $scope.disableContractorNameInput = true;
          $scope.disableDelegateNameInput = false;
        } else {
          $scope.disableContractorNameInput = false;
          $scope.disableDelegateNameInput = true;
        }
      
        if($scope.model.contractor.delegates){
          if(typeof($scope.model.contractor.delegates[0]) == 'object'){
            $scope.disableDelegateNameInput = $scope.model.contractor.delegates[0].id ? true : false;
            
            var splitPhoneNumber = $converter.phoneNumberSplit($scope.model.contractor.delegates[0].phone);
            $scope.model.contractor.delegates[0].phone  = splitPhoneNumber.number;
            $scope.model.contractor.delegates[0].phone_prefix =  splitPhoneNumber.prefix;
            
          }
        }
      }
      
    }, function(error){
     console.error(error);
    });
  };
  $scope.fetchData();
  
  $scope.bindDelegate = function(delegate){
    if(!$scope.model.contractor.id) return console.log('Missing contractor');
    if(delegate == null) return;
    
    for(var key in delegate){
      $scope.model.contractor.delegates[0][key] = delegate[key];
    }
    var splitPhoneNumber = $converter.phoneNumberSplit($scope.model.contractor.delegates[0].phone);
    $scope.model.contractor.delegates[0].phone  = splitPhoneNumber.number;
    $scope.model.contractor.delegates[0].phone_prefix =  splitPhoneNumber.prefix;
    
    attachDelegate($scope.model.contractor.delegates[0]);
  };
  
  $scope.saveDelegate = function (key){
    var delegate = $scope.model.contractor.delegates[0];
    
    if(!delegate.name) return console.log('Delegate name missing');
    
    var payload = angular.copy(delegate);
    payload.phone = payload.phone_prefix + payload.phone;
    delete payload.phone_prefix;
    
    var requestParameters;
    if(delegate.hasOwnProperty('id')) requestParameters = {url: settings.prefix + 'delegates/' + delegate.id, method: "PUT", data : payload}
    else requestParameters = {url: settings.prefix + 'delegates/', method: "POST", data: payload};
    
    $http(requestParameters)
    .then(function(response){
      $scope.model.contractor.delegates[0].id = response.data.data.id;
     
      var splitPhoneNumber = $converter.phoneNumberSplit(response.data.data.phone);
      $scope.model.contractor.delegates[0].phone  = splitPhoneNumber.number;
      $scope.model.contractor.delegates[0].phone_prefix =  splitPhoneNumber.prefix;
      
      if(requestParameters.method == 'POST') attachDelegate(delegate);
      displayCheckDone(key);
    }, function(err){ console.error(err.data)});
  };
  
  function attachDelegate(delegate){
    if(!$scope.model.contractor.id) return;
    if(!delegate.id) return;
    
    $http({url: settings.prefix + 'projects/' + $scope.model.id + '/contractors/' +  $scope.model.contractor.id + '/delegates/' + delegate.id, method: "PUT"})
        .then(function(response){
          $scope.disableDelegateNameInput = true;
        }, function(error){ 
          console.error(error);
    });
  }
  
  $scope.detachDelegate = function(delegate){
    if(!$scope.model.contractor.id) return;
    if(!delegate.id) return;
    $http({url: settings.prefix + 'projects/' + $scope.model.id + '/contractors/' +  $scope.model.contractor.id + '/delegates/' + delegate.id, method: "DELETE"})
        .then(function(response){
          $scope.model.contractor.delegates = [];
          $scope.disableDelegateNameInput = false;
        }, function(error){ 
          console.error(error);
    });
  };
  
  $scope.bindContractor = function(contractor){
    
    if(!contractor){
       $scope.model.contractor = {files:[]};
    } else {
      $scope.model.contractor = contractor;
      $scope.model.contractor.files = [];
    }
    
    if($scope.model.contractor.id != undefined) attachContractor(contractor);
    
  };
  
  $scope.saveContractor = function(key){
    var contractor = $scope.model.contractor;
    
    if(!contractor.name) return;

    var requestParameters;
    
    if(contractor.hasOwnProperty('id')) requestParameters = {url: settings.prefix + 'clients/' + contractor.id, method: "PUT", data : contractor}
    else requestParameters = {url: settings.prefix + 'clients/', method: "POST", data: contractor};
    
    $http(requestParameters)
    .then(function(res){
      contractor.id = res.data.data.id;
      if(requestParameters.method == 'POST') attachContractor(contractor);
      displayCheckDone(key);
    }, function(err){ console.error(err.data)});
  };
  
  function attachContractor(contractor) {
    if(!contractor.id) return;
    console.log(contractor);
    $http({url: settings.prefix + 'projects/' + $scope.model.id + '/contractors/' +  contractor.id, method: "PUT"})
      .then(function(res){ 
        $scope.disableContractorNameInput = true;  
        $scope.disableDelegateNameInput = false;
      }, function(err){ console.error(err.data)});
  }
  $scope.detachContractor = function(contractor) {
    if(!contractor.id) return;
    $http({url: settings.prefix + 'projects/' + $scope.model.id + '/contractors/' +  contractor.id, method: "DELETE"})
      .then(function(res){ 
        $scope.model.contractor = {files: []};
        $scope.disableContractorNameInput = false;
        $scope.disableDelegateNameInput = false;
      }, function(err){ console.error(err.data)});
  }
  $scope.saveChange = function(key){
    var data = {};
    data[key] = $scope.model[key];
    
    $http({url: settings.prefix + 'projects/'+ $scope.model.id , method:'put', data: data })
      .then(function (response){
        $scope.model.contractor = {};
        $scope.disableContractorNameInput = false;
        $scope.disableDelegateNameInput = true;
        displayCheckDone(key);
    }, function(err){ console.log(err); $scope.messages.errorMessage = err.data });
  };
  
  $scope.saveOutsource = function(key){
    
    var data = {};
    if(key.includes('date')) data[key] = $filter('date')($scope.model.outsource[key], 'yyyy-MM-dd');
    else if (hoursAndMinutes.includes(key) ) {
      data[key] = $converter.hoursTominutes($scope.model.outsource[key]);
      $scope.model.outsource[key] = $converter.minutesTohours(data[key]);
    }
    else data[key] = $scope.model.outsource[key];
    
    $http({url: settings.prefix + 'projects/'+ $scope.model.id + '/outsource', method:'put', data: data }).then(function (response){
      displayCheckDone(key);
    }, function(err){ console.log(err); $scope.messages.errorMessage = err.data });
  };
  $scope.submitFiles = function(files){
    
    for(var i in files){
      files[i].upload = Upload.upload({
        url: settings.prefix + 'projects/' + $scope.model.id + '/contractors/'+ $scope.model.contractor.id +'/files',
        data: {file: files[i]}
      });
      
      files[i].upload.then(function (response) {
        
        $scope.model.contractor.files.push(response.data.data);
      }, function (response) {
        console.error(response.data);
        $scope.messages.errorMessage = response.data.code;    
      });
    }
  };
  $scope.deleteFile = function(file){
   
    $http({url:settings.prefix+ 'projects/' +  $scope.model.id + '/files/' + file.id, method: 'DELETE'}).then(function(response){
        $scope.model.contractor.files.splice($scope.model.contractor.files.indexOf(file), 1);
    }, function(error){
      console.error(error);
    });
  };
  $scope.downloadFile = function(file){
     window.open(settings.prefix + 'projects/' +  $scope.model.id  + '/files/' + file.id, '_self');
  };
  
  $scope.updateProjectData =  function (client_id, data){

    $http({url: settings.prefix + 'projects/'+ $scope.model.id + '/contractors/' + client_id , method:'put', data: data })
    .then(function(response){
      displayCheckDone('description');
    }, function(error){
      console.log(error);
    });
  };
  
  function displayCheckDone(key){
    $scope.checkDataVerification();
    
    if(!$scope.updatedFields) $scope.updatedFields = {};
    
    if(!$scope.updatedFields[key]) $scope.updatedFields[key] = {};
    
   
    if($scope.updatedFields[key]) delete $scope.updatedFields[key];
    
    $timeout(function(){
      if(!$scope.updatedFields[key]) $scope.updatedFields[key] = true;
    },0);
  }
})
App.controller("project_info", function($scope, $http, $rootScope, $state, $stateParams, $filter, Upload, $timeout, $converter, $location){
  
  initialize();
  var hoursAndMinutes = ['real_capacity', 'planned_capacity'];
  const TYPE_INTEGER = ['cost', 'billing_rate'];
  $scope.redirect_path = $location.path();
  
  function getOrCreateProject(id, data) {
    if(id) return $http.get(settings.prefix + 'projects/' + id );
    else if (data) return $http({url: settings.prefix + 'projects/', method:'post', data: data});
  }
  
  function updateProject (id, data){
    return $http({url: settings.prefix + 'projects/'+ id , method:'put', data: data });
  }
  function initialize (){
    $scope.loader = true;
    var _id = $stateParams.id || undefined;
    if(!isNaN(_id)) {
      getOrCreateProject(_id).then(function (response){
        preCompile(response.data.data);
        if($scope.model.start_date === null || $scope.model.start_date === undefined) {
          $scope.model.start_date = null;
        }
        if($scope.model.finish_date === null || $scope.model.finish_date === undefined) {
          $scope.model.finish_date = null;
        }
      }, function(err){ console.log(err); $scope.messages.errorMessage = err });
    } else {
      preCompile({});
    }
  }

  function preCompile (data){
      $scope.$parent.model = data; //response.data.data;
      
      $scope.model = data;
      if(!data.name) return $scope.loader = false;
     
      $scope.model.procurement_code ? $scope.isProcurement = true : $scope.isProcurement = false;
       
      
      for(var key in $scope.model){
        if(hoursAndMinutes.includes(key)) $scope.model[key] = $converter.minutesTohours($scope.model[key]);
      }
      
      if(!$scope.model.contractor.name) $scope.model.contractor = {name: "", address: ""};
      //console.log($scope.model);
      if(data.outsource) $scope.outsource = data.outsource;
     
      //console.log($scope.model);
      $scope.loader = false;
    }
    
  
 
  $scope.saveChange = function(key){
    //$filter('date')(date, 'yyyy-MM-dd')
    //var parsedUnixTime = new Date('Mon, 25 Dec 1995 13:30:00 GMT').getUnixTime();
    var data = {};
    if (key == 'procurement_code') {
      if ($scope.model[key] === undefined) {
        var cantoast = true;
        var message = "Hanke viitenumber peab koosnema vähemalt kuuest märgist!";
        if(cantoast) {
          cantoast = false;
          $rootScope.toastNotification({ type: "simple", message: message, class: 'toast-error', hideDelay: 6000, position: 'bottom right'});
          $timeout(function() {
            cantoast = true;
          }, 6000);
        }
      }
    }
    $scope.savedFields = {};
    if(key.includes('date')) data[key] = $filter('date')($scope.model[key], 'yyyy-MM-dd');
    else if (key == 'name') {
      if($scope.model[key].length == 0){
        $scope.model[key] =  $scope.model.freckle_name ?  angular.copy($scope.model.freckle_name) : 'Nimetuseta projekt' ;
       
      }
      $scope.$parent.model.name = $scope.model[key];
      data[key] = $scope.model[key];
    }
    else if (hoursAndMinutes.includes(key)) {
      data[key] = $converter.hoursTominutes($scope.model[key]);
      $scope.model[key] = $converter.minutesTohours(data[key]);
      if(key == 'planned_capacity' && $scope.model.billing_rate){
        $scope.model.cost = Math.floor(($scope.model.billing_rate/ 60) * $converter.hoursTominutes($scope.model.planned_capacity));
        data['cost'] = $scope.model.cost;
      }
    }
    else if (key == 'billing_rate') {
      data[key] = $scope.model[key];
      var capacity = false;
      if( $converter.hoursTominutes($scope.model.planned_capacity) ) capacity =  $converter.hoursTominutes($scope.model.planned_capacity);
      else if( $converter.hoursTominutes($scope.model.real_capacity) ) capacity = $converter.hoursTominutes($scope.model.real_capacity);
      if(capacity){
        $scope.model.cost  = Math.floor(($scope.model.billing_rate / 60) * capacity);
        data['cost'] = $scope.model.cost;
      }
      
    }
    else data[key] = $scope.model[key];
    
    for(var dkey in data){
      if(TYPE_INTEGER.includes(dkey)) data[dkey] = parseInt(data[dkey], 10);
    }
    
    if($scope.model.id === undefined && $scope.model.name != undefined){
      //create new project
       getOrCreateProject(null, {name: $scope.model.name}).then(function (response){
        $scope.savedFields = data;
        
        $state.go('.', {id: response.data.data.id}, {reload: false, notify: false});
        preCompile(response.data.data);
      }, function(err){ console.log(err) });
    } else if ($scope.model.id){
      //update existing project
      updateProject($scope.model.id, data).then(function (response){
        $scope.savedFields = data;
        $scope.checkDataVerification();
      }, function(err){ console.log(err); $scope.messages.errorMessage = err.data });
    }else return;
    
  };
  
  $scope.switchProcurement = function (value){
   if(value === false && $scope.model.procurement_code != null){
     console.log('$scope.model.procurement_code:' + $scope.model.procurement_code);
      $scope.model.procurement_code = null;
      $scope.saveChange('procurement_code');
   }
   else $scope.savedFields = {};
  };
  
  $scope.debounceCollection = {};
  
  $scope.debounceBeforeSaving = function(key, debounceTime) {
    
    if($scope.debounceCollection[key]) {
      
      clearTimeout($scope.debounceCollection[key]);
    }
    
    $scope.debounceCollection[key] = setTimeout(function(){
      
      $scope.saveChange(key);
      
    }, debounceTime);
    
  };
  
})
App.controller("project_participant", function($scope, $http, $rootScope, $stateParams, $filter, $location, Upload, $timeout, $converter, $state){
  const PER_PAGE = 5;
  $scope.per_page = PER_PAGE;
  var project_id = $stateParams.id;
  $scope.redirect_path = $location.path();
 
  getData(project_id);
  
  function Participant (data, array) { 
      this.attributes = data;
      this._previousAttributes = angular.copy(data);
      this.array = array;
      this.fullname = this.attributes.user ? this.attributes.user.first_name + ' ' +this.attributes.user.last_name: "";
      this.hours = $converter.minutesTohours(this.attributes.minutes);
  }
  
  $scope.createParticipant = function(){
    var defaultParticipant = {
      minutes: 0,
      tasks: [],
      technologies: [],
      roles: [],
      additional: "",
      start_date: null,
      finish_date: null
    };
    var newParticipant = new Participant(defaultParticipant, $scope.data);
    $scope.data.push(newParticipant);
    renderContent();
    $location.search().page = $scope.pagination.pageCount;
  };
  
  function showSavedObject (id, category){
    //Display check-done to saved result
    $scope.checkDataVerification();
    
    $scope.savedFields = {};
    
    $timeout(function(){$scope.savedFields[id] = true },0);
  }
  
  $scope.$watch(function(){
    return $location.search();
  }, function(){
    if($scope.pagination){
      renderContent();
    }
  }, true);
  function sortByKey(array, key) {
    return array.sort(function(a, b) {
       
          var x =  (typeof(a.attributes.user[key]) == 'string') ? a.attributes.user[key].toLowerCase() : a.attributes.user[key];
          var y = (typeof(b.attributes.user[key]) == 'string') ? b.attributes.user[key].toLowerCase() : b.attributes.user[key];
          return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    
   
    });
  }
  function renderContent (){
    //order list alphabetically
    var list = $scope.data;
    $scope.savedFields = {}; //reset savedFields
    
    $scope.pagination = {
      pageCount: Math.ceil(list.length / PER_PAGE)
    };
    $scope.pagination.page = validActivePage(),
    
    $scope.participants = pageContent(list, new contentLimit(list.length, $scope.pagination.page, PER_PAGE));
   
    function validActivePage(){
      if(!$location.search().page) return 1;
      if($location.search().page){
        if($location.search().page > $scope.pagination.pageCount) return $scope.pagination.pageCount;
        else return $location.search().page;
      }
    }
    function pageContent(list, limit){
      var output = [];
      for(var i = limit.start; i < limit.end; i++){
        output.push(list[i]);
      }
      return output;
    }
    function contentLimit (length, page, per_page) {
      this.start = page * per_page - per_page;
      this.end =  (this.start + per_page > length) ? length : this.start + per_page;
    }
    
  }
  
  function getData(id) {
    $scope.loader = true;
     $http.get(settings.prefix + 'projects/' + id ).then(function(response){
      $scope.model = response.data.data;
      $scope.$parent.model = response.data.data;
      $scope.data = response.data.data.participants;
      for(var p = 0; p < $scope.data.length; p++) $scope.data[p] = new Participant($scope.data[p], $scope.data );
      
      $scope.data = sortByKey($scope.data, 'first_name');;
      renderContent($scope.data);
      $scope.loader = false;
     }, function(error){
       console.log(error);
     });
  };
  
  Participant.prototype.minutes = function(){
        this.attributes.minutes = $converter.hoursTominutes(this.hours);
        this.hours = $converter.minutesTohours(this.attributes.minutes);
        if(this._previousAttributes.minutes != this.attributes.minutes ) this.save();
      }
  Participant.prototype.destroy = function(){
    var _self = this;
    if(!_self.attributes.user) {
      
      _self.array.splice(_self.array.indexOf(_self), 1);
      renderContent();
    }
    //else if(!_self.attributes.user.hasOwnProperty('id')) return _self.array.splice(_self.array.indexOf(_self), 1);
    else $http({url: settings.prefix + 'projects/' + project_id + '/participants/'+ _self.attributes.user.id, method: 'delete'})
            .then(function(res){
              _self.array.splice(_self.array.indexOf(_self), 1)
              renderContent();
            }, function(err){ console.error(err.data)});
  };
  Participant.prototype.save = function (){
    var _self = this;
    if(!_self.attributes.user) return;
    var payload = angular.copy(_self.attributes);
    
    payload.start_date = $filter('date')(payload.start_date, 'yyyy-MM-dd');
    payload.finish_date = $filter('date')(payload.finish_date, 'yyyy-MM-dd');
    
    var requestParameters;
    
    if(_self.attributes.hasOwnProperty('id')) requestParameters = {url: settings.prefix + 'projects/' + project_id + '/participants/' + _self.attributes.user.id, method: "PUT", data : payload}
    else requestParameters = {url: settings.prefix + 'projects/' + project_id + '/participants/', method: "POST", data: payload};
    
    $http(requestParameters)
    .then(function(res){
      _self.attributes = res.data.data;
      _self._previousAttributes = angular.copy(res.data.data);
      showSavedObject(_self.attributes.id);
    }, function(err){ console.error(err.data)});
  };
})
App.controller("project_result", function($scope, $http, $rootScope, $stateParams, $filter, Upload, $timeout, $converter, $location){
  $scope.redirect_path = $location.path();
  var project_id = $stateParams.id;
  
  fetchData(project_id);
  
  function Result (data, array) { 
      this.attributes = data;
      this.array = array;
      this.share = this.attributes.private === true ? false : true; // Kas tahad jagada teistele?
  }
  
  $scope.createResult = function(){
    $scope.results.push(new Result({private: false, categories:[]}, $scope.results));
  };
  
  function showSavedObject (id){
    $scope.checkDataVerification();
    // Display check-done to saved result
    $scope.savedFields = {};
  
    $timeout(function(){
      $scope.savedFields[id] = true;
    },0);
  }
  
  function fetchData(id) {
    $scope.loader = true;
     $http.get(settings.prefix + 'projects/' + id ).then(function(response){
      $scope.model = response.data.data;
      $scope.results = response.data.data.results;
      $scope.$parent.model = response.data.data;
      if(!$scope.results.length) $scope.results.push({private: true, categories:[]});
      for(var i = 0; i < $scope.results.length; i++) $scope.results[i] = new Result($scope.results[i], $scope.results);
      $scope.loader = false;
       
     }, function(error){
       console.log(error);
     });
  }
  
  Result.prototype.destroy = function () {
    var _self = this;
    if(!_self.attributes.hasOwnProperty('id')) return _self.array.splice(_self.array.indexOf(_self), 1);
    else $http({url: settings.prefix + 'projects/' + project_id + '/results/'+ _self.attributes.id, method: 'delete'})
            .then(function(res){ 
              _self.array.splice(_self.array.indexOf(_self), 1);
              
            }, function(err){ console.error(err.data)});
  };
  
  Result.prototype.save = function (){
    var _self = this;
    console.log(this);
    
    this.attributes.private = (this.share === true) ?  false : true;
    
    if(_self.attributes.hasOwnProperty('id')){ 
      $http({url: settings.prefix + 'projects/' + project_id + '/results/' + _self.attributes.id, method: "PUT", data : _self.attributes})
            .then(function(res){ 
              showSavedObject(_self.attributes.id);
            }, function(err){ console.error(err.data)});
    } else { 
      $http({url: settings.prefix + 'projects/' + project_id + '/results/', method: "POST", data: _self.attributes})
            .then(function(res){
              _self.attributes.id = res.data.data.id;
              showSavedObject(_self.attributes.id);
            }, function(err){ console.error(err.data)});
    }
  };
  
})
App.controller("projects.add", function($scope, $http, $rootScope, $stateParams, $filter, Upload, $timeout, $converter, $location, $transitions, $verifyProjectData){
  $scope.EMAIL_PATTERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const DATA_COMPLETE_SUCCESS = 'Projekti andmed kinnitatud!';
  $scope.messages = {};
  $scope.model = {};
  $scope.tabs = [
    {_id: 0, label:'Projekti üldinfo',  state: "projects.general", url: "/projektid/:id/projekti_yldinfo"},
    {_id: 1, label:'Tellija',           state: "projects.client", url: "/projektid/:id/tellija"},
    {_id: 2, label:'Allhanke info',     state: "projects.contractor", url: "/projektid/:id/allhanke_info"},
    {_id: 3, label:'Töötajad',          state: "projects.participation", url: "/projektid/:id/tootajad"},
    {_id: 4, label:'Töötulemid',        state: "projects.results", url: "/projektid/:id/tootulemid"}
  ];
  $scope.isActiveTab = function(tab){
    
    if( $location.path().replace($stateParams.id, ":id") == tab.url ){ return true; }
    else{ return false;}
  };
  
  $http.get(settings.prefix + 'class').then(function(response) {
    $scope.class =  response.data;
  }, function(err){ console.log(err) });
  
  $scope.tags = function(searchText, category){
    var tags = $scope.class[category];
    var output = [];
    for(var i in tags){
      if(tags[i].value.toLowerCase().includes(searchText.toLowerCase())) output.push(tags[i]);
    }
    return output;
  };
  
  $scope.transformTag = function(input){
    if(input.id) return input;
    else return { value: input };
  };
  
  $scope.autocomplete = function(input, resource, existing) {
    // existing will be transformed to an array of ids of existing resources
    var attributes_id = ['clients', 'delegates'];
    if(resource == 'users') {
      if(existing) existing = existing.map(function(participation){  if(participation.attributes.user) return participation.attributes.user.id}); 
    }
    else if (attributes_id.includes(resource)){
       if(existing){
        existing = existing.map(function(obj){  if(obj.attributes.id) return obj.attributes.id});
       }
    }
    return $http.get(settings.prefix + resource + '/autocomplete?name=' + input)
      .then(function(response) { 
        if(existing){
          return response.data.filter(function(record) {
            return !existing.includes(record.id);
          });
        }
        else return response.data;
        
      }, function(err){ console.log(err) });
  };
  
  $scope.showPlaceholder = function(text, arrayLength){
    return arrayLength > 0 ? '' : text;
  };
  $scope.isDataComplete = false;
  $scope.checkDataVerification = function () {
    
      $verifyProjectData.isDataComplete($stateParams.id).then(function (data) {
        $scope.validationMessages = data.messages;
        $scope.isDataComplete = data.required_fields_filled;
        $scope.$apply();
      });
  
  };
  
  $scope.checkDataVerification();
  
  $scope.confirmDataComplete = function(){
    $http({url: settings.prefix + 'projects/'+ $stateParams.id +'/data_complete', method:'put' }).then(function(res){
      $scope.giveToast(DATA_COMPLETE_SUCCESS,'toast-success');
    }, function(err){
      console.log(err.data);
    });
  };
  
  $scope.giveToast = function(message, className){
    $rootScope.toastNotification({ type: "simple", message: message, class: className, hideDelay: 3000, position: 'bottom right'});
  };

}); 
App.config(function ($stateProvider, $urlRouterProvider){
 //$urlRouterProvider.when("/projects/:id", "/projects/:id/general");
 
  
  $stateProvider
  .state({
    abstract: true,
    name: "projects",
    url: "/projektid/:id",
    label: 'Muuda',
    templateUrl: "projects.add.tpl",
    controller: "projects.add",
    resolve: settings.resolveAll,
    reloadOnSearch: false,
    params: {
      id: {
         dynamic: true
      }
    }
  }).state({
    name: "projects.general",
    url: "/projekti_yldinfo",
    data: {
      label: 'Projekti üldinfo',
    },
    templateUrl: "project_info.tpl",
    controller: "project_info"
  }).state({
    name: "projects.client",
    url: "/tellija",
    data: {
      label: 'Tellija',
    },
    templateUrl: "project_client.tpl",
    controller: "project_client"
  }).state({
    name: "projects.contractor",
    url: "/allhanke_info",
    data: {
      label: 'Allhanke info',
    },
    templateUrl: "project_contractor.tpl",
    controller: "project_contractor"
  }).state({
    name: "projects.participation",
    url: "/tootajad",
    data: {
      label: 'Töötajad',
    },
    templateUrl: "project_participant.tpl",
    controller: "project_participant"
  }).state({
    name: "projects.results",
    url: "/tootulemid",
    data: {
      label: 'Töötulemid',
    },
    templateUrl: "project_result.tpl",
    controller: "project_result"
    
  });
  
  $stateProvider
  .state({
    abstract: true,
    name: "projects-view",
    url: "/projektid/:id",
    data: {
      label: 'Vaata abstract',
    },
    params: {
      id: {
         dynamic: true
      }
    },
    controller: "projects.view.abstract",
    templateUrl: "projects.view.abstract.tpl",
    resolve: settings.resolveAll,
    reloadOnSearch: false
    
  }).state({
    name: "projects-view.view",
    url: "/vaata",
    data: {
      label: 'Projekti eelvaade',
    },
    templateUrl: "projects.view.tpl",
    controller: "projects.view", 
    
  });
  
      
});
App.controller('projects.view.abstract', function($scope, $http, $stateParams){
  
});
App.controller('projects.view', function($scope, $rootScope, $http, $stateParams, $mdDialog, $verifyProjectData, $timeout){
  const DATA_COMPLETE_SUCCESS = 'Projekti andmed kinnitatud!';
  $scope.validationMessages = [];
  $scope.changeProject = $rootScope.userData.role_id > 2 ? true : false;
  
  $scope.settings_prefix = settings.prefix; //for file download link
  $scope.getProject = function() {
    $scope.loader = true;
    $http.get(settings.prefix + 'projects/' + $stateParams.id).then(function(response){
      
      $scope.projectData = response.data.data;
      
      //Check whether the project's "data_complete" could be set to true
      $verifyProjectData.isDataComplete(response.data.data.id).then(function (data) {
       
        $timeout(function(){
          console.log(data);
          $scope.validationMessages = data.messages;
          $scope.isDataComplete = data.required_fields_filled;
        }, 0);
        
      });
     
      $scope.loader = false;
      
    }, function(error){
      $scope.loader = false;
      $scope.projectData = false;
    });
  };
  $scope.getProject();
  
  $scope.confirmDataComplete = function(){
    
    $http({url: settings.prefix + 'projects/'+ $stateParams.id +'/data_complete', method:'put' }).then(function(res){
      console.log(res.data);
      $scope.projectData.data_complete = true;
      $scope.giveToast(DATA_COMPLETE_SUCCESS,'toast-success');
    }, function(err){
      console.log(err.data);
    });
  };
  
  $scope.giveToast = function(message, className){
    $rootScope.toastNotification({ type: "simple", message: message, class: className, hideDelay: 3000, position: 'bottom right'});
  };
  
  $scope.isFieldValid = function (object, field) {
    if(!$scope.validationMessages.length) return true;
    
    var messages = $scope.validationMessages;
    var valid = true;
    for(var i = 0; i < messages.length; i++) {
      if(typeof messages[i].field === 'string') continue;
      
      if(messages[i].field[0] !== field) continue;
      
      if(object.id === messages[i].field[1]) {
        valid = false;
        break;
      }
    }
    return valid;
  };
  
});

App.filter('clientSector', function() {
  return function(sector) {
    
    switch (sector) {
      case 'public':
        return "Avalik sektor";
        break;
      case 'private':
        return "Erasektor";
        break;
    }
  }
});
App.controller('projects.view.participant', function($scope, $http, $stateParams, $mdDialog, parentData){
  $scope.participant = JSON.parse(parentData);
  
  
  $scope.hide = function() {
    $mdDialog.hide();
  };
})
App.controller('projects.view.result', function($scope, $http, $stateParams, $mdDialog, parentData){
  $scope.result = JSON.parse(parentData);
  console.log($scope.result);
  
  $scope.hide = function() {
    $mdDialog.hide();
  };
});

App.filter('workResult', function() {
  return function(result) {
    
    switch (result) {
      case true:
        return "Sisemine";
        break;
      case false:
        return "Avalik";
        break;
    }
  }
});
App.controller('recover.controller', function($scope, $rootScope, $http, $location, $timeout) {
  $rootScope.headerless = true;
  const MESSAGE_DURATION = 3000;
  const QUERY_STRINGS = '?email=' + $location.search().email + '&code=' + $location.search().code;
  $scope.currentEmail = $location.search().email;
  $scope.loading;
  $scope.isValidLink;
  $scope.model = {};
  $scope.messages = {};
  $scope.passwordSuccess = false;
  
  $scope.submitPassword = function(new_pw, repeat_new_pw){
    if(new_pw != repeat_new_pw) {
      $scope.messages.errorMessage = 'Salasõnad ei ühti. Palun proovi uuesti.';
      $timeout(function(){
        $scope.messages.errorMessage = false;
      }, MESSAGE_DURATION);
      return;
    }
    
    $scope.loading = true;
    
    $http({url: settings.prefix + 'resetpassword' + QUERY_STRINGS , method: 'PUT', data: { password: new_pw } }).then(function(response) {
      $scope.loading = false;
      $scope.messages.alertMessage = 'Parool edukalt vahetatud!';
      $scope.passwordSuccess = true;
      //$location.path('/login');
    },function (error) {
      $scope.isValidLink = false;
      //$scope.loading = false;
      console.error(error);
    });
  };
  
  var validateLink = function (){
    $scope.loading = true;
    $http.get(settings.prefix + 'resetpassword' + QUERY_STRINGS).then(function(response) {
      $scope.loading = false;
      $scope.isValidLink = response.data.success;
    },function (error) {
      $scope.isValidLink = false;
      $scope.loading = false;
      console.error(error);
    });
  };
  
  validateLink();
  
})
App.config(function ($stateProvider){
   
  $stateProvider.state('resetpassword',{
    url: "/resetpassword?email&code",
    templateUrl: "recover.tpl",
    controller: "recover.controller"
  });
  
});
App.controller("settings", function($scope, $rootScope, $http, $location, $transitions, $timeout){
    $scope.accountRoles = [
      {label: "Admin", _id:1},
      {label : "Projektijuht", _id:2},
      {label : "Töötaja", _id:3},
    ];
    
    $scope.tabs = [
      {_id: 0, label:'Konto seaded',       min_role_id: 3, state: "settings.account", url: "/seaded/konto_seaded" },
      {_id: 1, label:'Kasutajad',          min_role_id: 1, state: "settings.users", url: "/seaded/kasutajad" },
      {_id: 2, label:'Sildid',             min_role_id: 2, state: "settings.tags", url: "/seaded/sildid" },
      {_id: 3, label:'Töötajad',           min_role_id: 2, state: "settings.employees", url: "/seaded/tootajad"},
      {_id: 4, label:'Peidetud projektid', min_role_id: 2, state: "settings.hidden", url: "/seaded/peidetud_projektid"},
      {_id: 5, label:'Süsteem',            min_role_id: 1, state: "settings.system", url: "/seaded/systeem"}
    ];
    
    $scope.isActiveTab = function(tab){
      if( $location.path() == tab.url ){ return true; }
      else{ return false;}
    }

    $scope.roleLabel = function(role_id){
        for(var i = 0; i < $scope.accountRoles.length; i++){
            if($scope.accountRoles[i]._id == role_id) return $scope.accountRoles[i].label;
        }
        return role_id;
    };
    
    
    $scope.giveToast = function(message, className){
        $rootScope.toastNotification({ type: "simple", message: message, class: className, hideDelay: 3000, position: 'bottom right'});
    };
    
});
App.config(function ($stateProvider, $urlRouterProvider){
  
  //$urlRouterProvider.otherwise("/settings/account");
  
  $urlRouterProvider.when("/seaded", "/seaded/konto_seaded");
  $stateProvider.state({
    abstract: true,
    name: "settings",
    url: "/seaded",
    templateUrl: "settings.tpl",
    controller: "settings",
    data: {
      label: 'Seaded',
    },
    resolve: settings.resolveAll,
    reloadOnSearch: false
  })
  .state({
    name: "settings.account",
    url: "/konto_seaded",
    data: {
      label: 'Konto seaded',
    },
    templateUrl: "account_settings.tpl",
    controller: "account_settings"
  })
  .state({
    name: "settings.users",
    url: "/kasutajad",
    data: {
      label: 'Kasutajad',
    },
    templateUrl: "manage_users.tpl",
    controller: "manage_users"
  })
  .state({
    name: "settings.hidden",
    url: "/peidetud_projektid",
    data: {
      label: 'Peidetud projektid',
    },
    templateUrl: "hidden_projects.tpl",
    controller: "hidden_projects"
  })
  .state({
    name: "settings.employees",
    url: "/tootajad",
    data: {
      label: 'Töötajad',
    },
    templateUrl: "manage_employees.tpl",
    controller: "manage_employees"
  })
  .state({
    name: "settings.tags",
    url: "/sildid",
    data: {
      label: 'Sildid',
    },
    templateUrl: "manage_tags.tpl",
    controller: "manage_tags"
  })
  .state({
    name: "settings.system",
    url: "/systeem",
    data: {
      label: 'Süsteem',
    },
    templateUrl: "system_settings.tpl",
    controller: "system_settings"
  });
});
App.controller("account_settings", function($scope, $rootScope, $http, $location, $timeout){
  const SELF_UPDATE_ERROR = 'Nime salvestamisel läks midagi valesti!';
  const SELF_UPDATE_SUCCESS = 'Nimi salvestatud!';
  const PASSWORDS_DO_NOT_MATCH = 'Salasõnad ei ühti, palun proovi uuesti!';
  const PASSWORD_SAVED = 'Salasõna uuendatud';
  
  var fullname = $rootScope.userData.first_name + ' ' + $rootScope.userData.last_name;
  $scope.account = {
    name: fullname.trim()
  };
  
  $scope.checkdone = {
    password: false
  };
  
  $scope.pw_model;
  
  $scope.changeUserData = function(data){
    var first_name, last_name;
    var arr = data.split(" ");
    
    if(arr.length < 2){
      first_name = data;
      last_name = "";
    } else {
      first_name = arr.splice(0, arr.length-1).join(' ');
      last_name = arr.pop();
    }
    
    if(first_name ===  $rootScope.userData.first_name && last_name === $rootScope.userData.last_name) return;
    
    $rootScope.userData.first_name = angular.copy(first_name);
    $rootScope.userData.last_name = angular.copy(last_name);
    $http({url: settings.prefix + 'accounts/'+ $rootScope.userData.id+ '/self',
          method: 'put',
          data: {'first_name': $rootScope.userData.first_name, 'last_name': $rootScope.userData.last_name}
    }).then(function(response){
        $scope.loader = false;
        $scope.giveToast(SELF_UPDATE_SUCCESS,'toast-success');
      }, function(err){
        $scope.loader = false;
        $scope.giveToast(SELF_UPDATE_ERROR,'toast-error');
    });
    
    
  };
 
  $scope.changePassword = function (password){
    if(!password.new_password || !password.confirm_new_password) {
      return;
    }
    $scope.err = false;
   
    if(password.new_password != password.confirm_new_password) {
      $scope.noMatch = true;
      if (password.confirm_new_password.length > 0) {
        return $scope.giveToast(PASSWORDS_DO_NOT_MATCH, 'toast-error');
      }
    }
    $scope.noMatch = false;
   
    $scope.loader = true;
     $http({url: settings.prefix + 'accounts/'+ $rootScope.userData.id+ '/password',
          method: 'put',
          data: {'password': password.new_password} })
     .then(function(response){
          $scope.loader = false;
          $scope.giveToast(PASSWORD_SAVED, 'toast-success');
      }, function(err){
          $scope.err = err;
          $scope.loader = false;
      });
       
   };
});

App.controller("manage_employees", function($scope, $rootScope, $http, $location, $timeout, $window){
  const EMAIL_PATTERN = settings.pattern.email;
  const NAME_PATTERN = settings.pattern.name;
  
  const EMPLOYEE_UPDATED = 'Töötaja andmed uuendatud!';
  const EMPLOYEE_ADDED = 'Uus töötaja lisatud';
  const EMPLOYEE_EMAIL_DUPLICATE = 'Sellise e-mailiga töötaja on juba lisatud!';
  const PLEASE_FILL_ALL_FIELDS = 'Palun täida kõik väljad!';
  const PLEASE_PROVIDE_VALID_FIRST_NAME = 'Palun sisesta sobilik eesnimi!';
  const PLEASE_PROVIDE_VALID_LAST_NAME = 'Palun sisesta sobilik perekonnanimi!';
  const PLEASE_PROVIDE_VALID_EMAIL = "Palun sisesta sobilik e-mail!";
  
  $scope.submitLoader = {};
  $scope.sort = {};
  
  function sortByKey(array, key) {
    return array.sort(function(a, b) {
        var x =  (typeof(a.attributes[key]) == 'string') ? a.attributes[key].toLowerCase() : a.attributes[key];
        var y = (typeof(b.attributes[key]) == 'string') ? b.attributes[key].toLowerCase() : b.attributes[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }
  $scope.sortArrowClass = function(key){
    if($scope.sort.key === key) return $scope.sort.order === 'desc' ? 'fa fa-sort-down' : 'fa fa-sort-up';
    return 'fa fa-sort';
  };
  $scope.sortAccounts = function(key){
    if($scope.sort.key == key) {
      $scope.sort.order =  $scope.sort.order === 'asc' ? 'desc': 'asc';
      return $scope.employees = $scope.employees.reverse(); 
    }
    
    $scope.sort.key = key;
    $scope.sort.order = 'desc';
    $scope.employees = sortByKey($scope.employees, key);
  };
  $scope.createEmployee = function(data){
    if(!data.first_name || !data.last_name || !data.email ) return $scope.giveToast(PLEASE_FILL_ALL_FIELDS, 'toast-error');
    
    if(NAME_PATTERN.test(data.first_name) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_FIRST_NAME, 'toast-error');
    
    if(NAME_PATTERN.test(data.last_name) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_LAST_NAME, 'toast-error');
    
    if(EMAIL_PATTERN.test(data.email) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_EMAIL,'toast-error');
    
    if(checkForExistingEmail(data.email,  $scope.employees)) return $scope.giveToast(EMPLOYEE_EMAIL_DUPLICATE, 'toast-error');
    
    var employee = new Employee(data);
    employee.save();
    $scope.employees.push(employee);
    $scope.newRow = false;
  }
  function Employee (obj) {
    this.attributes = obj;
    this._previousAttributes = angular.copy(obj);
  }
  Employee.prototype.isModified = function(){
    if(JSON.stringify(this.attributes)  != JSON.stringify(this._previousAttributes)) return true;
    else if (!this.hasAttribute('id')) return true;
    else false;
  };
  Employee.prototype.undo = function(){
    this.attributes = angular.copy(this._previousAttributes);
  };
  Employee.prototype.hasAttribute = function (attr) { return  this.attributes[attr] === undefined ? false : true };
  Employee.prototype.save = function (){
    if(NAME_PATTERN.test(this.attributes.first_name) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_FIRST_NAME,'toast-error');
    if(NAME_PATTERN.test(this.attributes.last_name) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_LAST_NAME, 'toast-error');
    if(EMAIL_PATTERN.test(this.attributes.email) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_EMAIL, 'toast-error');
    
    var _self = this;
    $scope.submitLoader[_self.attributes.email] = true;
    
    var requestAttributes;
    
    if(this.hasAttribute('id')) requestAttributes =  {url: settings.prefix + 'users/' + this.attributes.id, method: "PUT", data : this.attributes};
    else requestAttributes =  {url: settings.prefix + 'users', method: "POST", data: this.attributes};
    
    return $http(requestAttributes)
    .then(function(response){
      _self.attributes = response.data.data;
      _self._previousAttributes = angular.copy(response.data.data);
      $scope.submitLoader[_self.attributes.email]  = false;
      
      if(requestAttributes.method === 'POST'){
        $scope.giveToast(EMPLOYEE_ADDED, 'toast-success');
      } else {
        $scope.giveToast(EMPLOYEE_UPDATED, 'toast-success');
      }
    },function(err){
      console.error(err);
      $scope.submitLoader[_self.attributes.email]  = false;
    });
  };
  
  function getEmployees (){
    $scope.loader = true;
    $http({url: settings.prefix + 'users', method: "get"}).then(function(response) {
      $scope.loader = false;
      var data = response.data;
      for(var i = 0; i < data.length; i++) data[i] = new Employee(data[i]);
      
      $scope.employees = data;
    }, function(err) {
        console.error(err);
    });
  }
  getEmployees();
  
  $scope.sortArrowClass = function(key){
    if($scope.sort.key === key) return $scope.sort.order === 'desc' ? 'fa fa-sort-down' : 'fa fa-sort-up';
    return 'fa fa-sort';
  };
  $scope.toggleNewRow = function(){
    $scope.newRow = $scope.newRow ? false : {};
  };
  
  function checkForExistingEmail(email, existing ){
    var duplicate = false;
          
    for(var ii = 0; ii < existing.length; ii++){
      if(existing[ii].attributes.email.toLowerCase() === email.toLowerCase() ) {
        duplicate = true;
        break;
      }
    }
    return duplicate;
  }
  
  $scope.sortIcon = "keyboard_arrow_down";
  $scope.sortLink = function(){
    if ($scope.sortList) {
      $scope.sortList = false;
      $scope.sortIcon = "keyboard_arrow_down";
    } else {
      $scope.sortList = true;
      $scope.sortIcon = "keyboard_arrow_up";
    }
  };
  if ($window.innerWidth > 767) {
    $scope.sortList = true;
  }
  angular.element($window).on('resize', function(){
    if ($window.innerWidth > 767) {
      $scope.sortList = true;
    } else {
      $scope.sortList = false;
      $scope.sortIcon = "keyboard_arrow_down";
    }
  });
});
App.controller("hidden_projects", function($scope, $rootScope, $http, $location, $filter){
    var orderBy = $filter('orderBy');
    $scope.propertyName = 'name';
    $scope.reverse = true;
    $scope.sortArrow = 'fa fa-sort';
    $scope.sortBy = function(propertyName){
        $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
        $scope.hiddenProjects = orderBy($scope.hiddenProjects, $scope.propertyName, $scope.reverse);
        $scope.sortArrow = ($scope.reverse) ? 'fa fa-sort-up' : 'fa fa-sort-down';
    };
 
    $scope.deleteProject = function(item){
        $http({
            url: settings.prefix + 'projects/' + item.id,
            method: "DELETE",
            dataType: "json",
            contentType: "application/json; charset=utf-8"
        }).then(function(response){
            var index = $scope.hiddenProjects.indexOf(item);
            $scope.hiddenProjects.splice(index,1);
        }, function(err) {
            $scope.err = err;
        });
    };
    $scope.toggleProject = function(project) {
        $scope.hiddenProjectLoader = project.id;
        $http({ method: "PUT",
                url: settings.prefix + 'projects/' + project.id + '/show'
        }).then(function(response){
            var index = $scope.hiddenProjects.indexOf(project);
            $scope.hiddenProjects.splice(index,1);
            
            $scope.hiddenProjectLoader = false;
        }, function(response){
            $scope.hiddenProjectLoader = false;
            $scope.messages.errorMessage = response.data.code;
        });    
    };
    $scope.getHiddenprojects = function(){
        $scope.loader = true;
        $http({
            url: settings.prefix + 'projects?visible=false&pageSize=9999',
            method: "GET",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
        }).then(function(response){
            $scope.loader = false;
            $scope.hiddenProjects = response.data.list;
            
        }, function(response){
            $scope.loader = false;
            return
        });
    };
    $scope.getHiddenprojects();
    
});

App.controller("system_settings", function($scope, $http){
  
  const EMAIL_PATTERN = settings.pattern.email;
  
  const PLEASE_PROVIDE_VALID_EMAIL = 'Palun sisesta sobilik e-mail!';
  const PLEASE_FILL_ALL_FIELDS = 'Palun täida kõik väljad!';
  const SYSTEM_SAVED = 'Süsteemi konto on salvestatud!';
  
  var PW_TEXT = 'PLACEHOLDER';
  $scope.model = {
    outlook: {}
  };
  function getOutlookCredentials(){
    $scope.loader = true;
    $http({url: settings.prefix + 'settings/outlook', method: "get"}).then(function(response) {
      $scope.loader = false;
      $scope.model.outlook.username = response.data.username;
      $scope.model.outlook.password = response.data.password == true ? PW_TEXT : undefined;
    }, function(err) {
      console.error(err);
    });
  }
  
  $scope.saveOutlookCredentials = function(){
    var data = {
      username: $scope.model.outlook.username,
      password: $scope.model.outlook.password == PW_TEXT? undefined : $scope.model.outlook.password
    };
    
    if(EMAIL_PATTERN.test(data.username) === false && data.username.length) return $scope.giveToast(PLEASE_PROVIDE_VALID_EMAIL,'toast-error');
    //if(!data.password || !data.username) return $scope.giveToast(PLEASE_FILL_ALL_FIELDS,'toast-error');
    
    $http({url: settings.prefix + 'settings/outlook', method: "PUT", data : data}).then(function(response){
      $scope.giveToast(SYSTEM_SAVED, 'toast-success');
    },
    function(err){
      console.error(err);
    });
    
  };
  getOutlookCredentials();
});
App.controller("manage_tags", function($scope, $rootScope, $http, $location, $mdDialog, $filter, $window, $timeout){
  const PLEASE_ADD_VALUE = 'Palun lisa sildi väärtus!';
  const DUPLICATE_VALUE = 'Selline silt on juba loodud!';
  const TAG_ADDED = 'Uus silt lisatud!';
  const TAG_UPDATED = 'Silt uuendatud!';
  const TAG_REMOVED = "Silt eemaldatud!";
  
  $scope.loader = true;
  $scope.tagMergeBtn = true;
  
  $scope.classNameBook = {
    system_type: 'Süsteemi liigid',
    technology: 'Tehnoloogiad',
    role: 'Töötajate rollid',
    task: 'Teostatud tööd',
    result: 'Töötulemid',
    field: 'Valdkonnad'
  };
  
  $scope.submitLoader = {};
  
  var orderBy = $filter('orderBy');
  $scope.propertyName = 'attributes.count';
  $scope.reverse = true;
  $scope.sortBy = function(propertyName){
      $scope.reverse = (propertyName !== null && $scope.propertyName === propertyName) ? !$scope.reverse : false;
      $scope.propertyName = propertyName;
      $scope.class = orderBy($scope.class, $scope.propertyName, $scope.reverse);
  };
  $scope.sortArrow = function(propertyName){
    if ($scope.propertyName === propertyName) {
      return !$scope.reverse ? 'fa fa-sort-down' : 'fa fa-sort-up';
    } else {
      return 'fa fa-sort';
    }
  };
  
  $scope.class = [];
  $scope.activeTab = function(category){
    $scope.activeCategory = category;
    switch (category) {
      case 'system_type':
        $location.hash("systeemi_liigid");
        break;
      case 'technology':
        $location.hash("tehnoloogiad");
        break;
      case 'role':
        $location.hash("tootajate_rollid");
        break;
      case 'task':
        $location.hash("teostatud_tood");
        break;
      case 'result':
        $location.hash("tootulemid");
        break;
      case 'field':
        $location.hash("valdkonnad");
        break;
    }
    $scope.reverse = false;
    $scope.propertyName = 'attributes.count';
    $scope.sortBy('attributes.count');
  };
  
  $scope.bindTag = function(input){
    //value validation
    
    if(!input.value) return $scope.giveToast(PLEASE_ADD_VALUE, 'toast-error');
    
    if(checkForDuplicate(input, $scope.class)) return $scope.giveToast(DUPLICATE_VALUE, 'toast-error');
    
    var tag = new Tag(input);
    
    $scope.class.push(tag);
    tag.save();
    
    $scope.newTag = false;
  };
  
  function checkForDuplicate(tag, existing) {
    var duplicate = false;
    
    for(var ii = 0; ii < existing.length; ii++){
      console.log(tag);
      console.log(existing[ii]);
      if(existing[ii].attributes.value.toLowerCase() === tag.value.toLowerCase()
        && existing[ii].attributes.group === tag.group) {
        duplicate = true;
        break;
      }
    }
    return duplicate;
  }
  
  function Tag(obj) {
    this.attributes = obj;
    this._previousAttributes = angular.copy(obj);
  }
  
  Tag.prototype.isModified = function(){
    if(JSON.stringify(this.attributes)  != JSON.stringify(this._previousAttributes)) return true;
    else if (!this.hasAttribute('id')) return true;
    else false;
  };
 
  Tag.prototype.hasAttribute = function (attr) { return  this.attributes[attr] === undefined ? false : true };
  
  Tag.prototype.save = function (){
    var _self = this;
    $scope.submitLoader[_self.attributes.value] = true;
    var requestAttributes;
    var payload = angular.copy(this.attributes);
    delete payload.count;
    
    if(this.hasAttribute('id')) requestAttributes =  {url: settings.prefix + 'class/' + this.attributes.id, method: "PUT", data: payload};
    else requestAttributes =  {url: settings.prefix + 'class', method: "POST", data: payload};
    
    return $http(requestAttributes)
    .then(function(response){
    
      for(var i in response.data){
        if(i == 'id') {
           _self.attributes[i] = response.data[i];
        } else if (_self.attributes[i] != undefined) {
          _self.attributes[i] = response.data[i];
        }
      }
      _self._previousAttributes = angular.copy(_self.attributes);
      $scope.submitLoader[_self.attributes.value] = false;
      
      if(requestAttributes.method === 'POST'){
        $scope.giveToast(TAG_ADDED, 'toast-success');
      } else {
        $scope.giveToast(TAG_UPDATED, 'toast-success');
      }
    },function(err){
      console.error(err);
      $scope.submitLoader[_self.attributes.value] = false;
    });
  };
  
  Tag.prototype.remove = function(){
    var _self = this;
    $http({url: settings.prefix + 'class/' + this.attributes.id, method: "DELETE" }).then(function(response){
      $scope.class.splice($scope.class.indexOf(_self), 1);
      $scope.giveToast(TAG_REMOVED, 'toast-success');
    },function(err){
      
      console.error(err);
    });
  };
  
  Tag.prototype.removeFromList = function(){
    $scope.class.splice($scope.class.indexOf(this), 1);
  };
  
  Tag.prototype.undo = function(){
    this.attributes = angular.copy(this._previousAttributes);
  };
  $scope.getData = function(locationHash){
    $scope.loader = true;
    
    $http({url: settings.prefix + 'class', method: "get"})
    .then(function(response) {
      var data = response.data;
      var list = [];
      for(var i = 0; i < Object.keys(data).length; i++){
      for(var x = 0; x <  data[Object.keys(data)[i]].length; x++){
        
        var obj = data[Object.keys(data)[i]][x];
        obj.group = Object.keys(data)[i];
         
        list.push(obj);
      }
      }
      data = angular.copy(list);
      for(var i = 0; i < data.length; i++) {
        data[i] = new Tag(data[i], $scope.class);
      }
      
      $scope.class = data;
      
      $scope.keysArray = Object.keys(response.data);
     
     
      if($scope.keysArray.length){
        
        if(!locationHash){
          $location.hash('systeemi_liigid');
          $scope.activeTab('system_type');
        } else {
          var activeKey = $scope.keysArray.find(function(key){
            return $scope.hashList[key] === locationHash;
          });
          $scope.activeTab(activeKey);
        }
      }
      $scope.tabScroll();
      $scope.loader = false;
    }, function(err) {
      console.error(err);
    });
  };
  $scope.redirect_link = false;
  
  if($location.search().redirect) {
    $scope.redirect_link = $location.search().redirect;
  }
  $scope.getData(angular.copy($location.hash()));
  
  $scope.hashList = {
    'system_type': 'systeemi_liigid',
    'technology': 'tehnoloogiad',
    'role': 'tootajate_rollid',
    'task': 'teostatud_tood',
    'result': 'tootulemid',
    'field': 'valdkonnad'
  };
  
  $scope.toggleNewTag = function(){
    $scope.newTag = $scope.newTag ? false : {};
  };
  
  $scope.tabScroll = function(){
    $timeout(function(){
      
      var wrapper = document.querySelector(".tabs-wrapper") || {};
      var children = wrapper.children || [];
      var childrenWidth = 0;
      for(var i = 0; i < children.length; i++) {
        childrenWidth += children[i].clientWidth;
      }
      if (wrapper.clientWidth < childrenWidth) {
        wrapper.classList.add('overflow-x');
      } else {
       if(wrapper.classList) wrapper.classList.remove('overflow-x');
      }
    }, 500);
  };
  $scope.sortIcon = "keyboard_arrow_down";
  $scope.sortLink = function(){
    if ($scope.sortList) {
      $scope.sortList = false;
      $scope.sortIcon = "keyboard_arrow_down";
    } else {
      $scope.sortList = true;
      $scope.sortIcon = "keyboard_arrow_up";
    }
  };
  if ($window.innerWidth > 767) {
    $scope.sortList = true;
  }
  angular.element($window).on('resize', function(){
    $scope.tabScroll();
    if ($window.innerWidth > 767) {
      $scope.sortList = true;
    } else {
      $scope.sortList = false;
      $scope.sortIcon = "keyboard_arrow_down";
    }
  });
});


App.controller('manage_tags_delete', function($scope, $http, $stateParams, $mdDialog, parentData){
  $scope.item = JSON.parse(parentData);
  
  $scope.cancel = function() {
      $mdDialog.cancel();
  };
  $scope.hide = function(ans) {
    $mdDialog.hide(ans);
  };
})
App.controller('manage_tags_merge', function($scope, $rootScope, $http, $stateParams, $mdDialog, parentData){
  var data = JSON.parse(parentData);
  $scope.showSummary = false;
  $scope.model = {
    subject: data.item
  };
  
  $scope.mergeTags = function () {
    if(!$scope.model.target || !$scope.model.subject) return alert('no target or no subject');
    var data = {
      subject_id: $scope.model.subject.attributes.id,
      target_id: $scope.model.target.attributes.id
    };
    
    $http({url: settings.prefix + 'class/merge', method: "PUT", data: data}).then(function(response){
      $scope.model.target.attributes.count = response.data.targetCount;
      $scope.hide({ 'target': $scope.model.target, 'error': false, 'message': response.data.message || '' });
    }, function(response){
      $scope.hide({ 'error': true, 'message': response.data.message || '' });
    });
  };
  
  $scope.toggleSummary = function () {
    $scope.showSummary = $scope.showSummary === true ? false : true;
  };
  
  $scope.autocomplete = function (searchText, subject) {
    return data.list.filter(function(candidate){
      return candidate.attributes.value.toLowerCase().includes(searchText.toLowerCase())
      && candidate.attributes.group === subject.attributes.group
      && candidate.attributes.id !== subject.attributes.id;
    });
  };
  
  $scope.cancel = function() { 
    $mdDialog.cancel();
  };
  $scope.hide = function(data) { $mdDialog.hide(data) };
})
App.controller('manage_tags_save', function($scope, $rootScope, $http, $stateParams, $mdDialog, parentData){
  $scope.item = JSON.parse(parentData);
  
  $scope.cancel = function() {
    $mdDialog.cancel();
  };
  $scope.hide = function(ans) {
    $mdDialog.hide(ans);
  };
})
App.controller("manage_users", function($scope, $rootScope, $http, $location, $timeout, $window) {
  const EMAIL_PATTERN = settings.pattern.email;

  const USER_UPDATED = 'Kasutaja andmed uuendatud!';
  const USER_ADDED = 'Uus kasutaja lisatud';
  const USER_EMAIL_DUPLICATE = 'Sellise e-mailiga kasutaja on juba lisatud!';
  const PLEASE_FILL_ALL_FIELDS = 'Palun täida kõik väljad!';
  const PLEASE_PROVIDE_VALID_FIRST_NAME = 'Palun sisesta sobilik eesnimi!';
  const PLEASE_PROVIDE_VALID_LAST_NAME = 'Palun sisesta sobilik perekonnanimi!';
  const PLEASE_PROVIDE_VALID_EMAIL = "Palun sisesta sobilik e-mail!";
  const MESSAGE_NOT_SENT_ERROR = "E-maili väljastamine kasutajale ebaõnnestus!";
  const EMAIL_NOT_UNIQUE = "Sellise emailiga kasutaja juba eksisteerib!";
  
  $scope.rePositioning = function() {
    $timeout(function() {
      var openContainer = document.querySelector(".top-select.md-active");
      var windowHeight = $(window).height();
      $timeout(function() {
        var openContainerSize = openContainer.offsetTop + openContainer.offsetHeight;
        if (openContainerSize > windowHeight) {
          openContainer.style.top = openContainer.offsetTop - (openContainerSize - windowHeight) - 20 + "px";
        }
      }, 100);
    }, 100);
  };
  $scope.submitLoader = {};
  $scope.newAccount = false;
  $scope.sort = {};
  $scope.searchText = "";
  $scope.accountStatus = [
    { label: "Aktiveeritud", active: true },
    { label: "Deaktiveeritud", active: false },
  ];

  function sortByKey(array, key) {
    return array.sort(function(a, b) {
      var x = (typeof(a.attributes[key]) == 'string') ? a.attributes[key].toLowerCase() : a.attributes[key];
      var y = (typeof(b.attributes[key]) == 'string') ? b.attributes[key].toLowerCase() : b.attributes[key];
      return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
  }
  $scope.sortArrowClass = function(key) {
    if ($scope.sort.key === key) return $scope.sort.order === 'desc' ? 'fa fa-sort-down' : 'fa fa-sort-up';
    return 'fa fa-sort';
  };
  $scope.sortAccounts = function(key) {
    if ($scope.sort.key == key) {
      $scope.sort.order = $scope.sort.order === 'asc' ? 'desc' : 'asc';
      return $scope.users = $scope.users.reverse();
    }

    $scope.sort.key = key;
    $scope.sort.order = 'desc';
    $scope.users = sortByKey($scope.users, key);
  };
  $scope.getUserRole = function(id) {
    return $scope.accountRoles.filter(function(item) {
      return parseInt(id, 10) === item._id;
    });
  };

  $scope.bindUser = function(input) {
    if (!input.email || input.active === undefined || !input.role_id) return $scope.giveToast(PLEASE_FILL_ALL_FIELDS, 'toast-error');

    if (EMAIL_PATTERN.test(input.email) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_EMAIL, 'toast-error');

    if (checkForExistingEmail(input.email, $scope.users)) return $scope.giveToast(USER_EMAIL_DUPLICATE, 'toast-error');

    var attrs = {
      email: input.email,
      active: input.active,
      role_id: input.role_id
    };

    var user = new Account(attrs, $scope.users);
    user.save();
    user.array.push(user);

    $scope.newAccount = false;

  };

  function checkForExistingEmail(email, existing) {
    var duplicate = false;

    for (var ii = 0; ii < existing.length; ii++) {
      if (existing[ii].attributes.email.toLowerCase() === email.toLowerCase()) {
        duplicate = true;
        break;
      }
    }
    return duplicate;
  }

  $scope.autocomplete = function(searchText, existing) {

    return $http.get(settings.prefix + 'users/autocomplete?email=' + searchText)
      .then(function(response) {
        var output = [];
        if (existing) {
          for (var i = 0; i < response.data.length; i++) {
            var emailExists = checkForExistingEmail(response.data[i].email, existing)
            if (emailExists === false) output.push(response.data[i]);
          }

        }
        else {
          output = response.data;
        }
        return output;
      });
  };

  function Account(obj, array) {
    this.attributes = obj;
    this._previousAttributes = angular.copy(obj);
    this.array = array;
    this._id = randomId();
  }
  Account.prototype.isModified = function() {
    if (JSON.stringify(this.attributes) != JSON.stringify(this._previousAttributes)) return true;
    else if (!this.hasAttribute('id')) return true;
    else false;
  };
  Account.prototype.hasAttribute = function(attr) { return this.attributes[attr] === undefined ? false : true };
  Account.prototype.save = function() {
    if (EMAIL_PATTERN.test(this.attributes.email) === false) return $scope.giveToast(PLEASE_PROVIDE_VALID_EMAIL, 'toast-error');
    if (!this.isUnique('email', $scope.users)) return $scope.giveToast(EMAIL_NOT_UNIQUE, 'toast-error');
    
    var _self = this;
    $scope.submitLoader[_self.attributes.email] = true;
    var requestAttributes;

    if (this.hasAttribute('id')) requestAttributes = { url: settings.prefix + 'accounts/' + this.attributes.id, method: "PUT", data: this.attributes };
    else requestAttributes = { url: settings.prefix + 'accounts', method: "POST", data: this.attributes };

    return $http(requestAttributes)
      .then(function(response) {
        $scope.submitLoader[_self.attributes.email] = false;
        
        if(response.data.errors){
          //EMAIL SERVICE ERROR HANDLING
          if(response.data.account) {
             _self.attributes = response.data.account;
             _self._previousAttributes = angular.copy(response.data.account);
          }
          
          return $scope.giveToast(MESSAGE_NOT_SENT_ERROR, 'toast-warning');
        } else {
          //EMAIL SERVICE WAS NOT USED
          _self.attributes = response.data.account;
          _self._previousAttributes = angular.copy(response.data.account);
  
          if (requestAttributes.method === 'POST') {
            $scope.giveToast(USER_ADDED, 'toast-success');
          }
          else {
            $scope.giveToast(USER_UPDATED, 'toast-success');
          }
        }
      }, function(response) {
        $scope.submitLoader[_self.attributes.email] = false;
        console.error(response.data)
        
      });
  };
  Account.prototype.undo = function() {
    this.attributes = angular.copy(this._previousAttributes);
  };
  
  Account.prototype.isUnique = function (key, list){
    for(var i in list){
      if(this._id != list[i]._id){
        if(list[i].attributes[key].toLowerCase() == this.attributes[key].toLowerCase()) {
          return false;
        }
      }
    }
    return true;
  };
  
  $scope.getData = function() {
    $scope.loader = true;
    $http({ url: settings.prefix + 'accounts', method: "get" })
      .then(function(response) {

        var data = response.data;
        for (var i = 0; i < data.length; i++) data[i] = new Account(data[i], $scope.users);
        $scope.users = data;
        $scope.loader = false;
      }, function(err) {
        console.error(err);
      });
  };

  $scope.getData();

  $scope.toggleNewAccount = function() {
    $scope.newAccount = $scope.newAccount ? false : { active: true };
  };

  $scope.sortIcon = "keyboard_arrow_down";
  $scope.sortLink = function() {
    if ($scope.sortList) {
      $scope.sortList = false;
      $scope.sortIcon = "keyboard_arrow_down";
    }
    else {
      $scope.sortList = true;
      $scope.sortIcon = "keyboard_arrow_up";
    }
  };
  if ($window.innerWidth > 767) {
    $scope.sortList = true;
  }
  angular.element($window).on('resize', function() {
    if ($window.innerWidth > 767) {
      $scope.sortList = true;
    }
    else {
      $scope.sortList = false;
      $scope.sortIcon = "keyboard_arrow_down";
    }
  });
  
  function randomId(){
    //Do differentiate between clients for tabbing
    return Math.random().toString(36).substr(2, 5);
  }

});

